<?php

session_start(["cookie_lifetime" => 86400]);

/**
 * Alias to Folder
 * @var 	array
 */
$Apps = [
	"/^$|^g/"   	=> "home",
	"/^view/"		=> "articles",
	"/^try/"		=> "practices",
	"/^topic/" 		=> "topics",
	"/^search/" 	=> "searches",
	"/^tag/" 		=> "tags",
	"/^write/"		=> "users/writes",
	"/^summary/"	=> "summaries",
	"/^@/"			=> "users",
	"/^dashboard/"	=> "manage",
];

/**
 * Staic files
 */

define("page", "http://localhost/geye/");

define("folder", "/geye/");

define("assets", "assets/");

define("templates", "/templates/");

define("main", "main.php");

define("content", "content.php");

define("header", "header.php");

define("footer", "footer.php");

/**
 *  Set location
 *  Set default timezone
 */
setlocale(LC_TIME, "vc_VN");

date_default_timezone_set("Asia/Ho_Chi_minh");

/**
 * @require difination.
 */

define("DB_HOST_NAME", "localhost");

define("DB_USER_NAME", "root");

define("DB_PASSWORD", "");

define("DB_DATABASE", "geye");

/**
 * { item_description }
 */

define("__TEMPLATE__", assets.templates."design.php");

define("__404__", assets.templates."404.php");

define("__501__", assets.templates."501.php");

define ("__PROJECT__", "Geye");

define("__SIGNIN__", __PROJECT__ . "User");

/**
 *	@required __ROOT__
 * __ROOT__ is the current direction of your project.
 */
define("root", dirname(realpath(__FILE__))."/");

?>