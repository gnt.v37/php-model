<div class="ge-top-48">
	<div class="ge-medium">
		<div>
			<div class="ge-padding">
				<ul>
					<li>
						<div class="ge-row">
							<div class="ge-col l4 m4 s12">
								<ul class="ge-text-color">
									<li class="ge-hoz-8"><h4><a href="<?= page."view?v={$this->Info["article_id"]}" ?>"><?= $this->Info["title"] ?></a></h4></li>
									<li class="ge-top-4">Total time <span ge-time-spent='<?= json_encode([$this->Info["finish_time"], $this->Info["created_at"], $this->Info["limit_time"]]) ?>'><?= $this->Info["limit_time"] ?>
									</span> seconds</li>
									<li class="ge-top-4">Total questions <span ge-total-question="true"></span></li>
								</ul>
							</div>
							<div class="ge-col l4 m4 s12">
								<center class="ge-circle">
									<div class="ge-flex-cc ge-height">
										<h1><div class="ge-score-text">Score</div><div ge-data-score="true"></div></h1>
									</div>
								</center>
							</div>
							<div class="ge-col l4 m4 s12">
								<div class="ge-right-align">
									<ul class="ge-text-color">
										<li><button type="" class="ge-click"><svg width="21" height="21" viewBox="0 0 21 21"><path d="M4 7.331l6.032 6.67.495.547.495-.547 5.973-6.603-.989-.895-5.974 6.603h.99l-6.033-6.67z" fill-rule="evenodd"></path></svg></button></li>
										<li class="ge-top-4"><div>Started at <time datetime="<?= $this->Info["created_at"] ?>"><?= $this->Info["created_at"] ?></time></div></li>
										<li class="ge-top-4"><div>Finished at <time datetime="<?= $this->Info["finish_time"] ?>"><?= $this->Info["finish_time"] ?></time></div></li>
									</ul>
								</div>
									
							</div>
						</div>
					</li>
					<li>
						<div class="ge-top-48">
							<div class="ge-relative"><span class="ge-text-color ge-chart">Chart</span><span class="ge-line"></span></div>
						</div>
					</li>
					<li>
						<div class="ge-chart-space">
							<hidden>
								<input type="hidden" data-total-time="true" value="<?= $this->Info["limit_time"] ?>" name="">
								<input type="hidden" data-time-left="true" value='<?= floor($this->Info["limit_time"] - (strtotime($this->Info["finish_time"]) - strtotime($this->Info["created_at"])))?>' name=""></hidden>
							<div class="ge-row ge-flex-cc">
								<div class="ge-col l6">
									<canvas data-score="true"></canvas>
								</div>
								<div class="ge-col l6">
									<canvas data-time="true"></canvas>
								</div>
								
							</div>

						</div>
					</li>
					<li>
						<div class="ge-chart-space">
							<canvas data-answer="true"></canvas>
						</div>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<hidden><input type="hidden" ge-result="true" value='<?= $this->Result ?>'></hidden>
</div>