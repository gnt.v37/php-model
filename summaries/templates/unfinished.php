<div class="ge-top-32">
	<div>
		<div class="ge-medium-content">
			<div>
				<h1 style="padding-top: 32px;">You haven't finished this practice</h1>
			</div>
			<div class="ge-text-error">Would you like to finish it? <a href="<?= page."try?p={$this->ArticleID}" ?>" class="ge-homepage">Finish this practice now</a></div>
		</div>
		
		<div class="ge-error-bottom">
			<div class="ge-error-space">
				<div>
					<div class="ge-medium-content">
						<div class="ge-error-top-30"><h2>Still feeling lost? You’re not alone.</h2></div>
						<div class="ge-text-error">Enjoy these articles about getting lost, losing things, and finding what you never knew you were looking for.</div>
					</div>
				</div>
			</div>
		</div>
	</div>

</div>