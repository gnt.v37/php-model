<?php

/**
 * @Author: Rot
 * @Date:   2017-10-21 13:00:02
 * @Last Modified by:   Rot
 * @Last Modified time: 2017-12-20 12:53:34
 */

namespace System\Views;

import(geye.views.generic);

class PracticeSummaryView extends GenericView
{
	protected $Models = ["PracticeSummary", "Practice", "Article"];

	protected function Resolver ()
	{
		$this->Result = $this->Deeps->Summary();

		$this->Info = $this->Deeps->Info();

		$this->ArticleID = $this->Deeps->ArticleID();
	}
}

?>