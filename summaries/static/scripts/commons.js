(function () {

let GeResult = document.querySelector("[ge-result]");

if (GeResult)
{
	let Datas = JSON.parse(GeResult.value);

	let Scores = [];

	let Questions = [];

	var Length = Datas.length;

	var Sum = 0;

	let Score;

	for (var i = 0; i < Length; i++)
	{
		Score = Datas[i].Score;

		Sum = Sum + parseFloat(Score);

		Scores.push(Score);

		Questions.push(i + 1);
	}

	Sum = ( Sum / Length ) * 100;

	System.Practice = {Sum: Sum, Questions: Questions, Scores: Scores};
}



})();