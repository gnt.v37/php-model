/*
* @Author: Rot
* @Date:   2017-11-26 10:54:40
* @Last Modified by:   Rot
* @Last Modified time: 2017-11-26 11:01:43
*/
(function () {

class Summary
{
	constructor (Container)
	{
		let Result = System.Practice;

		this.Answer(Result);

		this.Score(Container, Result);

		this.Time();
	}

	Answer (Result)
	{
		let AnswerContent = document.querySelector("[data-answer]").getContext("2d");

		new Chart(AnswerContent, {
			type: "line",
			data: 
			{
				labels: Result.Questions,
				datasets: 
				[
					{
						data: Result.Scores,
						backgroundColor: "rgb(54, 162, 235)",
						borderColor: "rgb(54, 162, 235)",
					},
					
				]
			},
			options: {
				legend: 
				{ 
					position: "bottom", 
					display: false,
				},
				 scales: {
				 	xAxes: 
				 	[
						{
							display: true,
							scaleLabel: 
							{
								display: true,
								labelString: "Answer numbers"
							}
						}
				 	],
					yAxes: 
					[
						{
							display: true,
							scaleLabel: 
							{
								display: true,
								labelString: "Score"
							}
						}
					]
				},
				title: 
				{
					display: true,
					padding: 24,
					text: "The correct answers chart"
				}
			}

		})
	}

	Score (Container, Result)
	{
		let ScoreContext = Container.getContext("2d");

		let GeScore = document.querySelector("[ge-data-score]");

		let Total = document.querySelector("[ge-total-question]");

		GeScore.innerHTML = Result.Sum.toFixed(2);

		Total.innerHTML = Result.Questions.length;

		new Chart(ScoreContext, {
			type: "pie",
			data: {
				labels: ["Correct", "Incorrect"], 
				datasets: 
				[
					{
						label: "The answer time",
						data: [Result.Sum, 100 - Result.Sum],
						backgroundColor: [ "rgb(54, 162, 235)", "rgb(255, 99, 132)" ]
					}
				],
			},

			options: {
				responsive: true,
				legend: 
				{ 
					position: "bottom", 
					labels: { padding: 24 } 
				}
			}
		});
	}

	Time ()
	{
		let TimeContent = document.querySelector("[data-time]").getContext("2d");

		let TimeLeftElement = document.querySelector("[data-time-left]");

		let TotalTimeElement = TimeLeftElement.previousElementSibling;

		let TotalTime = TotalTimeElement.value;

		let TimeLeft = TimeLeftElement.value;

		if (TimeLeft < 0)
		{
			TimeLeft = 0;
		}

		new Chart(TimeContent, {
			type: "doughnut",
			data: {
				labels: ["Time spent", "Time left"], 
				datasets: 
				[
					{
						data: [TotalTime - TimeLeft, TimeLeft],
						backgroundColor: [ "rgb(54, 162, 235)", "rgb(255, 99, 132)" ]
					}
				],
			},

			options: {
				responsive: true,
				legend: 
				{ 
					position: "bottom", 
					labels: { padding: 24 } 
				}
			}
		});
	}
}

System.Summary = Summary;

})();