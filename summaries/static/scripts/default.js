/*
* @Author: Rot
* @Date:   2017-11-09 21:34:20
* @Last Modified by:   Rot
* @Last Modified time: 2017-12-02 15:49:39
*/

(function () {

var IsScore = document.querySelector("[data-score]");

if (IsScore)
{
	new System.Summary(IsScore);
}
else
{
	let GeHome = document.querySelector(".ge-homepage");

	var Matches = location.href.match(/(.*?\/)summary\/(\d+)\/\d+/);

	if (Matches)
	{
		GeHome.setAttribute("href", `${Matches[1]}try?p=${Matches[2]}`);
	}

}
	
})();
