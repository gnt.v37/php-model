<?php
namespace System\Models;

import(geye.models);

class Practice extends Schema 
{
	protected $Nerve = "practices";
}

class Article extends Schema
{
	protected $Nerve = "article_questions";
}

class PracticeSummary extends Procedure implements Nullable, Humanize
{
	protected $Name = "GPracticeSummary";

	public $QuestionIDs;

	public $AnswerIDs;

	public $UserID;

	public $ArticleID;

	public $Times;
	
	public static function Nullable ()
	{
		return ["UserID", "QuestionIDs", "AnswerIDs"];
	}
}


?>