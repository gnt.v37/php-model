<?php

namespace System\Deeps;

define("cookies", "cookies.php");

include_once practices.cookies;

use System\Cookies\PracticeCookie;

class SummaryDeep 
{
	private $Cookie;

	public function __construct ()
	{
		$this->Cookie = new PracticeCookie();
	}

	public function Info ()
	{
		if (isset($_SESSION[__SIGNIN__]))
		{
			return $this->Practice->Chain(["articles" => "article_id"])->Remember([
				"practices.article_id" => $this->SummaryForm->ArticleID, 
				"practices.user_id" => $this->SummaryForm->UserID, 
				"times" =>  $this->SummaryForm->Times,
			], true)->Get(["title", "limit_time", "practice_id", "articles.article_id", "status", "practices.created_at", "finish_time", "score"]);
		}
		else
		{
			$Info = $this->Article->Chain(["articles" => "article_id"])->Remember([
				"articles.article_id" => $this->SummaryForm->ArticleID, 
			], true)->Get(["title", "limit_time", "articles.article_id"]);

			if ($this->Cookie->Get())
			{
				$Cookie = explode(";", $this->Cookie->Get());

				$Info["created_at"] = date("Y-m-d h:i:s", strtotime($Cookie[1]));

				$Info["finish_time"] = date("Y-m-d h:i:s");

				return $Info;
			}
		}
	}

	public function ArticleID ()
	{
		return $this->SummaryForm->ArticleID;
	}

	public function Summary ()
	{
		$this->Cookie->SetArticle(self::ArticleID());

		if ($this->Cookie->IsExist())
		{
			$CookieData = $this->Cookie->Get();

			$GetPractice = explode(";", $CookieData);

			$Created = $GetPractice[1];

			$Convert = $this->Cookie->Convert();

			$Result = $this->PracticeSummary->Call([$Convert[0], $Convert[1], date("Y-m-d h:i:s:a", strtotime($Created)), null, $this->SummaryForm->ArticleID, null]);

			$CurrentDate = date("Y-m-d h:i:s:a");

			$Explode = explode(";", $CookieData);

			if (isset($Result[0]["Result"]) == false && isset($Explode[2]) == false || (isset($Explode[2]) && start_with($Explode[2], "finished") == false))
			{
				$CookieData .= ";finished={$CurrentDate}";

				$this->Cookie->Set($CookieData);
			}
		}
		else
		{
			$Result = $this->PracticeSummary->Call([null, null, null, $this->SummaryForm->UserID, $this->SummaryForm->ArticleID, $this->SummaryForm->Times]);
		}

		if (count($Result) == 1 && $Result[0]["Result"])
		{
			return $Result[0];
		}
		else
		{
			return json_encode($Result);
		}
	}

	
}

?>