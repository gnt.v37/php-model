/*
* @Author: Rot
* @Date:   2017-09-30 17:40:43
* @Last Modified by:   Rot
* @Last Modified time: 2017-10-29 01:46:51
*/

String.prototype.replaceAll = function(searchStr, replaceStr) {
    var str = this;
    
    // escape regexp special characters in search string
    searchStr = searchStr.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
    
    return str.replace(new RegExp(searchStr, 'gi'), replaceStr);
};

function getKeyByValue(object, value) {
  return Object.keys(object).find(key => object[key] === value);
}

var mt_rand = function (Max, Min)
{
	return Math.floor(Math.random() * (Max - Min + 1)) + Min;
};

var randomString = function (Length) 
{
	let StringData = '',
		Data = "0123456789ABCDEFHJKLMNPRTVWXYZabcdefghijklmnopqrstuvwxyz";

	for (var i = 0; i < Length; i++) 
	{
		var Random = mt_rand(0, Data.length - 1);
		StringData += Data[Random];
	}

	return StringData;
};