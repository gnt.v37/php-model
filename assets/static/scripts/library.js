/*
* @Author: Rot
* @Date:   2017-09-30 17:37:04
* @Last Modified by:   Rot
* @Last Modified time: 2017-12-28 12:11:31
*/

(function () {

var System = function () 
{
	this.XMLHttpRequest = null;
};

System.prototype.get = function (File)
{
	return new Promise( (Resolve, Reject) =>
	{
		if (this.XMLHttpRequest == null)
		{
			this.XMLHttpRequest = new XMLHttpRequest();
		}

		var Request = this.XMLHttpRequest;

		Request.open("GET", File);

		Request.onload = function ()
		{
			if (Request.status == 200)
			{
				Resolve(this.response);
			}
			else
			{
				Reject(new Error(this.statusText))
			}
		}

		Request.onerror = function ()
		{
			Reject(new Error("Network Error"));
		}

		Request.send();
	})
};

System.prototype.post = function (File, Data)
{
	return new Promise( (Resolve, Reject) =>
	{
		if (this.XMLHttpRequest == null)
		{
			this.XMLHttpRequest = new XMLHttpRequest();
		}

		var Request = this.XMLHttpRequest;

		Request.open("POST", File);

		Request.onload = function ()
		{
			if (Request.status == 200)
			{
				Resolve(this.response);
			}
			else
			{
				Reject(new Error(this.statusText))
			}
		}

		Request.onerror = function ()
		{
			Reject(new Error("Network Error"));
		}

		Request.send(Data);
	})
};

System.prototype.load = function(Element)
{
	var Images = Element.querySelectorAll("[data-src]");

	var Time = Element.querySelectorAll("time");

	if (Time.length > 0)
	{
		$(Time).timeago();
	}
	
	if (Images.length > 0)
	{
		Images.forEach(function (Image) {

			if (Image.getAttribute("data-src") == "")
			{
				if (Image.getAttribute("data-name") == "")
				{
					$(Image).initial();
				}
				else
				{
					$(Image).initial();
				}
			}
			else
			{
				$(Image).Lazy();
			}

		})
	}
		
};

System.prototype.PromiseWhile = function (Action, Options = {Condition: true})
{
	if (Options.Condition)
	{
		var Async = new Promise((resolve, reject) => {

			Action(resolve, Options);

		}).then(window.System.PromiseWhile.bind(null, Action, Options));
	}
}

System.prototype.Thread = function (Model, Callback, Wait = 1024)
{
	let Worker = setInterval(function () {

		window.System.Process = window.System.Process.then(function () {

			return new Promise(function(Resolve, Reject) {

				window.System.get(`${Model}?time=${new Date().getTime()}`).then(function (Response) {

					if (typeof Callback === "function")
					{
						Response = Response.match(/^{.*?}/gi);

						Callback(Response);
					}

					Resolve();
				});
			});
		})

	}, Wait);
}

System.prototype.Trigger = function(Element, Event, KeyCode = null)
{
	var NewEvent = document.createEvent("HTMLEvents");

	NewEvent.keyCode = KeyCode;

	NewEvent.which = KeyCode;

	NewEvent.trigger = true;

	NewEvent.shiftKey == false;

	NewEvent.initEvent(Event, false, true);

	Element.dispatchEvent(NewEvent); 
};

class Upload 
{
	constructor (File)
	{
		this.File = File;
	}

	get Type ()
	{
		return this.File.type;
	}

	get Size ()
	{
		return this.File.size;
	}

	get Name ()
	{
		return this.File.name;
	}

	Send (File, Data = null)
	{
		if (Data == null)
		{
			Data = new FormData();
		}

		Data.append("File", this.File, this.Name);

		Data.append("upload_file", true);

		return window.System.post(File, Data);
	}
}

class Cookie
{
	constructor (Name = null, Value = null, Expire = null)
	{
		if (Name && Value && Expire)
		{
			this.Write = {Name: Name, Value: Value, Expire: Expire};
		}
			
	}

	set Write (Data)
	{
		var D = new Date();

		D.setTime(D.getTime() + ( Data.Expire * 24 * 60 * 60 * 1000 ));

		document.cookie = `${Data.Name}=${encodeURIComponent(Data.Value)};expires=${D.toGMTString()};path=/`.trim();
	}

	Remember (CookieName, Getted = false)
	{
		var DecodedCookie = decodeURIComponent(document.cookie);

		var Cookies = DecodedCookie.split('; ');

		var Cookie = null;

		var Matches = null;

		for (var i = 0; i < Cookies.length; i++)
		{
			Cookie = Cookies[i];

			Matches = Cookie.match(/(.*?)=(.*?)$/);

			if (Matches)
			{
				if (Matches[1] == CookieName)
				{
					if (Getted)
					{
						return {Name: Matches[1], Value: decodeURIComponent(Matches[2])};
					}
					else
					{
						return true;
					}
				}
			}
		}

		return null;

	}
}

class Share 
{
	constructor ()
	{
	}

	set Twitter (Page = null)
	{
		const BASE_URL = "https://twitter.com/share?url=" + Page;

		window.open(BASE_URL, "_blank", "width=600, height=400, location=0, menubar=0, resizeable=0, scrollbars=0, status=0, titlebar=0, toolbar=0");
	}

	set Facebook (Page = null)
	{
		const BASE_URL = "https://facebook.com/sharer/sharer.php?u=" + Page;

		window.open(BASE_URL, "_blank", "width=600, height=400, location=0, menubar=0, resizeable=0, scrollbars=0, status=0, titlebar=0, toolbar=0");
	}

	set Google (Page = null)
	{
		const BASE_URL = "https://plus.google.com/share?url=" + Page;

		window.open(BASE_URL, "_blank", "width=400, height=600, location=0, menubar=0, resizeable=0, scrollbars=0, status=0, titlebar=0, toolbar=0");

	}
}

class Audio 
{
	constructor ()
	{
		this.Speech = new SpeechSynthesisUtterance();

		this.Voices = window.speechSynthesis.getVoices();

		this.Speech.volume = 1;

		this.Speech.rate = 1;

		this.Speech.pitch = 2;

		this.Speech.lang = "en-US";

		// this.Speech.voice = speechSynthesis.getVoices().filter(function(voice) { 
		// 	return voice.name == 'Microsoft Zira Desktop - English (United States)'; 
		// })[0];

	}

	Speak (Text)
	{
		this.Speech.text = Text;

		this.Speech.voice = speechSynthesis.getVoices()[1];

		window.speechSynthesis.speak(this.Speech);

		console.log(this.Speech);

	}
}

class Convert 
{
	SQLDateTime (YourDate)
	{
		var ISOTime = new Date((new Date(YourDate)).toISOString());

		var FixedTime = new Date(ISOTime.getTime()-(YourDate.getTimezoneOffset() * 60000));

		return FixedTime.toISOString().slice(0, 19).replace('T', ' ');
	}
}

window.System = new System();

window.System.Process = Promise.resolve();

window.System.Audio = Audio;

window.System.Upload = Upload;

window.System.Share = Share;

window.System.Cookie = Cookie;

window.System.Convert = Convert;

})();