/*
* @Author: Rot
* @Date:   2017-10-13 20:35:19
* @Last Modified by:   Rot
* @Last Modified time: 2017-12-29 18:02:14
*/
(function () {

class AutoRun
{
	constructor ()
	{
		var Idata = document.querySelector(`[idata="page"]`);

		const Container = document.querySelector(".ge-notification-container");

		const Detect = document.querySelector(".ge-notification-detect");

		this.Page = Idata.value;

		this.User = Idata.previousElementSibling.value;

		this.UserNotifications(Container, Detect);

		this.UserOptions();

		setTimeout( () => {
			this.Poll(Container, Detect);
		}, 2048)

		this.InvokeEvents();

		this.EmptyAvatar();
		
	}

	EmptyAvatar ()
	{
		var Avatar = document.querySelector(`[ge-avatar="true"]`);

		var UserModel = document.querySelector(`.ge-user-model-width`);

		if (Avatar && Avatar.getAttribute("data-src") == '' && Avatar.getAttribute("src") == null)
		{
			var Name = Avatar.getAttribute("data-name");

			$(Avatar).initial({ fontSize: 48 });

			$(UserModel).children().initial({name: Name, fontSize: 48 });

			$("[ge-user-avatar]").initial({name: Name, fontSize: 48 });

			$(".ge-user-image > img").initial({name: Name, fontSize: 48 });
		}
		else
		{
			var UserImages = document.querySelectorAll("[ge-user-avatar]");

			UserImages.forEach(function (UserImage) {

				if (UserImage.getAttribute("data-src") == '')
				{
					$(UserImage).initial({name: Name, fontSize: 48});
				}

			})
		}

		
	}

	InvokeEvents ()
	{
		var Element;

		document.body.addEventListener("click", function (e) {

			Element = document.querySelector("model.ge-block");

			if (Element)
			{
				var Data = Element.getAttribute("data-model");

				if (e.target.closest(`.${Data}`) == null && e.target.closest("model.ge-block") == null)
				{
					Element.classList.remove("ge-block");
				}
			}

		})
	}

	UserNotifications (Container, Detect)
	{
		var Loaded = false;

		const Genotifications = document.querySelector(".ge-button-user-notifications");

		if (Genotifications)
		{
			const NotificationBox = document.querySelector("[model-user-notifications]");

			const Notification = `${this.Page}g/notification_list`;

			const NotificationCouter = `${this.Page}g/notification_count`;

			const DetectElement = Detect.firstElementChild;

			Genotifications.addEventListener("click",  () => {

				var ClassList = NotificationBox.classList;
				
				if (ClassList.contains("ge-block"))
				{
					ClassList.remove("ge-block");
				}
				else
				{
					ClassList.add("ge-block");

					var NotifiNumber = DetectElement.firstElementChild.innerHTML;

					if (Loaded == false || NotifiNumber > 0)
					{
						Loaded = true;

						this.ResolveModels(Notification, function (Response) {

							console.log("?")

							Container.innerHTML = Response;

							System.load(Container);

							Detect.style.display = "";

							DetectElement.firstElementChild.innerHTML = 0;

						})
					}
				}
			})

			this.ResolveModels(NotificationCouter, function (Response) {

				Response = Response.match(/^{.*?}/);

				let Json = JSON.parse(Response);

				if (Json.Total > 0)
				{
					Detect.style.display = "block";
				}

				DetectElement.firstElementChild.innerHTML = Json.Total;
			})
		}
	}

	UserOptions ()
	{
		const Geoptions = document.querySelector(".ge-button-user-options");

		if (Geoptions)
		{
			const OptionsBox = document.querySelector("[model-user-options]");

			let SignOut = `${this.Page}g/auth/signout`;

			Geoptions.addEventListener("click", function () {

				var ClassList = OptionsBox.classList;

				if (ClassList.contains("ge-block"))
				{
					ClassList.remove("ge-block");
				}
				else
				{
					ClassList.add("ge-block");
				}
			})

			document.querySelector("[ge-signout]").addEventListener("click", () => {

				this.ResolveModels(SignOut, function () {
					window.location.reload();
				})

			})
		}

	}

	Poll (Container, Detect)
	{
		if (this.User)
		{
			const Poll = `${this.Page}g/poll`;

			const Expire = `${this.Page}g/expire?time=`;

			const Schedule = `${this.Page}g/schedule?time=`;

			const Notification = `${this.Page}g/notification_list`;

			new System.Thread(Poll, (Response) => {

				var Json = JSON.parse(Response);

				if (Json)
				{
					if (Json.Scheduled > 0)
					{
						this.ResolveModels(Schedule + new Date().getTime());
					}

					if (Json.Expired > 0)
					{
						this.ResolveModels(Expire + new Date().getTime());
					}

					if (Json.Notifications > 0)
					{
						let DetectData = new FormData();

						let Node = document.createElement("div");

						const DetectElement = Detect.firstElementChild;

						DetectData.set("Detect", 1);

						System.Process = System.Process.then(function () {

							return new Promise(function (Resolve, Reject) {

								return System.post(Notification, DetectData).then(function (Response) {

									Node.innerHTML = Response;

									let Length = Node.children.length - 1;

									for (var i = 0; i < Length; i++)
									{
										Container.insertBefore(Node.children[0], Container.firstElementChild);
									}

									DetectElement.firstElementChild.innerHTML = parseInt(DetectElement.textContent) + Length;

									Detect.style.display = "block";

									Resolve();
								})

							})

						})
					}
				}

					
				
			}, 1520);
		}
	}

	ResolveModels (Model, Callback = null)
	{
		System.Process = System.Process.then(function () {

			return new Promise(function (Resolve, Reject) {

				return System.get(Model).then(function (Response) {

					if (typeof Callback === "function")
					{
						Callback(Response);
					}

					Resolve();
				})

			})

		})
	}
}

		

new AutoRun();

System.load(document);

})();