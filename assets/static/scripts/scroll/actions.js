/*
* @Author: Rot
* @Date:   2017-11-20 09:02:16
* @Last Modified by:   Rot
* @Last Modified time: 2017-11-27 20:18:15
*/
(function () {

let Process = Promise.resolve();

class Scroll 
{
	constructor (Options)
	{
		let Business = new System.ScrollBusiness(Options);

		let WindowHeght = window.innerHeight;
        
        let _this = this;

        window.onresize = function ()
		{
			WindowHeght = window.innerHeight;
		}

        window.onscroll = function ()
        {
            if (Business.Ready && Business.Exist)
            {
                System.Process = System.Process.then(function () {

                    return new Promise(function (Resolve, Reject) {

                        Business.Binding(WindowHeght, _this.Callback).then(function () {

                            Resolve();
                            
                        });

                    })
                    
                })
                
            }
        }
	}

    Done (Callback)
    {
        if (typeof Callback === "function")
        {
            this.Callback = Callback;
        }
    }
}

System.Scroll = Scroll;

})();