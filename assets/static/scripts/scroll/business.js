/*
* @Author: Rot
* @Date:   2017-11-20 09:02:24
* @Last Modified by:   Rot
* @Last Modified time: 2017-11-25 15:18:46
*/
(function () {

class ScrollBusiness
{
    constructor (Options)
    {
        this.Model = new System.ScrollModel(Options.Models);

        this.Data = new FormData();

        this.Node = document.createElement("div");

        this.Offsets = Options.Offsets;

        this.Limits = Options.Limits;

        this.Ready = true;

        this.Exist = true;

        this.Data.set("LimitRecord", this.Limits);
    }

    Binding (WindowHeght, Callback)
    {
        this.Ready = false;

        var ScrollY = window.pageYOffset + WindowHeght;

		var	PageHeight = document.body.clientHeight;

		var	Limit = (PageHeight - WindowHeght);

        if (ScrollY > Limit)
		{
            this.Data.set("OffsetRecord", this.Offsets);

			return this.Model.Binding(this.Data).then ( (Response) => {

                this.Node.innerHTML = Response;

                if (this.Node.getElementsByTagName("*").length < 2)
                {
                    this.Exist = false;
                }

                this.Offsets += this.Limits;

                if (typeof Callback === "function")
                {
                    System.load(this.Node);
                    
                   Callback(this.Node);
                }

                this.Ready = true;

			})
		}
        else
        {
            this.Ready = true;
        }

        return Promise.resolve();
    }

}

System.ScrollBusiness = ScrollBusiness;

})();