/*
* @Author: Rot
* @Date:   2017-11-20 09:02:31
* @Last Modified by:   Rot
* @Last Modified time: 2017-11-24 22:54:08
*/
(function () {

class ScrollModel
{
    constructor (Models)
    {
    	if (typeof Models === "function")
    	{
    		
    		this.Models = Models();
    	}
    	else
    	{
       		this.Models = Models;
    	}
    }

    Binding (Data)
    {
        return System.post(this.Models, Data);
    }
}

System.ScrollModel = ScrollModel;

})();