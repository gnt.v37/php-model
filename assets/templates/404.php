<div class="ge-top-32">
	<div class="ge-medium-content">
		<div class="ge-relative">
			<div class="ge-row ge-flex-sc">
				<div class="ge-col l5">
					<h1 class="ge-error">404</h1>
				</div>
				<div class="ge-col l7">
					<h1 class="ge-error-mesaage">We couldn’t find this page.</h1>
				</div>
			</div>
			<div class="ge-suggest-home">
				<div class="ge-text-error">Maybe it’s out there, somewhere...	</div>	
				<div class="ge-text-error">You can always find insightful articles on our <a href="<?= page ?>" class="ge-homepage">homepage</a>.</div>
			</div>
		</div>
		<div class="ge-error-bottom">
			<div class="ge-error-space">
				<div>
					<div class="ge-medium-content">
						<div class="ge-error-top-30"><h2>Still feeling lost? You’re not alone.</h2></div>
						<div class="ge-text-error">Enjoy these articles about getting lost, losing things, and finding what you never knew you were looking for.</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>