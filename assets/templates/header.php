<div header="default" class=""><ul class="ge-row">
	<li class="ge-col l4 m4 s4"><div class="ge-flex-sc ge-header-height">
		<a href="<?= page ?>" prevent="true" class=""><div class="ge-row">
			<div class="ge-left ge-item-center" style="height: 47px;"><strong style="font-size: 28px;"><span class="ge-color">G</span>E<span class="ge-color">Y</span>E</strong></div>
	 </div></a></div></li>
	 <li class="ge-col l4 m4 s4"><div class="ge-header-height ge-flex-cc">
	 	<form action="/geye/search" method="get" accept-charset="utf-8">
	 		<input type="search" name="q" autocomplete="on" placeholder="Search" class="ge-input ge-font-seui" style="border-bottom: none;"/>
	 	</form>
	 </div></li>
	 <li class="ge-col l4 m4 s4"><div class="ge-flex-ec ge-header-height">
	<?php if (isset($_SESSION[__SIGNIN__])): $User = $_SESSION[__SIGNIN__]?>
		<div class="ge-relative"><div class="ge-user"><ul>
			<li class="ge-inline ge-left-18"><button class="ge-button-user-notifications">
				<svg viewBox="0 0 24 24" width=22 height=22><path style="fill:rgba(0,0,0,.44);" d="M12 22c1.1 0 2-.9 2-2h-4c0 1.1.9 2 2 2zm6-6v-5c0-3.07-1.64-5.64-4.5-6.32V4c0-.83-.67-1.5-1.5-1.5s-1.5.67-1.5 1.5v.68C7.63 5.36 6 7.92 6 11v5l-2 2v1h16v-1l-2-2z"></path></svg>
			</button><div class="ge-notification-detect" style="display: none;"><div><span class="ge-notification-number">0</span></div></div></li>
			<li class="ge-inline ge-left-18">
				<a class="ge-button-user-options"><figure class="ge-hoz-12">
				<?php if ($User["session_image_local"]): ?>
				<div><img ge-avatar="true" data-name="" data-src="<?= page.users.images."40/".$User["session_image_local"] ?>" alt=""></div>
				<?php else: ?>
					<div><img ge-avatar="true" data-name="<?= $User["session_firstname"] || $User["session_lastname"] ? "{$User["session_firstname"]} {$User["session_lastname"]}" : $User["session_email"] ?>" data-src="<?= $User["session_image_link"] ?>" alt=""></div>
				<?php endif ?>
			</figure></a></li>
		</ul></div>
			
		<model model-user-notifications="true" data-model="ge-button-user-notifications">
			<div class="ge-width"><div>
				<model-header><div class="ge-border-bottom">
					<h5>Notification</h5>
			</div></model-header>
			<model-content><div class="">
				<ul class="ge-notification-container">
					<li><div class="ge-hoz-16">No notifications yet.</div></li>
				</ul>
			</div></model-content>
		</div></div>
		</model>

		<model model-user-options="true" data-model="ge-button-user-options">
			<div class="ge-width"><div><model-header><div><ge-user><div class="ge-inline">
				<a href="<?= page."@{$User["userid"]}"; ?>" prevent="true">
					<div class="ge-inline ge-relative">
						<?php if ($User["session_image_local"]): ?>
							<div class="ge-user-model-width"><img data-src="<?= page.users.images."40/".$User["session_image_local"] ?>" alt=""></div>
						<?php else: ?>
							<div class="ge-user-model-width"><img data-name="<?= $User["session_firstname"] || $User["session_lastname"] ? "{$User["session_firstname"]} {$User["session_lastname"]}" : $User["session_email"] ?>" data-src="<?= $User["session_image_link"] ?>" alt=""></div>
						<?php endif ?>
						<div class="ge-avatar-radius">
							<svg ge-image-border="true" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 70 70">
								<path d="M5.53538374,19.9430227 C11.180401,8.78497536 22.6271155,1.6 35.3571429,1.6 C48.0871702,1.6 59.5338847,8.78497536 65.178902,19.9430227 L66.2496695,19.401306 C60.4023065,7.84329843 48.5440457,0.4 35.3571429,0.4 C22.17024,0.4 10.3119792,7.84329843 4.46461626,19.401306 L5.53538374,19.9430227 Z"></path>
								<path d="M65.178902,49.9077131 C59.5338847,61.0657604 48.0871702,68.2507358 35.3571429,68.2507358 C22.6271155,68.2507358 11.180401,61.0657604 5.53538374,49.9077131 L4.46461626,50.4494298 C10.3119792,62.0074373 22.17024,69.4507358 35.3571429,69.4507358 C48.5440457,69.4507358 60.4023065,62.0074373 66.2496695,50.4494298 L65.178902,49.9077131 Z"></path>
							</svg>
						</div>
					</div>
				</a></div>
			<div class="ge-inline ge-left-12">
				<div><a href="<?= page."@{$User["userid"]}"; ?>" prevent="true" class="ge-name">
				<div class="ge-username"><?= "{$User["session_lastname"]} {$User["session_firstname"]}"; ?></div>
				<div class="ge-email"><?= $User["session_email"]; ?></div></a></div>
			</div></ge-user></div></model-header>
			<model-content>
				<?php if ($User["session_role"] == "A"): ?>
				<div><ul class="ge-user-options">
					<li class="ge-user-option">
						<a href="<?= page ?>dashboard"><div class="ge-option-padding ge-flex-sc"><div class="ge-icon-space"><div class="ge-user-option-icon">
							<svg viewBox="0 0 95.103 95.103" style="pointer-events: none; display: block; width: 100%; height: 100%;" xml:space="preserve">
								<path d="M47.561,0C25.928,0,8.39,6.393,8.39,14.283v11.72c0,7.891,17.538,14.282,39.171,14.282 c21.632,0,39.17-6.392,39.17-14.282v-11.72C86.731,6.393,69.193,0,47.561,0z"/><path d="M47.561,47.115c-20.654,0-37.682-5.832-39.171-13.227c-0.071,0.353,0,19.355,0,19.355 c0,7.892,17.538,14.283,39.171,14.283c21.632,0,39.17-6.393,39.17-14.283c0,0,0.044-19.003-0.026-19.355 C85.214,41.284,68.214,47.115,47.561,47.115z"/><path d="M86.694,61.464c-1.488,7.391-18.479,13.226-39.133,13.226S9.875,68.854,8.386,61.464L8.39,80.82 c0,7.891,17.538,14.282,39.171,14.282c21.632,0,39.17-6.393,39.17-14.282L86.694,61.464z"/>
							</svg></div>
							</div><div>Admin Dashboard</div></div></a>
					</li>
					
				</ul></div>
				<?php endif ?>
				<div><ul class="ge-user-options">
					<li class="ge-user-option">
						<a href="<?= page ?>write"><div class="ge-option-padding ge-flex-sc"><div class="ge-icon-space"><div class="ge-user-option-icon"><svg viewBox="0 0 24 24" focusable="false" style="pointer-events: none; display: block; width: 100%; height: 100%;">
						<g><path d="M9 16h6v-6h4l-7-7-7 7h4zm-4 2h14v2H5z"></path></g></svg></div></div><div>New article</div></div></a>
					</li>
					<li class="ge-user-option">
						<a href="<?= page."@".$User["userid"] ?>/articles/drafts"><div class="ge-option-padding ge-flex-sc"><div class="ge-icon-space"><div class="ge-user-option-icon">
							<svg viewBox="0 0 24 24" focusable="false" style="pointer-events: none; display: block; width: 100%; height: 100%;"><g>
								<path d="M4 8h4V4H4v4zm6 12h4v-4h-4v4zm-6 0h4v-4H4v4zm0-6h4v-4H4v4zm6 0h4v-4h-4v4zm6-10v4h4V4h-4zm-6 4h4V4h-4v4zm6 6h4v-4h-4v4zm0 6h4v-4h-4v4z"></path>
      						</g></svg>
						</div></div><div>Articles</div></div></a>
					</li>
				</ul></div><div>
				<ul class="ge-user-options">
					<li class="ge-user-option">
						<a href="<?= page."@".$User["userid"] ?>/practices/incomplete"><div class="ge-option-padding ge-flex-sc"><div class="ge-icon-space"><div class="ge-user-option-icon"><svg viewBox="0 0 423.576 423.576" xml:space="preserve"><path d="M300,211.787c0,44.15-32.604,80.828-75,87.225v48.924c69.25-6.668,123.576-65.18,123.576-136.147S294.249,82.308,225,75.641v48.924C267.396,130.959,300,167.636,300,211.787z"/><path d="M75,211.787c0,70.969,54.327,129.479,123.576,136.146V299.01c-42.395-6.396-75-43.072-75-87.224c0-44.151,32.605-80.828,75-87.223V75.638C129.327,82.308,75,140.818,75,211.787z"/><path d="M273.576,211.787c0-29.535-20.835-54.283-48.576-60.353v120.705C252.741,266.07,273.576,241.322,273.576,211.787z"/><path d="M150,211.787c0,29.535,20.835,54.283,48.576,60.352V151.435C170.835,157.504,150,182.252,150,211.787z"/><path d="M48.576,211.787c0-85.547,66.16-155.926,150-162.675V0.424C87.928,7.268,0,99.445,0,211.787S87.928,416.306,198.576,423.15v-48.688C114.736,367.713,48.576,297.334,48.576,211.787z"/><path d="M423.576,211.787C423.576,99.445,335.648,7.268,225,0.424v48.688c83.84,6.749,150,77.129,150,162.676s-66.16,155.927-150,162.676v48.688C335.648,416.306,423.576,324.129,423.576,211.787z"/></svg></div></div><div>Practices</div></div></a>
						</li>
				</ul></div><div>
				<ul class="ge-user-options">
					<li class="ge-user-option">
						<a><div class="ge-option-padding ge-flex-sc"><div class="ge-icon-space"><div class="ge-user-option-icon">
							<svg viewBox="0 0 24 24" focusable="false" style="pointer-events: none; display: block; width: 100%; height: 100%;"><g><path d="M19.43 12.98c.04-.32.07-.64.07-.98s-.03-.66-.07-.98l2.1-1.65c.2-.15.25-.42.13-.64l-2-3.46c-.12-.22-.4-.3-.6-.22l-2.5 1c-.52-.4-1.08-.73-1.7-.98l-.37-2.65c-.06-.24-.27-.42-.5-.42h-4c-.27 0-.48.18-.5.42l-.4 2.65c-.6.25-1.17.6-1.7.98l-2.48-1c-.23-.1-.5 0-.6.22l-2 3.46c-.14.22-.08.5.1.64l2.12 1.65c-.04.32-.07.65-.07.98s.02.66.06.98l-2.1 1.65c-.2.15-.25.42-.13.64l2 3.46c.12.22.4.3.6.22l2.5-1c.52.4 1.08.73 1.7.98l.37 2.65c.04.24.25.42.5.42h4c.25 0 .46-.18.5-.42l.37-2.65c.6-.25 1.17-.6 1.7-.98l2.48 1c.23.1.5 0 .6-.22l2-3.46c.13-.22.08-.5-.1-.64l-2.12-1.65zM12 15.5c-1.93 0-3.5-1.57-3.5-3.5s1.57-3.5 3.5-3.5 3.5 1.57 3.5 3.5-1.57 3.5-3.5 3.5z"></path> </g></svg>
							</div></div><div>Settings</div></div></a>
					</li>
					<li class="ge-user-option">
						<a><div class="ge-option-padding ge-flex-sc"><div class="ge-icon-space"><div class="ge-user-option-icon">
							<svg viewBox="0 0 24 24" focusable="false" style="pointer-events: none; display: block; width: 100%; height: 100%;"><g><path d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm1 17h-2v-2h2v2zm2.07-7.75l-.9.92C13.45 12.9 13 13.5 13 15h-2v-.5c0-1.1.45-2.1 1.17-2.83l1.24-1.26c.37-.36.59-.86.59-1.41 0-1.1-.9-2-2-2s-2 .9-2 2H8c0-2.21 1.79-4 4-4s4 1.79 4 4c0 .88-.36 1.68-.93 2.25z"></path>
							</g></svg>
						</div></div><div>Help</div></div></a>
					</li>
					<li class="ge-user-option">
						<a ge-signout="true"><div class="ge-option-padding ge-flex-sc"><div class="ge-icon-space"><div class="ge-user-option-icon">
						<svg viewBox="0 0 24 24" focusable="false" style="pointer-events: none; display: block; width: 100%; height: 100%;"><g><path d="M10.09 15.59L11.5 17l5-5-5-5-1.41 1.41L12.67 11H3v2h9.67l-2.58 2.59zM19 3H5c-1.11 0-2 .9-2 2v4h2V5h14v14H5v-4H3v4c0 1.1.89 2 2 2h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2z" class="style-scope yt-icon"></path></g></svg>
						</div></div><div>Sign out</div></div></a>
					</li>
				</ul></div><div>
				<ul><li class="ge-user-option">
					<a><div class="ge-option-padding ge-flex-sc">
						<span>Language: </span><span class="ge-left-8">English</span></div></a>
					</li>
					<li class="ge-user-option">
						<a><div class="ge-option-padding ge-flex-sc">
						<span>Content location: </span><span class="ge-left-8">Vietnam</span></div></a>
					</li>
				</ul>
			</div></model-content><model-footer></model-footer>
			</div></div>
		</model></div>
	<?php else: ?>
		<div><a class="ge-color" href="<?= page ?>g/auth/signin" data-href="signin">Sign in / Sign up</a></div>
	<?php endif ?>
	</div></li>
    </ul>
</div>