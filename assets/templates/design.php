<!doctype html>
<html>
<head>
<meta charset='UTF-8' name='viewport' content='width=device-width, initial-scale=1'>
<title><?php empty($this->Title) ? print("Geye") : print($this->Title); ?></title>
<link rel="icon" type="image/ico" href="<?= page.assets.images ?>favicon.ico">
<link rel="stylesheet" type="text/css" href="<?= page.assets.styles ?>library.css">
<?php 

$Article = false;

switch ($App) 
{
	case "home/":
	case "topics/":
		$Article = true;
		break;
	default:
		# code...
		break;
}

?>

<?php if ($Article): ?>
	<link rel="stylesheet" type="text/css" href="<?= page.assets.styles ?>article.css">
<?php endif ?>
<style type="text/css" media="screen">
a,button{cursor:pointer;color:inherit;font:inherit;}button,ul{padding:0}button,input,select,textarea{outline:0;border:none;background-color:inherit}figure,p,ul{margin:0}ul{list-style-type:none}a{text-decoration:none}figure,ged{position:relative}input,select,textarea{font:inherit}textarea{resize:none}strong{font-weight:600}h1,h2,h3,h4,h5,h6{font-family:"Segoe UI",Arial,sans-serif;font-weight:600;margin:0}h1{font-size:36px}h2{font-size:30px}h3{font-size:24px}h4{font-size:20px}h5{font-size:16px}h6{font-size:14.5px}img{margin-bottom:-5px}main-content, ge-user, gef, trash-content, model-header, ged, model-content, fixed-header, absolute-header, surface, ge-answer, ge-question, ge-data, ge-article, ge-iframe, geif, ge-image, ge-content {display: block;}hidden {display: none}[rule="textbox"] {outline: none;}pre{margin: 0}.ge-row:after{content:"";display:table;clear:both}.ge-col{float:left;width:100%}.ge-col.s1{width:8.33333%}.ge-col.s2{width:16.66666%}.ge-col.s3{width:24.99999%}.ge-col.s4{width:33.33333%}.ge-col.s5{width:41.66666%}.ge-col.s6{width:49.99999%}.ge-col.s7{width:58.33333%}.ge-col.s8{width:66.66666%}.ge-col.s9{width:74.99999%}.ge-col.s10{width:83.33333%}.ge-col.s11{width:91.66666%}.ge-col.s12{width:99.99999%}@media (min-width:601px){.ge-col.m1{width:8.33333%}.ge-col.m2{width:16.66666%}.ge-col.m3{width:24.99999%}.ge-col.m4{width:33.33333%}.ge-col.m5{width:41.66666%}.ge-col.m6{width:49.99999%}.ge-col.m7{width:58.33333%}.ge-col.m8{width:66.66666%}.ge-col.m9{width:74.99999%}.ge-col.m10{width:83.33333%}.ge-col.m11{width:91.66666%}.ge-col.m12{width:99.99999%}}@media (min-width:993px){.ge-col.l1{width:8.33333%}.ge-col.l2{width:16.66666%}.ge-col.l3{width:24.99999%}.ge-col.l4{width:33.33333%}.ge-col.l5{width:41.66666%}.ge-col.l6{width:49.99999%}.ge-col.l7{width:58.33333%}.ge-col.l8{width:66.66666%}.ge-col.l9{width:74.99999%}.ge-col.l10{width:83.33333%}.ge-col.l11{width:91.66666%}.ge-col.l12{width:99.99999%}}.ge-left{float:left}.ge-right{float:right}.ge-flex-cc{display:flex;justify-content:center;align-items:center}.ge-item-center{display:flex;align-items:center}.ge-flex-sc{display:flex;justify-content:flex-start;align-items:center}.ge-flex-ec{display:flex;justify-content:flex-end;align-items:center}.ge-color{color:var(--ge-primary-color)}.ge-relative{position: relative;}.ge-inline{display: inline-block;} mathjax { display: inline-block; }
</style>
<?php $Styles = $this->GetStyle(); ?>
<?php if (is_array($Styles)): ?>
    	
	<?php foreach ($Styles as $Style): ?>
		<link rel="stylesheet" type="text/css" href="<?= page.$Style ?>">

    <?php endforeach ?>

<?php elseif ($Styles != ""): ?>
	<link rel="stylesheet" type="text/css" href="<?= $Styles ?>">
<?php endif ?>
</head>
<body>
	<div class="ge-animate-opacity">
		<div>
			<surface>
				<header class="ge-width-auto">
					<fixed-header>
						<div class="ge-head ge-white">
							<?php include_once assets.templates.header ?>

							<?php 
								if (isset($HeaderTemplate))
								{
									include_once $HeaderTemplate;
								}
								else
								{
									if (isset($Header))
									{
										if ($Header)
										{
											include_once $App.templates.$Header;
										}
									}
									else
									{
										include_once $App.templates.header; 
									}
								}
							?>
						</div>
					</fixed-header>
				</header>
				<main>
					<div class="">
						<trash-content></trash-content>
						<main-content>
							<?php 
								if (isset($ContentTemplate))
								{
									include_once $ContentTemplate;
								}
								else
								{
									include_once isset($Content) ?  $App.templates.$Content : $App.templates.content;  
								}
							?>
							
						</main-content>
							
					</div>
					</main>
				 <footer>
					<?php include_once assets.templates.footer ?>
				 </footer>
			</surface>
		</div>
		<hidden>
			<?php if (isset($_SESSION[__SIGNIN__]) ): ?>
				<?php $User = $_SESSION[__SIGNIN__] ?>
				<input type="hidden" idata="userid" value="<?= $User["userid"] ?>">
				<input type="hidden" idata="username" value="<?= $User["session_firstname"] || $User["session_lastname"] ? "{$User["session_firstname"]} {$User["session_lastname"]}" : $User["session_email"] ?>">
				<input type="hidden" idata="image" value="<?= $User["session_image_local"] ? users.images.$User["session_image_local"] : "" ?>">
			<?php endif ?>
			<input type="hidden" idata="user" value="<?= isset($_SESSION[__SIGNIN__]) ?>">
			<input type="hidden" idata="page" name="" value="<?= page ?>">
		</hidden>

	</div>
    <div id="Loading"></div>

	<script src="<?= page.assets.scripts ?>library.js"></script>
	<script src="<?= page.assets.scripts ?>commons.js"></script> 
	<script src="<?= page.assets.scripts ?>modules/jquery-3.2.1.js"></script> 
	<script src="<?= page.assets.scripts ?>modules/jquery.timeago.js"></script> 
	<script src="<?= page.assets.scripts ?>modules/jquery.lazy.js"></script> 

	<script src="<?= page.assets.scripts ?>modules/initial.min.js"></script> 
	<script src="<?= page.assets.scripts ?>default.js"></script> 
	<?php $Scripts = $this->GetScript(); ?>
	
	<?php if (is_array($Scripts)): ?>
		<?php foreach ($Scripts as $Script): ?>
			<script src="<?= page.$Script ?>"></script> 
		<?php endforeach ?>

	<?php elseif ($Scripts != null): ?>
			<script src="<?= $Scripts ?>"></script> 
	<?php endif ?>

</body>
</html>