/*
* @Author: Rot
* @Date:   2017-10-26 09:08:05
* @Last Modified by:   Rot
* @Last Modified time: 2017-11-24 23:42:50
*/
(function () {


window.onload = function () 
{

	let Containers = document.querySelectorAll(".ge-article-tag-list");

	let Scroll = new System.Scroll ({
		Models: `${location.href}/scroll`,
		Offsets: 15,
		Limits: 15
	});

	Scroll.Done(function (Response) {

		var Children = Response.children;

		var Length = Children.length;

		let Container = Containers[0];

		var Index = 0, Child;

		for (var i = 0; i < Length; i++)
		{
			Child = Children[0];

			if (Child.nodeName == "LI")
			{
				Container.appendChild(Child);
				Index++;
			}
			else
			{
				Child.remove();
			}

			if (Index == 3)
			{
				Container = Container.nextElementSibling;

				Index = 0;
			}
		}

	})

}

})();