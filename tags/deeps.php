<?php

namespace System\Deeps;

class TagDeep
{

	public function ConvertTags ()
	{
		$Datas = [];

		$Index = 0;

		foreach ($this->ArticleTags as $Key => $Value) 
		{
			$Article = $Value["article_id"];

			if (array_key_exists($Article, $Datas))
			{
				$Datas[$Article] += array($Index, array($Value["tag_id"], $Value["tag"]));

				$Index++;
			}
			else
			{
				$Datas[$Article] = array(array($Value["tag_id"], $Value["tag"]));

				$Index = 1;
			}
		}
		return $Datas;
	}

}

?>