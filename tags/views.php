<?php
namespace System\Views;

import(geye.views.generic);


class TagView extends GenericView
{
	protected $Scripts = [assets.scripts => ["scroll/models.js", "scroll/business.js", "scroll/actions.js"], "default.js"];
	
	protected function Resolver ()
	{
		$this->Articles = $this->Deeps->Articles;

		$this->Users = $this->Deeps->Users;

		$this->Tags = $this->Deeps->ConvertTags();
	}
	
}

class ScrollView extends GenericView
{
	protected $Models = ["Articles", "ArticleTags"];

	protected $Templates = "articles.php";

	protected function Resolver ()
	{
		$this->Articles = $this->Deeps->Articles;

		$this->Tags = $this->Deeps->ConvertTags();
	}

} 

?>