<?php

/**
 * @Author: Rot
 * @Date:   2017-09-11 18:15:39
 * @Last Modified by:   Rot
 * @Last Modified time: 2017-12-04 17:20:50
 */

import(tags.views, false);

$Urls = [
	"/^\/(?<Tag>[a-zA-Z0-9]*)$/" 		=> views.TagView,
	"/^\/(?<Tag>[a-zA-Z0-9]*)\/scroll/" => views.ScrollView
];

?>