<?php
namespace System\Models;

import(geye.models.routine);

class Articles extends Procedure implements Nullable
{
	protected $Name = "GTagArticles";

	public $UserID;

	public $Tag;

	public $LimitRecord;

	public $OffsetRecord;

	public static function Nullable ()
	{
		return "UserID";
	}

}

class ArticleTags extends Procedure implements Nullable
{
	protected $Name = "GArticleTags";

	public $Tag;

	public $ArticleID;

	public static function Nullable ()
	{
		return "ArticleID";
	}
}

class Users extends Procedure implements Nullable
{
	protected $Name = "GTagUsers";

	public $UserID;

	public $Tag;

	public static function Nullable ()
	{
		return "UserID";
	}
}


?>
