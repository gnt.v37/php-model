<?php 
$Length = count($this->Articles);

$List = ceil($Length / 5);

$Index = 0;

?>
<div class="tag-user">
	<div class="tag-user-space">
		<div class="tag-width">
			<div class="ge-flex-sc ge-tag-height">
				<ul>
					<?php foreach ($this->Users as $User): ?>
						<li class="ge-user-tag-list">
							<div class="user-wrapper">
								<ul>
									<li class="ge-row">
										<div class="ge-left"><a href="<?= page."@{$User["user_id"]}" ?>" class="ge-color"><span>@<?= $User["user_id"] ?></span></a></div>
										<div class="ge-right">
											<a href=""></a>
										</div>
									</li>
									<li class="ge-top-24"><ge-image class="ge-tag-user-image"><center><a href="<?= page."@{$User["user_id"]}" ?>">
										<?php if ($User["image_local"]): ?>
											<img src="<?= page.users.images."40/{$User["image_local"]}" ?>">
										<?php else: ?>
											<img data-src="" ge-user-avatar="true" data-name="<?= $User["lastname"] || $User["firstname"] ? "{$User["lastname"]} {$User["firstname"]}" : $User["email"] ?>">
										<?php endif ?>
									</a></center></ge-image></li>
									<li><center class="ge-tag-user-name"><a href="<?= page."@{$User["user_id"]}" ?>"><span><?= $User["firstname"] ?></span> <span><?= $User["lastname"] ?></span></a></center></li>
									<li class="ge-user-description">
										<div><span class="ge-text-color"><?= $User["description"] ?></span></div>
									</li>
								</ul>
									
							</div>
						</li>
					<?php endforeach ?>
						
				</ul>
			</div>
		</div>
	</div>
</div>
<div class="tag-width">
	<div class="ge-tab-light">
		<ul class="ge-row">
			<li class="ge-left ge-tag-title ge-flex-cc"><h4>Articles Tag</h4></li>
			<li class="ge-left ge-fstyle ge-active"><div><a class="ge-button">Latest</a></div></li>
			<!-- <li class="ge-left ge-fstyle"><div><a class="ge-button">Popular</a></div></li> -->
		</ul>
	</div>
</div>
<div class="tag-width"><div class="ge-tag-top"><div class="articles-tag">
<?php foreach ($this->Articles as $Article): ?>

<?php if ($Index == 0): ?>
	<ul class="ge-article-tag-list">
<?php endif ?>

<?php $Index++; ?>
	<li class="article-item" data="<?= $Index ?>"><ge-article class=""><div class="ge-space">
		<div class="ge-row ge-margin"><div class="ge-left"><ge-user><div class="ge-inline">
			<a href="<?= page."@{$Article["user_id"]}"; ?>" prevent="true">
				<div class="ge-relative ge-image40">
					<?php if ($Article["user_image_local"]): ?>
						<img src="<?= page.users.images."40/{$Article["user_image_local"]}" ?>">
					<?php else: ?>
						<img data-src="" ge-user-avatar="true" data-name="<?= $Article["lastname"] || $Article["firstname"] ? "{$Article["lastname"]} {$Article["firstname"]}" : $Article["email"] ?>">
					<?php endif ?>
					<div class="ge-avatar-radius">
						<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 70 70">
							<path d="M5.53538374,19.9430227 C11.180401,8.78497536 22.6271155,1.6 35.3571429,1.6 C48.0871702,1.6 59.5338847,8.78497536 65.178902,19.9430227 L66.2496695,19.401306 C60.4023065,7.84329843 48.5440457,0.4 35.3571429,0.4 C22.17024,0.4 10.3119792,7.84329843 4.46461626,19.401306 L5.53538374,19.9430227 Z"></path><path d="M65.178902,49.9077131 C59.5338847,61.0657604 48.0871702,68.2507358 35.3571429,68.2507358 C22.6271155,68.2507358 11.180401,61.0657604 5.53538374,49.9077131 L4.46461626,50.4494298 C10.3119792,62.0074373 22.17024,69.4507358 35.3571429,69.4507358 C48.5440457,69.4507358 60.4023065,62.0074373 66.2496695,50.4494298 L65.178902,49.9077131 Z"></path></svg>
				</div></div></a></div>
			<div class="ge-inline ge-left-12">
				<div><a href="<?= page."@{$Article["user_id"]}"; ?>" prevent="true" class="ge-name"><?php echo "{$Article["lastname"]} {$Article["firstname"]}"; ?></a></div>
				<div class="ge-time"><time datetime="<?= $Article["created_at"] ?>"></time></div>
			</div></ge-user>
		</div><div class="ge-right"><div class="ge-flex-cc ge-icon-height">
			<button type="" class="ge-icon-option"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width=16 viewBox="0 0 384 384" xml:space="preserve"><circle cx="192" cy="42.667" r="42.667"/><circle cx="192" cy="192" r="42.667"/><circle cx="192" cy="341.333" r="42.667"/>
			</svg></button></div>
		</div></div>
		<div class="ge-row"><ge-image>
			<a href="<?= page."view?v={$Article["article_id"]}" ?>" prevent="true" class="ge-scale">
				<img data-src="<?= page.home.images."280/{$Article["image"]}"; ?>" class="ge-bk-img" alt="" /></a>
		</ge-image>
		<ge-content><ul class="article-info">
			<li class="" style="position: relative;"><div class=""><h4 class="">
				<a href="<?= page."view?v={$Article["article_id"]}" ?>" class="
				<?php 
					/**
					 * Style mode of article
					 *
					 * @var        string
					 */
					$Height = "height-mode-z"; 

					/**
					 * Checking description length to show
					 *
					 * @var        boolean
					 */
					$IsShow = true;

					/**
					 * Title of article length
					 *
					 * @var        callable
					 */
					$TitleLength = strlen($Article["title"]);

					if (false)
					{
						$Height = "height-mode-o";
						if ($TitleLength >= 39)
							$IsShow = false;
						else 
						{
							if ($TitleLength >= 39 && $TitleLength < 74)
								$Height = "height-mode-tw";
							else if ($TitleLength >= 74)
								$Height = "height-mode-th";
						}
					}
				?> article-title" ><?= $Article["title"] ?></a>
			</h4></div>
		</li><li>
			<div><ul class="ge-tag">
				<?php foreach ($this->Tags[$Article["article_id"]] as $Key => $Value): ?>
					<li class="ge-inline">
					<?php if ($Key != 0): ?>
						<span class="ge-tag-dot">.</span>
					<?php endif ?>
						<a href="<?= folder ?>tag/<?= $Value["0"] ?>" class="ge-color">#<?= $Value["1"] ?></a></li>
				<?php endforeach ?>
			</ul></div>
		</li></ul></ge-content></div>
	</div></ge-article></li>
<?php if ($Index == $List): $Index = 0; ?>
	</ul>
<?php endif ?>

<?php endforeach ?>
</div></div></div>