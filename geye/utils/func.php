<?php

/**
 * @Author: Rot
 * @Date:   2017-09-15 18:20:06
 * @Last Modified by:   Rot
 * @Last Modified time: 2017-11-17 20:17:48
 */

function import ($FileMixin, $JustImport = true)
{
	$File = "";

	if ($JustImport)
	{
		if (is_dir($FileMixin))
		{
			preg_match("/^(?<Source>.*?\/)(?<File>\w+)\/$/", $FileMixin, $Matches);

			$SortFile = $Matches["Source"].$Matches["File"].".php";

			$LongFile = $Matches["Source"].$Matches["File"]."/".$Matches["File"].".php";

			if (file_exists($SortFile))
			{
				include_once $SortFile;
			}
			else if (file_exists($LongFile))
			{
				include_once $LongFile;
			}
		}
		else
		{
			if (file_exists($FileMixin))
			{
				include_once $FileMixin;
			}
			else
			{
				preg_match("/^(?<Source>.*?)(?<Fake>\w+)\.php(?<File>.*?)$/", $FileMixin, $Matches);

				if ($Matches["Fake"])
				{
					$File = $Matches["Source"].$Matches["Fake"]."/".$Matches["File"];

				}

				if (file_exists($File))
				{
					include_once $File;
				}
			}

		}
	}
	else
	{
		if (file_exists($FileMixin))
		{
			$File = $FileMixin;
		}
		else
		{
			preg_match("/^(?<Source>.*?\/)(?<File>\w+)\/$/", $FileMixin, $Matches);

			$File = $Matches["Source"].$Matches["File"].".php";
		}

		if (file_exists($File))
		{
			/**
			 * $Handle is a file urls.php
			 *
			 * @var        Function
			 */
			$Handle = fopen($File, "r");

			/**
			 * File content
			 *
			 * @var        Function
			 */
			$Content = fread($Handle, filesize($File));

			fclose($Handle);

			preg_match_all("/class \w+/", $Content, $Matches);

			/**
			 * Get all of class of this file.
			 * Get all of function of this file
			 *
			 * @var        array
			 */
			
			if (count($Matches) == 0)
			{
				return false;
			}

			$Datas = $Matches[0];

			if (count($Datas) == 0)
			{
				return false;
			}

			foreach ($Datas as $Data) 
			{
				/**
				 * Define a $Name store name of recore.
				 *
				 * @var        string
				 */
				$Name = "";


				/**
				 * if $Data is a class we will remove class and get class Name
				 */
				if (start_with($Data, "class"))
				{
					$Name = str_replace("class ", "", $Data);
				}

				/**
				 * When we get $Name, we difine it.
				 */
				if (defined($Name)) 
				{
					if (constant($Name) != $Name)
					{
						die("We can't define {$Name}. It already defined.");
					}
				}
				else
				{
					define($Name, $Name);
				}
			}

			include_once $File;
		}
	}
}

function classes ($File)
{

	/**
	 * $Handle is a file urls.php
	 *
	 * @var        Function
	 */
	$Handle = fopen($File, "r");

	/**
	 * File content
	 *
	 * @var        Function
	 */
		
	$Content = fread($Handle, filesize($File));

	fclose($Handle);

	preg_match_all("/class (?<Class>\w+)/", $Content, $Matches);

	/**
	 * Get all of class of this file.
	 *
	 * @var        array
	 */
	return $Matches["Class"];
}

function start_with ($Haystack, $Needle)
{
	return $Haystack[0] === $Needle[0]
        ? strncmp($Haystack, $Needle, strlen($Needle)) === 0
        : false;
}

function end_with ($Haystack, $Needle)
{
	return preg_match('/' . preg_quote($Needle, '/') . '$/', $Haystack);
}

function random_string ($Length = 1) 
{

	$String = '';
	// You can define your own characters here.
	$Data = "123456789ABCDEFHJKLMNPRTVWXYZabcdefghijklmnopqrstuvwxyz";

	for ($i = 0; $i < $Length; $i++) 
	{
		$String .= $Data[mt_rand(0, strlen($Data)-1)];
	}

	return $String;

}
?>