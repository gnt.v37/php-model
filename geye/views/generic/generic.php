<?php

/**
 * @Author: Rot
 * @Date:   2017-10-18 22:47:04
 * @Last Modified by:   Rot
 * @Last Modified time: 2017-11-19 08:37:39
 */

namespace System\Views;

import (geye.views.generic.base);

abstract class TemplateView extends StaticMixin
{
	protected $Templates;

	public function __construct ($App, $RegexURL, $URLParameter)
	{
		parent::__construct($App);

		if (empty($this->Templates))
		{
			include_once $App.templates.main;
		}
		else
		{
			include_once $App.templates.$this->Templates;
		}
	}

	public function GetStyle ($App = null)
	{
		return parent::ResolverStyle($App);
	}

	public function GetScript ($App = null)
	{
		return parent::ResolverScript($App);
	}
}

abstract class ObjectView extends ObjectResponseMixin
{
	public function __construct ($App, $RegexURL, $URLParameter)
	{
		parent::__construct($App, $RegexURL, $URLParameter);
	}

	protected function Resolver () {}
}


abstract class GenericView extends TemplateResponseMixin {}