<?php

/**
 * @Author: Rot
 * @Date:   2017-10-18 22:35:14
 * @Last Modified by:   Rot
 * @Last Modified time: 2017-11-30 21:34:07
 */
namespace System\Views;

import(geye.views.smartif);

abstract class RequirementMixin extends ObjectMixin
{
	private const Humanize = Space::Model."Humanize";

	private const Uncallable = Space::Model."Uncallable";

	private const InvokeData = Space::Model."InvokeData";

	private const Nullable = Space::Model."Nullable";

	private const UserRequirement = Space::Model."UserRequirement";

	private const ViewRequirement = Space::Model."ViewRequirement";

	protected function Requirement ($Model)
	{
		$Humanize = false;

		$Uncallable = false;

		$InvokeData = true;

		$Nullable = [];

		$Implements = class_implements($Model);

		foreach ($Implements as $Implement) 
		{
			switch ("\\".$Implement) 
			{
				case self::Humanize:

					$Humanize = true;

					break;
				case self::Uncallable:

					$Uncallable = true;

					break;

				case self::InvokeData:

					$InvokeData = $Model::Data();

					break;
				case self::Nullable:

					$Exceptions = $Model::Nullable();

					if (is_array($Exceptions))
					{
						foreach ($Exceptions as $Exception) 
						{
							$Nullable[$Exception] = 1;
						}
					}
					else
					{
						$Nullable[$Exceptions] = 1;
					}

					break;
				case self::UserRequirement:

					if (isset($_SESSION[__SIGNIN__]) != $Model::User())
					{
						$Uncallable = true;
					}

					break;
				case self::ViewRequirement:

					if ($Uncallable == false)
					{
						$View = get_called_class();

						$RequireViews = $Model::View();

						foreach ($RequireViews as $RequireView) 
						{
							if ("\\".$View == Space::View.$RequireView)
							{
								$Uncallable = true;
								break;
							}
						}
					}

					break;
				default:
					# code...
					break;
			}
		}

		return (object)array
		(
			"Humanize"			=> $Humanize, 
			"Uncallable"		=> $Uncallable, 
			"InvokeData"		=> $InvokeData,
			"Nullable"			=> $Nullable,
		);
	}
}

abstract class ObjectResponseMixin extends RequirementMixin
{
	private const Requirement 	= Space::Model."Requirement";

	private const Procedure 	= Space::Model."Procedure";

	private const Dynamic 		= Space::Model."Dynamic";

	private const UpdateView 	= Space::View."UpdateView";

	private $Setted = [];

	private $IsOverload = false;

	protected function ResolverRoutine ($ModelName, $Model, $FormAttributes, $Requirement = null)
	{
		/**
		 * Getting all of fields of the models
		 *
		 * @var        <array>
		 */
		$Fields = parent::ClassVars($Model);

		$FieldsLength = count($Fields);

		$FieldsCount = 0;

		$Datas = [];

		$Setted = [];

		foreach ($Fields as $Field => $Mixin) 
		{
			foreach ($FormAttributes as $FormName => $Attributes) 
			{
				/**
				 * Convert form attributes to similar model attributes
				 *
				 * @var        <array>
				 */
				$AttributesEmulator = parent::Emulator($Model, $Attributes);

				$Attribute = implode(" ", $AttributesEmulator);

				/**
				 * if model's field and attribute are exists. We push it in $Keys <array>
				 */
				if (!isset($Setted[$Field]) && parent::MatchFields($Field, $Attribute))
				{
					/**
					 * This make each form call only once.
					 */
					$Form = parent::ResolverForm($FormName);
					
					/**
					 * Setting attribute which are similar with model
					 */
					if (property_exists ($Form, $Field) == false)
					{
						/**
						 * Make feild to lowercase to compare with Attribute Emulattor
						 *
						 * @var        callable
						 */
						$LowerFeild = strtolower($Field);

						$Compact = array_search($LowerFeild, $AttributesEmulator);

						$Form->SearchByCompact($Compact, $Field);

						if ($Compact && $Form->$Compact !== null)
						{
							$FieldsCount++;

							$Datas[$Field] = $Form->$Compact;

							$Setted[$Field] = null;
						}

					}
					else
					{
						$Form->Search($Field, $Mixin);

						if ($Field && $Form->$Field !== null || isset($Requirement->Nullable[$Field]))
						{
							$FieldsCount++;

							$Datas[$Field] = $Form->$Field;

							$Setted[$Field] = null;
						}
					}
				}


				/**
				 * Break if match enough atrribute
				 */
				if ($FieldsCount == $FieldsLength)
				{
					break;
				}
			}
		}

		$this->Setted = $Setted;

		/**
		 * if we found the form's attribute and the model's field are match all
		 * We will call this model.
		 */
		if ($FieldsCount == $FieldsLength)
		{
			/**
			 * Invoking class
			 *
			 * @var        $
			 */
			
			$Class = new $Model();

			/**
			 * if $this->Deep set, we would set the model for the deep.
			 * if it didn't set we will set it in class's view.
			 */
			if ($Requirement && $Requirement->Humanize)
			{
				$this->Models[$ModelName] = $Class;
				
			}
			else
			{
				/**
				 * Setting the match key for model
				 */
				foreach ($Datas as $Key => $Data) 
				{
					$Class->$Key = $Data;
				}

				$this->Models[$ModelName] = $Class->Call();

			}

		}
	}

	protected function ResolverSchema ($ModelName, $Model, $FormAttributes, $Requirement = null)
	{
		if ($Requirement)
		{
			if ($Requirement->InvokeData != null)
			{
				$Attributes = $Requirement->InvokeData;

				foreach ($FormAttributes as $FormName => $Attributes)
				{
					$Form = parent::ResolverForm($FormName);
					
					if (is_array($Attributes))
					{
						foreach ($Attributes as $Attribute => $Value) 
						{
							if (is_integer($Attribute))
							{
								if (isset($this->Setted[$Value]) == false)
								{
									$Form->Search($Value, null);
								}
							}
							else
							{
								if (isset($this->Setted[$Attribute]) == false)
								{
									$Form->Search($Attribute, $Value);
								}
							}
						}
					}
					else
					{
						if (isset($this->Setted[$Attributes]) == false)
						{
							$Form->Search($Attributes, null);
						}
					}
				}

			}
		}
		else
		{
			if ($this->IsOverload == false)
			{
				$this->IsOverload = true;

				foreach ($FormAttributes as $FormName => $Attributes) 
				{
					$Form = parent::ResolverForm($FormName);

					foreach ($Attributes as $Attribute => $Value) 
					{
						if (isset($this->Setted[$Attribute]) == false)
						{
							$Form->Search($Attribute, $Value);
						}
					}
					
				}
			}
			
		}

		$this->Models[$ModelName] = new $Model();

		if (is_subclass_of($this, self::UpdateView))
		{
			$Prime = $this->Models[$ModelName]->GetPrime();
			
			if ($Prime)
			{

			}
			else
			{
				
			}
			
		}
		
		
	}
	
	protected function ResolverModelForm($Models, $Forms)
	{
		
		$Attributes = [];

		if (is_array($Forms))
		{
			foreach ($Forms as $FormName) 
			{
				$Form = Space::Form.$FormName;

				/**
				 * Getting attributes in forms 
				 *
				 * @var        array
				 */
				$Attributes += [$FormName => parent::ClassVars($Form, "Attributes")];
			}
		}
		else
		{
			$Form = Space::Form.$Forms;

			/**
			 * Getting attributes in forms 
			 *
			 * @var        array
			 */
			$Attributes = [$Forms => parent::ClassVars($Form, "Attributes")];
		}


		if (is_array($Models) == false)
		{
			$Models = [$Models];
		}

		foreach ($Models as $ModelName) 
		{

			/**
			 * { Chaining class and namespace }
			 *
			 * @var        <string>
			 */
			$Model = Space::Model.$ModelName;

			if (is_subclass_of($Model, self::Procedure) || is_subclass_of($Model, self::Dynamic))
			{
				/**
				 * Check required routine
				 */
				if (is_subclass_of($Model, self::Requirement))
				{
					$Require = parent::Requirement($Model);
					
					if ($Require->Uncallable == false)
					{
						self::ResolverRoutine($ModelName, $Model, $Attributes, $Require);
					}
					
				}
				else
				{

					self::ResolverRoutine($ModelName, $Model, $Attributes);
				}
			}
			else
			{
				/**
				 * Check required schema
				 */
				if (is_subclass_of($Model, self::Requirement))
				{
					$Require = parent::Requirement($Model);

					if ($Require->Uncallable == false)
					{
						self::ResolverSchema($ModelName, $Model, $Attributes, $Require);
					}

				}
				else
				{
					self::ResolverSchema($ModelName, $Model, $Attributes);
				}
			}
		}
	}
}

class StaticMixin
{
	/**
	 *
	 * @var        array
	 */		
	protected $Scripts;

	/**
	 *
	 * @var        array
	 */		
	protected $Styles;

	/**
	 *
	 * @var        array
	 */	
	protected $ScriptExtends;


	public function __construct ($App, $Scripts = null, $Styles = null)
	{
		$this->App = $App;

		$this->Scripts = $Scripts;

		$this->Styles = $Styles;
	}

	public function ResolverStyle ($App)
	{
		if ($App == null)
		{
			$App = $this->App;
		}

		$Source = $App.styles;

		$Styles = self::FileLoader($Source, "css");

		if (isset($this->Styles))
		{
			if ($this->Styles)
			{
				if (is_array($this->Styles))
				{
					foreach ($this->Styles as $Mixin => $Values) 
					{
						if (is_integer($Mixin))
						{
							array_push($Styles, $Source.$Values);
						}
						else
						{
							if (is_array($Values))
							{
								foreach ($Values as $Value) 
								{
									array_push($Styles, $Mixin.$Value);
								}
							}
							else
							{
								array_push($Styles, $Mixin.$Values);
							}
						}
					}

					return $Styles;
				}

				return array_push($Styles, $Source.$this->Styles);
			}
		}

		return $Styles;
	}

	public function ResolverScript ($App)
	{
		if ($App == null)
		{
			$App = $this->App;
		}
		
		$Source = $App.scripts;

		$Scripts = self::FileLoader($Source, "js");

		if (isset($this->Scripts))
		{
			if ($this->Scripts)
			{
				if (is_array($this->Scripts))
				{
					foreach ($this->Scripts as $Mixin => $Values) 
					{
						if (is_integer($Mixin))
						{
							array_push($Scripts, $Source.$Values);
						}
						else
						{
							if (is_array($Values))
							{
								foreach ($Values as $Value) 
								{
									array_push($Scripts, $Mixin.$Value);
								}
							}
							else
							{
								array_push($Scripts, $Mixin.$Values);
							}
							
						}
					}

					return $Scripts;
				}

				return array_push($Scripts, $Source.$this->Scripts);

			}
		}
		
		return $Scripts;
	}

	private function BackFolder ($Source)
	{
		/**
		 * Getting the lastest folder
		 */
		preg_match("/.+(\/.+)/", $Source, $Matches);

		/**
		 * Stop when we we file script folder or there is nothing match
		 */
		if (preg_match("/.+\/(static)\/$/", $Matches[0]) || count($Matches) == 0)
		{
			return null;
		}

		$Source = str_replace($Matches[1], "", $Source);

		$Source .= "/";

		return $Source;
	}

	private function FileLoader($Source, $Ext)
	{
		/**
		 * Saving the folder which is we need to back when loading all file in folder
		 *
		 * @var        array
		 */
		$ParentFolders = [];

		/**
		 * Store folder
		 *
		 * @var        array
		 */
		$Folders = [];

		/**
		 * Save the lastest index we already past.
		 *
		 * @var        array
		 */
		$Keys = [];

		/**
		 * Save data we found.
		 *
		 * @var        array
		 */
		$Data = [];

		/**
		 * Check folder exists
		 */
		
		$Index = 2;

		/**
		 * Cut the last slash of Source
		 *
		 * @var        callable
		 */
		$Mixin = substr($Source, 0, -1);

		$Humanize = false;

		while (true)
		{
			/**
			 * if $Mixin is a folder
			 */
			if (is_dir($Mixin))
			{
				/**
				 * A folder has at less two element
				 * 
				 */
				if (count($Folders) > 2)
				{
					if ($Folders[$Index] == "humanize")
					{
						$Humanize = true;
					}
					else
					{
						$Humanize = false;
					}

					/**
					 * Push old folder
					 */
					array_push($ParentFolders, $Folders);

					array_push($Keys, $Index);

					$Source = $Source.$Folders[$Index]."/";

				}
				
				/**
				 * Change to new folder
				 *
				 * @var        callable
				 */
				

				$Folders = scandir($Mixin);


				if (count($Folders) > 2 && $Humanize == false)
				{
					$Index = 2;

					$Mixin = $Mixin."/".$Folders[$Index];
				}
				else
				{
					$Source = self::BackFolder($Source);

					if ($Source == null)
					{
						break;
					}

					$Folders = array_pop($ParentFolders);

					$Index = array_pop($Keys) + 1;

					if (count($Folders) - 1 >= $Index)
					{
						$Mixin = $Source.$Folders[$Index];

					}
					else
					{
						$Mixin = null;
					}
				}

			}
			else
			{
				/**
				 * if match end with, we put it to array
				 */
				if (end_with($Mixin, $Ext))
				{
					array_push($Data, $Mixin);
				}


				/**
				 * Check if it still has elements
				 */
				if (count($Folders) - 1 > $Index)
				{
					$Index++;
					$Mixin = $Source.$Folders[$Index];

				}
				else
				{
					$Source = self::BackFolder($Source);

					if ($Source == null)
					{
						break;
					}

					$Folders = array_pop($ParentFolders);

					$Index = array_pop($Keys) + 1;

					if (count($Folders) - 1 >= $Index)
					{
						$Mixin = $Source.$Folders[$Index];

					}
					else
					{
						$Mixin = null;
					}

				}
			}
		}


		return $Data;
	}
}

/**
 * { item_description }
 */
abstract class TemplateResponseMixin extends ObjectResponseMixin
{
	/**
	 *
	 * @var        string
	 */
	protected $Templates;

	private $Static;

	protected $Styles;

	protected $Scripts;

	protected $Options;

	/**
	 * {  }
	 *
	 * @param      <type>  $App           The application
	 * @param      <type>  $RegexURL      The regular expression url
	 * @param      <type>  $URLParameter  The url parameter
	 */
	public function __construct ($App, $RegexURL, $URLParameter)
	{
		parent::__construct($App, $RegexURL, $URLParameter);

		$this->Static = new StaticMixin($App, $this->Scripts, $this->Styles);

		if (empty($this->Templates))
		{
			include_once $App.templates.main;
		}
		else
		{
			include_once $App.templates.$this->Templates;
		}
	}

	protected function Resolver () {}


	public function GetStyle ($App = null)
	{
		return $this->Static->ResolverStyle($App);
	}

	public function GetScript ($App = null)
	{
		return $this->Static->ResolverScript($App);
	}

}

?>