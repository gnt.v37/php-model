<?php

/**
 * @Author: Rot
 * @Date:   2017-10-13 11:09:19
 * @Last Modified by:   Rot
 * @Last Modified time: 2017-10-18 22:26:58
 */
namespace System\Views;

import (geye.views.generic.base);

interface ProcessEditView
{
	public function ProcessData ();
}

abstract class BaseEditView extends ObjectMixin
{
	protected function FieldsMatch ($ModelReal, $Fields, $FieldsLength)
	{
		/**
		 * Model fields
		 *
		 * @var        <type>
		 */
		$ModelField = parent::ClassVars($ModelReal);

		$FieldsString = implode(" ", array_keys($ModelField));

		/**
		 * Getting attribute nummber from view fields.
		 *
		 * @var        callable
		 */
		$FieldsCount = 0;

		foreach ($Fields as $Field) 
		{

			if (self::MatchFields($Field, $FieldsString))
			{
				$FieldsCount++;
			}

			if ($FieldsCount == $FieldsLength)
			{
				return true;
			}
		}

		if ($FieldsCount == $FieldsLength)
		{
			return true;
		}

		return false;
	}

	protected function ViewFields ()
	{
		$Fields = parent::ClassVars(get_called_class());

		unset($Fields["Models"]);
		unset($Fields["Forms"]);
		unset($Fields["Deeps"]);
		unset($Fields["ModelStore"]);

		return $Fields;
	}
}

abstract class EditView extends BaseEditView
{
	protected $ColumnsData = [];

	protected function ResolverSchema ($ModelName, $Model, $FormAttributes)
	{
		/**
		 * Convert view feilds to model fields
		 *
		 * @var        <type>
		 */
		
		$ViewFields = parent::ViewFields();

		/**
		 * Getting fields which similar with model fields
		 *
		 * @var        <type>
		 */
		$FieldsEmulator = self::Emulator($Model, $ViewFields);

		/**
		 * view fields length
		 *
		 * @var        callable
		 */
		$FieldsLength = count($FieldsEmulator);

		if (self::FieldsMatch($Model, $FieldsEmulator, $FieldsLength))
		{
			$FieldsCount = 0;

			$Setted = [];

			foreach ($FieldsEmulator as $Compact => $Field) 
			{
				foreach ($FormAttributes as $FormName => $Attributes) 
				{
					/**
					 * Convert form attributes to similar model attributes
					 *
					 * @var        <type>
					 */
					$AttributesEmulator = self::Emulator($Model, $Attributes);

					$Attribute = implode(" ", $AttributesEmulator);

					/**
					 * if model's field and attribute are exists. We push it in $Keys <array>
					 */
					if (!isset($Setted[$Field]) && self::MatchFields($Field, $Attribute))
					{
						/**
						 * This make each form call only once.
						 */
						$Form = parent::ResolverForm($FormName);

						/**
						 * Setting attribute which are similar with model
						 */
						if (isset($Form->$Field) == false)
						{
							$Key = array_search($Field, $AttributesEmulator);

							$Form->SetAttributeByCompact($Key, $Field, $ViewFields[$Compact]);

							if ($Form->$Key)
							{
								$FieldsCount++;

								$this->ColumnsData[$Field] = $Form->$Key;

								$Setted[$Field] = "";
							}
							
						}
						else
						{
							$Form->SetAttribute($Field, $ViewFields[$Compact]);

							if ($Form->$Field)
							{
								$FieldsCount++;

								$this->ColumnsData[$Field] = $Form->$Field;

								$Setted[$Field] = "";
							}
						}
					}

					/**
					 * Break if match enough atrribute
					 */
					if ($FieldsCount == $FieldsLength)
					{
						break;
					}

					
				}
			}

			if ($FieldsCount == $FieldsLength)
			{
				/**
				 * Invoking class
				 *
				 * @var        $
				 */
				$this->Models[$ModelName] = new $Model();
			}
		}
		print_r($this->Forms);
	}

	protected function ResolverModelForm ($Models, $Forms)
	{
		$Attributes = [];

		foreach ($Forms as $FormName) 
		{
			$Form = Space::Form.$FormName;

			/**
			 * Getting attributes in forms 
			 *
			 * @var        array
			 */
			$Attributes = [$FormName => parent::ClassVars($Form, "Attributes")];
		}

		foreach ($Models as $Model) 
		{
			/**
			 * { Chaining class and namespace }
			 *
			 * @var        <string>
			 */
			$ModelReal = Space::Model.$Model;

			if (is_subclass_of($ModelReal, Space::Model."Schema"))
			{
				/**
				 * Invkoke schema and form
				 */
				self::ResolverSchema($Model, $ModelReal, $Attributes);
			}
		}
	}
}

?>