<?php

/**
 * @Author: Rot
 * @Date:   2017-09-25 09:56:54
 * @Last Modified by:   Rot
 * @Last Modified time: 2017-10-18 23:55:00
 */
namespace System\Views;

interface Space 
{
	/**
	 * Model's space
	 *
	 * @var        string
	 */
	const Model = "\\System\\Models\\";

	/**
	 * Form's space
	 *
	 * @var        string
	 */
	const Form  = "\\System\\Forms\\";

	/**
	 * Deep's space
	 *
	 * @var        string
	 */
	const Deep  = "\\System\\Deeps\\";
}

abstract class ContextMixin implements Space
{
	abstract protected function Resolver ();

	protected function MatchFields ($Var, $VarsString)
	{
		return preg_match("/(\s" . $Var . "\s)|(^" . $Var . "\s)|(\s" . $Var . "$)|(^" . $Var . "$)/i", strtolower($VarsString));
	}

	protected function ClassVars ($Model, $Type = "Fields")
	{
		$Store = "";

		/**
		 * Getting all class attribute.
		 *
		 * @var        Function
		 */
		$Vars = get_class_vars($Model);

		/**
		 * if fields is empty remove it before next condition happend.
		 */
		if (empty($Vars[$Type]))
		{
			/**
			 * Remove field's attribute
			 */
			unset($Vars[$Type]);
		}

		if (isset($Vars[$Type]))
		{
			/**
			 * Getting all attribute in fields
			 *
			 * @var        <array>
			 */
			$Fields = $Vars[$Type];

			/**
			 * Add to $Attribute all of the fields.
			 */
			if (is_array($Fields))
			{
				/**
				 * Incase mutiple fields
				 */
				foreach ($Fields as $Mixin => $Var) 
				{
					/**
					 * If it dose have value
					 */
					if (is_numeric($Mixin))
					{
						if (preg_match("/" . $Var . "\s/", $Store))
						{
							die("{$Var} attribute difined once");
						}
						else
						{
							$Store .= "{$Var} ";
							$Vars[$Var] = "";
						}
						
					}
					else
					{
						if (preg_match("/" . $Mixin . "\s/", $Store))
						{
							die("{$Mixin} attribute difined once");
						}
						else
						{
							$Store .= "{$Mixin} ";
							$Vars[$Mixin] = $Var;
						}
						
					}
				}
			}
			else
			{
				if (preg_match("/" . $Fields . "\s/", $Store))
				{
					die("{$Fields} attribute difined once");
				}
				else
				{
					$Store .= "{$Fields} ";
					$Vars[$Fields] = "";
				}
				
			}

			unset($Vars[$Type]);

		}

		return $Vars;
	}

	protected function GetFileContext ($File)
	{
		if (file_exists($File))
		{
			include_once $File;

			return classes($File);
		}

		return [];
	}
}

abstract class DeepMixin extends ContextMixin
{
	/**
	 *
	 * @var        array
	 */
	protected $Deeps;

	private $DeepStore;

	private $DeepExist = false;

	protected function ImportDeeps ($App)
	{
		$File = $App."\\".deeps;
		
		if ($this->Deeps == null)
		{
			$this->DeepStore = parent::GetFileContext($File);
		}
		else
		{
			if (file_exists($File))
			{
				include_once $File;

				$this->DeepStore = $this->Deeps;
			}
		}

		if ($this->DeepStore)
		{
			$this->DeepExist = true;
		}

		$this->Deeps = null;

	}

	protected function ResolverDeeps ()
	{
		foreach ($this->DeepStore as $Deep) 
		{
			$DeepReal = Space::Deep.$Deep;

			$this->Deeps[$Deep] = new $DeepReal();
		}
	}

	protected function SetDeep ($Model, $Data)
	{
		foreach ($this->DeepStore as $Deep) 
		{
			$this->Deeps[$Deep]->$Model = $Data;
		}
	}

	protected function GetDeepExist ()
	{
		return $this->DeepExist;
	}

	private function AddDeep ()
	{
		if (is_array($this->Deeps))
		{
			foreach ($this->Deeps as $Deep) 
			{
				if (in_array($Deep, $this->DeepStore) == false)
				{
					array_push($this->DeepStore, $Deep); 
				}
			}
		}
		else
		{
			if (in_array($this->Deeps, $this->DeepStore) == false)
			{
				array_push($this->DeepStore, $Deep); 
			}
		}
	}
}

abstract class FormMixin extends DeepMixin
{
	/**
	 *
	 * @var        array
	 */
	protected $Forms;

	private $RegexURL;

	private $URLParameter;

	private $FormStore;

	public function __construct ($RegexURL, $URLParameter)
	{
		/**
		 * We use this to reduce argments in ResolverModelForm
		 */
		$this->RegexURL = $RegexURL;
		$this->URLParameter = $URLParameter;
	}

	protected function ImportForms ($App)
	{
		$File = $App."\\".forms;

		if ($this->Forms == null)
		{
			$this->FormStore = parent::GetFileContext($File);
		}
		else
		{
			if (file_exists($File))
			{
				include_once $File;

				$this->FormStore = $this->Forms;
			}
		}
		

		if ($this->Forms)
		{
			self::AddForm();
		}

		$this->Forms = null;
	}

	protected function ResolverForm ($FormName)
	{
		$Form = null;

		/**
		 * This make each form call only once.
		 */
		if ($this->Forms[$FormName])
		{
			$Form = $this->Forms[$FormName];
		}
		else
		{
			$FormReal = Space::Form.$FormName;

			$Form = new $FormReal($this->RegexURL, $this->URLParameter);
				
			$this->Forms[$FormName] = $Form;
		}

		return $Form;
	}

	protected function GetFormStore ()
	{
		return $this->FormStore;
	}

	private function AddForm ()
	{

		if (is_array($this->Forms))
		{
			foreach ($this->Forms as $Form) 
			{
				if (in_array($Form, $this->FormStore) == false)
				{
					array_push($this->FormStore, $Form); 
				}
			}
		}
		else
		{
			if (in_array($this->Forms, $this->FormStore) == false)
			{
				array_push($this->FormStore, $this->Forms); 
			}
		}
	}
}

abstract class ModelMixin extends FormMixin
{
	/**
	 * Getting the models who user set
	 *
	 * @var        array
	 */
	protected $Models;

	/**
	 * Save models
	 *
	 * @var        array
	 */
	private $ModelStore = [];

	protected function ImportModels ($App)
	{
		$File = $App."\\".models;

		if ($this->Models == null)
		{
			$this->ModelStore = parent::GetFileContext($File);

		}
		else
		{
			if (file_exists($File))
			{
				include_once $File;

				$this->ModelStore = $this->Models;
			}
		}

		$this->Models = null;
	}

	protected function Emulator ($Model, $Vars)
	{
		$ModelField = parent::ClassVars($Model);

		$FieldsString = implode(" ", array_keys($ModelField));

		$Datas = [];

		foreach ($Vars as $Mixin => $Var) 
		{
			/**
			 * If it is matching without check
			 */
			if (self::MatchFields($Mixin, $FieldsString))
			{
				$Datas[$Mixin] = $Mixin;
			}
			else
			{
				preg_match_all("/" . $Mixin . "/i", strtolower($FieldsString), $Matches);

				if (count($Matches[0]) == 1)
				{
					preg_match("/(.*?\s)*(\w*" . $Mixin . "\w*)/i", strtolower($FieldsString), $MatchesName);

					if (count($MatchesName) == 3)
					{
						$Datas[$Mixin] = $MatchesName[2];
					}

				}
			}
		}

		return $Datas;
	}

	protected function GetModelStore ()
	{
		return $this->ModelStore;
	}
}

abstract class ModelFormMixin extends ModelMixin
{
	/**
	 * { function_description }
	 *
	 * @param      <type>  $Models  The models
	 * @param      <type>  $Forms   The forms
	 */
	abstract protected function ResolverModelForm($Models, $Forms);

}

abstract class ObjectMixin extends ModelFormMixin
{

	/**
	 * Invoking class
	 *
	 * @param      <string>  $RegexURL      The regular expression url
	 * @param      <string>  $URLParameter  The url parameter
	 */
	public function __construct ($App, $RegexURL, $URLParameter)
	{
		parent::__construct($RegexURL, $URLParameter);
		/**
		 * if we set $this->Deep, we will invoke deep's class.
		 */
		parent::ImportForms($App);

		parent::ImportModels($App);

		parent::ImportDeeps($App);

		/**
		 * Resolve forms
		 *
		 * @param      <string>  $RegexURL      The regular expression url
		 * @param      <string>  $URLParameter  The url parameter
		 */

		$Models = parent::GetModelStore();

		$Forms  = parent::GetFormStore();

		/**
		 * Resolve models
		 *
		 * @param      <array> 	 $this->Models  The models
		 * @param      <string>  $RegexURL      The regular expression url
		 * @param      <string>  $URLParameter  The url parameter
		 */
		$this->ResolverModelForm($Models, $Forms, $RegexURL, $URLParameter);

		if (parent::GetDeepExist())
		{
			parent::ResolverDeeps();

			foreach ($Models as $Model) 
			{
				if (isset($this->Models[$Model]))
				{
					parent::SetDeep($Model, $this->Models[$Model]);
				}
			}

			$this->Models = null;

			foreach ($Forms as $Form) 
			{
				parent::SetDeep($Form, $this->Forms[$Form]);
			}

			$this->Forms = null;
		}

		/**
		 * It for user do after resolve models.
		 */
		$this->Resolver();
	}

}

?>