<?php

/**
 * @Author: Rot
 * @Date:   2017-10-13 11:09:11
 * @Last Modified by:   Rot
 * @Last Modified time: 2017-10-18 22:27:08
 */
namespace System\Views;

import (geye.views.generic.base);

abstract class View extends ObjectMixin
{
	private $Setted = [];

	protected function ResolverRoutine ($ModelName, $Model, $FormAttributes)
	{
		/**
		 * Getting all of fields of the models
		 *
		 * @var        <array>
		 */
		$Fields = parent::ClassVars($Model);

		$FieldsLength = count($Fields);

		$FieldsCount = 0;

		$Datas = [];

		$Setted = [];

		foreach ($Fields as $Field => $Mixin) 
		{
			foreach ($FormAttributes as $FormName => $Attributes) 
			{
				/**
				 * Convert form attributes to similar model attributes
				 *
				 * @var        <array>
				 */
				$AttributesEmulator = parent::Emulator($Model, $Attributes);


				$Attribute = implode(" ", $AttributesEmulator);

				/**
				 * if model's field and attribute are exists. We push it in $Keys <array>
				 */
				if (!isset($Setted[$Field]) && parent::MatchFields($Field, $Attribute))
				{
					/**
					 * This make each form call only once.
					 */
					$Form = parent::ResolverForm($FormName);
					
					/**
					 * Setting attribute which are similar with model
					 */
					if (isset($Form->$Field) == false)
					{

						/**
						 * Make feild to lowercase to compare with Attribute Emulattor
						 *
						 * @var        callable
						 */
						$LowerFeild = strtolower($Field);

						$Compact = array_search($LowerFeild, $AttributesEmulator);

						$Form->SetAttributeByCompact($Compact, $Field);

						if ($Form->$Compact !== "null")
						{
							$FieldsCount++;

							$Datas[$Field] = $Form->$Compact;

							$Setted[$Field] = "";
						}

					}
					else
					{
						$Form->SetAttribute($Field, $Mixin);
						

						if ($Form->$Field !== "null")
						{
							$FieldsCount++;

							$Datas[$Field] = $Form->$Field;

							$Setted[$Field] = "";
						}
					}
				}


				/**
				 * Break if match enough atrribute
				 */
				if ($FieldsCount == $FieldsLength)
				{
					break;
				}
			}
		}

		$this->Setted = $Setted;

		/**
		 * if we found the form's attribute and the model's field are match all
		 * We will call this model.
		 */
		if ($FieldsCount == $FieldsLength)
		{

			/**
			 * Invoking class
			 *
			 * @var        $
			 */
			
			$Class = new $Model();

			/**
			 * Setting the match key for model
			 */
			foreach ($Datas as $Key => $Data) 
			{
				$Class->$Key = $Data;
			}

			/**
			 * if $this->Deep set, we would set the model for the deep.
			 * if it didn't set we will set it in class's view.
			 */
			$this->Models[$ModelName] = $Class->Call();
		}
	}

	protected function ResolverSchema ($ModelName, $Model, $FormAttributes)
	{

		foreach ($FormAttributes as $FormName => $Attributes) 
		{
			$Form = parent::ResolverForm($FormName);

			foreach ($Attributes as $Attribute => $Value) 
			{
				if (isset($this->Setted[$Attribute]) == false)
				{
					$Form->SetAttribute($Attribute, $Value);
				}
			}
			
		}

		$this->Models[$ModelName] = new $Model();
	}
	
	protected function ResolverModelForm($Models, $Forms)
	{
		
		$Attributes = [];

		foreach ($Forms as $FormName) 
		{
			$Form = Space::Form.$FormName;

			/**
			 * Getting attributes in forms 
			 *
			 * @var        array
			 */
			$Attributes = [$FormName => parent::ClassVars($Form, "Attributes")];
		}

		foreach ($Models as $ModelName) 
		{

			/**
			 * { Chaining class and namespace }
			 *
			 * @var        <string>
			 */
			$Model = Space::Model.$ModelName;

			if (is_subclass_of($Model, Space::Model."Procedure"))
			{
				/**
				 * Check required
				 */
				if (method_exists($Model, "Required"))
				{
				
					$Implements = class_implements($Model);

					foreach ($Implements as $Implement) 
					{
						switch ("\\".$Implement) 
						{
							case Space::Model."UserRequirement":
								if (isset($_SESSION[__SIGNIN__]) == $Model::Required())
								{
									self::ResolverRoutine($ModelName, $Model, $Attributes);
								}

								break;
							
							default:
								echo Space::Model."UserRequirement";
								break;
						}
					}
				}
				else
				{
					self::ResolverRoutine($ModelName, $Model, $Attributes);
				}
				
				
			}
			else
			{
				self::ResolverSchema($ModelName, $Model, $Attributes);
			}
		}
	}
}

/**
 * { item_description }
 */
abstract class TemplateView extends View
{
	/**
	 *
	 * @var        string
	 */
	protected $Templates;

	/**
	 *
	 * @var        array
	 */		
	protected $Scripts;

	/**
	 *
	 * @var        array
	 */		
	protected $Styles;

	/**
	 *
	 * @var        array
	 */
	protected $Options;

	/**
	 * {  }
	 *
	 * @param      <type>  $App           The application
	 * @param      <type>  $RegexURL      The regular expression url
	 * @param      <type>  $URLParameter  The url parameter
	 */
	public function __construct ($App, $RegexURL, $URLParameter)
	{
		parent::__construct($App, $RegexURL, $URLParameter);
		 
		if (empty($this->Templates))
		{
			include_once $App.templates.main;
		}
		else
		{
			include_once $App.templates.$this->Templates;
		}
	}

	protected function Resolver () {}


	private function ResolverStyle ($App)
	{
		$Source = $App.styles;

		$StyleLocal = self::FileLoader($Source, "css");

		$Styles = $this->Styles;

		if (count($StyleLocal) > 0)
		{
			if (is_array($Styles))
			{
				array_merge($Styles, $StyleLocal);
			}
			else if (empty($Styles))
			{
				$Styles = $StyleLocal;
			}
			else
			{
				$Style = $Styles;

				$Styles = [$Style];

				array_merge($Styles, $StyleLocal);
			}
		}

		return $Styles;
	}

	private function ResolverScript ($App)
	{
		$Source = $App.scripts;

		$ScriptLocal = self::FileLoader($Source, "js");

		$Scripts = $this->Scripts;

		if (count($ScriptLocal) > 0)
		{
			if (is_array($Scripts))
			{
				array_merge($Scripts, $ScriptLocal);
			}
			else if (empty($Scripts))
			{
				$Scripts = $ScriptLocal;
			}
			else
			{
				$Script = $Scripts;

				$Scripts = [$Script];

				array_merge($Scripts, $ScriptLocal);
			}
		}

		return $Scripts;
	}


	private function BackFolder ($Source)
	{
		/**
		 * Getting the lastest folder
		 */
		preg_match("/.+(\/.+)/", $Source, $Matches);

		/**
		 * Stop when we we file script folder or there is nothing match
		 */
		if (preg_match("/.+\/(static)\/$/", $Matches[0]) || count($Matches) == 0)
		{
			return null;
		}

		$Source = str_replace($Matches[1], "", $Source);

		$Source .= "/";

		return $Source;
	}

	private function FileLoader($Source, $Ext)
	{
		/**
		 * Saving the folder which is we need to back when loading all file in folder
		 *
		 * @var        array
		 */
		$ParentFolders = [];

		/**
		 * Store folder
		 *
		 * @var        array
		 */
		$Folders = [];

		/**
		 * Save the lastest index we already past.
		 *
		 * @var        array
		 */
		$Keys = [];

		/**
		 * Save data we found.
		 *
		 * @var        array
		 */
		$Data = [];

		/**
		 * Check folder exists
		 */
		
		$Index = 2;

		$Mixin = $Source;

		while (true)
		{
			/**
			 * if $Mixin is a folder
			 */
			if (is_dir($Mixin))
			{
				if (count($Folders) > 2)
				{
					/**
					 * Push old folder
					 */
					array_push($ParentFolders, $Folders);

					array_push($Keys, $Index);

					$Source = $Source.$Folders[$Index]."/";

				}
				
				/**
				 * Change to new folder
				 *
				 * @var        callable
				 */
				$Folders = scandir($Mixin);

				if (count($Folders) > 2)
				{
					$Index = 2;

					$Mixin = $Mixin."/".$Folders[$Index];
				}
				else
				{
					$Source = self::BackFolder($Source);

					if ($Source == null)
					{
						break;
					}

					$Folders = array_pop($ParentFolders);

					$Index = array_pop($Keys) + 1;

					if (count($Folders) - 1 >= $Index)
					{
						$Mixin = $Source.$Folders[$Index];

					}
					else
					{
						$Mixin = null;
					}
				}

			}
			else
			{
				if (end_with($Mixin, $Ext))
				{
					array_push($Data, $Mixin);
				}

				if (count($Folders) - 1 > $Index)
				{
					$Index++;
					$Mixin = $Source.$Folders[$Index];

				}
				else
				{
					$Source = self::BackFolder($Source);

					if ($Source == null)
					{
						break;
					}

					$Folders = array_pop($ParentFolders);

					$Index = array_pop($Keys) + 1;

					if (count($Folders) - 1 >= $Index)
					{
						$Mixin = $Source.$Folders[$Index];

					}
					else
					{
						$Mixin = null;
					}

				}
			}
		}


		return $Data;
	}

	public function GetStyle ($App)
	{
		return self::ResolverStyle($App);
	}

	public function GetScript ($App)
	{
		return self::ResolverScript($App);
	}

}



?>