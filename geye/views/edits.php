<?php

/**
 * @Author: Rot
 * @Date:   2017-09-20 21:41:17
 * @Last Modified by:   Rot
 * @Last Modified time: 2017-10-13 13:48:44
 */
namespace System\Views;

import (geye.views.generic.edits);

abstract class BaseCreateView extends EditView implements ProcessEditView
{
	public function ProcessData ()
	{
		if (count($this->ColumnsData) > 0)
		{
			foreach ($this->Models as $ModelName => $Model) 
			{
				$Model->Create($this->ColumnsData);
			}
		}
		else
		{
			die ("Your attribute is not enough !!!");
		}
	}
}

abstract class CreateView extends BaseCreateView
{
	public function __construct ($App, $RegexURL, $URLParameter)
	{
		parent::__construct($App, $RegexURL, $URLParameter);

		parent::ProcessData();
	}

	public function Resolver() {}
}

abstract class BaseUpdateView extends EditView implements ProcessEditView
{
	public function ProcessData ()
	{
		if (count($this->ColumnsData) > 0)
		{
			$Columns = $this->ColumnsData;

			foreach ($this->Models as $ModelName => $Model) 
			{
				$Key = $Model->GetPrimaryKey();

				if (is_array($Key))
				{

				}
				else
				{
					if ($Key != "" && isset($this->ColumnsData[$Key]))
					{
						$Condition[$Key] = $this->ColumnsData[$Key];

						unset($Columns[$Key]);

						$Model->Change($Columns, $Condition);
					}
					else
					{
						die("Your primary key doesn't set.");
					}
				}
			}
		}
		else
		{
			die ("Your attribute is not enough !!!");
		}
	}
}

abstract class UpdateView extends BaseUpdateView
{
	public function __construct ($App, $RegexURL, $URLParameter)
	{
		parent::__construct($App, $RegexURL, $URLParameter);

		parent::ProcessData();
	}

	public function Resolver() {}
}

abstract class BaseDeleteView extends EditView implements ProcessEditView
{
	public function ProcessData ()
	{
		if (count($this->ColumnsData) > 0)
		{
			foreach ($this->Models as $ModelName => $Model) 
			{
				$Key = $Model->GetPrimaryKey();

				if (is_array($Key))
				{

				}
				else
				{
					if ($Key != "" && isset($this->ColumnsData[$Key]))
					{
						$Condition[$Key] = $this->ColumnsData[$Key];

						$Model->Forget($Condition);
					}
					else
					{
						die("Your primary key doesn't set.");
					}
				}
			}
		}
		else
		{
			die ("Your attribute is not enough !!!");
		}
	}
}

abstract class DeleteView extends BaseDeleteView
{
	public function __construct ($App, $RegexURL, $URLParameter)
	{
		parent::__construct($App, $RegexURL, $URLParameter);

		parent::ProcessData();
	}

	public function Resolver() {}
}

?>