<?php

/**
 * @Author: Rot
 * @Date:   2017-10-18 23:54:35
 * @Last Modified by:   Rot
 * @Last Modified time: 2017-11-26 15:49:20
 */
namespace System\Views;

interface Requirement {}

interface Humanize extends Requirement {}

interface UpdateView 
{
	public function Update ();
}

interface Space 
{
	/**
	 * Model's space
	 *
	 * @var        string
	 */
	const Model = "\\System\\Models\\";

	/**
	 * Form's space
	 *
	 * @var        string
	 */
	const Form  = "\\System\\Forms\\";

	/**
	 * Deep's space
	 *
	 * @var        string
	 */
	const Deep  = "\\System\\Deeps\\";

	/**
	 * View's space
	 *
	 * @var        string
	 */
	const View  = "\\System\\Views\\";
}

abstract class ContextMixin implements Space
{
	abstract protected function Resolver ();

	protected function MatchFields ($Var, $VarsString)
	{
		return preg_match("/(\s" . $Var . "\s)|(^" . $Var . "\s)|(\s" . $Var . "$)|(^" . $Var . "$)/i", strtolower($VarsString));
	}

	protected function ClassVars ($Model, $Type = "Fields")
	{
		$Store = "";

		/**
		 * Getting all class attribute.
		 *
		 * @var        Function
		 */
		$Vars = get_class_vars($Model);

		/**
		 * if fields is empty remove it before next condition happend.
		 */
		if (empty($Vars[$Type]))
		{
			/**
			 * Remove field's attribute
			 */
			unset($Vars[$Type]);
		}

		if (isset($Vars[$Type]))
		{
			/**
			 * Getting all attribute in fields
			 *
			 * @var        <array>
			 */
			$Fields = $Vars[$Type];

			/**
			 * Add to $Attribute all of the fields.
			 */
			if (is_array($Fields))
			{
				/**
				 * Incase mutiple fields
				 */
				foreach ($Fields as $Mixin => $Var) 
				{
					/**
					 * If it dose have value
					 */
					if (is_numeric($Mixin))
					{
						if (preg_match("/" . $Var . "\s/", $Store))
						{
							die("{$Var} attribute difined once");
						}
						else
						{
							$Store .= "{$Var} ";
							$Vars[$Var] = "";
						}
						
					}
					else
					{
						if (preg_match("/" . $Mixin . "\s/", $Store))
						{
							die("{$Mixin} attribute difined once");
						}
						else
						{
							$Store .= "{$Mixin} ";
							$Vars[$Mixin] = $Var;
						}
						
					}
				}
			}
			else
			{
				if (preg_match("/" . $Fields . "\s/", $Store))
				{
					die("{$Fields} attribute difined once");
				}
				else
				{
					$Store .= "{$Fields} ";
					$Vars[$Fields] = "";
				}
				
			}

			unset($Vars[$Type]);

		}

		return $Vars;
	}

	protected function GetFileContext ($File)
	{
		if (file_exists($File))
		{
			include_once $File;

			return classes($File);
		}

		return [];
	}
}

abstract class DeepMixin extends ContextMixin
{
	/**
	 *
	 * @var        array
	 */
	protected $Deeps;

	private $DeepStore;

	private $DeepExist = false;

	protected function ImportDeeps ($App)
	{
		$File = $App.deeps;
		
		if (isset($this->Deeps))
		{
			if ($this->Deeps != null)
			{
				if (file_exists($File))
				{
					include_once $File;

					$this->DeepStore = $this->Deeps;
				}
			}
		}
		else
		{
			$this->DeepStore = parent::GetFileContext($File);
		}

		if ($this->DeepStore)
		{
			$this->DeepExist = true;
		}

		$this->Deeps = null;

	}

	protected function ResolverDeeps ()
	{
		if (is_array($this->DeepStore))
		{
			if (count($this->DeepStore) == 1)
			{
				$DeepReal = Space::Deep.$this->DeepStore[0];

				$this->Deeps = new $DeepReal();
			}
			else
			{
				foreach ($this->DeepStore as $Deep) 
				{
					$DeepReal = Space::Deep.$Deep;

					$this->Deeps[$Deep] = new $DeepReal();
				}
			}
		}
		else
		{
			$DeepReal = Space::Deep.$this->DeepStore;

			$this->Deeps = new $DeepReal();
		}
	}

	protected function SetDeep ($Model, $Data)
	{
		if (is_array($this->DeepStore))
		{
			if (count($this->DeepStore) == 1)
			{
				$this->Deeps->$Model = $Data;
			}
			else
			{
				foreach ($this->DeepStore as $Deep) 
				{
					$this->Deeps[$Deep]->$Model = $Data;
				}
			}
		}
		else
		{
			$this->Deeps->$Model = $Data;
		}
		
	}

	protected function GetDeepExist ()
	{
		return $this->DeepExist;
	}
}

abstract class FormMixin extends DeepMixin
{
	/**
	 *
	 * @var        array
	 */
	protected $Forms;

	private $RegexURL;

	private $URLParameter;

	private $FormStore;

	public function __construct ($RegexURL, $URLParameter)
	{
		/**
		 * We use this to reduce argments in ResolverModelForm
		 */
		$this->RegexURL = $RegexURL;

		$this->URLParameter = $URLParameter;
	}

	protected function ImportForms ($App)
	{
		$File = $App.forms;

		if (isset($this->Forms))
		{
			if ($this->Forms != null)
			{
				if (file_exists($File))
				{
					include_once $File;

					$this->FormStore = $this->Forms;
				}
			}
		}
		else
		{
			$this->FormStore = parent::GetFileContext($File);
		}

		$this->Forms = null;
	}

	protected function ResolverForm ($FormName)
	{
		$Form = null;

		/**
		 * This make each form call only once.
		 */
		if ($this->Forms[$FormName])
		{
			$Form = $this->Forms[$FormName];
		}
		else
		{
			$FormReal = Space::Form.$FormName;

			$Form = new $FormReal($this->RegexURL, $this->URLParameter);
				
			$this->Forms[$FormName] = $Form;
		}

		return $Form;
	}

	protected function GetFormStore ()
	{
		return $this->FormStore;
	}
}

abstract class ModelMixin extends FormMixin
{
	/**
	 * Getting the models who user set
	 *
	 * @var        array
	 */
	protected $Models;

	/**
	 * Save models
	 *
	 * @var        array
	 */
	private $ModelStore = [];

	protected function ImportModels ($App)
	{
		$File = $App.str_replace("/", ".php", models);

		if (isset($this->Models))
		{
			if ($this->Models != null)
			{
				if (file_exists($File))
				{
					include_once $File;

					$this->ModelStore = $this->Models;
				}
			}
		}
		else
		{
			$this->ModelStore = parent::GetFileContext($File);
		}

		$this->Models = null;
	}

	protected function Emulator ($Model, $Vars)
	{
		$ModelField = parent::ClassVars($Model);

		$FieldsString = implode(" ", array_keys($ModelField));

		$Datas = [];

		foreach ($Vars as $Mixin => $Var) 
		{
			/**
			 * If it is matching without check
			 */
			if (self::MatchFields($Mixin, $FieldsString))
			{
				$Datas[$Mixin] = $Mixin;
			}
			else
			{
				preg_match_all("/" . $Mixin . "/i", strtolower($FieldsString), $Matches);

				if (count($Matches[0]) == 1)
				{
					preg_match("/(.*?\s)*(\w*" . $Mixin . "\w*)/i", strtolower($FieldsString), $MatchesName);

					if (count($MatchesName) == 3)
					{
						$Datas[$Mixin] = $MatchesName[2];
					}

				}
			}
		}

		return $Datas;
	}

	protected function GetModelStore ()
	{
		return $this->ModelStore;
	}
}

abstract class ModelFormMixin extends ModelMixin
{
	/**
	 * { function_description }
	 *
	 * @param      <type>  $Models  The models
	 * @param      <type>  $Forms   The forms
	 */
	abstract protected function ResolverModelForm($Models, $Forms);

}

abstract class ObjectMixin extends ModelFormMixin
{

	/**
	 * Invoking class
	 *
	 * @param      <string>  $RegexURL      The regular expression url
	 * @param      <string>  $URLParameter  The url parameter
	 */
	public function __construct ($App, $RegexURL, $URLParameter)
	{
		parent::__construct($RegexURL, $URLParameter);
		/**
		 * if we set $this->Deep, we will invoke deep's class.
		 */
		parent::ImportForms($App);

		parent::ImportModels($App);

		parent::ImportDeeps($App);

		/**
		 * Resolve forms
		 *
		 * @param      <string>  $RegexURL      The regular expression url
		 * @param      <string>  $URLParameter  The url parameter
		 */

		$Models = parent::GetModelStore();

		$Forms  = parent::GetFormStore();

		/**
		 * Resolve models
		 *
		 * @param      <array> 	 $this->Models  The models
		 * @param      <string>  $RegexURL      The regular expression url
		 * @param      <string>  $URLParameter  The url parameter
		 */
		$this->ResolverModelForm($Models, $Forms, $RegexURL, $URLParameter);

		if (parent::GetDeepExist())
		{
			parent::ResolverDeeps();

			if (is_array($Models))
			{
				foreach ($Models as $Model) 
				{
					if (isset($this->Models[$Model]))
					{
						parent::SetDeep($Model, $this->Models[$Model]);

						unset($this->Models[$Model]);
					}
				}
			}
			else
			{
				if (isset($this->Models[$Models]))
				{
					parent::SetDeep($Models, $this->Models[$Models]);

					unset($this->Models[$Models]);
				}
			}

			$this->Models = null;

			if (is_array($Forms))
			{
				foreach ($Forms as $Form) 
				{
					parent::SetDeep($Form, $this->Forms[$Form]);

					unset($this->Forms[$Form]);

				}
			}
			else
			{

				parent::SetDeep($Forms, $this->Forms[$Forms]);

				unset($this->Forms[$Forms]);
			}

			$this->Forms = null;
		}

		/**
		 * It for user do after resolve models.
		 */
		$this->Resolver();
	}

}
?>