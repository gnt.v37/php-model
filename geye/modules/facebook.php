<?php

/**
 * @Author: Rot
 * @Date:   2017-12-12 18:01:35
 * @Last Modified by:   Rot
 * @Last Modified time: 2017-12-12 20:22:10
 */

include_once "facebook/autoload.php";

class Facebook
{
	private const APP_ID = "1972398116352261";

	private const APP_SECRET = "a0bbbb6ee3471cc155829042b36d115e";

	private const APP_CALLBACK = "http://localhost/geye/g/auth/facebook_signin";

	private $App = null;

	private $Helper = null;

	public function __construct ()
	{
		$this->App = new Facebook\Facebook([
			"app_id" => self::APP_ID,
			"app_secret" => self::APP_SECRET,
			"default_graph_version" => "v2.2",
		]);

		$this->Helper = $this->App->getRedirectLoginHelper();
	}

	public function AuthUrl ()
	{
		return $this->Helper->getLoginUrl(self::APP_CALLBACK, ["email"]);
	}

	public function Get()
	{
		$OAuth2Client = $this->App->getOAuth2Client();

		$LongLived = $OAuth2Client->getLongLivedAccessToken($this->Helper->getAccessToken());

		$this->App->setDefaultAccessToken($LongLived);

		$Result = $this->App->get("/me?fields=name,first_name,last_name,email,link,gender,locale,picture");

		return $Result->getGraphNode()->asArray();
	}
}

?>