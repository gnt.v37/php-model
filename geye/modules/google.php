<?php

/**
 * @Author: Rot
 * @Date:   2017-12-11 22:37:48
 * @Last Modified by:   Rot
 * @Last Modified time: 2017-12-12 12:25:30
 */

set_include_path(get_include_path() . PATH_SEPARATOR . __DIR__ .'/google/google/apiclient/src');

require_once __DIR__.'/google/autoload.php';

use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\HttpFoundation\Response;

class Google
{
	private const CLIENT_ID = "758171547003-4bebacq20bsmcacfsru61patbli6qtvt.apps.googleusercontent.com";

	private const CLIENT_SECRET = "tnG53qyzkdwpWPcIqZPyqaBO";

	private const REDIRECT_URI = "http://localhost/geye/g/auth/google_signin";

	private const APPLICATION_NAME = "Geye";

	private $Client;

	public function __construct ()
	{
		$this->Client = new Google_Client();

		$this->Client->setApplicationName(self::APPLICATION_NAME);

		$this->Client->setClientId(self::CLIENT_ID);

		$this->Client->setClientSecret(self::CLIENT_SECRET);

		$this->Client->setRedirectUri(self::REDIRECT_URI);

		$this->Client->setScopes('email');

	}

	public function Authenticate ($Code)
	{
		$this->Client->authenticate($Code);
	}

	public function AccessToken($Session = null)
	{
		if ($Session)
		{
			$this->Client->setAccessToken($Session);
		}
		else
		{
			return $this->Client->getAccessToken();
		}
	}

	public function AuthUrl()
	{
		return $this->Client->createAuthUrl();
	}

	public function Service ()
	{
		return new Google_Service_Plus($this->Client);
	}
}


?>