<?php

class Imagick
{
	public $Quality = 0;

	/**
	 * Copy image from link
	 *
	 * @param      string  $Folder  The folder
	 * @param      <string>  $Url     The url
	 *
	 * @return     string  ( file name )
	 */
	public function Clone ($Folder, $Url)
	{
		/**
		 * Getting extend which determine the file we want to create
		 *
		 * @var        <type>
		 */
		$Ext = self::GetExt($Url);

		if ($Ext)
		{
			/**
			 * Create a empty file
			 *
			 * @var        string
			 */
			$File = $Folder . "/" . md5(mktime());

			$Image = "";

			switch ($Ext) 
			{
				case "jpg":
				case "jpeg":

					/**
					 * Getting image file
					 *
					 * @var        callable
					 */
					$Image = imagecreatefromjpeg($Url);

					$File .= ".{$Ext}";

					/**
					 * Copy file
					 */
					imagejpeg($Image, $File, $this->Quality);

					# code...
					break;
				case "png":

					/**
					 * Create image file by png
					 *
					 * @var        callable
					 */
					$Image = imagecreatefrompng($Url);

					$File .= ".{$Ext}";

					/**
					 * Copy file
					 */
					imagepng($Image, $File, $this->Quality);
					break;
				
				default:
					# code...
					break;
			}

			imagedestroy($Image);

			return $File;

		}
			
		return null;
	}

	public function Image ($Folder, $Base64, $Mime)
	{
		/**
		 * Convert image by base 64
		 */
		if ($Mime)
		{
			/**
			 * Create a empty file
			 *
			 * @var        string
			 */
			$File = $Folder . md5(mktime());

			$Image = "";

			switch ($Mime) 
			{
				case "image/jpeg":

					/**
					 * Setting a jpg image content
					 *
					 * @var        callable
					 */
					$Image = str_replace("data:image/jpeg;base64,", "", $Base64);

					/**
					 * Setting image extend
					 * 
					 * @var 	string
					 */		
					$File .= ".jpeg";

					# code...
					break;
				case "image/png":

					/**
					 * Settting a png image content
					 *
					 * @var        callable
					 */
					$Image = str_replace("data:image/png;base64,", "", $Base64);

					/**
					 * Setting image extend
					 * 
					 * @var 	string
					 */
					$File .= ".png";

					break;
				default:
					# code...
					break;
			}

			/**
			 * Change empty to plus
			 *
			 * @var        callable
			 */
			$Image = str_replace(' ', '+', $Image);

			/**
			 * Convert image
			 *
			 * @var        callable
			 */
			$Data = base64_decode($Image);

			/**
			 * Put image content to file
			 */
			file_put_contents($File, $Data);

			return $File;

		}
			
		return null;
	}

	public function Resize ($Folder, $Url, $ImageName, $ResizeWidth, $ResizeHeight = null)
	{
		$Ext = self::GetExt($Url);

		if ($Ext)
		{
			/**
			 * Create a empty file
			 *
			 * @var        string
			 */
			if ($ImageName)
			{
				$File = $Folder . "/" . $ImageName;
			}
			else
			{
				$File = $Folder . "/" . md5(mktime());
			}
			

			$Image = "";

			switch ($Ext) 
			{
				case "jpg":
				case "jpeg":

					/**
					 * Getting image file
					 *
					 * @var        callable
					 */
					$Image = imagecreatefromjpeg($Url);

					break;
				case "png":

					/**
					 * Create image file by png
					 *
					 * @var        callable
					 */
					$Image = imagecreatefrompng($Url);

					break;
				
				default:
					# code...
					break;
			}

			/**
			 * Set extend to file
			 */
			$File .= ".{$Ext}";

			/**
			 * Getting real file size x
			 *
			 * @var        callable
			 */
			$OriginalWidth = imagesx($Image);

			/**
			 * Getting real file size y
			 *
			 * @var        callable
			 */
			$OriginalHeight = imagesy($Image);

			if ($ResizeHeight == null)
			{
				$ResizeHeight = $OriginalHeight / $OriginalWidth * $ResizeWidth;
			}
			else if ($ResizeWidth == null)
			{
				$ResizeWidth = $OriginalWidth / $OriginalHeight * $ResizeHeight;
			}
			else
			{
				if ($OriginalWidth > $ResizeWidth || $OriginalHeight > $ResizeHeight)
				{
					if ($OriginalWidth > $OriginalHeight)
					{
						/**
						 * Resize height by width
						 *
						 * @var        callable
						 */
						$ResizeHeight = $OriginalHeight / $OriginalWidth * $ResizeWidth;

					}
					else if ($OriginalWidth < $OriginalHeight)
					{
						/**
						 * Resize width by height
						 *
						 * @var        callable
						 */
						$ResizeWidth = $OriginalWidth / $OriginalHeight * $ResizeHeight;
					}
				}
			}

			/**
			 * Set image size
			 *
			 * @var        callable
			 */
			$NewImage = imagecreatetruecolor($ResizeWidth, $ResizeHeight);

			/**
			 * Copyt image data
			 */
			imagecopyresampled($NewImage, $Image, 0, 0, 0, 0, $ResizeWidth, $ResizeHeight, $OriginalWidth, $OriginalHeight);

			switch ($Ext) 
			{
				case "jpg":
				case "jpeg":

					/**
					 * Create a jpg file
					 */
					imagejpeg($NewImage, $File, 100);
					break;
				case "png":

					/**
					 * Create a png image file
					 */
					imagepng($NewImage,$File, 9);

					break;
				
				default:
					# code...
					break;
			}

			imagedestroy($Image);
			imagedestroy($NewImage);

			return "{$ImageName}.{$Ext}";
		}
	}

	protected function GetExt ($Url)
	{
		preg_match("/.*?.(?P<ext>\w+)$/i", strtolower($Url), $Matches);
		
		if (isset($Matches["ext"]))
		{
			return $Matches["ext"];
		}

		return null;
	}

	/**
	 * Delete file
	 *
	 * @param      <string>  $File   The file
	 */
	public function Destroy($File)
	{
		if (file_exists($File))
		{
			unlink($File);
		}
	}

}

?>