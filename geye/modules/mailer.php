<?php 

namespace System;

include_once("geye/modules/mailer/phpmailer.php");
include_once("geye/modules/mailer/exception.php");
include_once("geye/modules/mailer/smtp.php");

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

class Mail 
{

	/**
     * This mail use STMP
     *
     * @var string
     */
	private $From = "response.geye@gmail.com";

	/**
     * The password of STMP mail.
     *
     * @var string
     */
	private $Secret = "asdssa12";

	/**
     * Specify main and backup SMTP servers.
     *
     * @var string
     */
	private $Host = "smtp.gmail.com";

	/**
     * Port.
     *
     * @var array
     */
    
    private $Port = 587;

    /**
     * The title of mail.
     *
     * @var array
     */
	private $Subject = NULL;

	/**
     * Mail content.
     *
     * @var string
     */
	private $Body = [];

	/**
     * PHPMailer.
     *
     * @var object
     */

	public function __construct ($To = NULL, $Subject = NULL, $Body = NULL)
	{
		if ($To && $Subject && $Body)
		{
			$this->To = $To;
			$this->Subject = $Subject;
			$this->Body = $Body;

			$this->Send();
		}
	}

	public function Send ()
	{
		$Mail = new PHPMailer(true);

		try 
		{
			//Server settings

			$Mail->SMTPDebug = 2;							// Enable verbose debug output

			$Mail->isSMTP();								// Set mailer to use SMTP

			$Mail->Host = $this->Host;						// Specify main and backup SMTP servers

			$Mail->SMTPAuth = true;							// Enable SMTP authentication

			$Mail->Username = $this->From;					// SMTP username

			$Mail->Password = $this->Secret;				// SMTP password

			$Mail->SMTPSecure = 'tls';						// Enable TLS encryption, `ssl` also accepted

			$Mail->Port = $this->Port;						// TCP port to connect to

			$Mail->SMTPOptions = array(
				'ssl' => array(
					'verify_peer' => false,
					'verify_peer_name' => false,
					'allow_self_signed' => true
				)
			);

			//Recipients
			
			$Mail->setFrom($this->From, "Geye");

			$Mail->addAddress($this->To);					// Add a recipient

			//Content

			$Mail->isHTML(true);							// Set email format to HTML

			$Mail->Subject = $this->Subject;

			$Mail->Body    = $this->Body["Content"];

			if (isset($this->Body["Description"]))
		    {
				$Mail->AltBody = $this->Body["Description"];
			}

			$Mail->send();

		} catch (Exception $e) 
		{
			echo 'Mailer Error: ' . $Mail->ErrorInfo;
		}
	}

	public function SetSubject ($Subject)
	{
		$this->Subject = $Subject;
	}

	public function SetTo ($To)
	{
		$this->To = $To;
	}

	public function SetBody ($Body)
	{
		$this->Body = $Body;
	}


}
?>