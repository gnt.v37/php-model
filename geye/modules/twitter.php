<?php

/**
 * @Author: Rot
 * @Date:   2017-12-12 11:52:59
 * @Last Modified by:   Rot
 * @Last Modified time: 2017-12-12 13:20:38
 */

require_once "twitter/autoload.php";

use Abraham\TwitterOAuth\TwitterOAuth;

class Twitter
{
	private $Connect = null;

	private const CONSUMER_KEY = "esSTI6FX6J2tGfMF3SYmOdHLe";

	private const CONSUMER_SECRET = "LmdlY6zBJz59yVRPDrgtVt0nTjmEiFTB8XE8KGWEFeKXwRG2fn";

	private const OAUTH_CALLBACK = "http://localhost/geye/g/auth/twitter_signin";

	public function __construct ($Auth = null, $Secret = null)
	{
		if ($Auth && $Secret)
		{
			$this->Connect = new TwitterOAuth(self::CONSUMER_KEY, self::CONSUMER_SECRET, $Auth, $Secret);
		}
		else
		{
			$this->Connect = new TwitterOAuth(self::CONSUMER_KEY, self::CONSUMER_SECRET);
		}
		
	}

	public function AuthUrl ()
	{
		$Request = $this->Connect->oauth("oauth/request_token", array("oauth_callback" => self::OAUTH_CALLBACK));

		return $this->Connect->url("oauth/authorize", array("oauth_token" => $Request["oauth_token"]));
	}

	public function AccessToken ($Verifier)
	{
		return $this->Connect->oauth("oauth/access_token", array("oauth_verifier" => $Verifier));
	}

	public function Credential ()
	{
		return $this->Connect->get("account/verify_credentials");
	}
}