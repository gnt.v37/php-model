<?php

/**
 * @Author: Rot
 * @Date:   2017-09-16 21:28:28
 * @Last Modified by:   Rot
 * @Last Modified time: 2017-11-26 15:42:30
 */
namespace System;

include_once geye."apps/registry.php";

class App extends Registry
{
	
	public function __construct($Apps)
	{
		parent::__construct($Apps);

		/**
		 * if urls exists we get urls else we get space.
		 *
		 * @var        string
		 */
		
		$URLPage = str_replace("urls=", "", $_SERVER["QUERY_STRING"]);

		foreach ($Apps as $Alias => $App) 
		{
			/**
			 * if we found a url match in apps
			 * get it and break;
			 * @var  string
			 */
			if (preg_match($Alias, $URLPage))
			{
				break;
			}
		}

		/**
		 * if $App is not null
		 * @return url or die
		 */
		if ($App)
		{
			/**
			 * Get url file of App.
			 *
			 * @var        string
			 */
			$File = $App."/".urls;

			/**
			 * Import url's app file.
			 */
			include_once geye.urls;
			include_once $File;

			/**
			 * Get all of class in file
			 *
			 * @var        <array>
			 */
			new Urls\Url($URLPage, $Urls, $Alias, $App."/");
		

		}
		else
		{
			die("This page doesn't found.");
		}
	}
}

?>