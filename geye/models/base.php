<?php

/**
 * @Author: Rot
 * @Date:   2017-09-15 18:20:05
 * @Last Modified by:   Rot
 * @Last Modified time: 2017-12-29 16:23:08
 */

namespace System\Models;

import(geye.models.brain.nerves);

interface BaseSort
{
	const Random = "RAND";

	const Decrease = "DESC";

	const Increase = "ASC";
}

interface BaseChain
{
	const Left = "LEFT OUTER JOIN";

	const Right = "RIGHT OUTER JOIN";
}

abstract class Model extends Nerve implements BaseSort, BaseChain
{
	protected const NotIn = "NotIn";

	protected const In = "In";
	
	public static function Contains ($Illusions, $AcceptableNull = false)
	{
		$IllusionCells = "(";

		$IllusionDatas = [];

		foreach ($Illusions as $Cell => $Data) 
		{
			if ($Data == null && $AcceptableNull == false)
			{
				continue;
			}

			$IllusionCells .= " {$Cell} LIKE ? AND ";

			array_push($IllusionDatas, "%{$Data}%");
		}

		$IllusionCells = substr($IllusionCells, 0, -4);

		$IllusionCells .= ") AND ";

		if ($IllusionCells && $IllusionDatas)
		{
			return (object)["IllusionCells" => $IllusionCells, "IllusionDatas" => $IllusionDatas];
		}

		return null;
	}

	public static function Equal ($Illusions, $AcceptableNull = false)
	{
		$IllusionCells = "(";

		$IllusionDatas = [];

		foreach ($Illusions as $Cell => $Data) 
		{
			if ($Data == null && $AcceptableNull == false)
			{
				continue;
			}

			$IllusionCells .= " {$Cell} = ? AND ";

			array_push($IllusionDatas, $Data);
		}

		$IllusionCells = substr($IllusionCells, 0, -4);

		$IllusionCells .= ") AND ";

		if ($IllusionCells && $IllusionDatas)
		{
			return (object)["IllusionCells" => $IllusionCells, "IllusionDatas" => $IllusionDatas];
		}

		return null;

	}

	public static function IsNull ($Illusions)
	{
		$IllusionCells = "(";

		$IllusionDatas = [];

		if (is_array($Illusions))
		{
			foreach ($Illusions as $Cell) 
			{
				$IllusionCells .= " {$Cell} IS NULL AND ";
			}
		}
		else
		{
			$IllusionCells .= " {$Illusions} IS NULL AND ";
		}
			

		$IllusionCells = substr($IllusionCells, 0, -4);

		$IllusionCells .= ") AND ";

		if ($IllusionCells || $IllusionDatas)
		{
			return (object)["IllusionCells" => $IllusionCells, "IllusionDatas" => $IllusionDatas];
		}

		return null;
	}

	public static function Or ($Illusions, $AcceptableNull = false)
	{
		if ($Illusions)
		{
			if (is_object($Illusions))
			{
				if ($Illusions)
				{
					$IllusionCells = str_replace("AND", "OR", $Illusions->IllusionCells);

					return (object)["IllusionCells" => $IllusionCells, "IllusionDatas" => $Illusions->IllusionDatas];
				}
			}
			else
			{
				$IllusionCells = "";

				$IllusionDatas = [];

				foreach ($Illusions as $Cell => $Data) 
				{
					if ($Data == null && $AcceptableNull == false)
					{
						continue;
					}

					$IllusionCells .= " {$Cell} = ? OR ";

					array_push($IllusionDatas, $Data);
				}


				if ($IllusionCells || $IllusionDatas)
				{
					return (object)["IllusionCells" => $IllusionCells, "IllusionDatas" => $IllusionDatas];
				}

			}
		}

		return null;
	}

	public static function And ($Illusions, $AcceptableNull = false)
	{
		if ($Illusions)
		{
			if (is_object($Illusions))
			{
				if ($Illusions)
				{
					return (object)["IllusionCells" => $IllusionCells, "IllusionDatas" => $Illusions->IllusionDatas];
				}
			}
			else
			{
				$IllusionCells = "(";

				$IllusionDatas = [];

				foreach ($Illusions as $Cell => $Data) 
				{
					if ($Data == null && $AcceptableNull == false)
					{
						continue;
					}

					$IllusionCells .= " {$Cell} = ? AND ";

					array_push($IllusionDatas, $Data);
				}

				$IllusionCells = substr($IllusionCells, 0, -4);

				$IllusionCells .= ") AND ";
				
				if ($IllusionCells || $IllusionDatas)
				{
					return (object)["IllusionCells" => $IllusionCells, "IllusionDatas" => $IllusionDatas];
				}

			}
		}

		return null;
	}

	public static function NotIn ($Illusions, $AcceptableNull = false)
	{
		$IllusionCells = " WHERE ";

		$IllusionDatas = [];

		foreach ($Illusions as $Cell => $Data) 
		{
			if ($Data == null && $AcceptableNull == false)
			{
				continue;
			}

			$IllusionCells .= " {$Cell} = ? AND ";

			array_push($IllusionDatas, $Data);
		}

		$IllusionCells = substr($IllusionCells, 0, -4);

		$IllusionCells .= ") AND ";

		if ($IllusionCells && $IllusionDatas)
		{
			return (object)["Type" => "NotIn", "IllusionCells" => $IllusionCells, "IllusionDatas" => $IllusionDatas];
		}
	}

}

