<?php 

namespace System\Models;

import(geye.models.base);

class BaseSchema extends Model
{
	protected $Prime;

	public function __construct ()
	{
		if (empty($this->NerveCells))
		{
			$this->NerveCells = parent::NerveCellsString();
		}
	}

	private function Constraint ($NerveCells)
	{
		/**
		 * Search Constraint
		 *
		 * @var        string
		 */
		$Constraint = "";

		/**
		 * Init for parameter
		 *
		 * @var        integer
		 */
		$Index = 0;

		/**
		 * Parameter
		 */
		$DataCells = [];

		if (is_object($NerveCells))
		{
			$NerveCells = [$NerveCells];
		}

		foreach ($NerveCells as $NerveCell => $Data) 
		{
			if (is_object($Data))
			{
				if (is_numeric($NerveCell))
				{
					$Constraint .= $Data->IllusionCells;

					foreach ($Data->IllusionDatas as $IllusionData) 
					{
						$DataCells[$Index] = $IllusionData;

						$Index++;
					}
				}
				else
				{
					if ($Data->Type == Schema::NotIn)
					{
						$Constraint .= " {$NerveCell} NOT IN ( SELECT {$NerveCell} FROM {$this->Nerve} {$Data->IllusionCells}";

						foreach ($Data->IllusionDatas as $IllusionData) 
						{
							$DataCells[$Index] = $IllusionData;

							$Index++;
						}
					}
				}
				continue;
			}

			if (is_numeric($NerveCell) && empty($Data))
			{
				continue;
			}

			/**
			 * Setting column Constraint
			 */
			$Constraint .= $NerveCell . " = ? AND ";

			/**
			 * Setting parameter start with index = 0;
			 */
			$DataCells[$Index] = "$Data";

			/**
			 * Increase index
			 */
			$Index++;
		}

		$Constraint = substr($Constraint, 0, -4);


		return ["Constraint" => $Constraint, "DataCells" => $DataCells];
	}

	public function Remember ($NerveCells, $JustOne = false)
	{
		if ($NerveCells)
		{
			$Constraint = self::Constraint($NerveCells);

			
			if ($Constraint["Constraint"] && $Constraint["DataCells"])
			{
				/**
				 * Remove the last AND word of Constraint
				 */
				$this->Constraint .= $Constraint["Constraint"];

				/**
				 * Set parameter
				 */
				$this->DataCells += $Constraint["DataCells"];
				
				/**
				 * if you want to get only column
				 */
				if ($JustOne)
				{
					$this->JustOne = $JustOne;
				}
			}
		}

		return $this;
	}

	public function JustOne ()
	{
		$this->JustOne = true;
		
		return $this;
	}

	public function Count ($NerveCells = NULL)
	{
		if ($NerveCells)
		{
			self::Remember($NerveCells, true);
		}

		$Prime = $this->Prime ? $this->Prime : "*";

		$this->NerveCells = " COUNT({$Prime}) AS Total";

		return parent::BaseRemember();
	}

	public function Write ($NerveCells)
	{
		/**
		 * Alias value
		 *
		 * @var        string
		 */
		$Values = "";

		/**
		 * Convert columns to string
		 *
		 * @var        string
		 */
		$NerveCellsString = "";

		/**
		 * Setting index to first parameter
		 *
		 * @var        integer
		 */
		$Index = 0;

		/**
		 * Contain parameter value
		 */
		$Parameter;

		foreach ($NerveCells as $NerveCell => $Data) {
			/**
			 * We use ? to resolve injection
			 */
			$Values .= "?, ";

			/**
			 * We use comma to make it became the column in sql
			 */
			$NerveCellsString .= $NerveCell . ", ";

			/**
			 * Set value to parameter
			 */
			$Parameter[$Index] = $Data;
			$Index++;
		}

		/**
		 * Remove comma of values
		 *
		 * @var        callable
		 */
		$Values = substr($Values, 0, -2);

		/**
		 * Remove comma of columns
		 *
		 * @var        callable
		 */
		$NerveCellsString = substr($NerveCellsString, 0, -2);
		
		/**
		 * Query
		 *
		 * @var        string
		 */
		$Query = "INSERT INTO {$this->Nerve} ({$NerveCellsString}) VALUES ({$Values})";

		return parent::BaseWrite($Query, $Parameter);
	}

	public function Change ($NerveCells, $Prime)
	{
		/**
		 * The fields we want to change
		 *
		 * @var        string
		 */
		$Setter = "";

		$Constraint = "";

		/**
		 * Parameter alway start from 0. 
		 *
		 * @var        integer
		 */
		$Index = 0;

		/**
		 * This contain value
		 */
		$DataCells;


		/**
		 * Resolve column to sql
		 */
		foreach ($NerveCells as $NerveCell => $Data) 
		{
			$Setter .= $NerveCell . " = ?, ";

			$DataCells[$Index] = $Data;

			$Index++;
		}

		/**
		 * Resolve Constraint to sql
		 */
		foreach ($Prime as $NerveCell => $Data) 
		{
			$Constraint .= $NerveCell . " = ? AND ";

			$DataCells[$Index] = $Data;

			$Index++;
		}

		$Setter = substr($Setter, 0, -2);
		
		$Constraint = substr($Constraint, 0, -4);

		$Illusion = "UPDATE " . $this->Nerve . " SET {$Setter} WHERE {$Constraint}";

		return parent::BaseWrite($Illusion, $DataCells);
	}

	public function Forget ($NerveCells)
	{
		$Constraint = self::Constraint($NerveCells);

		$Illusion = "DELETE FROM " . $this->Nerve . " WHERE {$Constraint["Constraint"]}";

		return parent::BaseWrite($Illusion, $Constraint["DataCells"]);
	}

} 

class Schema extends BaseSchema 
{
	public function __construct ()
	{
		if (empty($this->Nerve))
		{
			self::SetName();
		}
	}

	public function GetPrime ()
	{
		return $this->Prime;
	}

	protected function SetName ()
	{
		$Class = str_replace("System\\Models\\", "", get_called_class());
		
		$this->Nerve = strtolower($Class);
	}

	protected function NerveCellsString ()
	{
		$NerveCells = "";

		if ($this->NerveCells)
		{
			foreach ($this->NerveCells as $NerveCell) 
			{
				$NerveCells .= $this->Nerve. ".{$NerveCell}, ";
			}
		}
		else
		{
			$NerveCells = "*, ";
		}

		return substr($NerveCells, 0, -2);
	}

}

?>