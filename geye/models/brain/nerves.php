<?php

/**
 * @Author: Rot
 * @Date:   2017-09-15 18:20:05
 * @Last Modified by:   Rot
 * @Last Modified time: 2017-12-08 23:08:49
 */
namespace System\Models;

import(geye.models.brain);

class BaseNerve
{
	protected $Sort 		= null;
	
	protected $Nerve 		= null;

	protected $Chain		= null;
	
	protected $Illusion		= null;

	protected $JustOne		= false;

	protected $NerveCells	= null;

	protected $Limit 		= null;

	protected $Constraint	= null;

	protected $DataCells 	= [];

	protected function BaseRemember ()
	{
		$DataCells 		= null;

		if ($this->Illusion)
		{
			$Illusion = $this->Illusion;

			if ($this->DataCells)
			{
				$DataCells = $this->DataCells;
			}
		}
		else
		{
			$Illusion = "SELECT " . $this->NerveCells . " FROM " . $this->Nerve . $this->Chain;

			if ($this->Constraint)
			{
				if ($this->DataCells)
				{

					$Illusion .= " WHERE {$this->Constraint}" . $this->Sort;

					if ($this->Limit)
					{
						$Illusion .= " {$this->Limit}";
					}

					$DataCells = $this->DataCells;

				}
			}
			else
			{
				$Illusion .= $this->Sort;
				
				if ($this->Limit)
				{
					$Illusion .= " {$this->Limit}";
				}

			}
		}

		$JustOne = $this->JustOne;

		self::NeverMind();

		return Brain::Remember($Illusion, $DataCells, $JustOne);
	}

	protected function BaseWrite($Illusion, $Data)
	{
		return Brain::Write($Illusion, $Data);
	}

	private function NeverMind()
	{
		$this->Chain = null;

		$this->Constraint = null;
		
		$this->Illusion = null;

		$this->JustOne = false;

		$this->Limit = null;

		$this->Sort = null;

		$this->DataCells = [];

	}
}

class Nerve extends BaseNerve
{
	
	public function Illusion ($Illusion, $DataCells, $Get = true, $JustOne = false)
	{
		if ($JustOne)
		{
			$this->JustOne = $JustOne;
		}

		$this->DataCells = $DataCells;

		$this->Illusion = $Illusion;

		if ($Get)
		{
			return parent::BaseRemember();
		}
		else
		{
			parent::BaseWrite($Illusion, $DataCells);
		}
	}

	public function Limit ($Limit, $Offset = null)
	{
		/**
		 * Init limit
		 *
		 * @var        string
		 */
		$LimitTemp = "";

		/**
		 * If limit is array, it mean limit to offset
		 * Overwise limit are string which mean only limit
		 */
		if (is_array($Limit))
		{
			/**
			 * Set limit
			 *
			 * @var        string
			 */
			$LimitTemp = "LIMIT {$Limit[0]}";

			/**
			 * Set off set
			 */
			$LimitTemp .= " OFFSET {$Limit[1]}";
		}
		else
		{
			/**
			 * Set limit only.
			 *
			 * @var        string
			 */
			$LimitTemp = "LIMIT {$Limit}";

			if ($Offset)
			{
				$LimitTemp .= " OFFSET {$Offset}";
			}
		}
			
		/**
		 * Set local limit to class attribute
		 */
		$this->Limit = $LimitTemp;

		return $this;
	}

	public function Get ($NerveCells = NULL)
	{
		if ($NerveCells)
		{
			$NerveCellTemp = "";

			if (is_array($NerveCells))
			{
				foreach ($NerveCells as $NerveCell => $Alias) 
				{
					if (is_numeric($NerveCell))
					{
						$NerveCellTemp .= "{$Alias}, ";
					}
					else
					{
						$NerveCellTemp .= "{$Alias} AS {$NerveCell}, ";
					}
				}
				
				$this->NerveCells = substr($NerveCellTemp, 0, -2);
			}
			else
			{
				$this->NerveCells = $NerveCells;
			}
		}
		else
		{
			if (empty($NerveCells))
			{
				$this->NerveCells = " * ";
			}
		}

		return parent::BaseRemember();
	}

	public function Chain ($Nerves, $Method = "INNER JOIN")
	{
		/**
		 * Set nerve
		 *
		 * @var        <string>
		 */
		$Sequential = $this->Nerve;

		/**
		 * Init chain
		 *
		 * @var        string
		 */
		$Chain = "";

		$Prime = "";

		foreach ($Nerves as $Nerve => $Similars) 
		{
			if ($this->Prime)
			{
				$Prime = $this->Prime;
			}
			else
			{
				$Prime = $Similars;
			}

			if (is_array($Similars))
			{
				/**
				 * Add chain
				 */
				
				foreach ($Similars as $Similar => $Constraint) 
				{
					if (empty($Chain))
					{
						if ($this->Prime)
						{
							$Prime = $this->Prime;
						}
						else
						{
							$Prime = $Constraint;
						}

						$Chain .= " {$Method} {$Nerve} ON {$Sequential}.$Prime = {$Nerve}.$Constraint ";
					}
					else
					{
						$Chain .= "AND {$Similar} = {$Constraint} ";
					}

				}
			}
			else
			{
				/**
				 * Add chain
				 */
				$Chain .= " {$Method} {$Nerve} ON {$Sequential}.$Prime = {$Nerve}.$Similars ";
			}
			

			/**
			 * update nerve
			 *
			 * @var        <string>
			 */
			$Sequential = $Nerve;
		}

		$this->NerveCells = "*";

		$this->Chain .= $Chain;

		return $this;
	}
	
	public function Sort ($Sorts)
	{
		/**
		 * Sort code
		 *
		 * @var        string
		 */
		$SortTemp = "";

		/**
		 * Count the number of sort element
		 */
		if (count($Sorts) > 0)
		{
			/**
			 * Set Sort = Order by
			 *
			 * @var        string
			 */
			$SortTemp = "ORDER BY ";

			foreach ($Sorts as $Column => $Type) 
			{
				if ($Type == "Rand")
				{
					$SortTemp .= "RAND(), ";	
				}
				else
				{
					$SortTemp .= "{$Column} {$Type}, ";	
				}
				
			}

			/**
			 * Split the comma at the end
			 *
			 * @var        callable
			 */
			$SortTemp = substr($SortTemp, 0, -2);
		}
		
		$this->Sort = $SortTemp;

		return $this;
	}
}