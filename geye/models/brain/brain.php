<?php

/**
 * @Author: Rot
 * @Date:   2017-09-15 18:20:06
 * @Last Modified by:   Rot
 * @Last Modified time: 2017-11-17 17:07:19
 */
namespace System\Models;

class Brain
{
    static $Cursor = array(\PDO::ATTR_CURSOR => \PDO::CURSOR_FWDONLY);

    static $Wake = NULL;

    public function __construct ()
    {
        self::Memory();
        
    }

    public static function Memory ()
    {
        $DNS = "mysql:host=" .DB_HOST_NAME . "; dbname=" . DB_DATABASE;

        $Connection = NULL;

        try
        {
            $Options = array (
                \PDO::ATTR_PERSISTENT => TRUE,
                \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION,
                \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC,
                \PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"
            );

            $Connection = new \PDO($DNS, DB_USER_NAME, DB_PASSWORD, $Options);
        }
        catch (\PDOExeption $e)
        {
            die($e->getMessage());
        }

        return $Connection;

    }
    
    public function Sleep ()
    {
        self::$Wake = NULL;
    }
    
    public function IsConnect ()
    {
        if (self::$Wake)
        {
            return true;
        }
        return false;
    }
    
    public function Write ($Illusion, $Data = NULL)
    {
        if (self::$Wake == NULL)
        {
            self::$Wake = self::Memory();
        }

        $Statement = NULL;

        try
        {
            if ($Data === NULL)
            {
                $Statement = self::$Wake->prepare($Illusion);
                $Statement->execute();
            }
            else
            {
                $Statement = self::$Wake->prepare($Illusion, self::$Cursor);
                $Statement->execute($Data);
            }

            return $Statement;
        }
        catch (\PDOException $e)
        {
            echo $e->getMessage();
        }

        return $Statement;
    }

    public static function Remember ($Illusion, $Data = NULL, $JustOne = false)
    {
       if (self::$Wake == NULL)
        {
            self::$Wake = self::Memory();
        }

        $Statement = NULL;

        try
        {
            if ($Data === NULL)
            {
                $Statement = self::$Wake->prepare($Illusion);
                $Statement->execute();
            }
            else
            {
                $Statement =self::$Wake->prepare($Illusion, self::$Cursor);
                
                $Statement->execute($Data);
            }

            if ($JustOne)
            {
                return $Statement->fetch();
            }

            return $Statement->FetchAll();
        }
        catch (\PDOException $e)
        {
            die($e->getMessage());
        }

        return $Statement;
    }
}

?>