<?php

namespace System\Models;

import(geye.models.brain);

interface Requirement {}

interface UserRequirement extends Requirement
{
	public static function User();
}

interface ViewRequirement extends Requirement
{
	public static function View();
}

interface Nullable extends Requirement 
{
	public static function Nullable();
}

interface InvokeData extends Requirement
{
	public static function Data();
}

interface Humanize extends Requirement {}

interface Uncallable extends Requirement {}

abstract class Routine
{
	protected $Fields;

	protected $Name;

	protected function ImplodeFields ()
	{
		$Store = self::ResolverFields();

		$Fields = array();

		foreach ($Store as $Field => $Values) 
		{
			array_push($Fields, $this->$Field);
		}

		return $Fields;
	}

	private function ResolverFields ()
	{
		/**
		 * Get attribute of models
		 *
		 * @var        callable
		 */
		$Fields = get_class_vars(get_called_class());

		/**
		 * If attribute field is empty
		 */
		if (empty($Fields["Fields"]))
		{
			unset($Fields["Fields"]);
		}

		/**
		 * Convert attribute field to attribute.
		 */
		if (isset($Fields["Fields"]))
		{
			$Subs = $Fields["Fields"];

			if (count($Subs) > 1)
			{
				foreach ($Subs as $Sub) 
				{
					$Fields[$Sub] = "";
				}
			}
			else
			{
				$Fields[$Subs[0]] = "";
			}

			unset($Fields["Fields"]);
		}

		unset($Fields["Name"]);

		return $Fields;
	}

}

abstract class Procedure extends Routine
{

	public function Call ($Parameters = null, $IsGet = true)
	{
		if ($Parameters == null)
		{
			$Parameters = $this->ImplodeFields();
		}

		if (!$Parameters || count($Parameters) == 0)
		{
			$Query = "CALL {$this->Name}()";
		}
		else
		{
			$Length    = count($Parameters);
			$Injection = "";

			for ($i = 0; $i < $Length; $i++)
			{
				$Injection .= "?, ";
			}

			$Injection = substr($Injection, 0, -2);

			$Query = "CALL {$this->Name}({$Injection})";
		}

		if ($IsGet == false)
		{
			return Brain::Write($Query, $Parameters);
		}
		return Brain::Remember($Query, $Parameters);
	}

}

abstract class Dynamic extends Routine
{
	public function Call ($Parameters = null, $IsGet = true)
	{
		if ($Parameters == null)
		{
			$Parameters = $this->ImplodeFields();
		}

		if (!$Parameters || count($Parameters) == 0)
		{
			$Query = "SELECT {$this->Name}() AS Result";
		}
		else
		{
			$Length    = count($Parameters);
			$Injection = "";

			for ($i = 0; $i < $Length; $i++)
			{
				$Injection .= "?, ";
			}

			$Injection = substr($Injection, 0, -2);

			$Query = "SELECT {$this->Name}({$Injection}) AS Result";

		}

		if ($IsGet == false)
		{
			return Brain::Write($Query, $Parameter);
		}

		return Brain::Remember($Query, $Parameters, true);
	}
}
?>