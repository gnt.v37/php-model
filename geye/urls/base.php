<?php

/**
 * @author: Rot
 * @Date:   2017-09-17 00:24:29
 * @Last Modified by:   Rot
 * @Last Modified time: 2017-09-23 20:23:42
 */

namespace System\Urls;

/**
 * @abstract BaseUrl Class
 * @usedby Route Class 
 * @property-read $ViewSpace, $FormSpace
 * $property-write $Urls, $Form, $App
 */
abstract class BaseUrl 
{
	const ViewSpace = "\\System\\Views\\";

	protected $URLLocal;
	
	/**
	 * Set attribute for variable
	 * 
	 * @param      <string>  $App       The application
	 * @param      <string>  $URLLocal  The url local
	 */
	public function __construct ($App, $URLLocal)
	{
		$this->App = $App;
		$this->URLLocal = $URLLocal;
	}

	/**
	 * Replace the app name. It use when you move from a app to order app.
	 * 
	 * @param      <string>  $Alias    The alias
	 * @param      <string>  $URLPage  The url page
	 *
	 * @return     <string>  ( &v=100 )
	 */
	protected function RegexURL ($Alias, $URLPage)
	{
		return preg_replace($Alias, "", $URLPage);
	}


}