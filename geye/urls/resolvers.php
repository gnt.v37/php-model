<?php

/**
 * @Author: Rot
 * @Date:   2017-09-15 18:20:06
 * @Last Modified by:   Rot
 * @Last Modified time: 2017-11-26 16:20:19
 */

namespace System\Urls;

/**
 * @include BaseUrl
 */
include_once geye."urls/base.php";

abstract class Resolver extends BaseUrl
{
	/**
	 * Compare url in link and url config
	 *
	 * @param      <array>  $URLLocal  The url local
	 * @param      <string>  $URLPage   The url page
	 *
	 * @return     <array>  ( /^$/, home )
	 */
	protected function URLEntity ($URLLocal, $URLPage)
	{
		foreach ($URLLocal as $Alias => $Entity) 
		{	
			if (preg_match($Alias, $URLPage))
			{
				return [$Alias, $Entity];	
			}
		}
		return null;
	}

	/**
	 * Getting view 
	 * 
	 * @param      <string>  $URLPage  The url page
	 * 
	 * @return     <array>  ( /^&/ => views\HomeView )
	 */
	protected function ResolverEntity ($URLPage)
	{
		/**
		 * Set
		 *
		 * @var        <array>
		 */
		$Urls = $this->URLLocal;

		/**
		 * Set
		 *
		 * @var        <string>
		 */
		$App = $this->App;

		/**
		 * Getting the matches url and its alias
		 *
		 * @var        <Funtion>
		 */
		$URLEntity = self::URLEntity($Urls, $URLPage);

		/**
		 * Offset 1 of URLEntity is file which has views inside
		 * @example views\ArticleView
		 * 
		 * @var        <string>
		 */
		$Entity = $URLEntity[1];

		/**
		 * We use while to get all urls file
		 */
		while (end_with($Entity, ".php"))
		{
			$File = "{$App}/{$Entity}";

			if (file_exists($File))
			{
				$App = $App . self::ResolverApp($Entity) . "/";

				include_once $File;
			}
			else
			{

				$App = self::ResolverApp($Entity);

				include_once $Entity;
			}

			$URLPage = $this->RegexURL($URLEntity[0], $URLPage);

			$URLEntity = self::URLEntity($Urls, $URLPage);

			$Entity = $URLEntity[1];

		}

		$this->App = $App;


		return [$Entity, $URLEntity[0], $URLPage];
	}

	private function ResolverApp ($App)
	{
		preg_match("/(?<App>.*?)\/\w+.php/", $App, $Matches);

		return $Matches["App"];
	}
}

?>