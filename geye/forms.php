<?php
namespace System\Forms;

include_once geye."forms/process.php";

abstract class Form extends ProcessForm
{
	private $Setted = [];

	private function Attribute ($Attribute, $Value)
	{
		if ($Value === "null")
		{
			$Value = null;
		}
		
		$this->$Attribute = $Value;

		$this->Setted[$Attribute] = "";

		return $Value;
	}

	public function SearchByCompact ($Compact, $Attribute, $Value = null)
	{
		/**
		 * If attribute is not set
		 */
		if (isset($this->Setted[$Compact]) == false)
		{
			$DataCompact = parent::Request($Compact);

			if ($DataCompact !== null)
			{
				self::Attribute($Compact, $DataCompact);

				return true;
			}

			$DataAttribute = parent::Request($Attribute, $Value);

			if ($DataAttribute !== null)
			{
				self::Attribute($Compact, $DataAttribute);

				return true;
			}

			$DataCompactURL = parent::URLParameter($Compact);


			if ($DataCompactURL !== null)
			{
				self::Attribute($Compact, $DataCompactURL);

				return true;
			}

			$DataAttributeURL = parent::URLParameter($Attribute);

			if ($DataAttributeURL !== null)
			{
				self::Attribute($Compact, $DataAttributeURL);

				return true;
			}

			if (isset($_SESSION[__SIGNIN__]))
			{
				$DataCompactSession = parent::Session($_SESSION[__SIGNIN__], $Compact);

				if ($DataCompactSession !== null)
				{
					self::Attribute($Compact, $DataCompactSession);

					return true;
				}

				$DataAttributeSession = parent::Session($_SESSION[__SIGNIN__], $Attribute);

				if ($DataAttributeSession !== null)
				{
					self::Attribute($Compact, $DataAttributeSession);

					return true;
				}
			}

			return false;

		}

		return true;
		
	}
	public function Search ($Attribute, $Value = null)
	{
		/**
		 * If attribute is not set
		 */
		if (isset($this->Setted[$Attribute]) == false)
		{
			/**
			 * Checking attribute request 
			 *
			 * @var        <string>
			 */
			$DataAttribute = parent::Request($Attribute, $Value);

			if ($DataAttribute !== null)
			{	
				return self::Attribute($Attribute, $DataAttribute);
			}
			


			/**
			 * Checking attribute parameter
			 *
			 * @var        <string>
			 */
			$DataAttributeURL = parent::URLParameter($Attribute);
			
			if ($DataAttributeURL !== null)
			{
				return self::Attribute($Attribute, $DataAttributeURL);
			}
			
			/**
			 * Session Attribute
			 */
			if (isset($_SESSION[__SIGNIN__]))
			{
				$DataAttributeSession = parent::Session($_SESSION[__SIGNIN__], $Attribute);

				if ($DataAttributeSession !== null)
				{
					return self::Attribute($Attribute, $DataAttributeSession);
				}
			}

			return false;
		}


		return true;
	}

}

?>