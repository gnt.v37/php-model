<?php

/**
 * @Author: Rot
 * @Date:   2017-09-15 20:16:05
 * @Last Modified by:   Rot
 * @Last Modified time: 2017-11-18 02:03:57
 */

namespace System;


class Registry
{
	/**
	 * Class Constructor
	 *
	 * @param      <type>  $Apps   The apps
	 *
	 */
	public function __construct($Apps)
	{
		$Regex = "/\/|\\\\/";
		
		foreach ($Apps as $App) 
		{
			if (preg_match($Regex, $App))
			{
				$SubApps = preg_split($Regex, $App);

				foreach ($SubApps as $SubApp) 
				{
					self::AppDefinition($SubApp);
				}
			}
			else
			{
				self::AppDefinition($App);
			}
		}
	}

	private function AppDefinition ($App)
	{
		$SourceApp = "{$App}/";

		if (defined($App))
		{
			if (constant($App) != $SourceApp)
			{
				die("We can't define {$App}. It already defined.");
			}
		}
		else
		{
			define($App, $SourceApp);
		}
	}


}


?>