<?php

/**
 * @Author: Rot
 * @Date:   2017-09-21 16:16:44
 * @Last Modified by:   Rot
 * @Last Modified time: 2017-11-23 01:38:42
 */
namespace System\Forms;

abstract class BaseForm
{
	/**
	 *
	 * @var        array
	 */

	/**
	 * Make it private becasue we don't want it show in get_class_vars
	 */
	private $RegexURL;

	private $URLParameter;

	/**
	 * { Resolve regex }
	 * 
	 * @uses       <url> 	 URLParameter	The url parameter
	 *
	 * @param      <string>  $RegexURL      The regular expression url
	 * @param      <string>  $URLParameter  The url parameter
	 */
	public function __construct ($RegexURL, $URLParameter)
	{
		$this->RegexURL = $RegexURL;

		$this->URLParameter = $URLParameter;
	}

	protected function Iteration ($List, $Attribute)
	{
		$Lower = strtolower($Attribute);

		foreach ($List as $Key => $Value) 
		{
			/**
			 * Compare Session Login Key and Form Attribute 
			 * Future check key and attribute compactions
			 */
			if (preg_match("/^" . $Key . "$/i", $Lower))
			{
				/**
				 * if key is set and it it empty, we will set it agin
		 		 * @var        Function
				 */
				return $Value;
			}
		}

		return null;
	}
	
	/**
	 * if we found session login, we will compare key of User and Form attribute.
	 * if it match we will set it.
	 *
	 * @param      <array>  $User       The user
	 * @param      <string>  $Attribute  The attribute
	 */
	protected function Session ($User, $Attribute)
	{
		return self::Iteration($User, $Attribute);
	}

	protected function URLParameter ($Attribute)
	{
		if ($this->RegexURL && $this->URLParameter)
		{
			preg_match($this->RegexURL, $this->URLParameter, $Matches);

			return self::Iteration($Matches, $Attribute);
		}

		return null;
		
	}

	/**
	 * Set value to attribute 
	 *
	 * @param      <string>  $Attribute  The attribute
	 * @param      <string>  $Value      The value
	 */
	protected function Request ($Attribute, $Value = null)
	{
		/**
		 * Check if attribute has request
		 *
		 * @var        boolean
		 */
		$IsRequest = false;

		$Data = $Value;
		
		/**
		 * Transform text
		 * Link in url has to lowercase
		 *
		 * @var        Function
		 */
		$LowerAttribute = strtolower($Attribute);

		/**
		 * if we found Post attribute matches form attribute we'll set value for this attribute.
		 * we accept normal attribute and lowercase attribute
		 * 
		 * @var        Function
		 */
		if (isset($_POST[$Attribute]))
		{
			/**
			 * Normal
			 */
			$Data = $_POST[$Attribute];

			$IsRequest = true;
		}
		else if (isset($_POST[$LowerAttribute]))
		{
			/**
			 * Lowercase
			 */
			$Data = $_POST[$LowerAttribute];
		}
		else if (isset($_GET[$Attribute]))
		{
			/**
			 * Normal
			 */
			$Data = $_GET[$Attribute];

			$IsRequest = true;
		}
		else if (isset($_GET[$LowerAttribute]))
		{
			/**
			 * Lowercase
			 */
			$Data = $_GET[$LowerAttribute];

			$IsRequest = true;
		}
		else if (isset($this->$Attribute) && empty($this->$Attribute) == false)
		{
			/**
			 * it mean there is nothing match above and the attribute was set in form.
			 * We doesn't set it agin
			 *
			 * @var        <mxin>
			 */
			
			$Data = null;
		}

		if ($IsRequest == false)
		{
			return null;
		}

		/**
		 * Remove source when get value from url query
		 */
		preg_match_all("/\/\w+/", $this->URLParameter, $Matches);

		foreach ($Matches[0] as $Match) 
		{
			$Data = str_replace($Match, "", $Data);
		}

		return $Data;
		
	}

	protected function GetRegexURL ()
	{
		return $this->RegexURL;
	}

	protected function GetURLParameter ()
	{
		return $this->URLParameter;
	}
	
}

?>