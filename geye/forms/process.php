<?php

/**
 * @Author: Rot
 * @Date:   2017-10-12 10:01:52
 * @Last Modified by:   Rot
 * @Last Modified time: 2017-12-28 00:24:35
 */
namespace System\Forms;

include_once geye."forms/base.php";

abstract class ProcessForm extends BaseForm
{
	/**
	 * Save attribute
	 * 
	 * @uses 	   models 
	 *
	 * @var        array
	 */
	protected $Attributes = [];
	
	/**
	 * { function_description }
	 *
	 * @param      <string>  $RegexURL      The regular expression url
	 * @param      <string>  $URLParameter  The url parameter
	 */
	public function __construct ($RegexURL, $URLParameter)
	{
		parent::__construct($RegexURL, $URLParameter);

		self::Attributes();

	}

	protected function Attributes ()
	{
		/**
		 * Resolve Attribute
		 */
		if (is_array($this->Attributes))
		{
			/**
			 * Incase mutiple fields
			 */
			foreach ($this->Attributes as $Mixin => $Var) 
			{
				/**
				 * If it dose have value
				 */
				if (is_numeric($Mixin))
				{
					if (isset($this->$Var))
					{
						die("{$Var} attribute difined once");
					}
					else
					{
						$this->$Var = null;
					}
						
				}
				else
				{
					if (isset($this->$Mixin))
					{
						die("{$Mixin} attribute difined once");
					}
					else
					{
						$this->$Mixin = $Var;
					}
					
				}
			}
		}
		else
		{
			/**
			 * if attribute is a string
			 */
			if (isset($this->$Attributes))
			{
				die("{$Attributes} attribute difined once");
			}
			else
			{
				$this->$Vars[$Attributes] = "";
			}
			
		}
	}
}

?>