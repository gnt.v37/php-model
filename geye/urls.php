<?php

namespace System\Urls;

include_once geye."urls/resolvers.php";

abstract class RequirementUrl extends Resolver
{
	private const Humanize = parent::ViewSpace."Humanize";

	private const UserRequirement = parent::ViewSpace."UserRequirement";

	protected function Requirement ($View)
	{
		$Humanize = false;

		$Implements = class_implements($View);

		foreach ($Implements as $Implement) 
		{
			switch ("\\".$Implement) 
			{
				case self::Humanize:
					$Humanize = true;
					break;
			}
		}

		return (object)["Humanize" => $Humanize];
	}
}

class Url extends RequirementUrl
{
	/**
	 *
	 * @param      <array>  $URLPage   The url page
	 * @param      <array>  $URLLocal  The url local
	 * @param      <string>  $Alias     The alias
	 * @param      <string>  $App       The application
	 */
	public function __construct ($URLPage, $URLLocal, $Alias, $App)
	{
		parent::__construct($App, $URLLocal);
		
		self::URLResolver($this->RegexURL($Alias, $URLPage));
	}

	/**
	 * Ivoking class view.
	 *
	 * @param      <string>  $URLPage  The url page
	 */
	private function URLResolver ($URLPage)
	{
		/**
		 * Getting entity
		 *
		 * @var        <string>
		 */
		$Entity = parent::ResolverEntity($URLPage);

		/**
		 * Getting class name
		 *
		 * @var        Function
		 */
		$View = str_replace("views/", parent::ViewSpace, $Entity[0]);

		/**
		 * Invoking class
		 *
		 * @var        $
		 */
		
		if (class_exists($View))
		{
			if (get_parent_class($View) == null)
			{
				new $View();
			}
			else if (is_subclass_of($View, parent::ViewSpace."Requirement"))
			{
				$Require = parent::Requirement($View);

				if ($Require["Humanize"])
				{
					new $View($this->App, $Entity[1], $Entity[2]);
				}
			}
			else
			{
				new $View($this->App, $Entity[1], $Entity[2]);
			}
			
		}
	}

}

?>