<?php

/**
 * @author: Rot
 * @Date:   2017-09-16 21:28:37
 * @Last Modified by:   Rot
 * @Last Modified time: 2017-12-18 23:35:34
 */

namespace System;

/**
 * @define geye 
 * 
 * @var string
 */
define("geye", "geye/");

/**
 * @define 	forms
 * 
 * @var 	string
 * 	
 */
define("forms", "forms.php");

/**
 * @define 	models
 * 
 * @var 	string
 * 	
 */
define("models", "models/");

/**
 * 
 * @define 	models.brain
 * 
 * @var 	string
 * 	
 */
define("brain", "brain/");

/**
 * @belong	models.brain
 * 
 * @define 	nerves
 * 
 * @var 	string
 * 	
 */
define("nerves", "nerves.php");

/**
 * @belong	models
 * 
 * @define 	routine
 * 
 * @var 	string
 * 	
 */
define("routine", "routine.php");

/**
 * @belong	models
 * 
 * @define 	schema
 * 
 * @var 	string
 * 	
 */
define("schema", "schema.php");

/**
 * @define 	modules
 * 
 * @var 	string
 * 	
 */
define("modules", "modules/");

/**
 * @belong	modules
 * 
 * @define 	imagick
 * 
 * @var 	string
 * 	
 */
define("imagick", "imagick.php");

/**
 * @belong	modules
 * 
 * @define 	msoffice
 * 
 * @var 	string
 * 	
 */
define("msoffice", "msoffice.php");

/**
 * @belong	modules
 * 
 * @define 	google
 * 
 * @var 	string
 * 	
 */
define("google", "google.php");

/**
 * @belong	modules
 * 
 * @define 	google
 * 
 * @var 	string
 * 	
 */
define("twitter", "twitter.php");

/**
 * @belong	modules
 * 
 * @define 	google
 * 
 * @var 	string
 * 	
 */
define("facebook", "facebook.php");

/**
 * @define 	urls
 * 
 * @var 	string
 * 	
 */
define("urls", "urls.php");

/**
 * @define 	views
 * 
 * @var 	string
 * 	
 */
define("views", "views/");

/**
 * @belong	views
 * 
 * @define 	smartif
 * 
 * @var 	string
 * 	
 */
define("smartif", "smartif.php");

/**
 * @belong	views
 * 
 * @define 	generic
 * 
 * @var 	string
 * 	
 */
define("generic", "generic/");

/**
 * 
 * @define 	base
 * 
 * @var 	string
 * 	
 */
define("base", "base.php");

/**
 * 
 * @define 	base
 * 
 * @var 	string
 * 	
 */
define("deeps", "deeps.php");

/**
 * 
 * @define 	static
 * 
 * @uses 	template
 * 
 * @var 	string
 * 	
 */
define("humanize", "humanize/");

define("scripts", "static/scripts/");

define("styles", "static/contents/");

define("images", "static/images/");

/**
 * @requried func becasue it has all of user defined function. 
 */
include_once geye."utils/func.php";

include_once geye."apps.php";

/**
 * Defination apps
 * @var class
 * @example you can use echo home. it needn't blockqoute
 */

new App($Apps);

?>