<div>
	<div class="ge-container">
		<div class="ge-top">
			<div class="ge-row">
				<div class="ge-left">
					<div class="ge-relative">
						<div class="ge-info-ext">
							<ul>
								<li><div><h5>About your practice</h5>
									<div class="ge-finish">You finished <?= $this->AboutPractice[0] ?> practices</div>
									<?php if ($this->AboutPractice[1]): ?>
										<div>You haven't finished <?= $this->AboutPractice[1] ?> practices</div>
									<?php endif ?>
								</div></li>			
								<li class="ge-score-chart"><div><h5>Your latest score chart</h5></div>
									<div class="ge-score-container">
										<canvas ge-chart="true"></canvas>
									</div>
									<input type="hidden" value='<?= $this->ScoreChart ?>' name="">
								</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="ge-right">
					<div class="ge-mixin-content">
						<div>
							<div class="ge-des">
								<h5>Latest articles and practices</h5>
							</div>
						</div>
						<div>
							<ul ge-timeline="true"><?php include_once "mixin.php"; ?></ul>
						</div>
					</div>
				</div>
			</div>
					
		</div>
	</div>
</div>					