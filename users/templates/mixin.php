<?php foreach ($this->TimeLines as $Mixin): ?>

<li class="ge-mixin-item"><div class="ge-space"><div class="ge-row ge-margin">
	<div class="ge-left">
		<ge-user class="ge-user-timeline ge-flex-cc"><div class="ge-inline">
			<a href="<?= page."@{$Mixin["user_id"]}"; ?>">
				<div class="ge-relative ">
					<?php if ($Mixin["image_local"]): ?>
						<img ge-image="true" data-src="<?= users.images."64/".$Mixin["image_local"] ?>">
					<?php else: ?>
						<img data-name="<?= $Mixin["lastname"] ?>" ge-user-avatar="true" data-src="<?= $Mixin["image_link"] ?>">
					<?php endif ?>
					<div class="ge-avatar-radius">
						<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 70 70">
							<path d="M5.53538374,19.9430227 C11.180401,8.78497536 22.6271155,1.6 35.3571429,1.6 C48.0871702,1.6 59.5338847,8.78497536 65.178902,19.9430227 L66.2496695,19.401306 C60.4023065,7.84329843 48.5440457,0.4 35.3571429,0.4 C22.17024,0.4 10.3119792,7.84329843 4.46461626,19.401306 L5.53538374,19.9430227 Z"></path><path d="M65.178902,49.9077131 C59.5338847,61.0657604 48.0871702,68.2507358 35.3571429,68.2507358 C22.6271155,68.2507358 11.180401,61.0657604 5.53538374,49.9077131 L4.46461626,50.4494298 C10.3119792,62.0074373 22.17024,69.4507358 35.3571429,69.4507358 C48.5440457,69.4507358 60.4023065,62.0074373 66.2496695,50.4494298 L65.178902,49.9077131 Z"></path></svg>
			</div></div></a></div>
			<div class="ge-inline ge-left-12">
				<div><a href="<?= page."@{$Mixin["user_id"]}"; ?>" prevent="true" class="ge-name"><?php echo "{$Mixin["lastname"]} {$Mixin["firstname"]}"; ?></a></div>
				<div class="ge-time"><time datetime="<?= $Mixin["created_at"] ?>"></time></div>
			</div></ge-user>
	</div>
	<div class="ge-right">
		<button class="ge-article-option" type=""><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width=16 height=16 viewBox="0 0 60 60" xml:space="preserve"><g><path d="M8,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S12.411,22,8,22z"/><path d="M52,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S56.411,22,52,22z"/><path d="M30,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S34.411,22,30,22z"/></g></svg></button>
	</div></div><div><div>
	<div class="ge-row"><ge-content><ul><li class="ge-relative">
		<div class=""><div><h4 class="article-title"><a href="view?v=<?php echo $Mixin["article_id"] ?>" class="
			<?php 
				$Height = "height-mode-z"; 
				$IsShow = true;
				$TitleLength = strlen($Mixin["title"]);
				if (false) 
				{
					$Height = "height-mode-o";
					if ($TitleLength >= 39)
						$IsShow = false;
				}
				else 
				{
					if ($TitleLength >= 39 && $TitleLength < 74)
						$Height = "height-mode-tw";
					else if ($TitleLength >= 74)
						$Height = "height-mode-th";
				}	
			?>"><?= $Mixin["title"] ?></a>
			</h4></div>
	<?php if ($Mixin["category"] == 1): ?>
		<div><ge-image>
				<a href="view?v=<?= $Mixin["article_id"] ?>" prevent="true" class="ge-scale"><ge-real class="ge-bk-img ge-hover" data-src="<?= home.images."300/{$Mixin["image"]}"; ?>"></ge-real></a>
			</ge-image></div>
		<div class="article-description <?= $Height ?>">
			<span><?php if ($IsShow) echo $Mixin["description"] ?></span>	
		</div>
		</div></li>
	</ul>
	<?php else: ?>
		<div><ul>
			<li><div>
				<h5 class="ge-times ge-text-color"><a href="<?= page."summary/@{$Mixin["user_id"]}/{$Mixin["article_id"]}/{$Mixin["times"]}" ?>"><span><?= $Mixin["times"] ?></span> <span>times</span></a></h5>
			</div></li>
			<li class="ge-row">
				<div class="ge"><h5 class="ge-color"><a href="<?= page."summary/@{$Mixin["user_id"]}/{$Mixin["article_id"]}/{$Mixin["times"]}" ?>">
					<?php if ($Mixin["finish_time"]): ?>
					<span>You finished about</span> <time datetime="<?= $Mixin["finish_time"] ?>"></time>
					<span>with</span> <?php endif ?><span ge-score="true"><?= $Mixin["score"] ?> <span>scores</span> </span>
				</a></h5>
			</div>
		</li>
		</ul></div>
	<?php endif ?>
	</ge-content></div></div></div></div>
</li>

<?php endforeach ?>