<div class="ge-border ge-relative"><div class="ge-header-container"><div><div class="ge-row"><div class="ge-col l6">
<div><ul class="ge-user-info"><li>
	<h2 class=""><span ge-user-lastname="true" role="textbox"><?= $this->User["lastname"] ?></span> <span ge-user-firstname="true" role="textbox"><?= $this->User["firstname"] ?></span></h2>
	<div class="ge-color ge-join">
		<span>Geye member since</span>
		<span><time datetime="<?= $this->User["created_at"] ?>"></time></span>
	</div>
</li><li><div class="ge-right12"><h5 class="ge-weight400" ge-user-description="true" role="textbox"><?= $this->User["description"] ?></h5></div></li>
<li class="ge-verticle8"></li></ul></div></div>
<div class="ge-col l6"><center><div class="ge-top-16"><div class="ge-relative ge-inline">
<?php if ($this->User["image_local"]): ?>
	<img ge-user-avatar="true" class="ge-image-width" data-src="<?= $App."/".images."158/".$this->User["image_local"] ?>"  alt="">
<?php else: ?>
	<img ge-user-avatar="true" class="ge-image-width" data-name="<?= $this->User["lastname"] ?>" data-src="<?= $this->User["image_link"] ?>">
<?php endif ?>
	<div class="ge-image-wrapper">
		<svg viewBox="0 0 114 114" xmlns="http://www.w3.org/2000/svg"><path d="M7.66922967,32.092726 C17.0070768,13.6353618 35.9421928,1.75 57,1.75 C78.0578072,1.75 96.9929232,13.6353618 106.33077,32.092726 L107.66923,31.4155801 C98.0784505,12.4582656 78.6289015,0.25 57,0.25 C35.3710985,0.25 15.9215495,12.4582656 6.33077033,31.4155801 L7.66922967,32.092726 Z"></path><path d="M106.33077,81.661427 C96.9929232,100.118791 78.0578072,112.004153 57,112.004153 C35.9421928,112.004153 17.0070768,100.118791 7.66922967,81.661427 L6.33077033,82.338573 C15.9215495,101.295887 35.3710985,113.504153 57,113.504153 C78.6289015,113.504153 98.0784505,101.295887 107.66923,82.338573 L106.33077,81.661427 Z"></path></svg>
</div><div class="ge-pick-image"><div class="ge-flex-cc"><button ge-user-button-image="true"><span>
	<svg width="65" height="65" viewBox="0 0 65 65"><g fill-rule="evenodd"><path d="M10.61 44.486V23.418c0-2.798 2.198-4.757 5.052-4.757h6.405c1.142-1.915 2.123-5.161 3.055-5.138L40.28 13.5c.79 0 1.971 3.4 3.073 5.14 0 .2 6.51 0 6.51 0 2.856 0 5.136 1.965 5.136 4.757V44.47c-.006 2.803-2.28 4.997-5.137 4.997h-34.2c-2.854.018-5.052-2.184-5.052-4.981zm5.674-23.261c-1.635 0-3.063 1.406-3.063 3.016v19.764c0 1.607 1.428 2.947 3.063 2.947H49.4c1.632 0 2.987-1.355 2.987-2.957v-19.76c0-1.609-1.357-3.016-2.987-3.016h-7.898c-.627-1.625-1.909-4.937-2.28-5.148 0 0-13.19.018-13.055 0-.554.276-2.272 5.143-2.272 5.143l-7.611.01z"></path><path d="M32.653 41.727c-5.06 0-9.108-3.986-9.108-8.975 0-4.98 4.047-8.966 9.108-8.966 5.057 0 9.107 3.985 9.107 8.969 0 4.988-4.047 8.974-9.107 8.974v-.002zm0-15.635c-3.674 0-6.763 3.042-6.763 6.66 0 3.62 3.089 6.668 6.763 6.668 3.673 0 6.762-3.047 6.762-6.665 0-3.616-3.088-6.665-6.762-6.665v.002z"></path></g></svg></span></button></div></div></div></center></div></div></div>
<div class="ge-row">
	<ul class="ge-left ">
		<li class="ge-inline ge-right8"><a class="ge-height37 ge-flex-cc"><strong class="ge-right-4"><?= $this->Following ?></strong> <span>Follwing</span></a></li>
		<li class="ge-inline ge-horizotal8"><a class="ge-height37 ge-flex-cc"><strong class="ge-right-4 ge-follower"><?= $this->Followers ?></strong> <span>Followers</span></a></li></ul>
	<span class="ge-left ge-divider ge-flex-cc"></span>
	<ul class="ge-left ge-social-icon">
	<?php if ($this->User["instagram"]): ?>
		<li class="ge-inline ge-mright16">
			<a target="blank" href="<?= $this->User["instagram"] ?>" class="ge-height37 ge-flex-cc"><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="21" height="21" viewBox="0 0 512 512" xml:space="preserve">
				<path style="fill:#FDE494;" d="M114.759,512h61.103l-96.414-61.793L0.198,403.898C3.659,464.09,53.716,512,114.759,512z"/><path style="fill:#FEA150;" d="M335.448,476.69l-69.006-17.655H114.759c-34.072,0-61.793-27.721-61.793-61.793v-33.876 l-26.483-28.42L0,339.628v57.613c0,2.236,0.072,4.454,0.198,6.657L175.862,512h208.767L335.448,476.69z"/><path style="fill:#FF5D4A;" d="M441.655,440.14c-11.244,11.637-26.993,18.894-44.414,18.894H266.442L384.629,512h12.612 c41.202,0,77.396-21.829,97.64-54.527l-27.022-16.094L441.655,440.14z"/><path style="fill:#E45261;" d="M459.034,326.014v71.228c0,16.652-6.633,31.775-17.379,42.899l53.227,17.333 C505.728,439.954,512,419.318,512,397.241v-62.805l-27.396-15.391L459.034,326.014z"/><polygon style="fill:#FF4D95;" points="512,149.19 483.797,142.474 459.034,157.3 459.034,326.014 512,334.436 "/><path style="fill:#CB319C;" d="M512,114.759c0-57.633-42.708-105.473-98.136-113.55L344.43,30.101l-35.183,22.865h87.994 c34.072,0,61.793,27.721,61.793,61.793V157.3L512,149.19V114.759z"/>
				<path style="fill:#8A3293;" d="M317.732,0l-65.682,24.636l-51.805,28.33h109.002L413.864,1.208C408.435,0.417,402.887,0,397.241,0 H317.732z"/><path style="fill:#FF5D4A;" d="M256,406.069c18.358,0,35.954-3.32,52.226-9.38l-86.02-39.047l-91.178-18.657 C157.946,379.39,203.913,406.069,256,406.069z"/><path style="fill:#E45261;" d="M329.153,305.358c-15.883,23.465-42.748,38.918-73.153,38.918c-40.273,0-74.308-27.118-84.867-64.046
				l-23.682-14.801l-40.847,4.538c2.353,25.345,11.014,48.887,24.425,69.017l177.198,57.705c38.303-14.264,69.237-43.757,85.458-81.068 l-31.753-16.085L329.153,305.358z"/>
				<g><path style="fill:#FF4D95;" d="M167.724,256c0-21.878,8.018-41.907,21.247-57.346l-37.658-5.268l-38.25,16.892 c-4.625,14.422-7.132,29.784-7.132,45.722c0,4.712,0.244,9.365,0.671,13.966l64.53,10.262 C168.929,272.524,167.724,264.403,167.724,256z"/>
				<path style="fill:#FF4D95;" d="M406.069,256c0-32.138-10.159-61.946-27.428-86.39l-37.397-5.308l-38.418,16.917 c24.873,15.631,41.45,43.298,41.45,74.781c0,18.27-5.58,35.261-15.123,49.358l64.531,10.262 C401.634,297.334,406.069,277.18,406.069,256z"/></g><g>
				<path style="fill:#CB319C;" d="M256,167.724c17.194,0,33.242,4.959,46.826,13.495l75.815-11.609 c-27.196-38.493-72.03-63.679-122.641-63.679c-66.81,0-123.554,43.889-142.937,104.345l75.908-11.624 C205.173,179.742,229.203,167.724,256,167.724z"/><path style="fill:#CB319C;" d="M397.241,150.069c19.47,0,35.31-15.84,35.31-35.31s-15.84-35.31-35.31-35.31 c-19.47,0-35.31,15.84-35.31,35.31S377.771,150.069,397.241,150.069z"/></g>
				<polygon style="fill:#FF5D4A;" points="52.966,313.564 27.47,300.847 0,296.316 0,339.629 52.966,363.366 "/><polygon style="fill:#E45261;" points="0,253.014 0,296.316 52.966,313.564 52.966,261.437 25.446,251.543 "/><polygon style="fill:#FF4D95;" points="52.966,219.479 25.749,219.233 0,227.59 0,253.014 52.966,261.437 "/><polygon style="fill:#CB319C;" points="52.966,179.757 24.911,182.603 0,205.962 0,227.59 52.966,219.479 "/><polygon style="fill:#8A3293;" points="0,205.962 52.966,179.757 52.966,119.362 21.9,122.333 0,143.241 "/><path style="fill:#523494;" d="M205.059,0L84.206,46.481L1.387,96.928C0.477,102.741,0,108.695,0,114.759v28.482l52.966-23.878 v-4.605c0-34.072,27.721-61.793,61.793-61.793h85.487L317.732,0H205.059z"/><path style="fill:#2D2D87;" d="M114.759,0C57.545,0,9.978,42.088,1.387,96.928L205.059,0H114.759z"/>
		</svg></a></li>
	<?php endif ?>
	<?php if ($this->User["twitter"]): ?>
		<li class="ge-inline"><a target="blank" href="<?= $this->User["twitter"] ?>">
			<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="21" height="21" viewBox="0 0 511.537 511.537" xml:space="preserve">
				<path style="fill:#65BBF2;" d="M357.038,49.172c-59.284,0.898-105.993,52.098-105.993,111.382v14.372 C145.052,156.063,92.954,127.319,35.466,67.137c-34.133,66.47,3.593,122.161,44.912,152.702c-27.846,0-51.2-3.593-69.165-19.761 c-1.796-0.898-3.593,0-2.695,1.797c15.27,55.691,67.368,96.112,107.789,107.789c-36.828,0-61.081,5.389-87.13-10.779 c-1.796-0.898-3.593,0-2.695,1.796c19.761,54.793,59.284,71.86,116.772,71.86c-28.744,21.558-67.368,43.116-140.126,44.912 c-2.695,0-4.491,3.593-1.796,5.389c26.947,22.456,93.418,39.523,186.835,39.523c153.6,0,278.456-136.533,278.456-305.404v-8.982 c24.253-8.982,37.726-30.54,44.912-52.098c0-0.898-0.898-1.796-1.797-1.796l-51.2,17.965c-0.898,0-1.796-1.796-0.898-2.695 C479.2,92.288,495.368,70.73,502.554,50.07c0,0-0.898-0.898-1.797-0.898c-24.253,9.881-47.607,19.761-65.572,25.151 c-2.695,0.898-6.288,0.898-8.982-0.898C414.526,67.137,379.494,49.172,357.038,49.172"/>
			</a></li>
	<?php endif ?>
</ul></div>
<?php if (isset($_SESSION[__SIGNIN__]) && $_SESSION[__SIGNIN__]["userid"] != $this->User["user_id"]) : ?>
<div class="">
	<?php if ($this->UserFollow["Total"]): ?>
		<button type="" class="ge-follow ge-following">Following</button>
	<?php else: ?>
		<button type="" class="ge-follow">Follow</button>
	<?php endif ?>
</div>	
<?php endif ?>

<nav class="ge-row ge-top16"><div class="ge-left  ge-item-center">
<ul><li class="ge-left ge-fstyle ge-active"><div><a class="ge-button ge-color"><strong>Timeline</strong></a></div></li>
<li class="ge-left ge-fstyle"><div><a class="ge-button">Activity Log</a></div></li>
</ul></div><div class="ge-right">
<ul>
	<?php if (isset($_SESSION[__SIGNIN__])): $User = $_SESSION[__SIGNIN__] ?>
		<?php if ($this->User["user_id"] == $User["userid"]): ?>
			<li class="ge-left ge-fstyle"><a class="ge-button" ge-user-update="false" ge-user-edit="true">Edit</a></li>
		<?php endif ?>
	<?php endif ?>
</ul></div></nav></svg></div><input type="hidden" data-user value="<?= $this->User["user_id"] ?>">
<model ge-user-model="true"><div class="ge-user-model"><div>
	<model-header><div class="ge-model-text"><h4>Write a image link</h4></div></model-header>
	<model-main><div><input type="text" class="ge-input" placeholder="https://static.zerochan.net/Konan.full.923934.jpg" name=""></div></model-main>
	<model-footer><div class="ge-row"><div class="ge-right ge-ver-4"><div class="ge-left ge-right-4"><button type="" ge-submit="true" class="ge-color">Submit</button></div><div class="ge-right"><button type="" ge-cancel="true">Cancel</button></div></div></div></model-footer></div></div></model>
</div>