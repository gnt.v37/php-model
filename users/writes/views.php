<?php

/**
 * @Author: Rot
 * @Date:   2017-09-26 21:48:22
 * @Last Modified by:   Rot
 * @Last Modified time: 2017-12-17 09:59:43
 */

namespace System\Views;

import(geye.views.generic);

class DescriptionEditView extends GenericView
{
	protected $Models = ["ArticleDescription", "DraftArticle", "Article"];

	protected $Scripts = [assets.scripts.modules => ["mathjax/MathJax.js?config=AM_HTMLorMML-full"]];

	public function Resolver ()
	{
		$this->DraftWrite = $this->Deeps->GetArticleDescription();

		$this->ArticleID = $this->Deeps->GetArticleID();

		$this->Publish = $this->Deeps->GetPubish();

		$this->IsEdit = $this->Deeps->GetIsEdit();
	}

}

class PublishObject extends ObjectView
{
	protected $Models = "ArticlePublish";

	protected function Resolver ()
	{
		echo json_encode($this->Deeps->ArticlePublish);
	}
}

class ScheduleObject extends ObjectView
{
	protected $Models = "ArticleSchedule";

	protected function Resolver ()
	{
		echo json_encode($this->Deeps->ArticleSchedule);
	}
}

class SaveTitleObject extends ObjectView
{
	protected $Models = "SaveTitle";

	protected function Resolver ()
	{
		echo json_encode($this->Deeps->GetTitle());
	}
}


class SaveDescriptionObject extends ObjectView
{
	protected $Models = "SaveDescription";
}

class SaveDescriptionDetailObject extends ObjectView
{
	protected $Models = "SaveDescriptionDetail";

	protected function Resolver ()
	{
		echo json_encode($this->Deeps->GetDescription());
	}
}

class DeleteImageObject extends ObjectView
{
	protected $Models = ["DeleteWriteImage", "CheckImage", "DeleteWriteHomeImage"];

	protected function Resolver ()
	{
		echo json_encode($this->Deeps->DeleteImage());
	}
}

class CloneImageObject extends ObjectView
{
	protected $Models = ["SaveWriteHomeImage", "CheckImage"];

	public function Resolver ()
	{
		echo json_encode($this->Deeps->CloneImage());
	}
	
}

class BlurImageObject extends ObjectView
{
	protected $Models = "SaveWriteImage";

	public function Resolver ()
	{
		echo json_encode($this->Deeps->BlurImage());
	}
}

class DeleteArticleObject extends ObjectView
{
	protected $Models = "DeleteWriteArticle";

	public function Resolver ()
	{
		echo json_encode($this->Deeps->DeleteWriteArticle);
	}
}

class ShortcutView extends TemplateView
{
	protected $Templates = "shortcut.php";
}

?>