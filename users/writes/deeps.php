<?php

/**
 * @Author: Rot
 * @Date:   2017-09-26 21:48:42
 * @Last Modified by:   Rot
 * @Last Modified time: 2017-12-08 17:56:56
 */

namespace System\Deeps;

import(geye.modules.imagick);

use System\Forms\DescriptionForm;

class DescriptionDeep
{
	public function GetArticleDescription ()
	{
		return isset($this->ArticleDescription[0]) ? $this->ArticleDescription[0] : null;
	}

	public function CloneImage ()
	{
		$Magic = new \Imagick();

		$Form = $this->DescriptionForm;

		$Check = $this->CheckImage->Call([$Form->UserID, $Form->ArticleID, $Form->IsEdit]);

		$URLImage = $Form->Search("URLImage");

		if ($Check["Result"] == 0)
		{
			$Name = md5(mktime());

			$Link = page.home.images;

			$ImageName = $Magic->Resize(home.images."300", $URLImage, $Name, null, 300);

			$Magic->Resize(home.images."280", $Link."300/{$ImageName}", $Name, null, 280);

			$Magic->Resize(home.images."100", $Link."280/{$ImageName}", $Name, null, 100);

			$Magic->Resize(home.images."60", $Link."100/{$ImageName}", $Name, null, 60);

			$this->SaveWriteHomeImage->Call([$ImageName, $Form->UserID, $Form->ArticleID, $Form->IsEdit]);
		}

		$File = $Magic->Clone(root.users.writes.images, $URLImage);

		$Size = getimagesize($File);

		return ["Width" => $Size[0], "Height" => $Size[1], "Mime" => $Size["mime"], "Name" => str_replace(root, "", $File)];
	}

	public function BlurImage()
	{
		$Magic = new \Imagick();

		$Form = $this->DescriptionForm;

		$Form->Search("ImageBase64");

		$Form->Search("URLImage");

		$Form->Search("Mime");

		$File = $Magic->Image(
			root.articles.images, 
			$Form->ImageBase64, 
			$Form->Mime
		);

		$Magic->Destroy($this->DescriptionForm->URLImage);

		$File = str_replace(root, "", $File);

		$ImageID = $this->SaveWriteImage->Call([$File, $Form->UserID, $Form->ArticleID, $Form->IsEdit]);

		return ["Name" => $File, "ID" => $ImageID[0]["image"]];
	}

	public function DeleteImage ()
	{
		$Magic = new \Imagick();

		$Form = $this->DescriptionForm;

		$Check = $this->CheckImage->Call([$Form->UserID, $Form->ArticleID, $Form->IsEdit]);

		if ($Check["Result"] == 1)
		{
			$Result = $this->DeleteWriteHomeImage->Call([$Form->UserID, $Form->ArticleID, $Form->IsEdit]);

			$Image = $Result[0]["image"];

			if ($Image && is_dir($Image) == false)
			{
				$Magic->Destroy(home.images."60/{$Image}");
				$Magic->Destroy(home.images."100/{$Image}");
				$Magic->Destroy(home.images."280/{$Image}");
				$Magic->Destroy(home.images."300/{$Image}");
			}
		}

		$Result = $this->DeleteWriteImage->Call([$Form->ImageID, $Form->UserID, $Form->ArticleID, $Form->IsEdit]);

		$Image = $Result[0]["image"];

		if ($Image && is_dir($Image) == false)
		{
			$Magic->Destroy($Image);

			return ["Result" => 1];
		}

		return ["Result" => 0];
	}

	public function GetTitle ()
	{
		return $this->SaveTitle;
	}

	public function GetDescription ()
	{
		return $this->SaveDescriptionDetail;
	}

	public function GetArticleID ()
	{
		$Form = $this->DescriptionForm;

		if ($Form->IsEdit)
		{
			$Count = $this->Article->Count(["article_id" => $Form->ArticleID, "user_id" => $Form->UserID]);

			if ($Count["Total"])
			{
				return $Form->ArticleID;
			}
		
		}
		else
		{
			$Count = $this->DraftArticle->Count(["draft_article_id" => $Form->ArticleID, "user_id" => $Form->UserID]);

			if ($Count["Total"])
			{
				return $Form->ArticleID;
			}
		}

		if ($Form->ArticleID == null)
		{
			return null;
		}

		return ["Permission" => 403];
	}

	public function GetPubish ()
	{
		$Form = $this->DescriptionForm;

		if ($Form->IsEdit == false)
		{
			return $this->DraftArticle->Remember(["draft_article_id" => $Form->ArticleID, "user_id" => $Form->UserID], true)
				->Get("publish_at");
		}
		else
		{
			return null;
		}
	}

	public function GetIsEdit ()
	{
		return $this->DescriptionForm->IsEdit;
	}
}

?>