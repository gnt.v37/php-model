<?php

/**
 * @Author: Rot
 * @Date:   2017-12-07 10:06:39
 * @Last Modified by:   Rot
 * @Last Modified time: 2017-12-15 21:32:23
 */

namespace System\Deeps;

use System\Models\Schema;

class SettingDeep
{
	public function GetArticleChapters ()
	{
		$Form = $this->ChapterForm;

		if ($Form->IsEdit)
		{
			return $this->ArticleChapter->Chain(["chapters" => "chapter_id"])
				->Remember(["article_id" => $Form->ArticleID])->Sort(["article_chapters.created_at" => Schema::Decrease])->Get("chapters.*");
		}
		else
		{
			return $this->DraftArticleChapter->Chain(["chapters" => "chapter_id"])
				->Remember(["draft_article_id" => $Form->ArticleID])->Sort(["draft_article_chapters.created_at" => Schema::Decrease])->Get("chapters.*");
		}
	}

	public function GetQuestions ()
	{
		$Form = $this->ChapterForm;

		if ($Form->IsEdit)
		{
			return $this->ArticleQuestion
				->Chain(["questions" => "question_id"])
				->Chain(["question_chapters" => ["question_id", "question_chapters.chapter_id" => $Form->ChapterID]], Schema::Left)
				->Chain(["article_chapters" => [
					"article_id", 
					"article_chapters.chapter_id" => "question_chapters.chapter_id",
				]], Schema::Left)
				->Remember([
					"article_questions.article_id" => $Form->ArticleID, 
				])->Get(["checked" => "article_chapters.chapter_id", "questions.question_id", "question", "title"]);
		}
		else
		{
			return $this->DraftArticleQuestion
				->Chain(["questions" => "question_id"])
				->Chain(["question_chapters" => ["question_id", "question_chapters.chapter_id" => $Form->ChapterID]], Schema::Left)
				->Chain(["draft_article_chapters" => [
					"draft_article_id", 
					"draft_article_chapters.chapter_id" => "question_chapters.chapter_id",
				]], Schema::Left)
				->Remember([
					"draft_article_questions.draft_article_id" => $Form->ArticleID, 
				])->Get(["checked" => "draft_article_chapters.chapter_id", "questions.question_id", "question"]);
		}
	}

	public function SaveChapterQuestions ()
	{
		$Form = $this->ChapterForm;

		$Total = $this->QuestionChapter->Count(["question_id" => $Form->QuestionID, "chapter_id" => $Form->ChapterID]);

		if ($Total["Total"] == 0)
		{
			$this->QuestionChapter->Write(["chapter_id" => $Form->ChapterID, "question_id" => $Form->QuestionID]);
		}
		else
		{
			$this->QuestionChapter->Forget(["chapter_id" => $Form->ChapterID, "question_id" => $Form->QuestionID]);
		}
	}

	public function GetArticleID ()
	{
		$Form = $this->ChapterForm;

		if ($Form->IsEdit)
		{
			$Count = $this->Article->Count(["article_id" => $Form->ArticleID, "user_id" => $Form->UserID]);

			if ($Count["Total"])
			{
				return $Form->ArticleID;
			}
		
		}
		else
		{
			$Count = $this->DraftArticle->Count(["draft_article_id" => $Form->ArticleID, "user_id" => $Form->UserID]);

			if ($Count["Total"])
			{
				return $Form->ArticleID;
			}
		}

		if ($Form->ArticleID == null)
		{
			return null;
		}

		return ["Permission" => 403];

	}

	public function GetIsEdit ()
	{
		return $this->ChapterForm->IsEdit;
	}

	public function GetPubish ()
	{
		$Form = $this->ChapterForm;

		if ($Form->IsEdit == false)
		{
			return $this->DraftArticle->Remember(["draft_article_id" => $Form->ArticleID, "user_id" => $Form->UserID], true)
				->Get("publish_at");
		}
		else
		{
			return null;
		}
	}
}

?>