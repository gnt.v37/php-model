<?php

/**
 * @Author: Rot
 * @Date:   2017-12-07 10:06:06
 * @Last Modified by:   Rot
 * @Last Modified time: 2017-12-08 23:12:45
 */

namespace System\Views;

import(geye.views.generic);

class ChapterView extends GenericView
{
	protected $Styles = [users.writes.styles => ["writer.css", "modules/material-datetime-picker.css"]];

	protected $Scripts = [users.writes.scripts => 
		["commons.js", "writer.js", "modules/moment.js", "modules/rome.standalone.js", "modules/zmaterial-datetime-picker.js"],

	];

	public function Resolver ()
	{
		$this->ArticleID = $this->Deeps->GetArticleID();

		$this->Publish = $this->Deeps->GetPubish();

		$this->IsEdit = $this->Deeps->GetIsEdit();

		$this->ArticleChapters = $this->Deeps->GetArticleChapters();
	}
}

class GetQuestionsObject extends ObjectView 
{
	protected $Models = ["ArticleQuestion", "DraftArticleQuestion"];

	public function Resolver ()
	{
		echo json_encode($this->Deeps->GetQuestions());
	}
}

class SaveChapterObject extends ObjectView
{
	protected $Models = "SaveChapter";

	public function Resolver ()
	{
		echo json_encode($this->Deeps->SaveChapter);
	}
}

class SaveChapterQuestionsObject extends ObjectView
{
	protected $Models = "QuestionChapter";

	public function Resolver ()
	{
		$this->Deeps->SaveChapterQuestions();
	}
}

class DeleteChapterObject extends ObjectView
{
	protected $Models = "DeleteChapter";
}

?>