<div class="ge-top-32"><div class="ge-main-width ge-relative"><div>
<div><button class="ge-button-icon" ge-new-chapter="true">
	<span class="ge-icon"><span class="ge-relative">C</span></span><span class="ge-new-text">Add a new chapter</span></button>
</div><div>

<?php if ($this->ArticleChapters): ?>
	<?php foreach ($this->ArticleChapters as $ArticleChapter): $ChapterID = $ArticleChapter["chapter_id"] ?>

	<section class="ge-section"><div><div class="ge-space"><ul>
		<li><div><h4 class="ge-chapter-title ge-text" data="chapter-title">
			<div contenteditable="true" role="textbox" class="ge-paragraph" ge-chapter="true" data-model="chapter" data-chapter="<?= $ChapterID ?>"><?= $ArticleChapter["chapter"] ?></div>
		</h4></div></li>
		<li class="ge-show ge-chapter">
			<div class="ge-flex ge-center">
				<div class="ge-inline ge-right-24">
					<span class="ge-guide ge-width158">Show chapter</span>
				</div>
				<div class="ge-inline">
					<ul class="ge-top-6">
						<li class="ge-inline ge-show-space ge-width256"><button type="" class="ge-flex" ge-chapter-show="true" data-code="1" data-model="show" data-chapter="<?= $ChapterID ?>">
							<div class="ge-inline ge-radio"><input type="radio" <?= $ArticleChapter["show"] ? "checked" : "" ?> name="show-chapter-<?= $ChapterID ?>" class="ge-hide" ><ins class="ge-mark-type"><span></span></ins>
							</div><div class="ge-inline">Show</div></button></li>
						<li class="ge-inline ge-show-space ge-width158"><button type="" class="ge-flex" ge-chapter-show="true" data-code="0" data-model="show" data-chapter="<?= $ChapterID ?>">
							<div class="ge-inline ge-radio"><input type="radio" <?= $ArticleChapter["show"] == 0 ? "checked" : "" ?> name="show-chapter-<?= $ChapterID ?>" class="ge-hide"><ins class="ge-mark-type"><span></span></ins></div>
							<div class="ge-inline">Hide</div></button></li>
					</ul>
				</div>
					
			</div>
				
		</li>	
		<li class="ge-chapter"><div class="ge-row"><div class="ge-left"><div class="ge-flex">
			<div class="ge-inline ge-right-24"><span class="ge-guide ge-width158">Limit question</span></div><div class="ge-inline">
				<h5 class="ge-chapter-limit-question ge-text ge-right-24" data="chapter-limit-question">
					<div contenteditable="true" role="textbox" class="ge-paragraph ge-width256" ge-chapter="true" data-model="limit" data-chapter="<?= $ChapterID ?>"><?= $ArticleChapter["limit_question"] ?></div>
				</h5></div></div></div>
			<div class="ge-left"><div class="ge-flex">
				<div class="ge-inline ge-right-24"><span class="ge-guide ge-width158">Priority</span></div><div class="ge-inline">
				<h5 class="ge-chapter-priority ge-text" data="chapter-priority">
					<div contenteditable="true" role="textbox" class="ge-paragraph ge-width256" ge-chapter="true" data-model="priority" data-chapter="<?= $ChapterID ?>"><?= $ArticleChapter["priority"] ?></div>
		</h5></div></div></div></div></li>	
		<li><div class="ge-top-8"><ul><li><button type="" data-open-question="true" data-chapter="<?= $ArticleChapter["chapter_id"] ?>" class="ge-guide ge-open-question" >Choose the questions</button></li></ul>
		</li>
		</ul></div></div>			
	</section>
	<?php endforeach ?>
<?php endif ?>
	
</div>
<model class="ge-model-question" model-questions="true">
	<div class="ge-white">
		<model-header>
		</model-header>
		<model-content>
			<ul container-questions="true" class="ge-ver-1632"></ul>
		</model-content>
		<model-footer></model-footer>
	</div>
</model>
</div>
</div></div>