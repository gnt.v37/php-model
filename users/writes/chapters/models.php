<?php

/**
 * @Author: Rot
 * @Date:   2017-12-07 10:06:21
 * @Last Modified by:   Rot
 * @Last Modified time: 2017-12-08 23:12:25
 */

namespace System\Models;

import(geye.models);

class Article extends Schema 
{
	public $Nerve = "articles";
}

class DraftArticle extends Schema 
{
	public $Nerve = "draft_articles";
}

class ArticleChapter extends Schema 
{
	public $Nerve = "article_chapters";
}

class DraftArticleChapter extends Schema 
{
	public $Nerve = "draft_article_chapters";
}

class ArticleQuestion extends Schema 
{
	public $Nerve = "article_questions";
}

class DraftArticleQuestion extends Schema 
{
	public $Nerve = "draft_article_questions";
}

class QuestionChapter extends Schema 
{
	public $Nerve = "question_chapters";
}

class SaveChapter extends Procedure implements Nullable
{
	protected $Name = "DSaveWriteChapter";
	
	public $Chapter;

	public $IsShow;

	public $Limit;

	public $Priority;

	public $ChapterID;

	public $UserID;

	public $ArticleID;

	public $IsEdit;

	public static function Nullable ()
	{
		return ["Chapter", "IsShow", "Limit", "Priority", "ChapterID", "ArticleID"];
	}
}

class DeleteChapter extends Dynamic 
{
	protected $Name = "DDeleteWriteChapter";
	
	public $ChapterID;

	public $UserID;

	public $ArticleID;

	public $IsEdit;
}

?>