<?php

/**
 * @Author: Rot
 * @Date:   2017-12-07 10:06:32
 * @Last Modified by:   Rot
 * @Last Modified time: 2017-12-08 15:18:17
 */

namespace System\Forms;

include_once geye.forms;

class ChapterForm extends Form
{
	public $Publish;

	public $Chapter;

	public $IsShow;

	public $Limit;

	public $Priority;

	public $QuestionID;
	
	public $ChapterID;
	
	public $UserID;

	public $ArticleID;

	public $IsEdit;

}