<?php

/**
 * @Author: Rot
 * @Date:   2017-12-07 10:06:12
 * @Last Modified by:   Rot
 * @Last Modified time: 2017-12-08 16:55:24
 */

import(users.writes.chapters.views, false);

$Urls = [

	/**
	 * URL get question page
	 */
	"/^(\/(?<ArticleID>\d+))?((?<IsEdit>\/edit))?$/" => views.ChapterView,
	/**
	 *
	 * @uses 	Ajax save question title
	 * 
	 * @var 	Regex
	 * 
	 */
	
	"/^\/get_questions\/\d+$/"				=> views.GetQuestionsObject,
	"/^\/save_chapter\/\d+$/"				=> views.SaveChapterObject,
	"/^\/save_chapter_questions\/\d+$/"		=> views.SaveChapterQuestionsObject,
	"/^\/delete_chapter\/\d+$/"				=> views.DeleteChapterObject,

]

?>