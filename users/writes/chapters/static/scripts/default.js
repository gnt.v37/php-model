/*
* @Author: Rot
* @Date:   2017-12-07 10:30:49
* @Last Modified by:   Rot
* @Last Modified time: 2017-12-17 09:52:30
*/

(function () {

window.onload = function ()
{
	new System.Chapter();

	new System.Options();
	
	new System.Publish();
}

document.querySelector(".ge-chapters").classList.add("ge-active");

})();