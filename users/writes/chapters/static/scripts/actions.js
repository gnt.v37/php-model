/*
* @Author: Rot
* @Date:   2017-12-07 10:58:50
* @Last Modified by:   Rot
* @Last Modified time: 2017-12-08 17:01:22
*/

(function () {

class BaseChapter 
{
	InvokeShowQuestion (Business, Question)
	{
		Question.addEventListener("click", function () {

			System.Process = System.Process.then(function () {

				return new Promise( function (Resolve, Reject) {

					Business.ShowQuestion(Question).then(function () {

						Resolve();
						
					})

				})

			})

		})
	}

	InvokeChapter (Business, Chapter)
	{
		let Delete = false;

		Chapter.addEventListener("keyup", function (e) {

			if (e.which == 8)
			{
				if (Business.CheckDelete(this))
				{
					Delete = true;

					System.Process = System.Process.then(function () {

						return new Promise(function (Resolve, Reject) {

							Business.DeleteChapter(Chapter).then(function () {

								Delete = false;

								Resolve();
							})

						})

					})

				}
			}

			Business.Writer.Placeholder(this);

			if (Delete == false)
			{
				System.Process = System.Process.then(function () {

					return new Promise(function(Resolve, Reject) {

						Business.SaveChapter(Chapter).then(function () {

							Resolve();

						})

					})

				})
			}

		})
	}

	InvokeChapterShow (Business, Chapter)
	{
		Chapter.addEventListener("click", function () {

			System.Process = System.Process.then(function () {

				return new Promise(function(Resolve, Reject) {

					Business.SaveChapter(Chapter).then(function () {
						Resolve();
					})

				})

			})

		})

	}
}

class Chapter extends BaseChapter
{
	constructor ()
	{
		super();

		let Business = new System.Business();

		this.InvokeInsertChapter(Business);

		this.InvokeShowQuestions(Business);

		this.InvokeChapterModel(Business);

		this.InvokeChapters(Business);

	}

	InvokeChapters (Business, Element = document)
	{
		Element.querySelectorAll("[ge-chapter]").forEach( (Chapter) => {

			Business.Writer.Placeholder(Chapter);

			this.InvokeChapter(Business, Chapter);

		})

		Element.querySelectorAll("[ge-chapter-show]").forEach( (Chapter) => {

			this.InvokeChapterShow(Business, Chapter);

		})
	}

	InvokeChapterModel (Business)
	{
		let Double = 0;

		document.querySelector("[container-questions]").addEventListener("click", function (e) {

			let Target = e.target.closest("button");

			if (Target)
			{

				Double = 0;

				System.Process = System.Process.then(function () {

					return new Promise( function (Resolve, Reject) {

						Business.SaveChapterQuestion(Target).then(function () {

							Resolve();

						});

					})

				})
			}
			else
			{
				Business.CloseQuestionModel();
			}

		})

		document.querySelector("[model-questions]").oncontextmenu = function (e)
		{
			Double++;

			if (Double >= 2)
			{
				Business.CloseQuestionModel();

				Double = 0;
			}

			e.preventDefault();
		}
	}

	InvokeInsertChapter (Business)
	{
		var Chapter = document.querySelector("[ge-new-chapter]");

		Chapter.addEventListener("click",  () => {

			var TheNew = Business.InsertChapter(Chapter);

			this.InvokeShowQuestion(Business, TheNew.querySelector("[data-open-question]"));

			this.InvokeChapters(Business, TheNew);

		})
	}

	InvokeShowQuestions (Business)
	{
		document.querySelectorAll("[data-open-question]").forEach( (Question) => {

			this.InvokeShowQuestion(Business, Question);

		});
	}
}

System.Chapter = Chapter;

})();