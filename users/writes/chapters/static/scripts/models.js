/*
* @Author: Rot
* @Date:   2017-12-07 10:59:15
* @Last Modified by:   Rot
* @Last Modified time: 2017-12-08 16:45:22
*/

(function () {

class Models 
{
	constructor (Options)
	{
		this.URLWriter = Options.URLWriter;

		this.ArticleID = Options.ArticleID;

		this.IsEdit = Options.IsEdit;

		if (this.ArticleID)
		{
			this.Setted = true;
		}
		else
		{
			this.Setted = false;
		}
	}

	SetArticle (ArticleID)
	{
		this.ArticleID = ArticleID;

		let ALinks = document.querySelectorAll(".ge-steps a");

		ALinks.forEach(function (ALink) {
			ALink.href = ALink.href + "/" + ArticleID;
		})

		var Url = `${location.href}/${this.ArticleID}`;

		history.pushState(null, null, Url);

		this.Setted = true;
	}

	GetQuestions (Data)
	{
		Data.set("ArticleID", this.ArticleID);

		Data.set("IsEdit", this.IsEdit);

		return System.post(`${this.URLWriter}/get_questions/${new Date().getTime()}`, Data);
	}

	SaveChapter (Data)
	{
		Data.set("ArticleID", this.ArticleID);

		Data.set("IsEdit", this.IsEdit);

		return System.post(`${this.URLWriter}/save_chapter/${new Date().getTime()}`, Data);
	}

	SaveChapterQuestion (Data)
	{
		Data.set("ArticleID", this.ArticleID);

		Data.set("IsEdit", this.IsEdit);

		return System.post(`${this.URLWriter}/save_chapter_questions/${new Date().getTime()}`, Data);
	}

	DeleteChapter (Data)
	{
		Data.set("ArticleID", this.ArticleID);

		Data.set("IsEdit", this.IsEdit);

		return System.post(`${this.URLWriter}/delete_chapter/${new Date().getTime()}`, Data);
	}
}

System.Models = Models;

})();