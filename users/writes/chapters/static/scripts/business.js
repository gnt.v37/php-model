/*
* @Author: Rot
* @Date:   2017-12-07 10:58:58
* @Last Modified by:   Rot
* @Last Modified time: 2017-12-16 01:03:02
*/

(function () {

class BaseBusiness
{
	constructor ()
	{
		this.ChapterShowQuestionData = new FormData();

		this.ChapterQuestionData = new FormData();

		this.ChapterData = new FormData();

		this.DeleteChapterData = new FormData();

		this.Models = new System.Models(System.URLResolver());

		this.QuestionModal = document.querySelector("[model-questions]");

		this.QuestionContainer = document.querySelector("[container-questions]");

		this.Node = document.createElement("div");
	}

	SaveChapter (Element)
	{
		if (Element.hasAttribute("data-chapter"))
		{
			this.ChapterData.set("ChapterID", Element.getAttribute("data-chapter"));
		}
		else
		{
			this.ChapterData.delete("ChapterID");
		}

		this.ChapterData.delete("Chapter");

		this.ChapterData.delete("Limit");

		this.ChapterData.delete("Priority");

		switch (Element.getAttribute("data-model")) 
		{
			case "chapter":

				this.ChapterData.set("Chapter", Element.textContent);

				break;

			case "limit":

				var Data = parseInt(Element.textContent.trim());

				var ClassList = Element.classList;

				if (Data >= 0)
				{
					this.ChapterData.set("Limit", Data);

					if (ClassList.contains("ge-error-text"))
					{
						ClassList.remove("ge-error-text");
					}
				}
				else
				{
					if (ClassList.contains("ge-error-text") == false)
					{
						ClassList.add("ge-error-text");
					}
				}

				break;
			case "priority":

				var Data = parseInt(Element.textContent.trim());

				var ClassList = Element.classList;

				if (Data >= 0)
				{
					this.ChapterData.set("Priority", Data);

					if (ClassList.contains("ge-error-text"))
					{
						ClassList.remove("ge-error-text");
					}
				}
				else
				{
					if (ClassList.contains("ge-error-text") == false)
					{
						ClassList.add("ge-error-text");
					}
				}
				break;
			case "show": 

				if (Element.hasAttribute(("data-code")))
				{
					var Data = parseInt(Element.getAttribute("data-code"));

					this.ChapterData.set("IsShow", Data);			
				}

				Element.querySelector("input").checked = true;

				break;
			default:
				// statements_def
				break;
		}

		return this.Models.SaveChapter(this.ChapterData).then( (Response) => {

			Response = Response.match(/^\[{.*?}\]/g);

			var Json = JSON.parse(Response);

			Json = Json[0];

			if (Element.hasAttribute("data-chapter") == false)
			{
				var Parent = Element.parentElement;

				while (Parent && Parent.nodeName != "SECTION")
				{
					Parent = Parent.parentElement;
				}

				Parent.querySelectorAll("[ge-chapter]").forEach(function (Chapter) {

					Chapter.setAttribute("data-chapter", Json.ChapterID);

				})

				Parent.querySelectorAll("[ge-chapter-show]").forEach(function (Chapter) {

					Chapter.setAttribute("data-chapter", Json.ChapterID);

				})
			}

			if (this.Models.Setted == false)
			{
				this.Models.SetArticle(Json.ArticleID);
			}
		});
	}

	DeleteChapter(Element)
	{
		var Parent = Element.parentElement;

		while (Parent && Parent.nodeName != "SECTION")
		{
			Parent = Parent.parentElement;
		}

		var Previous =  Parent.previousElementSibling;

		if (Previous)
		{
			Previous.querySelector("[ge-chapter]").focus();
		}
		else
		{
			var Next = Parent.nextElementSibling;

			if (Next)
			{
				Next.querySelector("[ge-chapter]").focus();
			}
		}

		if (Element.hasAttribute(("data-chapter")))
		{
			this.DeleteChapterData.set("ChapterID", Element.getAttribute("data-chapter"));

			return this.Models.DeleteChapter(this.DeleteChapterData).then(function () {

				Parent.remove();

			});
		}

		Parent.remove();

		return Promise.resolve();
	}

	SaveChapterQuestion (Question)
	{
		const QuestionID = Question.getAttribute("data-question");

		const ChapterID = Question.getAttribute("data-chapter");

		this.ChapterQuestionData.set("QuestionID", QuestionID);
		
		this.ChapterQuestionData.set("ChapterID", ChapterID);

		return this.Models.SaveChapterQuestion(this.ChapterQuestionData).then(function () {

			let Input = Question.querySelector(`input`);

			if (Input.checked)
			{
				Input.checked = false;
			}
			else
			{
				Input.checked = true;
			}
		})
	}

	ShowQuestion (Element)
	{
		const ChapterID = Element.getAttribute("data-chapter");

		this.ChapterShowQuestionData.set("ChapterID", ChapterID);

		return this.Models.GetQuestions(this.ChapterShowQuestionData).then( (Response) => {

			Response = Response.match(/\[{.*?}\]/);

			let Jsons = JSON.parse(Response);

			let Data = "";

			let Content = null;

			Jsons.forEach( (Json) => {

				this.Node.innerHTML = Json.question;

				var QuestionContent = this.Node.querySelector("[question-content]");

				if (QuestionContent && QuestionContent.innerHTML.trim().length > 0 && QuestionContent.innerHTML.trim() != "<br>")
				{
					Content = QuestionContent.innerHTML;
				}
				else
				{
					this.Node.innerHTML = Json.title;

					var QuestionTitle = this.Node.querySelector("[question-title]");

					if (QuestionTitle)
					{
						Content = QuestionTitle.innerHTML;
					}
					else
					{
						Content = this.Node.innerHTML;
					}
				}

				if (Content.trim().length == 0)
				{
					var Captions = this.Node.querySelectorAll("question-caption");

					for (let Caption of Captions) 
					{
						Content = Caption.innerHTML;

						if (Content != "")
						{
							break;
						}

					}

					if (Content.trim().length == 0)
					{
						let Sources = this.Node.querySelectorAll("[data-src]");

						for (let Source of Sources)
						{
							Source.classList.add("ge-vertical8");

							if (Source.nodeName == "IMG")
							{
								Content = Source.outerHTML;

								Content += Source.nextElementSibling.innerHTML;
							}
							else
							{	
								Source.setAttribute("height", 129);

								Content = Source.outerHTML;
							}

							if (Content != "")
							{
								break;
							}
						}
					}

					if (Content.trim().length == 0)
					{
						let Transform = this.Node.querySelector("[transform]");

						if (Transform)
						{
							Content = Transform.getAttribute("transform");
						}
						else
						{
							Content = "Empty content";
						}
					}
				}

				Data += this.InsertQuestion(Json.question_id, ChapterID, Json.checked, Content);

			})

			this.QuestionContainer.innerHTML = Data;

			let Style = this.QuestionModal.style;

			if (Style.display == "")
			{
				Style.display = "block";
			}
			else
			{
				Style.display = "";
			}

			System.load(this.QuestionContainer);

		})
	}
}

class Business extends BaseBusiness
{
	constructor ()
	{
		super();

		this.Writer = new System.Writer({
			Placeholder: 
			{
				"chapter-title": "Writer a chapter",
				"chapter-limit-question": "Write the limit questions",
				"chapter-priority": "Write the priority"
			}
		})
	}

	InsertChapter (Element)
	{
		this.Node.innerHTML =  `<section class="ge-section"><div><div class="ge-space"><ul><li><div>
		<h4 class="ge-chapter-title ge-text" data="chapter-title">
			<div contenteditable="true" role="textbox" ge-chapter="true" data-model="chapter" class="ge-paragraph"></div>
			<div class="ge-absolute ge-content ge-block"><span class="ge-placeholder">Write the chapter content here...</span></div>
		</h4>
		</div></li><li class="ge-show ge-chapter"><div class="ge-flex ge-center">
			<div class="ge-inline ge-right-24"><span class="ge-guide ge-width158">Show chapter</span></div>
			<div class="ge-inline"><ul class="ge-top-6">
				<li class="ge-inline ge-show-space ge-width256"><button ge-chapter-show="true" data-model="show" data-code="1" class="ge-flex">
					<div class="ge-inline ge-radio"><input type="radio" name="show-chapter" checked="checked" class="ge-hide"><ins class="ge-mark-type"><span></span></ins>
				</div><div class="ge-inline">Show</div></button></li>
				<li class="ge-inline ge-show-space ge-width158"><button ge-chapter-show="true" data-model="show" data-code="0" class="ge-flex">
					<div class="ge-inline ge-radio"><input type="radio" name="show-chapter" class="ge-hide"><ins class="ge-mark-type"><span></span></ins></div>
					<div class="ge-inline">Hide</div></button></li>
			</ul></div>
		</div></li>	
		<li class="ge-chapter"><div class="ge-row"><div class="ge-left"><div class="ge-flex">
			<div class="ge-inline ge-right-24"><span class="ge-guide ge-width158">Limit question</span></div><div class="ge-inline">
			<h5 class="ge-chapter-limit-question ge-text ge-right-24" data="chapter-limit-question">
				<div contenteditable="true" role="textbox" class="ge-paragraph ge-width256" ge-chapter="true" data-model="limit"></div>
				<div class="ge-absolute ge-content ge-block"><span class="ge-placeholder">Write the limit question...</span></div>
			</h5></div></div></div>
			<div class="ge-left"><div class="ge-flex">
				<div class="ge-inline ge-right-24"><span class="ge-guide ge-width158">Priority</span></div><div class="ge-inline">
				<h5 class="ge-chapter-priority ge-text" data="chapter-priority">
					<div contenteditable="true" role="textbox" class="ge-paragraph ge-width256" ge-chapter="true" data-model="priority"></div>
					<div class="ge-absolute ge-content ge-block"><span class="ge-placeholder">Write the priority...</span></div>
				</h5></div></div></div></div></li>	
		<li><div class="ge-top-8"><ul><li><button type="" data-open-question="true" data-chapter="" class="ge-guide ge-open-question" >Choose the questions</button></li></ul>
		</li></ul></div></div></div></section>`;

		 var Container = Element.parentElement.nextElementSibling;

		 var TheNew = this.Node.firstElementChild;

		 Container.insertBefore(TheNew, Container.firstElementChild);

		 return TheNew;

	}

	InsertQuestion (QuestionID, ChapterID, Checked, Text)
	{
		return `<li class="ge-hoz-16"><button data-chapter="${ChapterID}" data-question="${QuestionID}" class="ge-flex ge-center ge-vertical8 ge-relative">
		<div class="ge-inline"><input type="checkbox" ${Checked ? "checked" : ""}></input><ins class="ge-mark-type ge-inline"><span class="ge-inline"></span></ins></div>
		<div class="ge-inline"><div class="ge-relative">${Text}</div></div><div class="ge-mark"></div></button></li>`;
	}

	CheckDelete (Element)
	{
		var Parent = Element.parentElement;

		while (Parent && Parent.nodeName != "SECTION")
		{
			Parent = Parent.parentElement;
		}

		let Editors = Parent.querySelectorAll("[ge-chapter]");

		for (let Editor of Editors) 
		{
			if (Editor.textContent.trim().length > 0 || Editor.nextElementSibling == null || (Editor.nextElementSibling &&  Editor.nextElementSibling.classList.contains("ge-block") == false))
			{
				Editor.focus();
				
				return false;
			}

		}

		return true;
	}

	CloseQuestionModel ()
	{
		this.QuestionModal.style.display = "";

		this.QuestionContainer.innerHTML = "";
	}
}

System.Business = Business;

})();