<div class="ge-write-header ge-top-32" select="false">
	<div class="ge-row ge-border-top">
		<div class="ge-left">
			<?php  
				$Link = null;

				if ($this->ArticleID)
				{
					$Link = "/{$this->ArticleID}";
				}

				if ($this->IsEdit)
				{
					$Link .= $this->IsEdit;
				}
			?>
			<ul class="ge-row ge-steps">
				<li class="ge-left ge-fstyle ge-description"><div><a href="<?= page."write{$Link}" ?>" class="ge-button">Description</a></div></li>
				<li class="ge-left ge-fstyle ge-questions"><div><a href="<?= page."write/questions{$Link}" ?>" class="ge-button">Questions</a></div></li>
				<li class="ge-left ge-fstyle ge-chapters"><div><a href="<?= page."write/chapters{$Link}" ?>" class="ge-button">Chapters</a></div></li>
				<li class="ge-left ge-fstyle ge-settings"><div><a  href="<?= page."write/settings{$Link}" ?>" class="ge-button">Settings</a></div></li>
			</ul>
		</div>
		<div class="ge-right">
			<ul class="ge-relative ge-steps">
				<li class="ge-left ge-fstyle ge-none ge-previous"><div><a href="<?= page ?>write/" class="ge-button ge-previous ge-previous-button">Previous</a></div></li>
				<li class="ge-left ge-fstyle ge-next"><div><a href="<?= page ?>write/" class="ge-button ge-next-button">Next</a></div></li>
				<li class="ge-left ge-fstyle ge-options"><div><a class="ge-button">Options</a>
					<input type="hidden" value='<?= json_encode(["redirect" => page."@".$_SESSION[__SIGNIN__]["userid"]."/articles/drafts", "action" => page."write/delete_article".$Link]) ?>' ge-options-info="true"></div>
				</li>
				<li class="ge-left ge-fstyle <?= $this->IsEdit ? null : "ge-publish" ?>"><div>
					<?php if ($this->IsEdit): ?>
						<a href="<?= page."view?v={$this->ArticleID}" ?>" class="ge-button ge-color">View</a>
					<?php else: ?>
						<a class="ge-button ge-color"><?= $this->Publish["publish_at"] ? "Scheduled" : "Publish" ?></a>
					<?php endif; ?>
				</div></li>
			</ul>
		</div>
	</div>
	<div class="ge-full-line"></div>
	<hidden><input type="hidden" data-article-id="true" value="<?= $this->ArticleID ?>" name=""><input type="hidden" data-is-edit="<?= $this->IsEdit ?>" name="">
	<input type="hidden" data-publish-id="true" value="<?= $this->Publish["publish_at"] ?>" name="">
	</hidden>
</div>
