<?php  $User = $_SESSION[__SIGNIN__];?>
<div class="ge-top-32"><div class="ge-write"><div><ul>
	<li class="ge-small-mode"><div class="ge-header">
		<ge-user class="ge-flex-sc"><div class="ge-inline"><a href="<?= page."@{$User["userid"]}"; ?>" prevent="true">
			<div class="ge-inline ge-relative">
			<?php if ($User["session_image_local"]): ?>
				<div class="ge-user-image"><img data-src="<?= page.users.images."64/".$User["session_image_local"] ?>" alt=""></div>
			<?php else: ?>
				<div class="ge-user-image"><img data-src="<?= $User["session_image_link"] ?>" class="ge-user-avatar" alt=""></div>
			<?php endif ?>
			<div class="ge-avatar-radius">
				<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 70 70">
					<path d="M5.53538374,19.9430227 C11.180401,8.78497536 22.6271155,1.6 35.3571429,1.6 C48.0871702,1.6 59.5338847,8.78497536 65.178902,19.9430227 L66.2496695,19.401306 C60.4023065,7.84329843 48.5440457,0.4 35.3571429,0.4 C22.17024,0.4 10.3119792,7.84329843 4.46461626,19.401306 L5.53538374,19.9430227 Z"></path><path d="M65.178902,49.9077131 C59.5338847,61.0657604 48.0871702,68.2507358 35.3571429,68.2507358 C22.6271155,68.2507358 11.180401,61.0657604 5.53538374,49.9077131 L4.46461626,50.4494298 C10.3119792,62.0074373 22.17024,69.4507358 35.3571429,69.4507358 C48.5440457,69.4507358 60.4023065,62.0074373 66.2496695,50.4494298 L65.178902,49.9077131 Z">
					</path>
				</svg></div>
		</div></a></div>
		<div class="ge-inline ge-left-12">
			<div><a href="<?= page."@{$User["userid"]}"; ?>" prevent="true" class="ge-color"><?= "{$User["session_lastname"]} {$User["session_firstname"]}"; ?></a></div>
					<div class="ge-draft"><span>Draft</span></div>
		</div></ge-user></div></li>
	<li><div class="ge-content"><article><div class="ge-top-12">
		<?php $ID = "w" . random_string(10) . "-0" ?>
		<div class="ge-edit" ge-edit="true">
		<?php if (isset($this->DraftWrite) && $this->DraftWrite["description_detail"]):  ?>
			<?= $this->DraftWrite["description_detail"] ?>
		<?php else: ?>
			<section><ge-mode class="ge-padding ge-small-mode">
				<div data="paragraph" class="ge-paragraph" ><ged id="<?= $ID ?>" role="textbox" contenteditable="true" ><br /></ged></div>
			</ge-mode></section>
		<?php endif ?>
		
		</div>
		<hidden>
			<input type="hidden" default-data-id value="<?= $ID ?>">
			<input type="hidden" latest-id value="">
			<input type="hidden" default-element="true" value='<div data="paragraph" class="ge-paragraph" ><ged role="textbox" contenteditable="true" ><br /></ged></div>' />
		</hidden>
		</div></article></div>
	</li>
</ul></div>
</div></div>