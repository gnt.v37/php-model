<?php

/**
 * @Author: Rot
 * @Date:   2017-09-26 21:48:52
 * @Last Modified by:   Rot
 * @Last Modified time: 2017-12-07 13:46:19
 */

namespace System\Models;

import(geye.models);

abstract class WriterProcedure extends Procedure
{
	public $UserID;

	public $ArticleID;

	public $IsEdit;
}

abstract class WriterDynamic extends Dynamic
{
	public $UserID;

	public $ArticleID;

	public $IsEdit;
}

/**
 * Schema
 */

class Article extends Schema 
{
	public $Nerve = "articles";
}

class DraftArticle extends Schema 
{
	public $Nerve = "draft_articles";
}

/**
 * Procedure
 */
class ArticleDescription extends WriterProcedure implements Nullable
{
	protected $Name = "GWriteDescription";

	public static function Nullable ()
	{
		return ["ArticleID", "IsEdit"];
	}
}

class ArticlePublish extends Procedure
{
	protected $Name = "DWriteArticlePublish";

	public $UserID;
	
	public $ArticleID;
}

class ArticleSchedule extends Procedure implements Nullable
{
	protected $Name = "DWriteArticleSchedule";

	public $UserID;
	
	public $ArticleID;

	public $Publish;

	public static function Nullable ()
	{
		return "Publish";
	}
}

class SaveWriteImage extends WriterProcedure implements Nullable, Humanize
{
	protected $Name = "DSaveWriteImage";

	public $Image;

	public static function Nullable ()
	{
		return ["Image", "ArticleID"];
	}
}

class DeleteWriteImage extends WriterProcedure implements Humanize
{
	protected $Name = "DDeleteWriteImage";

	public $ImageID;
}

class DeleteWriteHomeImage extends WriterProcedure implements Humanize
{
	protected $Name = "DDeleteWriteHomeImage";
}

class DeleteWriteArticle extends WriterProcedure implements Nullable
{
	protected $Name = "DDeleteWriteArticle";

	public static function Nullable ()
	{
		return "IsEdit";
	}
}

/**
 * Function
 */
class SaveDescriptionDetail extends WriterDynamic implements Nullable
{
	protected $Name = "DSaveWriteDescriptionDetail";

	public $DescriptionDetail;

	public static function Nullable ()
	{
		return "ArticleID";
	}

}

class SaveDescription extends WriterDynamic implements Nullable
{
	protected $Name = "DSaveWriteDescription";

	public $Description;

	public static function Nullable ()
	{
		return "ArticleID";
	}

}

class SaveTitle extends WriterDynamic implements Nullable
{
	protected $Name = "DSaveWriteTitle";

	public $Title;

	public static function Nullable ()
	{
		return ["ArticleID"];
	}

}

class SaveWriteHomeImage extends WriterDynamic implements Humanize, Nullable
{
	protected $Name = "DSaveWriteHomeImage";

	public $ImageLink;

	public static function Nullable ()
	{
		return "ImageLink";
	}

}

class CheckImage extends WriterDynamic implements Humanize
{
	protected $Name = "DWriteCheckImage";
}

?>