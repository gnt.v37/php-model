<?php

/**
 * @Author: Rot
 * @Date:   2017-09-26 21:48:15
 * @Last Modified by:   Rot
 * @Last Modified time: 2017-12-17 10:42:25
 */
import(users.writes.views, false);

define("questions", "questions/");

define("settings", "settings/");

define("chapters", "chapters/");

$Urls = [


	/**
	 * It accept only url have draft id with type are integer
	 * @var 	Regex
	 */
	"/^(\/(?<ArticleID>\d+))?((?<IsEdit>\/edit))?$/"	=> views.DescriptionEditView,

	/**
	 * @uses 	Ajax clone image
	 * 
	 * @var 	Regex
	 * 
	 */
	"/^\/(?<ArticleID>\d+)\/publish/" 			=> views.PublishObject,
	"/^\/(?<ArticleID>\d+)\/schedule/" 			=> views.ScheduleObject,
	"/^\/clone_image\/\d+/"						=> views.CloneImageObject,
	"/^\/blur_image\/\d+$/"						=> views.BlurImageObject,
	"/^\/save_title\/\d+$/"						=> views.SaveTitleObject,
	"/^\/save_description\/\d+$/"				=> views.SaveDescriptionObject,
	"/^\/save_description_detail\/\d+$/"		=> views.SaveDescriptionDetailObject,
	"/^\/delete_image\/\d+$/"					=> views.DeleteImageObject,

	/**
	 * @uses       <element_ref>  Delete article
	 * 
	 * @var 		Regex
	 */		
	"/^\/delete_article\/(?<ArticleID>\d+)((?<IsEdit>\/edit))?$/"	=> views.DeleteArticleObject,

	/**
	 * { item_description }
	 */

	"/\/questions/"	=> questions.urls,
	"/\/chapters/"	=> chapters.urls,
	"/\/settings/"	=> settings.urls
];

?>