<?php

/**
 * @Author: Rot
 * @Date:   2017-11-20 11:33:36
 * @Last Modified by:   Rot
 * @Last Modified time: 2017-12-06 20:22:24
 */

namespace System\Forms;

include_once geye.forms;

class SettingForm extends Form 
{
	public $TopicID;

	public $TagID;

	public $AudienceID;

	public $Times;

	public $Topic;
	
	public $IsGuide;

	public $Tag;

	public $Questions;

	public $UserName;

	public $UserID;

	public $ArticleID;

	public $IsEdit;

}

?>