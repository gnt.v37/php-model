<?php

/**
 * @Author: Rot
 * @Date:   2017-11-20 11:33:43
 * @Last Modified by:   Rot
 * @Last Modified time: 2017-12-07 07:40:59
 */

namespace System\Views;

import(geye.views.generic);

class SettingEditView extends GenericView
{
	protected $Models = ["Audience", "ArticleAudiences", "ArticleSettings", "ArticleTags", "DraftArticle", "Article"];

	protected $Styles = [users.writes.styles => ["writer.css", "modules/material-datetime-picker.css"]];

	protected $Scripts = [users.writes.scripts => 
		["commons.js", "writer.js", "modules/moment.js", "modules/rome.standalone.js", "modules/zmaterial-datetime-picker.js"]
	];

	protected function Resolver ()
	{

		$this->ArticleAudiences = $this->Deeps->GetArticleAudience();
		
		$this->LimitQuestion = $this->Deeps->GetLimitQuestion();

		$this->ArticleSetting = $this->Deeps->GetArticleSetting();

		$this->AudienceModes = $this->Deeps->GetAudienceModes();

		$this->ArticleTags = $this->Deeps->GetArticleTags();

		$this->ArticleID = $this->Deeps->GetArticleID();

		$this->Publish = $this->Deeps->GetPubish();

		$this->IsEdit = $this->Deeps->GetIsEdit();

	}
}

class GetTopicObject extends ObjectView
{
	protected $Models = "Topic";

	protected function Resolver ()
	{
		echo json_encode($this->Deeps->GetTopics());
	}
}

class GetTagObject extends ObjectView
{
	protected $Models = "Tag";

	protected function Resolver ()
	{
		echo json_encode($this->Deeps->GetTags());
	}
}

class GetUserObject extends ObjectView
{
	protected $Models = "User";

	protected function Resolver ()
	{
		echo json_encode($this->Deeps->GetUsers());
	}
}

class SaveAudienceObject extends ObjectView
{
	protected $Models = "SaveAudience";

	protected function Resolver ()
	{
		print_r($this->Deeps);
	}
}

class SaveTopicObject extends ObjectView
{
	protected $Models = "SaveTopic";

	protected function Resolver ()
	{
		echo json_encode($this->Deeps->SaveTopic[0]);
	}
}

class SaveTagObject extends ObjectView
{
	protected $Models = "SaveTag";

	protected function Resolver ()
	{
		echo json_encode($this->Deeps->SaveTag[0]);
	}
}

class SaveAudienceModeObject extends ObjectView
{
	protected $Models = "SaveAudienceMode";

	protected function Resolver ()
	{
		echo json_encode($this->Deeps->SaveAudienceMode[0]);
	}
}

class SaveLimitQuestionObject extends ObjectView
{
	protected $Models = "SaveLimitQuestion";

	protected function Resolver ()
	{
		echo json_encode($this->Deeps->SaveLimitQuestion);
	}
}

class SaveLimitTimeObject extends ObjectView
{
	protected $Models = "SaveLimitTime";

	protected function Resolver ()
	{
		echo json_encode($this->Deeps->SaveLimitTime);
	}
}

class SaveLimitTryObject extends ObjectView
{
	protected $Models = "SaveLimitTry";

	protected function Resolver ()
	{
		echo json_encode($this->Deeps->SaveLimitTry);
	}
}

class SaveGuideObject extends ObjectView
{
	protected $Models = "SaveGuide";

	protected function Resolver ()
	{
		echo json_encode($this->Deeps->SaveGuide);
	}
}

class DeleteTagObject extends ObjectView
{
	protected $Models = "DeleteTag";
}


class DeleteAudienceObject extends ObjectView
{
	protected $Models = "DeleteAudience";
}

?>