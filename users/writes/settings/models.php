<?php

/**
 * @Author: Rot
 * @Date:   2017-11-20 11:33:29
 * @Last Modified by:   Rot
 * @Last Modified time: 2017-12-07 07:36:20
 */

namespace System\Models;

import(geye.models);

/**
 * Schema
 */

class Article extends Schema 
{
	public $Nerve = "articles";
	
}

class DraftArticle extends Schema 
{
	public $Nerve = "draft_articles";
}

class Audience extends Schema implements InvokeData
{
	protected $Nerve = "audiences";

	public $Fields = ["audience_id", "audience"];

	public static function Data ()
	{
		return null;
	}
}

class Topic extends Schema implements InvokeData
{
	protected $Nerve = "topics";

	public $Fields = ["topic_id", "topic"];

	public static function Data ()
	{
		return "topic";
	}

}

class User extends Schema 
{
	protected $Nerve = "users";

	protected $Prime = "user_id";
}

class Tag extends Schema implements InvokeData
{
	protected $Nerve = "tags";

	public $Fields = ["tag_id", "tag"];

	public static function Data ()
	{
		return "tag";
	}
}


/**
 * Procedure
 */

abstract class WriterProcedure extends Procedure
{
	public $UserID;

	public $ArticleID;

	public $IsEdit;
}

abstract class WriterDynamic extends Dynamic implements Nullable
{
	public $UserID;

	public $ArticleID;

	public $IsEdit;

	public static function Nullable ()
	{
		return ["ArticleID"];
	}
}

class ArticleAudiences extends WriterProcedure implements Nullable, UserRequirement
{
	protected $Name = "GWriteArticleAudiences";

	public static function Nullable ()
	{
		return "IsEdit";
	}

	public static function User ()
	{
		return true;
	}
}

class ArticleSettings extends WriterProcedure implements Nullable
{
	protected $Name = "GWriteArticleSettings";

	public static function Nullable ()
	{
		return "IsEdit";
	}
}

class ArticleTags extends WriterProcedure implements Nullable
{
	protected $Name = "GWriteArticleTags";

	public static function Nullable ()
	{
		return "IsEdit";
	}

}

class SaveTopic extends WriterProcedure implements Nullable
{
	protected $Name = "DSaveWriteTopic";

	public $Topic;

	public static function Nullable ()
	{
		return "ArticleID";
	}

}

class SaveTag extends WriterProcedure implements Nullable
{
	protected $Name = "GSaveWriteTag";

	public $Tag;

	public static function Nullable ()
	{
		return "ArticleID";
	}

}

class SaveAudienceMode extends WriterProcedure implements Nullable
{
	protected $Name = "DSaveWriteAudienceMode";

	public $AudienceID;

	public static function Nullable ()
	{
		return "ArticleID";
	}
}

/**
 * Function
 */

class SaveAudience extends WriterDynamic
{
	protected $Name = "DSaveWriteAudience";

	public $AudienceID;

}

class SaveLimitQuestion extends WriterDynamic
{
	protected $Name = "DSaveWriteLimitQuestion";

	public $Questions;

}

class SaveLimitTime extends WriterDynamic
{
	protected $Name = "DSaveWriteLimitTime";

	public $Times;

}

class SaveLimitTry extends WriterDynamic
{
	protected $Name = "DSaveWriteLimitTry";

	public $Times;
}

class SaveGuide extends WriterDynamic
{
	protected $Name = "DSaveWriteGuide";

	public $IsGuide;

	
}

class DeleteTag extends WriterDynamic
{
	protected $Name = "DDeleteWriteTag";

	public $TagID;

	
}

class DeleteAudience extends WriterDynamic
{
	protected $Name = "DDeleteWriteAudience";

	public $AudienceID;

}

?>