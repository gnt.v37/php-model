<?php

/**
 * @Author: Rot
 * @Date:   2017-11-03 21:21:45
 * @Last Modified by:   Rot
 * @Last Modified time: 2017-12-07 13:14:13
 */

if (isset($this->ArticleID["Permission"]))
{
	$Header = "";
	
	$ContentTemplate = assets.templates."403.php";
}
else
{

	$HeaderTemplate = users.writes.templates.header;

}

include_once __TEMPLATE__;

?>