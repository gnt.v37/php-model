<div class="ge-top-32"><div class="ge-width-mode ge-relative"><div><div><ul><li class="ge-text-color">
	<div class="ge-flex ge-vertical16"><div class="ge-inline ge-width-option"><h4 class="ge-setting-text">Your post audience</h4></div>
	<div class="ge-inline ge-left-32"><ul class="ge-options ge-row">
		<?php 
			if (empty($this->ArticleAudiences))
			{
				$Choosed = 1;
			}
			else
			{
				$Choosed = $this->ArticleAudiences ? $this->ArticleAudiences[0]["Audience"] : 4; 
			}
		?>
		<?php foreach ($this->AudienceModes as $Key => $Audience): ?>
			<li audience-mode="<?php switch ($Key) 
			{ 	case 0: echo "all"; break; 
				case 1: echo "member"; break; 
				case 2: echo "follower"; break; 
				case 3: echo "specific"; break;
			} ?>" class="ge-row <?= $Choosed - 1 == $Key ? "ge-active" : ""; ?>" data-audience-mode="<?= $Audience["audience_id"] ?>"><div class="ge-radio">
				<input type="radio" name="audience"><span></span>
			</div><div class="ge-left"><button><span>
				<?php if ($Key == 1): ?>
					<?= __PROJECT__ ?>
				<?php endif ?>
				<?= $Audience["audience"] ?>
				<?php if ($Key == 0): ?>
					<?= __PROJECT__ ?>
				<?php endif ?></span></button></div></li>
		<?php endforeach ?>
	</ul><ul class="ge-audience-explain">
		<li class="ge-all <?= $Choosed - 1 == 0 ? "ge-active" : ""; ?>"><span>This is the publish mode, it mean anyone on or off <?= __PROJECT__ ?> can see this article.</span></li>
		<li class="ge-member <?= $Choosed - 1 == 1 ? "ge-active" : ""; ?>"><span>If audiences want to see this article, they have to login <?= __PROJECT__ ?>. It mean it just publish member only</span></li>
		<li class="ge-follwer <?= $Choosed - 1 == 2 ? "ge-active" : ""; ?>"><span>This mode will accept only the people who are your followers can see this article</span></li>
		<li class="ge-specific ge-relative <?= $Choosed - 1 == 3 ? "ge-active" : ""; ?>"><div><div>
			<h4 class="ge-setting-text ge-text" data="ge-audience" placeholder="Write your tags ..."><div contenteditable="true" ge-audience="true" rule="textbox" class="ge-paragraph"></div><div class="ge-absolute ge-block"><span class="ge-placeholder">Write members can see this post ...</span></div></h4>
			<div class="ge-tag16"><ul audience-container>
			<?php if ($Key == 3 && isset($this->ArticleAudiences[0]["user_id"])): ?>
				<?php foreach ( $this->ArticleAudiences as $ArticleAudience): ?>
					<li class="ge-inline ge-relative ge-tag-item"><div><a href="" target="blank" class="ge-tag"><?= "{$ArticleAudience["lastname"]} {$ArticleAudience["firstname"]}" ?></a>
					</div><div class="ge-remove" audience="<?= $ArticleAudience["user_id"] ?>"><button><span>x</span></button></div>
					</li>
				<?php endforeach ?>
			<?php endif ?>
			</ul></div>
		</div><model model-audience="true"><div class="ge-relative"><model-content class="model-content">
			<div><ul class="ge-audience ge-inline-list ge-row">
				<li data-audience="true"><button class="ge-button"></button></li>
				<li data-audience="true"><button></button></li>
				<li data-audience="true"><button></button></li>
				<li data-audience="true"><button></button></li>
				<li data-audience="true"><button></button></li>
				<li data-audience="true"><button></button></li>
			</ul></div></model-content>
			<div class="ge-popover-arrow"></div>
		</div></model></div></li>
</ul></div>
</div></li>
<li class="ge-margin-h8 ge-relative">
	<div class="ge-text-color ge-relative">
		<div class="">
			<?php $Topic = $this->ArticleSetting["topic"]; ?>
			<h4 class="ge-setting-text" data="ge-topic" placeholder="Write us known your article topic ..."><div contenteditable="true" ge-topic="true" rule="textbox" class="ge-paragraph"><?= $Topic ?></div>
			<?php if ($Topic == null): ?>
				<div class="ge-absolute ge-block"><span class="ge-placeholder">Write us known your article topic ...</span></div>
			<?php endif ?>	</h4>
		</div>
		<model model-topic="true"><div class="ge-relative"><model-content class="model-content">
			<div><ul class="ge-inline-list ge-row">
				<li data-topic="true"><button class="ge-button"></button></li>
				<li data-topic="true"><button></button></li>
				<li data-topic="true"><button></button></li>
				<li data-topic="true"><button></button></li>
				<li data-topic="true"><button></button></li>
				<li data-topic="true"><button></button></li>
			</ul></div></model-content>
			<div class="ge-popover-arrow"></div>
		</div></model>
	</div>
</li>
<li class="ge-margin-h8 ge-relative">
	<div class="ge-text-color"><div>
		<h4 class="ge-setting-text ge-text" data="ge-tag" placeholder="Write a tag ..."><div contenteditable="true" ge-tag="true" rule="textbox" class="ge-paragraph"></div><div class="ge-absolute ge-block"><span class="ge-placeholder">Write a tag ...</span></div></h4>
		<div class="ge-tag16"><ul tag-container>
		<?php if (count($this->ArticleTags) > 0): ?>
			<?php foreach ($this->ArticleTags as $Tag): ?>
				<li class="ge-inline ge-relative ge-tag-item"><div><a href="" target="blank" class="ge-tag"><?= $Tag["tag"] ?></a></div><div class="ge-remove" tag="<?= $Tag["tag_id"] ?>"><button><span>x</span></button></div></li>
			<?php endforeach ?>
		<?php endif ?>
		</ul></div><div class="ge-text-color ge-explain">Add or change tags (up to 5) so readers know what your story is about</div>
	</div>
	<model model-tag="true"><div class="ge-relative">
		<model-content class="model-content"><div><ul class="ge-inline-list ge-row">
			<li data-tag="true"><button></button></li>
			<li data-tag="true"><button></button></li>
			<li data-tag="true"><button></button></li>
			<li data-tag="true"><button></button></li>
			<li data-tag="true"><button></button></li>
			<li data-tag="true"><button></button></li>
			<li data-tag="true"><button></button></li>
			<li data-tag="true"><button></button></li>
		</ul></div></model-content>
		<div class="ge-popover-arrow"></div>
	</div></model></div>
</li>
</ul></div>
<div class="ge-practice-option">
<ul class="ge-text-color ge-row">
	<li class="ge-padding-h10">
		<div class="ge-flex ge-center">
			<div class="ge-inline ge-width-option">
				<h4 class="ge-setting-text ge-text">Practice limit question</h4>
			</div>
			<?php $LimitQuestion =  $this->ArticleSetting["limit_question"] <= $this->LimitQuestion["Total"] ? $this->ArticleSetting["limit_question"] : $this->LimitQuestion["Total"] ?>
			<div class="ge-inline ge-left-32">
				<div limit-question="true" class="ge-row ge-active">
					<div class=""><h4 class="ge-setting-text ge-text" data="data-limit">
						<div data-limit-question="true" contenteditable="true"  rule="textbox" class="ge-limit-question ge-paragraph" min="0" max="<?= $this->LimitQuestion["Total"] ?>" ><?=  $LimitQuestion ?><br /></div>
						<div class="ge-absolute ge-block"><span class="ge-placeholder">Write the limit questions ...</span></div>
				</h4>
			</div>
	</li>
	<li class="ge-padding-h10">
		<div class="ge-flex">
			<div class="ge-inline ge-width-option">
				<h4 class="ge-setting-text ge-text"> Practice limit time<input type="" name="" class="ge-none"></h4>
			</div>
			<div class="ge-inline ge-left-32">
				<?php  
					$Time = $this->ArticleSetting["limit_time"];

					$IsInfinite = true;

					if ($Time > 0)
					{
						$IsInfinite = false;
					}

					$Hours = 0;

					$Minutes = 0;

					$Seconds = 0;

					if ($Time >= 3600)
					{
						$Hours = floor($Time / 3600);

						$Time = $Time % 3600;
					}

					if ($Time >= 60)
					{
						$Minutes = floor($Time / 60);

						$Time = $Time % 60;
					}

					if ($Time > 0)
					{
						$Seconds = $Time;

						$Time = 0;
					}
				?>
				<ul class="ge-options ge-row">
					<li data="infinite" limit-time="true" class="ge-row <?= $IsInfinite == true ? "ge-active" : "" ?>"><div><div class="ge-radio">
						<input type="radio" name="limit-time"><span></span>
						</div><div class="ge-left"><button><span>Infinite</span></button></div></div></li>
					<li data="hour" limit-time="true" class="ge-row <?= $Hours > 0 ? "ge-active" : "" ?>"><div class="ge-flex"><div class="ge-inline ge-textbox"><input type="number" data-time="hour" ge-time="true" min="0" class="ge-limit" value="<?= $Hours ?>"></div><div class="ge-inline ge-row"><div class="ge-radio">
						<input type="radio" ge-time="true" name="limit-time"><span></span>
						</div><div class="ge-left"><button><span>Hours</span></button></div><div></div></li>
					<li data="minute" limit-time="true" class="ge-row <?= $Minutes > 0 ? "ge-active" : "" ?>"><div class="ge-flex"><div class="ge-inline ge-textbox"><input type="number" data-time="minute" ge-time="true" min="1" class="ge-limit" value="<?= $Minutes ?>"></div><div class="ge-inline ge-row"><div class="ge-radio">
						<input type="radio" ge-time="true" name="limit-time"><span></span>
						</div><div class="ge-left"><button><span>Minutes</span></button></div><div></div></li>
					<li data="second" limit-time="true" class="ge-row <?= $Seconds > 0 ? "ge-active" : "" ?>"><div class="ge-flex"><div class="ge-inline ge-textbox"><input type="number" data-time="second" ge-time="true" min="1" class="ge-limit" value="<?= $Seconds ?>"></div><div class="ge-inline ge-row"><div class="ge-radio">
						<input type="radio" ge-time="true" name="limit-time"><span></span>
						</div><div class="ge-left"><button><span>Seconds</span></button></div><div></div></li>
					</ul>
			</div>
		</div>
		
	</li>
	<?php 
		$Try = $this->ArticleSetting["limit_try"];
	?>
	<li class="">
		<div class="ge-flex">
			<div class="ge-inline ge-width-option">
				<h4 class="ge-setting-text ge-text">Members can try this in</h4>
			</div>
			<div class="ge-inline ge-left-32">
				<ul class="ge-options ge-row">
					<li data="many" limit-try="true" class="ge-row <?= $Try == 0 ? "ge-active" : "" ?>"><div><div class="ge-radio">
						<input type="radio" name="limit-try"><span></span>
						</div><div class="ge-left"><button><span>Many times</span></button></div></div></li>
					<li data="fixed" limit-try="true" class="ge-row <?= $Try > 0 ? "ge-active" : "" ?>"><div class="ge-flex"><div class="ge-inline ge-textbox"><input type="number" data-time min="1" value="<?= $Try ?>" class="ge-limit" value="<?= $Try ?>"></div><div class="ge-inline ge-row"><div class="ge-radio">
						<input type="radio" name="limit-try"><span></span>
						</div><div class="ge-left"><button><span>Fixed times</span></button></div><div></div></li>
					</ul>
			</div>
		</div>
	</li>
	
	<?php  
		$Guide = $this->ArticleSetting["guide"] === null ? 1 : $this->ArticleSetting["guide"];
	?>
	<li class="ge-padding-h10">
		<div class="ge-flex">
			<div class="ge-inline ge-width-option">
				<h4 class="ge-setting-text ge-text">Show guide in practice </h4>
			</div>
			<div class="ge-inline ge-left-32">
				<ul class="ge-options ge-row">
				<li data="all" class="ge-row <?= $Guide == 1 ? "ge-active" : ""; ?>" guide="true"><div><div class="ge-radio">
					<input type="radio" name="guide"><span></span>
					</div><div class="ge-left"><button><span>Yes</span></button></div></div></li>
				<li data="member" class="ge-row <?= $Guide == 0 ? "ge-active" : ""; ?>" guide="true"><div class="ge-radio">
					<input type="radio" name="guide"><span></span>
					</div><div class="ge-left"><button><span>No</span></button></div></li>
				</ul>
			</div>
		</div>
	</li>
</ul>
</div></div>
</div>
</div></div>

