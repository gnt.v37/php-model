<?php

/**
 * @Author: Rot
 * @Date:   2017-11-20 11:33:48
 * @Last Modified by:   Rot
 * @Last Modified time: 2017-12-06 20:21:50
 */

import(users.writes.settings.views, false);

$Urls = [

	/**
	 * we need to edit because of the article or the draft article
	 */

	"/^(\/(?<ArticleID>\d+))?((?<IsEdit>\/edit))?$/" => views.SettingEditView,
	/**
	 * @uses 	Ajax get topic when keyup
	 * 
	 * @var 	Regex
	 */
	"/^\/get_topic\/\d+$/"						=> views.GetTopicObject,
	"/^\/get_user\/\d+$/"						=> views.GetUserObject,
	"/^\/get_tag\/\d+$/"						=> views.GetTagObject,
	"/^\/save_tag\/\d+$/"						=> views.SaveTagObject,
	"/^\/save_topic\/\d+$/"						=> views.SaveTopicObject,
	"/^\/save_audience\/\d+$/"					=> views.SaveAudienceObject,
	"/^\/save_audience_mode\/\d+$/"				=> views.SaveAudienceModeObject,
	"/^\/save_limit_try\/\d+$/"					=> views.SaveLimitTryObject,
	"/^\/save_limit_time\/\d+$/"				=> views.SaveLimitTimeObject,
	"/^\/save_limit_question\/\d+$/"			=> views.SaveLimitQuestionObject,
	"/^\/save_guide\/\d+$/"						=> views.SaveGuideObject,
	"/^\/delete_tag\/\d+$/"						=> views.DeleteTagObject,
	"/^\/delete_audience\/\d+$/"				=> views.DeleteAudienceObject,
	
]

?>