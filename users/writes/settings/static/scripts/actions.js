/*
* @Author: Rot
* @Date:   2017-11-04 00:39:45
* @Last Modified by:   Rot
* @Last Modified time: 2017-12-15 01:51:38
*/
(function () {

class Settings
{
	constructor ()
	{
		let Business = new System.SettingBusiness();

		this.AudienceMode(Business);

		this.Topic(Business);

		this.Audience(Business);

		this.Tag(Business);

		this.LimitQuestion(Business);
		
		this.LimitTime(Business);

		this.LimitTry(Business);

		this.Guide(Business);
	}

	AudienceMode (Business)
	{
		var Audiences = document.querySelectorAll("[audience-mode]"),
			AudienceParent = Audiences[0].parentElement,
			AudienceDescription = AudienceParent.nextElementSibling;

		Audiences.forEach(function (Audience, Key) {

			Audience.addEventListener("click", function () {

				var Element = this;

				Business.Audience(AudienceParent, AudienceDescription, this, Key);

				System.Process = System.Process.then (function () {

					return new Promise(function (Resolve, Reject) {

						Business.SaveAudienceMode(Element, Key).then (function () {
							Resolve();
						})

					})

				})

			})
			
		})

	}

	Topic (Business)
	{
		var Topic = document.querySelector("[ge-topic]"),
			TopicData = document.querySelectorAll("[data-topic]");

		Topic.addEventListener("keyup", function (e) {

			Business.Placeholder(Topic);

			System.Process = System.Process.then(function (Resonse) {

				return new Promise(function (Resolve, Reject) {

					Business.GetTopic(Topic).then(function () {
						Resolve();
					})

				})

			})

			System.Process = System.Process.then(function (Resolve) {

				return new Promise(function (Resolve, Reject) {

					Business.SaveTopic(Topic).then(function () {
						setTimeout(function () {
							Resolve();
						}, 256)
						
					})
				})

			})

			if (e.which == 8)
			{
				e.preventDefault();
			}

		})

		TopicData.forEach(function(TopicElement) {

			TopicElement.addEventListener("click", function () {

				Business.GetTopicText(Topic, this);

				System.Process = System.Process.then(function (Resolve) {

					return new Promise(function (Resolve, Reject) {

						Business.SaveTopic(Topic).then(function () {
							setTimeout(function () {
								Resolve();
							}, 512)
						
						})

					})

				})

			})

		})

	}

	Tag (Business)
	{
		var Tag = document.querySelector("[ge-tag]"),
			TagData = document.querySelectorAll("[data-tag]"),
			TagContainer = document.querySelector("[tag-container]");

		Tag.addEventListener("keydown", function (e) {
			if (e.which == 13)
			{
				e.preventDefault();
			}
		})

		Tag.addEventListener("keyup", function (e) {

			if (e.which == 13)
			{
				System.Process = System.Process.then(function (Resolve) {

					return new Promise(function (Resolve, Reject) {

						Business.SaveTag(TagContainer, Tag).then(function () {
							Resolve();
						})
					})

				})

				return false;
			}

			Business.Placeholder(this);

			System.Process = System.Process.then(function (Resonse) {

				return new Promise(function (Resolve, Reject) {

					Business.GetTag(Tag).then(function () {
						Resolve();
					})

				})

			})

			
		})

		TagData.forEach(function(TagElement) {

			TagElement.addEventListener("click", function () {

				Business.GetTopicText(Tag, this);

				System.Process = System.Process.then(function (Resolve) {

					return new Promise(function (Resolve, Reject) {

						Business.SaveTag(TagContainer, Tag).then(function () {
							setTimeout(function () {
								Resolve();
							}, 512)
						
						})


					})

				})

			})

		})

		TagContainer.addEventListener("click", function (e) {

			var Closest = e.target.closest(".ge-remove");

			if (Closest)
			{
				System.Process = System.Process.then(function (Resonse) {

					return new Promise(function (Resolve, Reject) {

						Business.DeleteTag(Closest).then(function () {
							Resolve();
						})

					})

				})
			}
		})
	}

	Audience (Business)
	{
		var Audience = document.querySelector("[ge-audience]"),
			AudienceData = document.querySelectorAll("[data-audience]"),
			AudienceContainer = document.querySelector("[audience-container]");

		Audience.addEventListener("keyup", function (e) {

			Business.Placeholder(this);

			System.Process = System.Process.then(function (Resonse) {

				return new Promise(function (Resolve, Reject) {

					Business.GetAudience(Audience).then(function () {
						Resolve();
					})

				})

			})

		})

		AudienceData.forEach(function(AudienceElement) {

			AudienceElement.addEventListener("click", function () {

				var Element = this;

				System.Process = System.Process.then(function (Resolve) {

					return new Promise(function (Resolve, Reject) {

						Business.SaveAudience(AudienceContainer, Audience, Element).then(function () {
							Resolve();
						})


					})

				})

			})

		})

		AudienceContainer.addEventListener("click", function (e) {

			var Closest = e.target.closest(".ge-remove");

			if (Closest)
			{
				System.Process = System.Process.then(function () {

					return new Promise(function (Resolve, Reject) {
						Business.DeleteAudience(Closest);
					})

				})
			}
				

		})
	}

	LimitQuestion(Business)
	{
		let LimitQuestion = document.querySelector("[data-limit-question]");

		Business.Writer.Placeholder(LimitQuestion);

		LimitQuestion.addEventListener("keyup", function () {

			var Element = this;

			Business.Writer.Placeholder(this);

			if (Business.LimitQuestion(this))
			{
				System.Process = System.Process.then(function () {

					return new Promise(function (Resolve, Reject) {

						Business.SaveLimitQuestion(Element).then(function () {

							Resolve();
							
						});

					})

				})
			}

		})
	}

	LimitTime (Business)
	{
		var Limits = document.querySelectorAll("[limit-time]"),
			LimitParent = Limits[0].parentElement;

		Limits.forEach(function (Limit, Key) {

			Limit.addEventListener("click", function (e) {

				if (Business.LimitTime(LimitParent, this, e) == false)
				{
					return false;
				}

				var Element = this;

				if (Key == 0)
				{
					System.Process = System.Process.then(function () {

						return new Promise(function (Resolve, Reject) {

							Business.SaveLimitTime(LimitParent, Element).then(function () {
								Resolve();
							});

						})

					})
				}
			})

			if (Key == 0)
			{
				return false;
			}

			Limit.querySelector("[data-time]").addEventListener("keyup", function () {

				var Element = this;

				System.Process = System.Process.then(function () {

					return new Promise(function (Resolve, Reject) {

						Business.SaveLimitTime(LimitParent, Element).then(function () {
							Resolve();
						});

					})

				})
			})

		})
	}

	LimitTry (Business)
	{
		var Limits = document.querySelectorAll("[limit-try]"),
			LimitParent = Limits[0].parentElement;

		Limits.forEach(function (Limit, Key) {

			Limit.addEventListener("click", function () {

				if (Business.LimitTry(LimitParent, this) == false)
				{
					return false;
				}

				if (Key == 0)
				{
					var Element = this;

					System.Process = System.Process.then(function () {

						return new Promise(function (Resolve, Reject) {

							Business.SaveLimitTry(LimitParent, Element).then(function () {
								Resolve();
							});

						})

					})
				}
					
			})

			if (Key == 0)
				return false;

			Limit.querySelector("[data-time]").addEventListener("keyup", function () {

				var Element = this;

				System.Process = System.Process.then(function () {

					return new Promise(function (Resolve, Reject) {

						Business.SaveLimitTry(LimitParent, Element).then(function () {
							Resolve();
						});

					})

				})
			})

		})
	}

	Guide (Business)
	{
		var Guides = document.querySelectorAll("[guide]"),
			ParentElement = Guides[0].parentElement;

		Guides.forEach(function (Guide, Key) {

			Guide.addEventListener("click", function () {
				var Element = this;

				System.Process = System.Process.then(function () {

					return new Promise(function (Resolve, Reject) {
						Business.SaveGuide(ParentElement, Element, Key).then(function () {
							Resolve();
						});

					})
				})
			})

		})
	}
}

System.Settings = Settings;

})();