/*
* @Author: Rot
* @Date:   2017-11-04 00:40:48
* @Last Modified by:   Rot
* @Last Modified time: 2017-12-06 20:16:50
*/
(function () {

class SettingModel
{
	constructor (Options)
	{
		this.URLWriter = Options.URLWriter;

		this.ArticleID = Options.ArticleID;

		this.IsEdit = Options.IsEdit;

		if (this.ArticleID)
		{
			this.Setted = true;
		}
		else
		{
			this.Setted = false;
		}
	}

	SetArticle (Response)
	{
		Response = Response.match(/^{.*?}/gi);

		var Json = JSON.parse(Response);

		if (Json)
		{
			let ArticleID = Json.Result;

			let ALinks = document.querySelectorAll(".ge-steps a");

			ALinks.forEach(function (ALink) {
				ALink.href = ALink.href + "/" + ArticleID;
			})

			this.ArticleID = ArticleID;

			var Url = `${location.href}/${this.ArticleID}`;

			history.pushState(null, null, Url);

			this.Setted = true;
		}

	}

	GetAudience (Data)
	{
		return System.post(`${this.URLWriter}/get_user/${new Date().getTime()}`, Data);
	}

	GetTopic (Data)
	{
		return System.post(`${this.URLWriter}/get_topic/${new Date().getTime()}`, Data);
	}

	GetTag(Data)
	{
		return System.post(`${this.URLWriter}/get_tag/${new Date().getTime()}`, Data);
	}

	SaveLimitQuestion (Data)
	{
		Data.set("ArticleID", this.ArticleID);

		Data.set("IsEdit", this.IsEdit);

		return System.post(`${this.URLWriter}/save_limit_question/${new Date().getTime()}`, Data).then( (Response) => {

			if (this.Setted == false)
			{
				this.SetArticle(Response);
			}
		});
	}

	SaveLimitTime (Data)
	{
		Data.set("ArticleID", this.ArticleID);

		Data.set("IsEdit", this.IsEdit);
		
		return System.post(`${this.URLWriter}/save_limit_time/${new Date().getTime()}`, Data).then( (Response) => {

			if (this.Setted == false)
			{
				this.SetArticle(Response);
			}
		});
	}

	SaveLimitTry (Data)
	{
		Data.set("ArticleID", this.ArticleID);

		Data.set("IsEdit", this.IsEdit);
		
		return System.post(`${this.URLWriter}/save_limit_try/${new Date().getTime()}`, Data).then( (Response) => {

			if (this.Setted == false)
			{
				this.SetArticle(Response);
			}
		});
	}

	SaveTag (Data)
	{
		Data.set("ArticleID", this.ArticleID);
		
		Data.set("IsEdit", this.IsEdit);
		
		return System.post(`${this.URLWriter}/save_tag/${new Date().getTime()}`, Data).then( (Response) => {

			if (this.Setted == false)
			{
				this.SetArticle(Response);
			}
		});
	}

	SaveTopic (Data)
	{
		Data.set("ArticleID", this.ArticleID);
		
		Data.set("IsEdit", this.IsEdit);
		
		return System.post(`${this.URLWriter}/save_topic/${new Date().getTime()}`, Data).then( (Response) => {

			if (this.Setted == false)
			{
				this.SetArticle(Response);
			}
		});
	}

	SaveAudienceMode (Data)
	{
		Data.set("ArticleID", this.ArticleID);
		
		Data.set("IsEdit", this.IsEdit);
		
		return System.post(`${this.URLWriter}/save_audience_mode/${new Date().getTime()}`, Data).then( (Response) => {

			if (this.Setted == false)
			{
				this.SetArticle(Response);
			}
		});
	}

	SaveAudience (Data)
	{
		Data.set("ArticleID", this.ArticleID);

		Data.set("IsEdit", this.IsEdit);
		
		return System.post(`${this.URLWriter}/save_audience/${new Date().getTime()}`, Data);
	}

	SaveGuide (Data)
	{
		Data.set("ArticleID", this.ArticleID);

		Data.set("IsEdit", this.IsEdit);
		
		return System.post(`${this.URLWriter}/save_guide/${new Date().getTime()}`, Data).then( (Response) => {

			if (this.Setted == false)
			{
				this.SetArticle(Response);
			}
		});
	}

	DeleteTag (Data)
	{
		Data.set("ArticleID", this.ArticleID);

		Data.set("IsEdit", this.IsEdit);
		
		return System.post(`${this.URLWriter}/delete_tag/${new Date().getTime()}`, Data);
	}

	DeleteAudience (Data)
	{
		Data.set("ArticleID", this.ArticleID);
		
		Data.set("IsEdit", this.IsEdit);
		
		return System.post(`${this.URLWriter}/delete_audience/${new Date().getTime()}`, Data);
	}
}

System.SettingModel = SettingModel;

})();