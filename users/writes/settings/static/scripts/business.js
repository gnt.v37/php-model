/*
* @Author: Rot
* @Date:   2017-11-04 00:40:04
* @Last Modified by:   Rot
* @Last Modified time: 2017-12-08 20:24:01
*/
(function () {

class BaseSetting 
{
	constructor ()
	{
		this.Writer = new System.Writer({

			Placeholder: {
				"data-topic": "Write us known your article topic ...",
				"data-tag": "Write your tags ...", 
				"data-limit": "Writer the limit questions"
			}
			
		});

		this.TopicBox = document.querySelector("[model-topic]");

		this.AudienceBox = document.querySelector("[model-audience]");

		this.TagBox = document.querySelector("[model-tag]");

	}

	Audience (Parent, Description, Element, Key)
	{
		this.RemoveActive(Parent);

		Element.classList.add("ge-active");

		var Children = Description.children;

		this.RemoveActive(Description);

		Children[Key].classList.add("ge-active");
	}

	ResolveTag (Container, Element)
	{
		var Content = Element.textContent.trim();

		if (Content.length == 0)
		{
			return false;
		}

		var Node = document.createElement("li");

		Element.innerHTML = "";

		this.Placeholder(Element);

		Node.classList.add("ge-inline", "ge-relative", "ge-tag-item")

		Node.innerHTML = `<div><a target="blank" class="ge-tag">${Content}</a></div>
							<div class="ge-remove" tag=""><button><span>x</span></button></div>`;

		Container.appendChild(Node);
	}

	RemoveActive (Parent)
	{
		var Children = Parent.children, Child;

		for (Child of Children)
		{
			if (Child.classList.contains("ge-active"))
			{
				Child.classList.remove("ge-active");
			}
		}
	}

	Placeholder (Element)
	{
		this.Writer.Placeholder(Element);
	}

	GetTopicText (TopicInput, Element)
	{
		TopicInput.innerHTML = System.TextContent(Element.firstElementChild);

		this.TopicBox.classList.remove("ge-block");
	}

	ResolveAudience (Container, AudienceInput, Element)
	{
		AudienceInput.innerHTML = "";

		this.AudienceBox.classList.remove("ge-block");

		var Node = document.createElement("li");

		this.Placeholder(AudienceInput);

		Node.classList.add("ge-inline", "ge-relative", "ge-tag-item")

		Node.innerHTML = `<div><a target="blank" class="ge-tag">${Element.textContent}</a></div>
							<div class="ge-remove" audience="${Element.getAttribute("data")}"><button><span>x</span></button></div>`;

		Container.appendChild(Node);
	}

	LimitQuestion (Element)
	{
		let ClassList = Element.classList;

		let Data = parseInt(Element.textContent);

		if (Data != NaN && Data <= parseInt(Element.getAttribute("max")))
		{
			if (ClassList.contains("ge-error-text"))
			{
				ClassList.remove("ge-error-text");
			}
		}
		else
		{
			ClassList.add("ge-error-text");

			return false;
		}

		return true;
	}

	LimitTime (Parent, Element, e)
	{
		var Data = Element.getAttribute("data");

		if (Data == "infinite")
		{
			if (Element.classList.contains("ge-active"))
			{
				return false;
			}

			this.Tick(Parent, Element);

			return true;
		}

		var Infinite = Parent.querySelector("[data=infinite]").classList,
			IsActive = Element.classList;

		if (Infinite.contains("ge-active"))
		{
			Infinite.remove("ge-active");
		}

		if (IsActive.contains("ge-active"))
		{
			if (e.target.closest("[data-time]") == null)
			{
				IsActive.remove("ge-active");
			}
		}
		else
		{
			IsActive.add("ge-active");
		}

		if (Parent.querySelector(".ge-active") == null)
		{
			Infinite.add("ge-active");
		}

		return true;
	}

	LimitTry(Parent, Element)
	{
		if (Element.classList.contains("ge-active") && Element.getAttribute("data") == "many")
		{
			return false;
		}

		this.Tick(Parent, Element);

		return true;
	}

	Tick (Parent, Element)
	{
		this.RemoveActive(Parent);

		Element.classList.add("ge-active");
	}
}

class Setting extends BaseSetting
{
	constructor ()
	{
		super();
	}
}

class SettingBusiness extends Setting
{
	constructor ()
	{
		super();

		this.AudienceData 		= new FormData();

		this.LimitQuestionData 	= new FormData();

		this.LimitTimeData 		= new FormData();

		this.LimitTryData 		= new FormData();

		this.GuideData 			= new FormData();

		this.TopicData 			= new FormData();

		this.AudienceData 		= new FormData();

		this.TagData 			= new FormData();

		this.Model 				= new System.SettingModel(System.URLResolver());
	}

	GetTag (Element)
	{
		var Content = Element.textContent.trim();

		if (Content.length == 0)
		{
			return Promise.resolve();
		}

		this.TagData.set("Tag", Content);

		return this.Model.GetTag(this.TagData).then( (Response) => {

			var Matches = Response.match(/^\[.*?\]/);

			var Jsons = JSON.parse(Matches);

			var Container = this.TagBox.querySelectorAll("[data-tag]");

			Container.forEach( (TagElement, Key) => {

				let Json = Jsons[Key];

				if (Json)
				{
					var ClassList = TagElement.classList;

					TagElement.setAttribute("data", Json["tag_id"]);

					TagElement.firstElementChild.innerHTML = Json["tag"];

					if (ClassList.contains("ge-none"))
					{
						ClassList.remove("ge-none");
					}
				}
				else
				{
					TagElement.classList.add("ge-none");
					
				}
				
			})

			var Genone = this.TagBox.querySelectorAll(".ge-none");

			if (Genone && Genone.length == 8)
			{
				this.TagBox.classList.remove("ge-block");
			}
			else
			{
				this.TagBox.classList.add("ge-block");
			}
		})
	}

	GetTopic (Element)
	{
		var Content = Element.textContent.trim();

		if (Content.length == 0)
		{
			return Promise.resolve();
		}

		this.TopicData.set("Topic", Content);

		return this.Model.GetTopic(this.TopicData).then( (Response) => {

			var Matches = Response.match(/^\[.*?\]/);

			var Jsons = JSON.parse(Matches);
			var Container = this.TopicBox.querySelectorAll("[data-topic]");

			Container.forEach( (TopicElement, Key) => {

				let Json = Jsons[Key];

				if (Json)
				{
					var ClassList = TopicElement.classList;

					TopicElement.setAttribute("data", Json["topic_id"]);

					TopicElement.firstElementChild.innerHTML = Json["topic"];

					if (ClassList.contains("ge-none"))
					{
						ClassList.remove("ge-none");
					}
				}
				else
				{
					TopicElement.classList.add("ge-none");
					
				}
				
			})

			var Genone = this.TopicBox.querySelectorAll(".ge-none");

			if (Genone && Genone.length == 6)
			{
				this.TopicBox.classList.remove("ge-block");
			}
			else
			{
				this.TopicBox.classList.add("ge-block");
			}
		});
	}

	GetAudience (Element)
	{
		var Content = Element.textContent.trim();

		if (Content.length == 0)
		{
			return Promise.resolve();
		}

		this.AudienceData.set("UserName", Content);

		return this.Model.GetAudience(this.AudienceData).then( (Response) => {

			var Matches = Response.match(/^\[.*?\]/);

			var Jsons = JSON.parse(Matches);

			var Container = this.AudienceBox.querySelectorAll("[data-audience]");

			Container.forEach( (AudienceElement, Key) => {

				let Json = Jsons[Key];

				if (Json)
				{
					var ClassList = AudienceElement.classList;

					AudienceElement.setAttribute("data", Json["user_id"]);

					AudienceElement.firstElementChild.innerHTML = Json["lastname"] + " " + Json["firstname"];

					if (ClassList.contains("ge-none"))
					{
						ClassList.remove("ge-none");
					}
				}
				else
				{
					AudienceElement.classList.add("ge-none");
					
				}
				
			})

			var Genone = this.AudienceBox.querySelectorAll(".ge-none");

			if (Genone && Genone.length == 6)
			{
				this.AudienceBox.classList.remove("ge-block");
			}
			else
			{
				this.AudienceBox.classList.add("ge-block");
			}
		});
	}

	SaveLimitQuestion (Element)
	{
		let Data = parseInt(Element.innerHTML);

		console.log(Data)

		if (Data >= 0)
		{
			this.LimitQuestionData.set("Questions", Data);

			return this.Model.SaveLimitQuestion(this.LimitQuestionData);
		}

		return Promise.resolve();
	}

	SaveLimitTime (Parent, Element)
	{

		if (Element.getAttribute("data") == "infinite" && Element.classList.contains("ge-active"))
		{
			this.LimitTimeData.set("Times", 0);
		}
		else
		{
			var Actives = Parent.getElementsByClassName("ge-active"), Data = 0;


			for (let Active of Actives)
			{
				var TimeElement = Active.querySelector("[ge-time]"),
					Time = TimeElement.getAttribute("data-time");

				switch (Time)
				{
					case "hour":
						Data = Data + parseFloat(TimeElement.value) * 3600;
						break;
					case "minute":
						Data = Data + parseFloat(TimeElement.value) * 60;
						break;
					case "second":
						Data = Data + parseInt(TimeElement.value);
						break;
				}


			}

			this.LimitTimeData.set("Times", parseInt(Data));
		}

		return this.Model.SaveLimitTime(this.LimitTimeData);
	}

	SaveLimitTry (Parent, Element)
	{
		if (Element.getAttribute("data") == "many" && Element.classList.contains("ge-active"))
		{
			this.LimitTryData.set("Times", 0);
		}
		else
		{
			var Time;
			if (Element.hasAttribute("data-time"))
			{
				Time = Element.value;
			}
			else
			{
				Time = Element.querySelector("[data-time]").value;
			}
			
			this.LimitTryData.set("Times", parseInt(Time)); 
		}

		return this.Model.SaveLimitTry(this.LimitTryData);
	}

	SaveAudienceMode (Element, Key)
	{
		this.AudienceData.set("AudienceID", Key + 1);

		return this.Model.SaveAudienceMode(this.AudienceData);
	}

	SaveAudience (Container, AudienceInput, Element)
	{
		this.AudienceData.set("AudienceID", Element.getAttribute("data"));

		this.AudienceData.delete("UserName");

		return this.Model.SaveAudience(this.AudienceData).then( () => {

			this.ResolveAudience(Container, AudienceInput, Element);

		});
	}

	SaveGuide (ParentElment, Element, Key)
	{
		this.GuideData.set("IsGuide", 1 - Key);

		return this.Model.SaveGuide(this.GuideData).then( () => {
			this.Tick(ParentElment, Element);
		});
	}

	SaveTopic (Element)
	{
		var Content = Element.textContent.trim();

		if (Content.length == 0)
		{
			return Promise.resolve();
		}

		this.TopicData.set("Topic", Content);

		return this.Model.SaveTopic(this.TopicData);
	}

	SaveTag (Container, Element)
	{
		var Content = Element.textContent.trim();

		this.TagBox.classList.remove("ge-block");

		if (Content.length == 0)
		{
			return Promise.resolve();
		}

		this.TagData.set("Tag", Content);

		return this.Model.SaveTag(this.TagData).then( () => {
			this.ResolveTag(Container, Element)
		});
	}

	DeleteTag (Element)
	{
		var Parent = Element.parentElement;

		this.TagData.set("TagID", Element.getAttribute("tag"));

		this.TagData.delete("Tag");

		return this.Model.DeleteTag(this.TagData).then(function () {

			Parent.remove();

		})
	}

	DeleteAudience (Element)
	{
		var Parent = Element.parentElement;

		console.log(Parent);

		this.AudienceData.set("AudienceID", Element.getAttribute("audience"));

		this.AudienceData.delete("UserName");

		return this.Model.DeleteAudience(this.AudienceData).then(function () {

			Parent.remove();

		})
	}
}

System.SettingBusiness = SettingBusiness;

})();