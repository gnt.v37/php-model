<?php

/**
 * @Author: Rot
 * @Date:   2017-11-20 11:33:53
 * @Last Modified by:   Rot
 * @Last Modified time: 2017-12-16 00:59:21
 */

namespace System\Deeps;

use System\Models\Schema;

class SettingDeep
{
	public function GetAudienceModes ()
	{
		return $this->Audience->Get();
	}

	public function GetArticleID ()
	{
		$Form = $this->SettingForm;

		if ($Form->IsEdit)
		{
			$Count = $this->Article->Count(["article_id" => $Form->ArticleID, "user_id" => $Form->UserID]);

			if ($Count["Total"])
			{
				return $Form->ArticleID;
			}
		
		}
		else
		{
			$Count = $this->DraftArticle->Count(["draft_article_id" => $Form->ArticleID, "user_id" => $Form->UserID]);

			if ($Count["Total"])
			{
				return $Form->ArticleID;
			}
		}

		if ($Form->ArticleID == null)
		{
			return null;
		}

		return ["Permission" => 403];
	}

	public function GetIsEdit ()
	{
		return $this->SettingForm->IsEdit;
	}

	public function GetArticleSetting ()
	{
		return isset($this->ArticleSettings[0]) ? $this->ArticleSettings[0] : null;
	}

	public function GetArticleAudience ()
	{
		return isset($this->ArticleAudiences) ? $this->ArticleAudiences : null;
	}

	public function GetArticleTags ()
	{
		return isset($this->ArticleTags) ? $this->ArticleTags : null;
	}

	public function GetTopics ()
	{
		return $this->Topic->Remember(
			Schema::Contains(["topic" => $this->SettingForm->Topic])
		)->Limit(6)->Get();
	}

	public function GetTags ()
	{
		return $this->Tag->Remember(
			Schema::Contains(["tag" => $this->SettingForm->Tag])
		//	"tag_id" => Schema::NotIn(["article_id" => $this->SettingForm->ArticleID])
		)->Limit(8)->Get();
	}

	public function GetPubish ()
	{
		$Form = $this->SettingForm;

		if ($Form->IsEdit == false)
		{
			return $this->DraftArticle->Remember(["draft_article_id" => $Form->ArticleID, "user_id" => $Form->UserID], true)
				->Get("publish_at");
		}
		else
		{
			return null;
		}
	}

	public function GetUsers ()
	{
		$Username = $this->SettingForm->UserName;

		$UserID = $this->SettingForm->UserID;

		return $this->User->Chain(["user_follows" => "follower"])
			->Remember([
				"user_id" => Schema::NotIn(["user_id" => $UserID]), 
				"following" => $UserID,
				Schema::Or(Schema::Contains(["firstname" => $Username, "lastname" => $Username])),
			])->Limit(4)->Get(["users.user_id", "firstname", "lastname"]);
	}

	public function GetLimitQuestion ()
	{
		if ($this->SettingForm->IsEdit)
		{
			return $this->Article->Chain(["article_questions" => "article_id"])->Count(
				["articles.article_id" => $this->SettingForm->ArticleID, "user_id" => $this->SettingForm->UserID]
			);
		}
		else
		{
			return $this->DraftArticle->Chain(["draft_article_questions" => "draft_article_id"])->Count(
				["draft_articles.draft_article_id" => $this->SettingForm->ArticleID, "user_id" => $this->SettingForm->UserID]
			);
		}	
	}
}
?>