/*
* @Author: Rot
* @Date:   2017-10-03 12:13:36
* @Last Modified by:   Rot
* @Last Modified time: 2017-12-17 10:13:40
*/
(function () {

class BaseDescription
{
	constructor ()
	{

	}

	InvokeWrite (Business, Element)
	{
		System.Process = System.Process.then(function () {

			return new Promise(function (Resolve, Reject) {

				setTimeout(function () {
					Business.Write(Element).then(function () {
						Resolve();
					});
				}, 1024)
						

			})
		})
	}

	InvokeEditor (Business, Element)
	{
		let Sleep = false;

		Business.Writer.Placeholder(Element);

		Element.addEventListener("keydown", (e) => {

			switch(e.which)
			{
				case 83:
					if (e.ctrlKey && e.altKey)
					{
						Business.TextPlain(Element);
					}
					break;
				case 86:
					if (e.shiftKey)
					{
						Business.TextPlain(Element);
					}
					break;

				case 67:
					if (e.ctrlKey == true)
					{
						Sleep = true;
					}
					break;
				case 8:

					var TheNew = Business.BackSpace(Element, e.ctrlKey);

					if (TheNew)
					{
						Business.Writer.Placeholder(TheNew);

						this.InvokeEditor(Business, TheNew);
					}

					if (Business.RemoveMath())
					{
						e.preventDefault();
					}
					break;
				case 13:

					if (e.shiftKey == false)
					{
						e.preventDefault();
					}

					break;
			}

		})

		Element.addEventListener("paste", (e) => {

			Sleep = true;

			setTimeout( () => {

				Business.Paste(Element);

				Business.ResolveText(Element);

				this.InvokeWrite(Business, Element);

			}, 128)

		})

		Element.addEventListener("keyup", (e) => {

			switch(e.which)
			{
				case 192:

					Business.MathJax(Element);
					
					break;
				
				case 13:

					if (e.shiftKey == false)
					{
						var TheNew = Business.NewLine(Element, e.ctrlKey);

						this.InvokeEditor(Business, TheNew);

						setTimeout( () => {

							System.Process = System.Process.then( () => {

								return new Promise( (Resolve, Reject) => {

									Business.Transform(Element, (TheNew) => {

										if (TheNew)
										{
											var ImageMarks = TheNew.querySelectorAll(".ge-figure-mark");

											var Editors = TheNew.querySelectorAll(`[contenteditable="true"]`);

											this.InvokeEditors(Business, Editors);

											this.InvokeMarkImages(Business, ImageMarks);

										}

									}).then(function () {
										
										Resolve();
									});

								})
							})

						}, 128)
							

						return null;
					}

					break;
			

			}

			Business.ResolveText(Element);

			if (Sleep == true)
			{
				setTimeout(function () {
					Sleep = false;
				}, 2048)

				return null;
			}

			if (e.which !== 0)
			{
				var Code = String.fromCharCode(e.which);

				if (e.which == 8 || Code.match(/\w|\s/))
				{
					this.InvokeWrite(Business, Element);
				}
				
			}
			
			
		})
	}

	InvokeImage(Business, Element)
	{
		Element.addEventListener("click", function () {

			Business.Figure.ChangeMode(this);

			System.Process = System.Process.then(function () {

				return new Promise(function (Resolve, Reject) {

					Business.Write(null).then(function () {
						Resolve();
					});

				})

			})
				
		})
			
	}
}

class Description extends BaseDescription
{
	constructor (Options, Figures)
	{
		super();

		let Editors = document.querySelectorAll(`[contenteditable="true"]`);

		let MarkImages = document.querySelectorAll(".ge-figure-mark");

		let Business = new System.Business();

		this.InvokeEditors(Business, Editors);

		this.InvokeMarkImages(Business, MarkImages)
	}

	InvokeEditors (Business, Editors)
	{
		Editors.forEach( (Element) => {

			if (Element.children.length == 0 && Element.textContent.length == 0)
			{
				Element.innerHTML = "<br />";
			}

			this.InvokeEditor(Business, Element);


		});
	}

	InvokeMarkImages (Business, MarkImages)
	{
		MarkImages.forEach( (MarkImage) => {

			var Parent = MarkImage.parentElement.parentElement.parentElement;

			if (Parent.hasAttribute("style"))
			{
				Parent.removeAttribute("style");
			}

			this.InvokeImage(Business, MarkImage);

			

		})
	}
}

System.Description = Description;


})();









