(function () {

class BaseWriter
{
    constructor (Models)
    {
        if (Models)
        {
            this.Clone = Models.Clone;

            this.Blur = Models.Blur;

            this.URLImage = new FormData();

            this.Canvas = document.createElement("canvas");

            this.Image = new Image();

            this.CanvasContext = this.Canvas.getContext("2d");

            this.Page = document.querySelector(`[idata="page"]`).value;

            this.DataImage = new FormData();

        }
    }

    DrawImage (Images, Container, Datas, Callback)
    {
        return new Promise( (Resolve, Reject) => {

            let Keys = [];

            for (let Key in Images)
            {
                Keys.push(Key);
            }

            Keys.reduce( (Sequence, Key) => {

                return Sequence.then ( () => {

                    this.URLImage.set("URLImage", Images[Key]);

                    return System.post(this.Clone + `/${new Date().getTime()}`, this.URLImage).then( (CloneImage) => {

                        CloneImage = CloneImage.match(/^{.*?}/gi);

                        var Json = JSON.parse(CloneImage);

                        this.Image.src = this.Page + Json["Name"];

                        var Width = Json["Width"],

                            Height = Json["Height"],

                            Mime = Json["Mime"];

                        this.Canvas.width = Width;

                        this.Canvas.height = Height;

                        this.Image.onload = () => 
                        {
                            this.CanvasContext.drawImage(this.Image, 0, 0);

                            StackBlur.canvasRGB(this.Canvas, 0, 0, Width, Height, 180);

                            var Base64 = this.Canvas.toDataURL(Mime);

                            if (Datas)
                            {
                                for (let Data in Datas)
                                {
                                    this.DataImage.set(Data, Datas[Data]);
                                }
                            }

                            this.DataImage.set("ImageBase64", Base64);

                            this.DataImage.set("URLImage", Json["Name"]);

                            this.DataImage.set("Mime", Mime);

                            setTimeout( () => {

                                return System.post(this.Blur + `/${new Date().getTime()}`, this.DataImage).then( (BlurImage) => {

                                    console.log(BlurImage)

                                    BlurImage = BlurImage.match(/^{.*?}/gi);

                                    Json = JSON.parse(BlurImage);

                                    var Local = Container.querySelector(`#${Key}`);

                                    var Figure = Local.querySelector("figure");

                                    Local.querySelector(".ge-image-blur").src = this.Page + Json["Name"];

                                    Figure.setAttribute("image", Json["Name"]);

                                    Figure.setAttribute("data-image", Json["ID"]);

                                    Callback();

                                    Resolve();
                                    
                                });
                            }, 256)
                        }


                    }, function (Message) {
                        console.warn(Message)
                    })
                            
                })
            }, Promise.resolve());
        })
    }
}

class Writer extends BaseWriter
{
	constructor (Options)
    {
        super(Options.Models);

        this.PlaceholderData = Options.Placeholder;

        if (System.WriterTransform)
        {
             this.Transform = new System.WriterTransform({

                Paragraph: function (Paragraph)
                {
                    return `<div data="paragraph" class="ge-paragraph" ><ged role="textbox" contenteditable="true" >${Paragraph}<br /></ged></div>`;
                },

                Image: function (ID, Image)
                {
                    return `<div ge-figure="true" transform="true"><gef id=${ID}><div>
                    <figure><ge-image><img class="ge-image-blur" /><button type="button"><img data-src="${Image}" src="${Image}" /></button></ge-image><figcaption data="figure"><div role="textbox" ge-figure="true" class="ge-paragraph" contenteditable="true"><br /></div></figcaption></figure>
                    <div class="ge-figure-mark" data-mode="0"></div></div></gef></div>`;
                },

                IFrame: function (ID, IFrame)
                {
                    IFrame = IFrame.replace(/width="\d+"/, `width="100%"`);

                    return `<div ge-iframe="true" transform="true"><geif id=${ID}><div>
                    <div class="ge-relative"><ge-iframe><div><iframe ${IFrame}></iframe></div></ge-iframe>
                    <div data="iframe" class="ge-relative ge-caption">
                    <div role="textbox" ge-iframe="true" class="ge-paragraph" contenteditable="true"><br /></div></div></div>
                    <div class="ge-figure-mark" data-mode="0"></div></div></geif></div>`;
                }
            });
        }
    }

    TransformImage (Element)
    {
        if (!Element.nodeName || Element.nodeName === "#TEXT")
        {
            return this.Transform.Image(Element);
        }

        return this.Transform.Image(System.TextContent(Element));
    }

    TransformIFrame (Element)
    {
        return this.Transform.IFrame(Element.innerHTML);
    }

    Placeholder (Element, Update = false)
    {
        if (Element == null)
        {
            return null;
        }
        
        var Content = Element.textContent.trim(),
            Placeholder = Element.nextElementSibling;

        if (Placeholder)
        {
            let PlaceholderContent = Placeholder.textContent.trim();
            
            if (Content == "")
            {
                Placeholder.classList.add("ge-block");
            }
            else
            {
                Placeholder.classList.remove("ge-block");
            }

            if (PlaceholderContent == "" || PlaceholderContent == "undefined" || Update)
            {
                var Parent = Element.parentElement;

                if (Parent.hasAttribute("placeholder"))
                {
                    Message = Parent.getAttribute("placeholder");
                }
                else if (Parent.hasAttribute("data"))
                {
                    var Data = Parent.getAttribute("data");

                    Message = this.PlaceholderData[Data];
                    
                    Parent.setAttribute("placeholder", Message);
                }

                Placeholder.firstElementChild.innerHTML = Message;
            }
        }
        else
        {
            if (Content.length > 0)
            {
                return false;
            }

            var Node = document.createElement("div"),
                Parent = Element.parentElement,
                Message;

            if (Parent.hasAttribute("placeholder"))
            {
                Message = Parent.getAttribute("placeholder");
            }
            else if (Parent.hasAttribute("data"))
            {
                var Data = Parent.getAttribute("data");

                Message = this.PlaceholderData[Data];
                
                Parent.setAttribute("placeholder", Message);
            }

            Node.classList.add("ge-absolute", "ge-block");

            Node.innerHTML = `<div class="ge-placeholder">${Message}</div>`

            Parent.appendChild(Node);
        }
    }
}

System.Writer = Writer;

})();