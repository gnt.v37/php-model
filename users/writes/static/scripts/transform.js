/*
* @Author: Rot
* @Date:   2017-09-30 18:25:54
* @Last Modified by:   Rot
* @Last Modified time: 2017-12-11 13:05:15
*/
(function () {

class TransformParagraph
{
	constructor (FigureParagraph)
	{
		this.FigureParagraph = FigureParagraph;

		this.Node = document.createElement("div");

		this.Node.setAttribute("container", "true");
	}

	Transform (Content)
	{
		this.Node.innerHTML = Content;

		var Figure = this.Node.querySelector("[transform]");

		var Children = Figure.parentElement.childNodes;

		let Transform = "";

		for (let Child of Children)
		{
			if (Child.parentElement.hasAttribute("ge-transfrom-parent") == false)
			{
				Child.parentElement.setAttribute("ge-transfrom-parent", "true");
			}

			if (Child.nodeName == "BR")
			{
				continue;
			}

			if (Child.nodeName == "#text")
			{
				var Clone = this.Node.cloneNode(true);
				
				var ParentClone = Clone.querySelector("[ge-transfrom-parent]");

				if (ParentClone)
				{
					ParentClone.innerHTML = Child.textContent.trim();
				}
				else
				{
					Clone.innerHTML = Child.textContent.trim();
				}
				

				Transform += this.FigureParagraph(Clone.innerHTML);
			}
			else
			{
				Child.setAttribute("transform", "true");

				if (Child.hasAttributes("transform") == false && Child.textContent.trim().length == 0)
				{
					continue;
				}
				
				if (Child.getAttribute("transform"))
				{
					Transform += Child.outerHTML;
				}
				else
				{
					var Clone = this.Node.cloneNode(true);

					var Parent = Clone.querySelector(`[ge-transfrom-parent]`).parentElement;

					Parent.innerHTML = "";

					Parent.appendChild(Child);

					Transform += this.FigureParagraph(Clone.innerHTML);
				}
			}

		}

		this.Node.innerHTML = Transform;

		return Transform;
	}
}

class TransformIFrame 
{
	constructor (FigureParagraph, FigureIFrame)
	{
		this.FigureIFrame = FigureIFrame;

		this.Paragrahp = new TransformParagraph(FigureParagraph);
	}

	IsIFrame (Content)
	{
		const FullRegex = /(<|&lt;)iframe.*?\/iframe(>|&gt;)/g;

		return Content.match(FullRegex);
	}

	Transform (Content)
	{
		let IFrames = this.IsIFrame(Content);

		let IFrameDatas = [];

		if (IFrames)
		{
			let Transform = Content;

			let Exists = "";

			for (let IFrame of IFrames)
			{
				if (Exists.match(IFrame) == null)
				{
					let Key = `f${randomString(8)}`;

					let IFrameAttributes = IFrame.match(/(iframe)(.*?)(>|&gt;)/);

					IFrameDatas[Key] = IFrame;
					
					Transform = Transform.replaceAll(IFrame, this.FigureIFrame(Key, IFrameAttributes[2]));

					Exists += ` ${IFrame}`;

				}
			}

			Transform = this.Paragrahp.Transform(Transform);
			
			return {IFrames: IFrames, Transform: Transform};
		}

		return null;
	}
}

class TransformImage 
{
	constructor (FigureParagraph, FigureImage)
	{
		this.FigureImage = FigureImage;

		this.Paragrahp = new TransformParagraph(FigureParagraph);
	}

	IsImage (Content)
	{
		const FullRegex = /http.*?\.(jpg|jpeg|png)/gi;

		return Content.match(FullRegex);
	}

	Transform (Content)
	{
		let Images = this.IsImage(Content);

		let ImageDatas = [];

		if (Images)
		{
			let Transform = Content;

			let Exists = "";

			for (let Image of Images)
			{
				if (Exists.match(Image) == null)
				{
					let Key = `f${randomString(8)}`;

					ImageDatas[Key] = Image;
					
					Transform = Transform.replaceAll(Image, this.FigureImage(Key, Image));

					Exists += ` ${Image}`;
				}
			}

			Transform = this.Paragrahp.Transform(Transform);

			return {Images: ImageDatas, Transform: Transform};
		}

		return null;
	}
}

class WriterTransform
{
	constructor (Options)
	{
		this.TransformImage = new TransformImage(Options.Paragraph, Options.Image);

		this.TransformIFrame = new TransformIFrame(Options.Paragraph, Options.IFrame);
	}

	Image (Content)
	{
		return this.TransformImage.Transform(Content);
	}

	IFrame (Content)
	{
		return this.TransformIFrame.Transform(Content);
	}
}


System.WriterTransform = WriterTransform;

})();