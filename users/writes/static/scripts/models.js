/*
* @Author: Rot
* @Date:   2017-10-27 08:30:11
* @Last Modified by:   Rot
* @Last Modified time: 2017-12-08 18:10:34
*/
(function () {

class Models 
{
	constructor (Options)
	{
		this.URLWriter = Options.URLWriter;

		this.ArticleID = Options.ArticleID;

		this.IsEdit = Options.IsEdit;

		if (this.ArticleID)
		{
			this.Setted = true;
		}
		else
		{
			this.Setted = false;
		}

	}

	SetArticle (Response)
	{
		Response = Response.match(/^{.*?}/gi);

		var Json = JSON.parse(Response);

		if (Json)
		{
			let ArticleID = Json.Result;

			let ALinks = document.querySelectorAll(".ge-steps a");

			ALinks.forEach(function (ALink) {
				ALink.href = ALink.href + "/" + ArticleID;
			})

			this.ArticleID = ArticleID;

			var Url = `${location.href}/${this.ArticleID}`;

			history.pushState(null, null, Url);

			this.Setted = true;
		}

	}

	WriteTitle (Data)
	{
		Data.set("ArticleID", this.ArticleID);

		Data.set("IsEdit", this.IsEdit);

		return System.post(`${this.URLWriter}/save_title/${new Date().getTime()}`, Data).then( (Response) => {

			if (this.Setted == false)
			{
				this.SetArticle(Response);
			}
			
		});
	}

	WriteDescription (Data)
	{
		Data.set("ArticleID", this.ArticleID);

		Data.set("IsEdit", this.IsEdit);

		return System.post(`${this.URLWriter}/save_description/${new Date().getTime()}`, Data);
	}

	WriteDescriptionDetail (Data)
	{
		Data.set("ArticleID", this.ArticleID);

		Data.set("IsEdit", this.IsEdit);

		return System.post(`${this.URLWriter}/save_description_detail/${new Date().getTime()}`, Data).then( (Response) => {

			if (this.Setted == false)
			{
				this.SetArticle(Response);
			}
			
		});
	}

	SaveImage (Data)
	{
		Data.set("ArticleID", this.ArticleID);

		Data.set("IsEdit", this.IsEdit);

		return System.post(`${this.URLWriter}/save_image/${new Date().getTime()}`, Data);
	}

	CloneImage (Data)
	{
		Data.set("ArticleID", this.ArticleID);

		Data.set("IsEdit", this.IsEdit);

		return System.post(`${this.URLWriter}/clone_image/${new Date().getTime()}`, Data);
	}
	
	BlurImage (Data)
	{
		Data.set("ArticleID", this.ArticleID);

		Data.set("IsEdit", this.IsEdit);

		return System.post(`${this.URLWriter}/blur_image/${new Date().getTime()}`, Data);
	}

	RemoveImage(Data)
	{
		Data.set("ArticleID", this.ArticleID);

		Data.set("IsEdit", this.IsEdit);
		
		return System.post(`${this.URLWriter}/delete_image/${new Date().getTime()}`, Data);
	}
}

System.Models = Models;

})();