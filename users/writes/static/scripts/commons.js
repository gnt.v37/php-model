(function () {

class Publish
{
	constructor ()
	{
		this.Model = document.createElement("model");

		this.Model.setAttribute("model-publish", "true");

		this.Model.setAttribute("data-model", "ge-publish");

		this.Page = document.querySelector(`[idata="page"]`).value;

		this.ArticleID = document.querySelector("[data-article-id]").value;

		this.Model.innerHTML = this.HTMLModel(document.querySelector("[data-publish-id]").value);

		this.Schedule();

		this.Publish();
	}

	HTMLModel (Publish)
	{
		return `<div><div class="publish-wrapper"><model-header>
			<div><h4 class="ge-text-dark ge-publish-header">${ Publish ? "Article scheduled for<br/>" + moment(Publish).format("MMMM Do YYYY") : "Ready to publish?" }</h4></div>
		</model-header><model-content class="ge-text-color"><div><ul><li class="ge-tab-light ge-line ge-tag-note"><div>
		Add or change tags (up to 5) so readers know what your story is about:</div><div class="ge-goto-tag">
		<a href="http://localhost/geye/write/${this.ArticleID}/settings" class="ge-color">Go to add tags</a></div></li>
		<li class="ge-tab-light ge-line ge-border-bottom">Tip: add a high resolution image to your story to capture people’s interest</li></ul>
		</div><div><ul><li class="ge-line"><div><a class="ge-flex" ge-schedule="false"><input type="radio" name="schedule" ${ Publish ? '' : 'checked="checked"' } /><ins class="ge-mark-type ge-inline"><span></span></ins><span>Not scheduled</span></a></div>
		<div class="ge-top-15">The article will be published when you click Publish</div></li>
		<li><div><a class="ge-flex" ge-schedule="true"><input type="radio" name="schedule" ${ Publish ? 'checked="checked"' : '' } /><ins class="ge-mark-type ge-inline"><span></span></ins><span>Scheduled to be published on</span></a>
		<div ${this.ArticleID  ? 'style="display: none;"' : "" }>Scheduling wil become available after you start writing</div></div>
		<div class="ge-top-10"><input type="datetime" ge-schedule-pick="true" class="ge-input" time="${ Publish }" ${ Publish ? '' : 'disabled="disabled"' }  /></div>
		<div class="ge-line">The article will be published automatically within five munutes of the specified time</div></li></ul></div></model-content><model-footer><div>
			<div><div class="ge-row"><div class="ge-left"></div>
			<div class="ge-publish-style ge-right"><button class="ge-color" ge-publish="true">Publish</button>
		</div></div></div></div></model-footer></div><div class="ge-popover-arrow"></div></div>`;
	}

	Schedule ()
	{
		let SchedulePick = this.Model.querySelector("[ge-schedule-pick]");

		let Schedules = this.Model.querySelectorAll("[ge-schedule]");

		let OpenButton = document.querySelector(".ge-publish"); 

		const ResolveModels = (PublishData) =>
		{
			if (this.ArticleID)
			{
				var Url = this.Page + "write/" + this.ArticleID + "/schedule";

				System.Process = System.Process.then( () => {

					return new Promise ( (Resolve, Reject) => {

						System.post(Url, PublishData).then( (Response) => {

							Response = Response.match(/^\[{.*?}\]/gi);

							var Json = JSON.parse(Response);

							Json = Json[0];

							if (Json.success)
							{
								var Title = this.Model.querySelector(".ge-publish-header");

								var ButtonText = OpenButton.querySelector("a");

								if (PublishData.has("Publish") == false || PublishData.get("Publish") == '')
								{
									Title.innerHTML = "Ready to publish?";

									ButtonText.innerHTML = "Publish";
								}
								else
								{
									Title.innerHTML = "Article scheduled for<br/>" + moment(PublishData.get("Publish")).format("MMMM Do YYYY")
								
									ButtonText.innerHTML = "Scheduled";
								}
							}
							else
							{
								this.Message("You need at least a image and a topic",  () => {
									this.Model.querySelector(`[ge-schedule="false"] input`).checked = true;
								});
							}

							Resolve();
						})

					})
				})
			}
			else
			{
				var Data = System.URLResolver();

				this.ArticleID = Data.ArticleID;

				e.preventDefault();
			}
		}

		let Picker = new MaterialDatetimePicker({

			default: moment().add(1, "hour")

		}).on("submit", function (Response) {

			SchedulePick.value = Response.format("MMMM Do YYYY, h:mm:ss a");

			SchedulePick.setAttribute("time", Response.format());

			let Publish = new FormData();

			Publish.set("Publish", Response.format())

			ResolveModels(Publish);

		})

		Schedules.forEach( (Schedule) => {

			Schedule.addEventListener("click",  (e) => {

				Schedule.querySelector("input").checked = true;

				var Pick = this.Model.querySelector("[ge-schedule-pick]");

				let Publish = new FormData();

				if (Schedule.getAttribute("ge-schedule") == "true")
				{
					var Valid = new Date().getTime() + 10 * 1000;

					var Time = SchedulePick.getAttribute("time");

					if (new Date(Time).getTime() > Valid)
					{
						Publish.set("Publish", Time);
					}
					else
					{
						Publish.delete("Publish");

						this.Message("You can’t schedule a article in the past. We’d like to avoid those sticky time paradoxes.");
					}

					Pick.disabled = false;
				}
				else
				{
					Pick.disabled = true;

					if (Picker.pickerEl)
					{
						Picker.close();
					}

				}

				ResolveModels(Publish);
				
			})

		})


		SchedulePick.addEventListener("focus", function () {

			if (location.href.match(/.*?\/\d+$/))
			{
				Picker.options.default = moment(this.getAttribute("time")).format();

				Picker.open();

				Picker.pickerEl.classList.add("ge-publish");
			}

		})

		if (OpenButton)
		{
			OpenButton.addEventListener("click",  () => {
				
				var Current;

				if (OpenButton.children.length == 1)
				{
					OpenButton.appendChild(this.Model);

					if (SchedulePick.getAttribute("time") == false)
					{
						Current = moment().add(1, "hour");
					}
					else
					{
						Current = moment(SchedulePick.getAttribute("time"));
					}
				}
				else
				{
					var ClassList = this.Model.classList;

					if (ClassList.contains("ge-block") == false)
					{
						ClassList.add("ge-block");

						if (SchedulePick.getAttribute("time") == false)
						{
							Current = moment().add(1, "hour");
						}
						else
						{
							Current = moment(SchedulePick.getAttribute("time"));
						}
					}
				}

				if (Current)
				{
					SchedulePick.value = Current.format("MMMM Do YYYY, h:mm:ss a");

					SchedulePick.setAttribute("time", Current.format());
				}
				
			})
		}
	}

	Publish ()
	{

		this.Model.classList.add("ge-block");

		this.Model.querySelector("[ge-publish]").addEventListener("click", () => {

			if (this.ArticleID)
			{
				var Url = this.Page + "write/" + this.ArticleID + "/publish";

				System.Process = System.Process.then( () => {

					return new Promise( (Resolve, Reject) => {
						
						return System.get(Url).then( (Response) => {

							Response = Response.match(/^\[{.*?}\]/gi);

							var Json = JSON.parse(Response);

							Json = Json[0];

							if (Json.success)
							{
								window.location.href = this.Page + "view?v=" + Json.success;
							}
							else
							{
								this.Message("You need at least a image and a topic");
							}
							//

							Resolve();
						})

					})

				})
			}
			else
			{
				var Data = System.URLResolver();

				this.ArticleID = Data.ArticleID;

				e.preventDefault();
			}
		})
	}


	Message (Message, Callback)
	{
		var Node = document.createElement("div");

		var Element = this.Model.querySelector(".ge-publish-style");

		Element.parentElement.insertBefore(Node, Element);

		Node.innerHTML = Message;

		Node.setAttribute("style", "color: crimson; padding-bottom: 12px;");

		setTimeout( () => {

			Node.remove();

			if (typeof Callback === "function")
			{
				Callback();
			}

			this.Model.querySelector(`[ge-schedule="false"] input`).checked = true;

		}, 10000)
	}
}

class Options
{
	constructor ()
	{
		let Idata = document.querySelector(`[idata="page"]`);


		this.Model = document.createElement("model");

		this.Page = Idata.value;

		this.User = Idata.previousElementSibling.value;

		this.ArticleID = document.querySelector("[data-article-id]").value;

		this.Model.innerHTML = this.HTMLModel();

		this.Model.setAttribute("model-options", "true");

		this.Model.setAttribute("data-model", "ge-options");

		this.Open();
	}

	HTMLModel ()
	{
		return `<div class="options-wrapper"><div><model-header><div>
			<h5 class="ge-text-dark">Action</h5></div></model-header>
			<model-content class="ge-text-color"><div><ul>
			<li class="ge-bottom-12" ge-delete="true" data-redirect="">
			<a data-href="" class="ge-option-action">Delete</a></li>
			</ul><ul>
				<li><h5>Help</h5></li>
				<li><a class="ge-option-action">Hints and keyboard shortcuts</a>
					<div class="ge-shortcut"><div class="ge-shortcut-wrapper"><div class="ge-shortcut-space"><ul>
						<li><center><h3>Keyboard shortcuts</h3></center><div>
							<div class="ge-row">
								<div class="ge-left">
									<div class="ge-ver-8"><h4>Patse</h4></div>
									<div><div class="ge-bottom-8"><h5>Image</h5></div>
									<div class="ge-bottom-8">https://mir-s3-cdn-cf.behance.net/a097b757450033.59d62aea604e0.jpg</div></div>
									<div><div class="ge-bottom-8"><h5>IFrame</h5></div>
									<div class="ge-bottom-8">&lt;iframe src="https://www.youtube.com/embed/eFXRQKYFbXE">&lt;/iframe></div></div>
								</div>
								<div class="ge-right">
									<div>
										<div class="ge-ver-8"><h4>Ctrl + Shift + V</h4></div>
										<div>Patse element with default style</div>
									</div>
									<div>
										<div class="ge-ver-8"><h4>Ctrl + Alt + S</h4></div>
										<div>Convert element to default style</div>
									</div>
									<div>
										<div class="ge-ver-8"><h4>Math</div>
										<div class="ge-bottom-6">` + "`" + "math expression" + "`"  + `</div>
										<div>Example` + "`" + "2x^2" + "`"  + `to 2x<sup>2</sup></div>
									</div>
								</div>
							</div>
					</div></li>
				</ul></div></div></div>
			</li>
			</ul></div></model-content></div><div class="ge-popover-arrow"></div></div>`;
	}


	DeleteArticle (Info)
	{
		this.Model.querySelector("[ge-delete]").addEventListener("dblclick",  () => {

			System.Process = System.Process.then( () => {

				return new Promise( (Resolve, Reject) => {

					System.get(Info.action).then( (Response) => {

						Response = Response.match(/^\[{.*?}\]/gi);

						var Json = JSON.parse(Response);

						Json = Json[0];

						if (Json.success)
						{
							window.location.href = Info.redirect;
						}
						
					})

				})	

			})
		});
	}

	Open ()
	{
		const OpenButton = document.querySelector(".ge-options");

		let Info = JSON.parse(OpenButton.querySelector("[ge-options-info]").value);		

		if (OpenButton)
		{
			OpenButton.addEventListener("click",  () => {
				
				if (this.User)
				{
					if (this.ArticleID)
					{
						if (OpenButton.lastElementChild.hasAttribute("model-options"))
						{
							var ClassList = this.Model.classList;

							if (ClassList.contains("ge-block") == false)
							{
								ClassList.add("ge-block");
							}
						}
						else
						{
							OpenButton.appendChild(this.Model);

							this.Model.classList.add("ge-block");

							this.DeleteArticle(Info);
						}
					}
					else
					{
						const URL = location.href.match(System.URLRegex);

						if (URL && URL[3])
						{
							this.ArticleID = URL[3].substring(1);

							Info = {redirect: Info.redirect, action: `${URL[1]}/delete_article/${this.ArticleID }`};

							OpenButton.click();

						}
					}
				}
				
			})
		}
	}
}


System.URLResolver = function ()
{
    var IsEdit = 0;

    if (System.URL[5] == "/edit" || System.URL[4] == "/edit")
    {
        IsEdit = 1;
    }

	if (System.URL)
	{
		var App = System.URL[1];

		var ArticleID = System.URL[3];

		if (System.URL[2])
		{
			App += System.URL[2];
		}

		if (ArticleID)
		{
			ArticleID = ArticleID.substring(1);
		}
		else
		{
			ArticleID = null;
		}

		return {URLWriter: App, "ArticleID": ArticleID, "IsEdit": IsEdit};
	}

	return {URLWriter: System.URL[1], ArticleID: null, IsEdit: 0};
}

System.TextContent = function (Editor, IsNode = true)
{
	var Content;

	if (IsNode)
	{
		Content = Editor.innerHTML.replace(/<script>/gi, "&lt;script&gt;");
	}
	else
	{
		Content = Editor.replace(/<script>/gi, "&lt;script&gt;");
	}

	Content  = Content.replace(/<\/script>/gi, "&lt;/script&gt;"),
	//Content  = Content.replace(/\t?\n?/gi, "");
	Content  = Content.replace(/ src=/gi, " data-src=");
	Content  = Content.trim();

	return Content;
}

System.CaretPosition = function (Control)
{
	var Position = {};

	if (Control.selectionStart && Control.selectionEnd)
	{
		Position.start = Control.selectionStart;
		Position.end = Control.selectionEnd;
	}
	else
	{
		var Range = document.selection.createRange();

		Position.start = (Range.offsetLeft - 1) / 7;

		Position.end = Position.start + (Range.text.length);
	}

	Position.length = Position.end - Position.start;

	return Position;
}

System.Publish = Publish;

System.Options = Options;

System.URLRegex = /^(.*?\/write)(\/[a-zA-Z]+)?(\/\d+)?(\/edit)?/;

System.URL = location.href.match(System.URLRegex);


setTimeout(function () {
	var Active = document.querySelector(".ge-steps .ge-active");

	var NextSibling = Active.nextElementSibling;

	var PreviousSibling = Active.previousElementSibling;

	var NextButton = document.querySelector(".ge-next-button");

	var PreviousButton = document.querySelector(".ge-previous-button");

	var PreviousClassList = PreviousButton.parentElement.parentElement.classList;

	var NextClassList = NextButton.parentElement.parentElement.classList;

	if (NextSibling)
	{
		if (NextClassList.contains("ge-none"))
		{
			NextClassList.remove("ge-none");
		}

		NextButton.href = NextSibling.querySelector("a").href;
	}
	else
	{
		if (NextClassList.contains("ge-none") == false)
		{
			NextClassList.add("ge-none");
		}
	}

	if (PreviousSibling)
	{
		if (PreviousClassList.contains("ge-none"))
		{
			PreviousClassList.remove("ge-none");
		}

		PreviousButton.href = PreviousSibling.querySelector("a").href;

	}
	else
	{
		if (PreviousClassList.contains("ge-none") == false)
		{
			PreviousClassList.add("ge-none");
		}
	}

}, 256);

	

})();