/*
* @Author: Rot
* @Date:   2017-10-06 00:43:09
* @Last Modified by:   Rot
* @Last Modified time: 2017-12-17 10:40:55
*/
(function () {

class BaseFigure
{
	constructor ()
	{

	}

	Enough (Container, Element)
	{
		var ElementHeight = 0, 
			ContainerHeight = Container.offsetHeight;
		
		var Next = Element.nextElementSibling;

		while (Next)
		{
			ElementHeight += Next.offsetHeight;

			if (ElementHeight < ContainerHeight - Next.offsetHeight)
			{
				console.log(ElementHeight)
				Container.appendChild(Next);

				Next = Element.nextElementSibling;
			}
			else
			{
				break;
			}

		}
	}

	Fill (Element)
	{
		var Sibling = document.createElement("ge-mode");

		Sibling.setAttribute("class", Element.parentElement.getAttribute("class"))

		var Next = Element.nextElementSibling;

		while (Next)
		{
			Sibling.appendChild(Next);

			Next = Element.nextElementSibling;
		}

		return Sibling;
	}
}

class Figure extends BaseFigure
{
	constructor ()
	{
		super();
	}
	
	ClearClass(Element)
	{
		Element.removeAttribute("class");

		Element.classList.add("ge-padding");
	}

	LeftMode (Element, Gemode)
	{
		var NewMode = Gemode;

		var Previous = Element.previousElementSibling;

		if (Previous)
		{
			NewMode = document.createElement("ge-mode");

			var Section = Gemode.parentElement;
		
			NewMode.appendChild(Element);

			Section.insertBefore(NewMode, Gemode.nextSibling);
		}
		else
		{
			Previous = Element;
		}
		
		
		if (Previous)
		{
			setTimeout( () => {
				var EnoughMode = this.Enough(NewMode, Previous);
				
				if (Section)
				{
					Section.insertBefore(this.Fill(Previous), NewMode.nextSibling);
				}
				

			}, 128);
		}

		NewMode.classList.add("ge-row", "ge-left-mode");

		return NewMode;
	}

	RightMode (Element, Gemode)
	{
		var Node = Gemode;

		if (Gemode.classList.contains("ge-left-mode") == false)
		{
			var Node = this.LeftMode(Element, Gemode);
		}

		Node.classList.remove("ge-left-mode");

		Node.classList.add("ge-row", "ge-right-mode");

	}

	NormalMode (Gemode)
	{
		this.ClearClass(Gemode)

		Gemode.classList.add("ge-small-mode");
	}

	MediumMode (Gemode)
	{
		this.ClearClass(Gemode)

		Gemode.classList.add("ge-medium-mode");
	}

	LargeMode (Gemode)
	{
		this.ClearClass(Gemode)

		Gemode.classList.add("ge-large-mode");
	}

	ChangeMode (Element)
	{
		let Mode = 0;

		if (Element.hasAttribute("data-mode"))
		{
			Mode = parseInt(Element.getAttribute("data-mode")) + 1;

			if (Mode > 4)
			{
				Mode = 0;
			}
		}

		Element.setAttribute("data-mode", Mode);

		if (Element)
		{
			var Gef = Element.parentElement;

			while (Gef.nodeName != "GEF")
			{
				Gef = Gef.parentElement;
			}

			Gef = Gef.parentElement;

			var ParentElement = Gef.parentElement;

			switch (Mode) 
			{
				case 0:

					this.NormalMode(ParentElement);
					break;
				case 1:

					this.LeftMode(Gef, ParentElement);
					
					break;
				case 2:

					this.RightMode(Gef, ParentElement);

					break;
				case 3:

					this.MediumMode(ParentElement);
					break;	

				case 4:
					this.LargeMode(ParentElement);
				default:
					break;
			}
		}
	}
}

class BaseBusiness
{
	constructor ()
	{
		this.Models = new System.Models(System.URLResolver());

		this.TitleData		 = new FormData();

		this.DescriptionData = new FormData();

		this.DescriptionDetailData = new FormData();

		this.URLImage = new FormData();

		this.DataImage = new FormData();

		this.DeleteImageData = new FormData();

		this.Page = document.querySelector(`[idata="page"]`).value;

		this.Image = new Image();

		this.Canvas = document.createElement("canvas");

		this.SaveWriteNode = document.createElement("div");

		this.CanvasContext = this.Canvas.getContext("2d");

	}

	Write (Element)
	{
		this.SaveWriteNode.innerHTML = this.Gedit.innerHTML;

		var Matjaxs = this.SaveWriteNode.querySelectorAll("mathjax");

		if (Matjaxs)
		{
			Matjaxs.forEach(function (Mathjax) {

				var MathjaxText = Mathjax.lastElementChild;

				if (MathjaxText)
				{
					Mathjax.innerHTML = "`" + MathjaxText.textContent + " <br>`";
				}

			})
			
		}

		this.DescriptionDetailData.set("DescriptionDetail", System.TextContent(this.SaveWriteNode));

		if (Element)
		{
			var Parent = Element.parentElement;

			if (Parent.hasAttribute("data-title"))
			{

				this.TitleData.set("Title", Element.textContent.trim());

				/**
				 * Save title first and wait 1024ms then save description.
				 */
				return this.Models.WriteTitle(this.TitleData).then( () => {

					this.Models.WriteDescriptionDetail(this.DescriptionDetailData);
					
				});
			}

			if (Parent.hasAttribute("data-description"))
			{
				this.DescriptionData.set("Description", Element.textContent.trim().substring(0, 256));

				return this.Models.WriteDescription(this.DescriptionData).then( () => {

					this.Models.WriteDescriptionDetail(this.DescriptionDetailData);
					
				});
			}
		}

		return this.Models.WriteDescriptionDetail(this.DescriptionDetailData);
	}

	WriteImage (Images, Container, Callback)
	{
		return new Promise( (Resolve, Reject) => {

			let Keys = [];

			for (let Key in Images)
			{
				Keys.push(Key);
			}

			Keys.reduce( (Sequence, Key) => {

				return Sequence.then ( () => {

					this.URLImage.set("URLImage", Images[Key]);

					return this.Models.CloneImage(this.URLImage).then( (CloneImage) => {

						console.log(CloneImage);

						CloneImage = CloneImage.match(/^{.*?}/gi);

						var Json = JSON.parse(CloneImage);

						this.Image.src = this.Page + Json["Name"];

						var Width = Json["Width"],

							Height = Json["Height"],

							Mime = Json["Mime"];

						this.Canvas.width = Width;

						this.Canvas.height = Height;

						this.Image.onload = () => 
						{
							this.CanvasContext.drawImage(this.Image, 0, 0);

							StackBlur.canvasRGB(this.Canvas, 0, 0, Width, Height, 180);

							var Base64 = this.Canvas.toDataURL(Mime);

							this.DataImage.set("ImageBase64", Base64);

							this.DataImage.set("URLImage", Json["Name"]);

							this.DataImage.set("Mime", Mime);

							setTimeout( () => {

								this.Models.BlurImage(this.DataImage).then( (BlurImage) => {

									BlurImage = BlurImage.match(/^{.*?}/gi);

									Json = JSON.parse(BlurImage);

									var Local = Container.querySelector(`#${Key}`);

									var Figure = Local.querySelector("figure");

									Local.querySelector(".ge-image-blur").src = this.Page + Json["Name"];

									Figure.setAttribute("image", Json["Name"]);

									Figure.setAttribute("data-image", Json["ID"]);

									Callback();

									this.Write(null).then(function () {
										console.log("?");
										Resolve();
									})
								});
							}, 256)
						}


					}, function (Message) {
						console.warn(Message)
					})
							
				})
			}, Promise.resolve());
		})
	}

	RemoveImage(Element, Callback)
	{
		var Image = Element.parentElement;

		while (Image.hasAttribute("image") == false || Image.nodeName != "FIGURE")
		{
			Image = Image.parentElement;
		}

		if (Image)
		{
			this.DeleteImageData.set("URLImage", Image.getAttribute("image"));

			this.DeleteImageData.set("ImageID", Image.getAttribute("data-image"));
			/**
			 * Injection
			 */
			return this.Models.RemoveImage(this.DeleteImageData).then(function (Response) {
				console.log(Response);
				
				Response = Response.match(/^{.*?}/gi);

				var Json = JSON.parse(Response);

				if (typeof Callback === "function" && (Json && Json.Result == 1) || Json == null)
				{
					Callback(Image);
				}
			})
		}
			
	}
}

class Business extends BaseBusiness
{
	constructor ()
	{
		super();

		this.Figure = new Figure(document.querySelector(".ge-image-mode"));

		this.Writer = new System.Writer({
			Placeholder: {
				paragraph: "Write something...", 
				figure: "Write caption for image...",
				iframe: "Write caption for iframe...",

			} 
		});

		this.Gedit = document.querySelector("[ge-edit]");

		this.NewElement = this.Gedit.nextElementSibling.querySelector("[default-element]").value;

		this.Node = document.createElement("div");

		this.ResolveText();
	}

	IsFigure (Element)
	{
		if (Element.nodeName != "GED" || Element.hasAttribute("ge-figure") || Element.parentElement.nodeName == "FIGCAPTION" || Element.querySelector("img"))
		{
			return true;
		}

		return false;
	}

	IsIFrame (Element)
	{
		if (Element.hasAttribute("ge-iframe") || Element.querySelector("iframe"))
		{
			return true;
		}

		return false;
	}

	Paste (Element)
	{
		var PasteElements = Element.getElementsByTagName("*");

		for (let PasteElement of PasteElements)
		{
			if (PasteElement.closest("mathjax"))
			{
				continue;
			}

			var Attributes 	= PasteElement.attributes,
				Length 		= Attributes.length,
				Attribute;

			for (var i = 0; i < Length; i++)
			{
				Attribute = Attributes[0].name;

				if (Attribute == "href" || Attribute == "src" || Attribute == "data-src" || Attribute == "style" || Attribute == "target")
					continue;

				PasteElement.removeAttribute(Attribute);
			}
		}
	}

	TextPlain (Element)
	{
		Element.innerHTML = Element.textContent.trim();
	}

	MathJax (Element)
	{
		var Content = Element.innerHTML;

		console.log(Element)

		var Matches = Content.match(/`.*?`/);

		if (Matches)
		{
			Element.innerHTML = Content.replace(Matches, `<mathjax>${Matches}</mathjax>&nbsp;`);

			var Children = Element.children;

			var Length = Children.length;

			var Child = null;

			for (var i = 0; i < Length; i++)
			{
				Child = Children[i];

				if (Child.nodeName == "MATHJAX" && Child.children.length == 0)
				{
					MathJax.Hub.Queue(["Typeset", MathJax.Hub, Child]);
				}
			}
			
		}
	}

	ResolveText (Element)
	{
		if (Element)
		{
			this.Writer.Placeholder(Element);
		}
		else
		{
			return null;
		}

		var GeModes = this.Gedit.querySelectorAll("ge-mode"),
			GeMode, Content, Children, Child, IsTitle = false;

		for (GeMode of GeModes)
		{
			Children = GeMode.children;

			for (Child of Children)
			{
				if (Child.hasAttribute("ge-figure"))
				{
					continue;
				}

				if (Child.firstElementChild == null)
				{
					return null;
				}

				Content = Child.firstElementChild.textContent.trim();

				if (IsTitle)
				{
					if (Content.length > 5)
					{
						if (Child.hasAttribute("data-description") == false)
						{
							var Description = this.Gedit.querySelector("[data-description]");

							if (Description)
							{
								Description.removeAttribute("data-description");
							}

							Child.setAttribute("data-description", true);
						}

						return null;
					}
				}
				else if (Content.length > 1)
				{
					if (Child.hasAttribute("data-title") == false)
					{
						var Title = this.Gedit.querySelector("[data-title]");

						if (Title)
						{
							Title.removeAttribute("data-title");
						}

						Child.setAttribute("data-title", true);
					}
					
					IsTitle = true;
				}
				
			}
		}
	}

	NewLine (Element, IsCtrl)
	{
		this.Node.innerHTML = this.NewElement;

		var Parent = Element.parentElement;
		
		var TheEditor = this.Node.querySelector(`[contenteditable="true"]`);

		var Gemode = null;

		this.Writer.Placeholder(TheEditor);

		if (this.IsFigure(Element) || this.IsIFrame(Element))
		{
			while (Parent)
			{
				if (Parent.nodeName == "GEF" || Parent.nodeName == "GEIF")
				{
					break;
				}

				Parent = Parent.parentElement;
			}

			Parent = Parent.parentElement;

		}
		else if (IsCtrl == false)
		{
			var Last = Element.lastElementChild;

			while (Last && Last.nodeName == "BR")
			{
				Last = Last.previousElementSibling;
			}

			if (Last)
			{
				Last.style.display = "block";

				Last.style.minHeight = "22px";

				var Clone = Last.cloneNode(true);

				Clone.innerHTML = "<br />";

				TheEditor.replaceChild(Clone, TheEditor.firstElementChild);
			}

		}

		Gemode = Parent.parentElement;

		if (Gemode.nodeName == "GE-MODE")
		{
			Gemode.insertBefore(this.Node.firstElementChild, Parent.nextSibling);
		}
		else 
		{
			var Section = this.Gedit.querySelector("section");

			if (Section)
			{
				Gemode = Section.lastElementChild;

				if (Gemode)
				{
					Gemode.appendChild(this.Node.firstElementChild);
				}
			}
		}

		setTimeout( () => 
		{
			TheEditor.focus();
			
		}, 128)

		return TheEditor;
	}

	BackSpace(Element)
	{
		var Children = Element.childNodes;

		if (Children.length == 0 || Children.length == 1 && Children[0].nodeName == "BR")
		{
			var Parent = Element.parentElement;

			var Previous = Parent.previousElementSibling;

			var Next = Parent.nextElementSibling;

			if (Element.hasAttribute("ge-figure") || Element.parentElement.nodeName == "FIGCAPTION")
			{
				this.RemoveImage(Parent, (Image) => {

					var Gef = Image.parentElement;

					while (Gef && Gef.nodeName != "GEF")
					{
						Gef = Gef.parentElement;
					}

					Gef = Gef.parentElement;

					var Gemode = Gef.parentElement;

					$(Gef).slideUp(500, () => {

						this.Figure.NormalMode(Gemode);

						if (Gef)
						{
							Gemode.removeChild(Gef);
						}
						
					})

					if (Gemode.children.length == 1)
					{
						this.Node.innerHTML = this.NewElement;

						var TheNew = this.Node.firstElementChild;

						Gemode.appendChild(TheNew);

						return TheNew.querySelector("[contenteditable]");
					}
				});
			}
			else if (Element.hasAttribute("ge-iframe"))
			{
				var Geif = Element.parentElement;

				while (Geif && Geif.nodeName != "GEIF")
				{
					Geif = Geif.parentElement;
				}

				Geif = Geif.parentElement;

				var Gemode = Geif.parentElement;

				this.Node.innerHTML = this.NewElement;

				var TheNew = this.Node.firstElementChild;

				Gemode.insertBefore(TheNew, Geif);

				$(Geif).slideUp(500, () => {

					if (Geif)
					{
						Gemode.removeChild(Geif);
					}
						
				})

				var Editor = TheNew.querySelector("[contenteditable]");

				Editor.focus();

				return Editor;
			}
			else
			{		
				var Gemode = Parent.parentElement;

				var Editor;

				if (Previous)
				{
					Editor = Previous.querySelector(`[contenteditable="true"]`);

					if (Editor)
					{
						Editor.focus();
					}
					else
					{
						Previous.remove();
					}
				}
				else if (Next)
				{
					Editor = Next.querySelector(`[contenteditable="true"]`);

					if (Editor)
					{
						Editor.focus();
					}
					else
					{
						Next.remove();
					}
				}

				if (Gemode.children.length == 1)
				{
					var Section = Gemode.parentElement;

					if (Section.children.length > 1)
					{
						Section.removeChild(Gemode);
					}
					else
					{
						Gemode.removeAttribute("class");

						Gemode.classList.add("ge-padding", "ge-small-mode");
					}

				}
				else
				{
					Parent.parentElement.removeChild(Parent);
				}

			}
			
		}
		else
		{
			var Table = Element.querySelector("table");

			if (Table)
			{
				Table.remove();
			}
		}
	}

	Transform (Element, Callback)
	{
		if (this.IsFigure(Element))
		{
			if (typeof Callback === "function")
			{
				Callback(null);
			}

			return Promise.resolve();
		}

		let Content = this.Writer.TransformIFrame(Element);

		var Placeholder = Element.nextElementSibling;

		let Temp = null;

		if (Content)
		{
			Temp = this.Writer.TransformImage(Content.Transform);
		}
		else
		{
			Temp = this.Writer.TransformImage(Element);
		}

		if (Temp)
		{
			Content = Temp;
		}

		if (Content)
		{
			var Parent = Element.parentElement;

			var Images = Content.Images;

			var Node = document.createElement("div");

			Node.innerHTML = Content.Transform;

			Element.innerHTML =  "";

			Placeholder.firstElementChild.innerHTML = "Waiting a few seconds...";

			Placeholder.classList.add("ge-block");

			if (typeof Callback === "function")
			{
				Callback(Node);
			}

			if (Images)
			{
				return this.WriteImage(Images, Node, () => {

					var Children = Node.children;

					var Length = Children.length;

					var Child;

					for (var i = 0; i < Length; i++)
					{
						Child = Children[0];

						Child.style.display = "none";

						Parent.parentElement.insertBefore(Child, Parent);

						$(Child).slideDown(400);
					}

					var TextElements = Element.getElementsByTagName("*");

					if (TextElements.length == 1 && TextElements[0].nodeName == "BR")
					{
						Element.innerHTML = "<br />";
					}
					else
					{
						for (let TextElement of TextElements)
						{
							let Children = TextElement.childNodes;

							for (let Child of Children)
							{
								if (Child.nodeName == "#text")
								{
									Child.remove();
								}
							}
						}
					}

					Element.focus();

					this.Writer.Placeholder(Element, true);
					
				})
			}
			else
			{
				var IFrame = Content.IFrame;

				var Children = Node.children;

				var Length = Children.length;

				var Child;

				for (var i = 0; i < Length; i++)
				{
					Child = Children[0];

					Child.style.display = "none";

					if (this.IsIFrame(Child))
					{
						let Editor = Child.querySelector("[contenteditable]");

						Parent.parentElement.insertBefore(Child, Parent);

						this.Writer.Placeholder(Editor);

						System.load(Child);

						$(Child).slideDown(400);
					}
					else
					{
						Child.remove();
					}
				}

				this.Writer.Placeholder(Element, true);

				return this.Write(Element);

			}
		}
		else
		{
			if (typeof Callback === "function")
			{
				Callback(this.Node);
			}
		}

		return Promise.resolve();
	}

	RemoveMath()
	{
		var Selection = document.getSelection();

		var Anchor = Selection.anchorNode.parentElement;

		if (Anchor.hasAttribute("contenteditable") == false)
		{
			Anchor = Anchor.parentElement;

			while (Anchor && Anchor.hasAttribute("contenteditable") == false)
			{
				if (Anchor.nodeName == "MATHJAX")
				{
					break;
				}

				Anchor = Anchor.parentElement;
			}

			if (Anchor && Anchor.nodeName == "MATHJAX")
			{
				Anchor.remove();

				return true;
			}
		}

		return false;
	}
}



System.Business = Business;


})();