<?php

/**
 * @Author: Rot
 * @Date:   2017-11-20 11:33:43
 * @Last Modified by:   Rot
 * @Last Modified time: 2017-12-23 21:55:43
 */

namespace System\Views;

import(geye.views.generic);

class PracticeEditView extends GenericView
{
	protected $Models = ["ArticleQuestions", "DraftArticle", "Article"];

	protected $Styles = [users.writes.styles => ["writer.css", "modules/material-datetime-picker.css"]];

	protected $Scripts = [assets.scripts.modules => ["mathjax/MathJax.js?config=AM_HTMLorMML-full"], users.writes.scripts => 
		["commons.js", "transform.js", "writer.js", "modules/moment.js", "modules/rome.standalone.js", "modules/zmaterial-datetime-picker.js", "stackblur.min.js"],

	];

	public function Resolver ()
	{
		$this->DraftPractice = $this->Deeps->GetArticleQuestions();

		$this->ArticleID = $this->Deeps->GetArticleID();

		$this->IsEdit = $this->Deeps->GetIsEdit();

		$this->Publish = $this->Deeps->GetPubish();
	}
}

/**
 * Delete Object
 */

class DeleteAnswerObject extends ObjectView
{
	protected $Models = "DeleteAnswer";

	protected function Resolver ()
	{
		echo json_encode($this->Deeps->GetDeleteAnswer());
	}
}

class DeleteQuestionObject extends ObjectView
{
	protected $Models = "DeleteQuestion";

	protected function Resolver ()
	{
		echo json_encode($this->Deeps->GetDeleteQuestion());
	}
}

class DeletePracticeImageObject extends ObjectView
{
	protected $Models = "DeletePracticeImage";

	protected function Resolver ()
	{
		echo json_encode($this->Deeps->DeleteImage());
	}
}
/**
 * Save Object
 */

class SaveQuestionTitleObject extends ObjectView
{
	protected $Models = "SaveQuestionTitle";

	protected function Resolver ()
	{
		echo json_encode($this->Deeps->GetQuestionTitle());
	}
}

class SaveQuestionContentObject extends ObjectView
{
	protected $Models = "SaveQuestionContent";

	protected function Resolver ()
	{
		echo json_encode($this->Deeps->GetQuestionContent());
	}
}

class SaveAnswerObject extends ObjectView
{
	protected $Models = "SaveAnswer";

	protected function Resolver ()
	{
		echo json_encode($this->Deeps->GetAnswer());
	}
}

class SaveCorrectObject extends ObjectView
{
	protected $Models = "SaveCorrect"; 

	protected function Resolver ()
	{
		echo json_encode($this->Deeps->SaveCorrect);
	}
}

class SaveCloneImageObject extends ObjectView
{
	protected $Models = "";

	protected function Resolver ()
	{
		echo json_encode($this->Deeps->CloneImage());
	}
}

class SaveBlurImageObject extends ObjectView
{
	protected $Models = "SaveImage";

	protected function Resolver ()
	{
		echo json_encode($this->Deeps->BlurImage());
	}
}

class ImportDataObject extends ObjectView
{
	protected $Models = "";

	protected function Resolver ()
	{
		echo json_encode($this->Deeps->ImportData());
	}
}


?>