/*
* @Author: Rot
* @Date:   2017-10-07 00:41:55
* @Last Modified by:   Rot
* @Last Modified time: 2017-12-10 23:58:44
*/
(function () {

class QuestionModels
{
	constructor (Options)
	{
		this.URLWriter = Options.URLWriter;

		this.ArticleID = Options.ArticleID;

		this.IsEdit = Options.IsEdit;

		if (this.ArticleID)
		{
			this.Setted = true;
		}
		else
		{
			this.Setted = false;
		}
	}

	SetArticle (ArticleID)
	{
		this.ArticleID = ArticleID;

		let ALinks = document.querySelectorAll(".ge-steps a");

		ALinks.forEach(function (ALink) {
			ALink.href = ALink.href + "/" + ArticleID;
		})

		var Url = `${location.href}/${this.ArticleID}`;

		history.pushState(null, null, Url);

		this.Setted = true;
	}

	SaveQuestionContent (Data)
	{
		Data.set("ArticleID", this.ArticleID);

		Data.set("IsEdit", this.IsEdit);

		return System.post(`${this.URLWriter}/save_question_content/${new Date().getTime()}`, Data);
	}

	SaveQuestionTitle (Data)
	{
		Data.set("ArticleID", this.ArticleID);

		Data.set("IsEdit", this.IsEdit);

		return System.post(`${this.URLWriter}/save_question_title/${new Date().getTime()}`, Data);
	}

	SaveCorrect(Data)
	{
		Data.set("ArticleID", this.ArticleID);

		Data.set("IsEdit", this.IsEdit);
		
		return System.post(`${this.URLWriter}/save_correct/${new Date().getTime()}`, Data);
	}

	SaveAnswer (Data)
	{
		Data.set("ArticleID", this.ArticleID);

		Data.set("IsEdit", this.IsEdit);
		
		return System.post(`${this.URLWriter}/save_answer/${new Date().getTime()}`, Data);
	}

	ImportData(File)
	{
		let Upload = new System.Upload(File.files[0]);

		let Data = new FormData();

		Data.set("ArticleID", this.ArticleID);

		Data.set("IsEdit", this.IsEdit);
		
		return Upload.Send(`${this.URLWriter}/import_data/${new Date().getTime()}`, Data);
	}

	DeleteAnswer (Data)
	{
		Data.set("ArticleID", this.ArticleID);

		Data.set("IsEdit", this.IsEdit);
		
		return System.post(`${this.URLWriter}/delete_answer/${new Date().getTime()}`, Data);
	}

	DeleteQuestion (Data)
	{
		Data.set("ArticleID", this.ArticleID);

		Data.set("IsEdit", this.IsEdit);
		
		return System.post(`${this.URLWriter}/delete_question/${new Date().getTime()}`, Data);
	}

	DeleteImage (Data)
	{
		Data.set("ArticleID", this.ArticleID);

		Data.set("IsEdit", this.IsEdit);

		return System.post(`${this.URLWriter}/delete_image/${new Date().getTime()}`, Data);
	}
}

System.QuestionModels = QuestionModels;

})();