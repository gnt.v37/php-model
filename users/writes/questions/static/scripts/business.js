/*
* @Author: Rot
* @Date:   2017-10-06 16:17:40
* @Last Modified by:   Rot
* @Last Modified time: 2017-12-28 15:16:18
*/
(function () {

class BaseQuestion
{
	FillDataQuestion (Container, QuestionID)
	{
		var Gecontent = Element.parentElement,
			Hidden = Container.querySelector("hidden"),
			Editors = Container.querySelectorAll("[contenteditable]");

		Editors.forEach( (Editor) =>  {

			if (Editor.hasAttribute("data-question") == false)
			{
				Editor.setAttribute("data-question", QuestionID);
			}
			else
			{
				var CurrentID = Editor.getAttribute("data-question");

				if (CurrentID != QuestionID)
				{
					Editor.setAttribute("data-question", QuestionID);
				}
			}
		})


		Hidden.innerHTML = `<input type="hidden" data-question value="${QuestionID}" />`;

		Container.querySelector("ge-content").setAttribute("data-question", QuestionID);

		Container.setAttribute("data-question", QuestionID);
	}

	FillDataAnswer (Content, Element, IsQuestion, QuestionID, AnswerID)
	{
		Element.setAttribute("data-answer", AnswerID);

		var NodeName;

		if (IsQuestion == false)
		{
			var Container = Content.parentElement;

			while (Container)
			{
				NodeName = Container.nodeName;

				if (NodeName == "SECTION")
				{
					break;
				}

				if (NodeName == "MAIN")
				{
					Container = null;

					break;
				}

				Container = Container.parentElement;
			}

			if (Container)
			{
				this.FillDataQuestion(Container, QuestionID);
			}
		}

		var Answer = Element.parentElement;

		while (Answer)
		{
			NodeName = Answer.nodeName;

			if (NodeName == "GE-ANSWER")
			{
				break;
			}

			if (NodeName == "SECTION" || NodeName == "MAIN")
			{
				Answer = null;

				break;
			}

			Answer = Answer.parentElement;
		}

		if (Answer)
		{
			var Correct = Answer.querySelector("[ge-correct]");

			Correct.setAttribute("data-answer", AnswerID);

			Correct.setAttribute("data-question", QuestionID);

			Answer.setAttribute("data-answer", AnswerID);

			Answer.querySelector("[answer-content]").setAttribute("data-answer", AnswerID);
		}

	}

}

class Question extends BaseQuestion
{
	constructor ()
	{
		super();

		this.TitleData = new FormData();

		this.ContentData = new FormData();

		this.AnswerData = new FormData();

		this.DeleteAnswerData = new FormData();

		this.DeleteQuestionData = new FormData();

		this.ImageData = new FormData();

		this.CorrectData = new FormData();

		this.URLResolver = System.URLResolver();

		this.Models = new System.QuestionModels(this.URLResolver);

		this.SaveQuestionNode = document.createElement("div");

		this.SaveAnswerNode = document.createElement("div");

	}

	SaveQuestionTitle (Container, Element)
	{
		var Content = Element.textContent.replace(/`/g, ""),
			IsDataExist = false;

		if (Element.hasAttribute("data-question"))
		{
			this.TitleData.set("QuestionID", Element.getAttribute("data-question"));

			IsDataExist = true;
		}
		else
		{
			this.TitleData.delete("QuestionID");
		}

		this.TitleData.set("Title", Content);

		return this.Models.SaveQuestionTitle(this.TitleData).then ( (Response) => {
			
			var Matches = Response.match(/^{.*?}/gi);

			var Json = JSON.parse(Matches);

			if (IsDataExist == false && Json)
			{
				this.FillDataQuestion(Container, Json.QuestionID);

				if (this.Models.Setted == false)
				{
					this.Models.SetArticle(Json.ArticleID);
				}

			}
		})
	}

	SaveQuestionContent (Container, Element)
	{
		this.SaveQuestionNode.innerHTML = Element.parentElement.parentElement.innerHTML;

		var Matjaxs = this.SaveQuestionNode.querySelectorAll("mathjax");

		if (Matjaxs)
		{
			Matjaxs.forEach(function (Mathjax) {

				var MathjaxText = Mathjax.lastElementChild;

				if (MathjaxText)
				{
					Mathjax.innerHTML = "`" + MathjaxText.textContent + "&nbsp;`";
				}

			})
			
		}

		var Content = System.TextContent(this.SaveQuestionNode),
			IsDataExist = false;

		if (Element.hasAttribute("data-question"))
		{
			this.ContentData.set("QuestionID", Element.getAttribute("data-question"));

			IsDataExist = true;
		}
		else
		{
			this.ContentData.delete("QuestionID");
		}

		this.ContentData.set("Question", Content);

		return this.Models.SaveQuestionContent(this.ContentData).then ( (Response) => {

			var Matches = Response.match(/^{.*?}/gi);

			var Json = JSON.parse(Matches);

			if (IsDataExist == false && Json)
			{
				this.FillDataQuestion(Container, Json.QuestionID);

				if (this.Models.Setted == false)
				{
					this.Models.SetArticle(Json.ArticleID);
				}
			}
		})
	}

	SaveAnswer (Container, Element)
	{
		this.SaveAnswerNode.innerHTML = Element.parentElement.parentElement.innerHTML;

		var Matjaxs = this.SaveAnswerNode.querySelectorAll("mathjax");

		if (Matjaxs)
		{
			Matjaxs.forEach(function (Mathjax) {

				var MathjaxText = Mathjax.lastElementChild;

				if (MathjaxText)
				{
					Mathjax.innerHTML = "`" + MathjaxText.textContent + "&nbsp;`";
				}

			})
			
		}

		var Content = System.TextContent(this.SaveAnswerNode),
			IsDataAnswerExist = false,
			IsDataQuestionExist = false;

		if (Element.hasAttribute("data-answer"))
		{
			this.AnswerData.set("AnswerID", Element.getAttribute("data-answer"));

			IsDataAnswerExist = true;
		}
		else
		{
			this.AnswerData.delete("AnswerID");
		}

		if (Element.hasAttribute("data-question"))
		{
			this.AnswerData.set("QuestionID", Element.getAttribute("data-question"));

			IsDataQuestionExist = true;
		}
		else
		{
			this.AnswerData.delete("QuestionID");
		}

		this.AnswerData.set("Answer", Content);

		return this.Models.SaveAnswer(this.AnswerData).then ( (Response) => {

			var Matches = Response.match(/^{.*?}/gi);

			var Json = JSON.parse(Matches);

			if (IsDataAnswerExist == false && Json)
			{
				this.FillDataAnswer(Container, Element, IsDataQuestionExist, Json.QuestionID, Json.AnswerID);

				if (this.Models.Setted == false)
				{
					this.Models.SetArticle(Json.ArticleID);
				}
			}
		})
	}

	SaveCorrect(Element)
	{
		var AnswerID = Element.getAttribute("data-answer"),
			QuestionID = Element.getAttribute("data-question"),
			ClassLisst = Element.classList;

		if (ClassLisst.contains("ge-active"))
		{
			this.CorrectData.set("Correct", 0);
		}
		else
		{
			this.CorrectData.set("Correct", 1);
		}

		this.CorrectData.set("AnswerID", AnswerID);
		
		this.CorrectData.set("QuestionID", QuestionID);

		return this.Models.SaveCorrect(this.CorrectData).then(function () {

			ClassLisst.contains("ge-active") ? ClassLisst.remove("ge-active") : ClassLisst.add("ge-active");

		})
	}

	ImportData (Element, SectionContainer, Callback)
	{
		return this.Models.ImportData(Element).then(function (Response) {

			Response = Response.match(/(\[.*?\])<div.*?>/);

			return new Promise(function (Resolve, Reject) {

				Resolve(JSON.parse(Response[1]));

				Element.nextElementSibling.value = "";

			})

		});
	}

	DeleteAnswer (Element)
	{
		if (Element.hasAttribute("data-answer") && Element.hasAttribute("data-question"))
		{
			var Answer = Element.getAttribute("data-answer");

			var Question = Element.getAttribute("data-question");

			if (Answer && Question)
			{
				this.DeleteAnswerData.set("QuestionID", Question);

				this.DeleteAnswerData.set("AnswerID", Answer);

				return this.Models.DeleteAnswer(this.DeleteAnswerData).then(function (Response) {
					console.log(Response);
				}, function (Message) {
					console.log(Message);
				})
			}
			else
			{
				this.DeleteAnswerData.delete("QuestionID");

				this.DeleteAnswerData.delete("AnswerID");
			}
		}

		return Promise.resolve();
	}

	DeleteQuestion (Element)
	{
		if (Element.hasAttribute("data-question"))
		{
			var Question = Element.getAttribute("data-question");

			if (Question)
			{
				this.DeleteQuestionData.set("QuestionID", Question);

				return this.Models.DeleteQuestion(this.DeleteQuestionData).then(function (Response) {
					console.log(Response);
				})
			}
			else
			{
				this.DeleteQuestionData.delete("QuestionID");
			}
		}

		return Promise.resolve();
	}
	
	DeleteImage (Container, Element)
	{
		let Gef = Element.parentElement;

		let Figure = null;

		while (Gef && Gef.nodeName != "GEF")
		{
			Gef = Gef.parentElement;

			if (Gef.nodeName == "FIGURE")
			{
				Figure = Gef;
			}
		}

		if (Figure)
		{
			this.ImageData.set("Image", Figure.getAttribute("image"));

			this.ImageData.set("ImageID", Figure.getAttribute("data-image"));

			if (Element.hasAttribute("data-question"))
			{
				this.ImageData.set("QuestionID", Element.getAttribute("data-question"));
			}
			else if (Element.hasAttribute("data-answer"))
			{
				this.ImageData.set("AnswerID", Element.getAttribute("data-answer"));
			}

			return this.Models.DeleteImage(this.ImageData).then( (Response) => {

				Response = Response.match(/{.*?}/);

				let Json = JSON.parse(Response);

				if (Json.Result == 1 && Gef)
				{
					let Parent = Gef.parentElement;

					$(Parent).slideUp( () => {

						var PreviousSibling = Parent.previousElementSibling;

						var NextSibling = Parent.nextElementSibling;

						var Writer = Parent.parentElement;

						Parent.remove();

						if (PreviousSibling)
						{
							PreviousSibling.querySelector(`[contenteditable="true"]`).focus();
						}
						else if (NextSibling)
						{
							NextSibling.querySelector(`[contenteditable="true"]`).focus();
						}

						setTimeout(function () {

						}, 128)

						if (Element.hasAttribute("data-answer"))
						{
							
							return this.SaveAnswer (Container.querySelector("ge-content"), Writer.querySelector("[answer-content]"));
						}
						else if (Element.hasAttribute("data-question"))
						{
							return this.SaveQuestionContent(Container, Container.querySelector("[question-content]"));
						}

					})
				}
			})
		}

		return Promise.resolve();
	}

}

class QuestionBusiness extends Question
{
	constructor ()
	{
		super();

		const URL = this.URLResolver.URLWriter;

		this.Writer = new System.Writer({
			Models: 
			{
				Clone: `${URL}/clone_image`, 
				Blur: `${URL}/blur_image`,
			},
			Placeholder: 
			{
				"question-content": "Write the question content here...",
				"question-title": "Write the question title here...",
				"answer-content": "Write the answer here...",
				"figure": "Write the caption here...",
				"iframe": "Write the caption here...",
			}
		});

		this.MathNode = document.createElement("div");

		document.querySelectorAll("ge-writer").forEach( (GeWriter) => {

			let QuestionID = GeWriter.getAttribute("data-question");

			let AnswerID;

			if (GeWriter.hasAttribute("data-answer"))
			{
				AnswerID = GeWriter.getAttribute("data-answer");
			}

			GeWriter.querySelectorAll("gef").forEach(function (Gef) {

				let Editor = Gef.querySelector(`[contenteditable="true"]`).parentElement;

				Gef.parentElement.removeAttribute("style");

				Editor.setAttribute("data", "figure");

				Editor.removeAttribute("placeholder");

			})

			GeWriter.querySelectorAll("geif").forEach(function (Geif) {

				let Editor = Geif.querySelector(`[contenteditable="true"]`).parentElement;

				Geif.parentElement.removeAttribute("style");

				Editor.setAttribute("data", "iframe");

				Editor.removeAttribute("placeholder");
			})

			GeWriter.querySelectorAll(`[contenteditable="true"]`).forEach( (GeEditor) => {

				if (QuestionID)
				{
					GeEditor.setAttribute("data-question", QuestionID);
				}

				if (AnswerID)
				{
					GeEditor.setAttribute("data-answer", AnswerID);
				}

				this.Writer.Placeholder(GeEditor);

			})

		})
			
	}

	InsertQuestion (Container, Callback)
	{
		var Section 	= document.createElement("section"),
			QuestionNum = parseInt(Container.getAttribute("total-question"));

		Section.classList.add("ge-section");

		Section.innerHTML = `<hidden></hidden><div class="ge-count">Question <span question-number></span></div><ge-content><ge-question><div class="ge-writer-space"><div question-container="true"><ge-writer>
				<h5 class="ge-text" data="question-title">
					<div contenteditable="true" question="true" rule="textbox" question-title="true" class="ge-paragraph"></div>
					<div class="ge-absolute ge-title ge-block"><span class="ge-placeholder">Write the question title here...</span></div>
				</h5>
			</ge-writer><ge-writer class="ge-bottom-8">
				<h4 class="ge-text" data="question-content">
					<div contenteditable="true" question="true" rule="textbox" question-content="true" class="ge-paragraph"></div>
					<div class="ge-absolute ge-content ge-block"><span class="ge-placeholder">Write the question content here...</span></div>
				</h4>
			</ge-writer></div><div><button class="ge-button-icon" new-answer="true">
				<span class="ge-icon ge-answer-icon"><span class="ge-relative">A</span></span>
				<span class="ge-new-text">Add a new answer</span>
			</button></div></div></ge-question></ge-content>`;

		Section.querySelector("[question-number]").innerHTML = QuestionNum;

		Container.insertBefore(Section, Container.firstElementChild);

		Container.setAttribute("total-question", QuestionNum + 1);

		Section.querySelector("[contenteditable]").focus();

		if (typeof Callback === "function")
		{
			Callback(Section);
		}
	}

	InsertAnswer (Container, Callback)
	{
		var Answer = document.createElement("ge-answer"),
			QuestionID = Container.parentElement.querySelector("input[data-question]");

		Answer.innerHTML = `<div>
			<div class="ge-relative">
			<div class="ge-absolute ge-block">
				<button class="ge-button-icon" ge-correct="true"><span class="ge-icon ge-correct-icon"><span class="ge-relative">C</span></span></button>
			</div>
			<ge-writer class="ge-space">
				<h5 class="ge-text ge-answer" data="answer-content" >
					<div contenteditable="true" answer-content="true" rule="textbox" class="ge-paragraph"><br /></div>
					<div class="ge-absolute ge-content ge-block"><span class="ge-placeholder">Write the answer here...</span></div>
				</h5></ge-writer>
			</div>
		</div>`;

		Container.insertBefore(Answer, Container.firstElementChild.nextElementSibling);

		if (QuestionID)
		{
			Answer.querySelector("[contenteditable]").setAttribute("data-question", QuestionID.value);
		}

		Answer.querySelector("[contenteditable]").focus();

		if (typeof Callback === "function")
		{
			Callback(Answer);
		}
	}

	UpdateSectionID (SectionContainer)
	{
		var Sections = SectionContainer.children;

		var Numb = Sections.length;

		SectionContainer.setAttribute("total-question", Numb);

		for (let Section of Sections)
		{
			Section.querySelector("[question-number]").innerHTML = Numb;

			Numb -= 1;
		}
	}

	IsFigure (Element)
	{
		if (Element.hasAttribute("ge-figure") || Element.parentElement.nodeName == "FIGCAPTION" || Element.querySelector("img"))
		{
			return true;
		}

		return false;
	}

	IsIFrame (Element)
	{
		if (Element.hasAttribute("ge-iframe") || Element.querySelector("iframe"))
		{
			return true;
		}

		return false;
	}

	TextPlain (Element)
	{
		Element.innerHTML = Element.textContent.trim();
	}

	MathJax (Element)
	{
		var Content = Element.innerHTML;

		var Matches = Content.match(/`.*?`/);

		if (Matches)
		{
			Element.innerHTML = Content.replace(Matches, `<mathjax>${Matches}</mathjax>&nbsp;`);

			var Children = Element.children;

			var Length = Children.length;

			var Child = null;

			for (var i = 0; i < Length; i++)
			{
				Child = Children[i];

				if (Child.nodeName == "MATHJAX" && Child.children.length == 0)
				{
					MathJax.Hub.Queue(["Typeset", MathJax.Hub, Child]);
				}
			}
			
		}
	}

	Transform (Element, Callback)
	{
		if (this.IsFigure(Element) || Element.hasAttribute("data-question") == false || Element.hasAttribute("question-title"))
		{
			if (typeof Callback === "function")
			{
				Callback(null);
			}

			return Promise.resolve();
		}

		let Data = {ArticleID: this.URLResolver.ArticleID, QuestionID: Element.getAttribute("data-question")};

		var Content = null;

		if (Element.hasAttribute("data-answer"))
		{
			Data["AnswerID"] = Element.getAttribute("data-answer");

			Content = this.Writer.TransformImage(Element);
		}
		else
		{
			Content = this.Writer.TransformIFrame(Element);

			let Temp = null;

			if (Content)
			{
				Temp = this.Writer.TransformImage(Content.Transform);
			}
			else
			{
				Temp = this.Writer.TransformImage(Element);
			}

			if (Temp)
			{
				Content = Temp;
			}

		}

		if (Content)
		{
			var Parent = Element.parentElement;

			var Images = Content.Images;

			var Node = document.createElement("div");

			var Placeholder = Element.nextElementSibling;

			Node.innerHTML = Content.Transform;

			Element.innerHTML =  "";

			Placeholder.firstElementChild.innerHTML = "Waiting a few seconds...";

			Placeholder.classList.add("ge-block");

			if (typeof Callback === "function")
			{
				Callback(Node);
			}

			if (Images)
			{
				return this.Writer.DrawImage(Images, Node, Data, () => {

					var Children = Node.children;

					var Length = Children.length;

					var Child;

					for (var i = 0; i < Length; i++)
					{
						Child = Children[0];

						Child.style.display = "none";

						if (this.IsFigure(Child))
						{
							let Editor = Child.querySelector("[contenteditable]");

							Parent.parentElement.insertBefore(Child, Parent);

							Editor.setAttribute("data-question", Element.getAttribute("data-question"));

							if (Element.hasAttribute("data-answer"))
							{
								Editor.setAttribute("answer-caption", "true");

								Editor.setAttribute("data-answer", Element.getAttribute("data-answer"));
							}
							else
							{
								Editor.setAttribute("question-caption", "true");
							}

							this.Writer.Placeholder(Editor);

							$(Child).slideDown(400);
						}
						else
						{
							Child.remove();
						}
					}

					this.Writer.Placeholder(Element, true);

				})
			}
			else
			{
				var IFrame = Content.IFrame;

				var Children = Node.children;

				var Length = Children.length;

				var Child;

				for (var i = 0; i < Length; i++)
				{
					Child = Children[0];

					Child.style.display = "none";

					if (this.IsIFrame(Child))
					{
						let Editor = Child.querySelector("[contenteditable]");

						Parent.parentElement.insertBefore(Child, Parent);

						Editor.setAttribute("data-question", Element.getAttribute("data-question"));

						if (Element.hasAttribute("data-answer"))
						{
							Editor.setAttribute("answer-caption", "true");

							Editor.setAttribute("data-answer", Element.getAttribute("data-answer"));
						}
						else
						{
							Editor.setAttribute("question-caption", "true");
						}

						this.Writer.Placeholder(Editor);

						$(Child).slideDown(400);
					}
					else
					{
						Child.remove();
					}
				}

				this.Writer.Placeholder(Element, true);

				return Promise.resolve();
			}
		}
		else
		{
			if (typeof Callback === "function")
			{
				Callback(null);
			}

			return Promise.resolve();
		}
	}

	ImportQuestion (Element, Data)
	{
		var Title = Element.querySelector("[question-title]");

		var Content = Element.querySelector("[question-content]");

		Title.innerHTML = Data.title;

		Content.innerHTML = Data.question;

		this.Writer.Placeholder(Title);

		this.Writer.Placeholder(Content);

		this.MathJax(Content);

		System.Trigger(Title, "keyup", 13);
		
		System.Trigger(Content, "keydown", 13);

		System.Trigger(Content, "keyup", 13);

	}

	ImportAnswer(Element, Data)
	{
		var Answer = Element.querySelector("[answer-content]");

		Answer.innerHTML = Data.answer;

		this.Writer.Placeholder(Answer);

		this.MathJax(Answer);

		System.Trigger(Answer, "keyup", 17);

		setTimeout(function () {

			System.Trigger(Answer, "keydown", 13);

		}, 256)

	}

	RemoveSectionWithNoCondition (Element)
	{

		var FocusElement = Element.previousElementSibling;
		
		var Parent = Element.parentElement;

		if (FocusElement)
		{
			FocusElement.querySelector("[contenteditable]").focus();
		}
		else
		{
			FocusElement = Element.nextElementSibling;

			if (FocusElement)
			{
				FocusElement.querySelector("[contenteditable]").focus();
			}
		}

		Element.remove();


		this.UpdateSectionID(Parent);
		
	}

	RemoveAnswerWithNoCondition (Element)
	{
		var Answer = Element.parentElement,
			NodeName = "GE-ANSWER";
			
		while (Answer && Answer.nodeName != NodeName)
		{
			Answer = Answer.parentElement;
		}

		if (Answer)
		{
			var Previous = Answer.previousElementSibling;

			var Next = Answer.nextElementSibling;

			if (Previous && Previous.nodeName == NodeName)
			{
				Previous.querySelector("[contenteditable]").focus();
			}
			else if (Next && Next.nodeName == NodeName)
			{
				Next.querySelector("[contenteditable]").focus();
			}
			else if (Previous.nodeName == "GE-QUESTION")
			{
				Previous.querySelector("[contenteditable]").focus();
			}

			Answer.remove();

		}
	}

	RemoveSection (Element, Section)
	{
		var IsEmpty = true;

		var Previous = Element.parentElement.previousElementSibling;

		if (Previous)
		{
			Previous.querySelector(`[contenteditable="true"]`).focus();

			return false;
		}

		Section.querySelectorAll("[contenteditable]").forEach( (Editor) =>
		{
			let Content = Editor.textContent.trim();

			var NextElement = Editor.nextElementSibling;

			if (NextElement && NextElement.classList.contains("ge-block") == false)
			{
				IsEmpty = false;
			}

			if (Editor.textContent != "")
			{
				IsEmpty = false;
			}
			else if (NextElement && NextElement.classList.contains("ge-block") == false)
			{
				this.Writer.Placeholder(Editor);
			}

			
		})
		
		if (IsEmpty)
		{
			this.RemoveSectionWithNoCondition(Section);

			return true;
		}

		return false;
	}

	RemoveCondition (Element)
	{
		var NextElement = Element.nextElementSibling;

		if ((Element.childNodes.length == 0 && NextElement && NextElement.classList.contains("ge-block")) || (Element.childNodes.length == 1 && Element.childNodes[0].nodeName == "BR"))
		{
			return true;
		}

		return false;
	}

	RemoveMath()
	{
		var Selection = document.getSelection();

		var Anchor = Selection.anchorNode.parentElement;

		if (Anchor.hasAttribute("contenteditable") == false)
		{
			Anchor = Anchor.parentElement;

			while (Anchor && Anchor.hasAttribute("contenteditable") == false)
			{
				if (Anchor.nodeName == "MATHJAX")
				{
					break;
				}

				Anchor = Anchor.parentElement;
			}

			if (Anchor && Anchor.nodeName == "MATHJAX")
			{
				Anchor.remove();

				return true;
			}
		}

		return false;
	}

	RemoveAnswer (Element)
	{
		var Previous = Element.parentElement.previousElementSibling;

		if (Previous)
		{
			Previous.querySelector(`[contenteditable="true"]`).focus();

			return false;

		}

		if (this.RemoveCondition(Element))
		{
			this.RemoveAnswerWithNoCondition(Element);

			return true;
		}

		this.Writer.Placeholder(Element);

		return false;
	}

	RemoveIFrame (Element, Callback)
	{
		var NextElement = Element.nextElementSibling;

		if (this.RemoveCondition(Element))
		{
			var IFrame = Element.parentElement;

			while (IFrame && IFrame.hasAttribute("ge-iframe") == false)
			{
				IFrame = IFrame.parentElement;
			}

			$(IFrame).slideUp(function () {

				let Previous = IFrame.previousElementSibling;

				if (Previous)
				{
					Previous.querySelector(`[contenteditable="true"]`).focus();
				}
				else
				{
					let Next = IFrame.nextElementSibling;

					Next.querySelector(`[contenteditable="true"]`).focus();
				}

				Callback();

				IFrame.remove();

			})
		}
		else
		{
			Callback();
		}

		this.Writer.Placeholder(Element);
	}
	
	RemoveImage (Element)
	{
		var NextElement = Element.nextElementSibling;

		console.log(this.RemoveCondition(Element))
		if (this.RemoveCondition(Element))
		{
			return true;
		}

		this.Writer.Placeholder(Element);

		return false;
	}

}


System.QuestionBusiness = QuestionBusiness;

})();