/*
* @Author: Rot
* @Date:   2017-10-07 23:57:22
* @Last Modified by:   Rot
* @Last Modified time: 2017-12-28 14:24:02
*/

(function () {

class BaseQuestion 
{
	constructor ()
	{

	}

	InvokeCorrect (Business, Answer = null)
	{
		var Corrects = [];

		if (Answer == null)
		{
			Corrects = document.querySelectorAll("[ge-correct]");
		}
		else
		{
			Corrects = Answer.querySelectorAll("[ge-correct]");
		}

		Corrects.forEach(function(Correct) {

			Correct.addEventListener("click", function () {

				var Element = this;

				System.Process = System.Process.then(function () {

					return new Promise(function (Resolve, Reject) {
						
						Business.SaveCorrect(Element).then(function () {
							Resolve();
						})
					})
				})

			})

		})
	}

	InvokeAnswer (Business, Answer)
	{
		var Content 	 	= Answer.parentElement,
			_this 			= this,
			Paragraph 	 	= Answer.querySelector("[answer-content]"),
			InsertAnswer 	= Content.querySelector("[new-answer]");
		
		let DeleteAnswer = false;

		if (Paragraph == null)
		{
			return false;
		}

		Paragraph.addEventListener("keydown", function (e) {

			if (e.ctrlKey)
			{
				if (e.altKey)
				{
					switch (e.which) 
					{
						case 83:
							Business.TextPlain(this);
							break;
						default:
							// statements_def
							break;
					}

					e.preventDefault();
				}
				else if (e.shiftKey)
				{
					switch (e.which) 
					{
						case 86:
							Business.TextPlain(this);
							break;
						default:
							// statements_def
							break;
					}
				}
			}

			if (( (e.shiftKey == false || e.shiftKey == undefined) && e.which == 13) || e.which == 9)
			{
				System.Process = System.Process.then( () => {

					return new Promise( (Resolve, Reject) => {

						Business.Transform(this, (TheNew) => {

							if (TheNew)
							{
								var Children = TheNew.children;

								var Length 	 = Children.length;

								for (var i = 0; i < Length; i++)
								{
									_this.InvokeImage(Business, Content.parentElement, Children[i].querySelector("[contenteditable]"));
								}
							}
							else
							{

								if (e.trigger == undefined)
								{
									InsertAnswer.click();
								}
								
							}

						}).then( () => {

							return Business.SaveAnswer(Content, this);

						})
						.then(function () {

							Resolve();
						});

					})

				})

				e.preventDefault();
			}

			if ((e.which == 46 || e.which == 8) && e.shiftKey)
			{
				DeleteAnswer = true;

				e.preventDefault();

			}

			switch(e.which)
			{
				case 8:
					if (Business.RemoveMath())
					{
						e.preventDefault();
					}
					break;
			}
		})

		Paragraph.addEventListener("keyup", function (e) {
			
			switch (e.which)
			{
				case 8:
					if (Business.RemoveAnswer(this))
					{
						DeleteAnswer = true;
					}
					break;
				case 13:

					e.preventDefault();

					return null;
				default:

				case 192:

					Business.MathJax(this);
					break;
			}

			if (DeleteAnswer)
			{
				System.Process = System.Process.then ( () => {

					return new Promise( (Resolve, Reject) => {

						Business.DeleteAnswer(this).then(function (Response) {
							Resolve();
						})

					})
				})

				e.preventDefault();
			}
			else
			{
				System.Process = System.Process.then( () => {

					return new Promise( (Resolve, Reject) => {

						if (this.hasAttribute("answer-content"))
						{
							Business.SaveAnswer(Content, this).then(function () {
								Resolve();
							});
						}

					})
				})
						
			}

			Business.Writer.Placeholder(this);
		});
	}

	InvokeQuestion (Business, Section)
	{
		var InsertAnswer = Section.querySelector("[new-answer]"),
			_this = this,
			IsDeleteQuestion = false;

		Section.querySelectorAll("[question]").forEach(function (Editor) {

			Editor.addEventListener("keydown", function (e) {

				if (e.ctrlKey)
				{
					if (e.altKey)
					{
						switch (e.which) 
						{
							case 83:
								Business.TextPlain(this);
								break;
							default:
								// statements_def
								break;
						}

						e.preventDefault();
					}
					else if (e.shiftKey)
					{
						switch (e.which) 
						{
							case 86:
								Business.TextPlain(this);
								break;
							default:
								// statements_def
								break;
						}
					}
				}

				switch(e.which)
				{
					case 8:
						if (Business.RemoveMath())
						{
							e.preventDefault();
						}
						break;
					case 9:
						if (e.target.closest("[question-content]"))
						{
							var Answer = Section.querySelector("ge-answer");

							if (Answer == null || Answer.textContent.length > 0)
							{
								InsertAnswer.click();
							}
							else
							{
								Answer.focus();
							}
							
							e.preventDefault();
						}

						break;
					case 13:

						System.Process = System.Process.then( () => {

							return new Promise( (Resolve, Reject) => {

								Business.Transform(this, (TheNew) => {

									if (TheNew)
									{
										var Children = TheNew.children;

										var Length 	 = Children.length;

										for (var i = 0; i < Length; i++)
										{
											if (Children[i].hasAttribute("ge-figure"))
											{
												_this.InvokeImage(Business, Section, Children[i].querySelector("[contenteditable]"));
											}
											else
											{
												System.load(Children[i]);
												
												_this.InvokeIFrame(Business, Section, Children[i].querySelector("[contenteditable]"));
											}
										}
									}

								}).then( () => {
									Resolve();
								})

							})

						})

						e.preventDefault();
						break;
						
				}

			})

			Editor.addEventListener("keyup", function (e) {

				switch (e.which)
				{
					case 8: 

						if (Business.RemoveSection(this, Section))
						{
							IsDeleteQuestion = true;
						}
						break;
					case 192:

						Business.MathJax(this);

						break;
				}

				if (IsDeleteQuestion)
				{
					System.Process = System.Process.then( () => {

						return new Promise( (Resolve, Reject) => {

							Business.DeleteQuestion(this).then( function () {
								setTimeout(function () {
									Resolve();
								}, 512)
							})

						})

					})

					return false;
				}

				Business.Writer.Placeholder(this);

				System.Process = System.Process.then( () => {

					return new Promise( (Resolve, Reject) => {

						if (this.hasAttribute("question-title"))
						{
							Business.SaveQuestionTitle(Section, this).then (function () {
								Resolve();
							});						
						}
						else if (this.hasAttribute("question-content"))
						{
							Business.SaveQuestionContent(Section, this).then(function () {
								Resolve();
							});							
						}
					})
				})
			})
		})

		this.InvokeImages(Business, Section);

		this.InvokeIFrames(Business, Section);
	}

	InvokeIFrame (Business, Section, Caption)
	{
		Caption.addEventListener("keyup", function () {

			Business.RemoveIFrame(this, function () {

				System.Process = System.Process.then( () => {

					return new Promise( (Resolve, Reject) => {

						if (Caption.hasAttribute("question-caption") )
						{
							Business.SaveQuestionContent(Section, Section.querySelector("[question-content]")).then(function () {

								Resolve();

							});
						}
					})
				})

			});
			
		})
	}

	InvokeImage (Business, Section, Caption)
	{
		Caption.addEventListener("keyup", function () {

			if (Business.RemoveImage(this))
			{
				System.Process = System.Process.then( () => {

					return new Promise( (Resolve, Reject) => {

						Business.DeleteImage(Section, this).then(function () {

							Resolve();

						})
					})
				})
			}
			else
			{
				System.Process = System.Process.then( () => {

					return new Promise( (Resolve, Reject) => {

						if (Caption.hasAttribute("question-caption") )
						{
							Business.SaveQuestionContent(Section, Section.querySelector("[question-content]")).then(function () {

								Resolve();

							});
						}
						else if (Caption.hasAttribute("answer-caption"))
						{
							Business.SaveAnswer(Section.querySelector("ge-content"), Section.querySelector("[answer-content]")).then(function () {

								Resolve();

							});
						}
					})
				})
			}
		})
	}
}

class Question extends BaseQuestion
{
	constructor ()
	{
		super();

		let Business = new System.QuestionBusiness();

		var SectionContainer = document.querySelector("[section-container]");

		this.InvokeQuestions(Business, SectionContainer);

		this.InvokeAnswers(Business, SectionContainer);
		
		this.InsertAnswers(Business, SectionContainer);

		this.InsertQuestion(Business, SectionContainer);

		this.InvokeCorrect(Business);

		this.InvokeImport(Business, SectionContainer);
	}

	InsertAnswers (Business, Element)
	{
		var Answers = Element.querySelectorAll("[new-answer]"),
			Content, NodeName;

		Answers.forEach( (Answer) => 
		{
			Answer.addEventListener("click", () => {

				Content = Answer.parentElement;

				while (Content)
				{
					NodeName = Content.nodeName;

					if (NodeName == "GE-CONTENT")
						break;

					if (NodeName == "SECTION")
					{
						Content == null;

						break;
					}

					Content = Content.parentElement;
				}

				Business.InsertAnswer(Content, (AnswerElement) => {

					this.InvokeAnswer(Business, AnswerElement);

					this.InvokeCorrect(Business, AnswerElement);

				})	
			})
		});
	}

	InsertQuestion (Business, SectionContainer)
	{
		document.querySelector("[new-question]").addEventListener("click", () => {

			Business.InsertQuestion(SectionContainer, (Section) => {

				var InsertAnswer = Section.querySelector("[new-answer]"),
					Content 	 = Section.querySelector("ge-content");

				this.InvokeQuestion(Business, Section)

				InsertAnswer.addEventListener ("click", () => {

					Business.InsertAnswer(Content, (Answer) => {

						this.InvokeAnswer(Business, Answer);
						
						this.InvokeCorrect(Business, Answer);

					});
				})
			});
		})
	}

	InvokeIFrames (Business, Section)
	{
		Section.querySelectorAll(`[ge-iframe][contenteditable="true"]`).forEach( (Caption) => {
			this.InvokeIFrame(Business, Section, Caption);
		})
	}

	InvokeImages (Business, Section)
	{
		Section.querySelectorAll(`figcaption [contenteditable="true"]`).forEach( (Caption) => {
			this.InvokeImage(Business, Section, Caption);
		})
	}

	InvokeAnswers (Business, Element)
	{
		var Answers = Element.querySelectorAll("ge-answer"),
			Content;

		Answers.forEach( (Answer) => {
			this.InvokeAnswer(Business, Answer);
		})
	}

	InvokeQuestions (Business, Element)
	{
		var Sections = Element.querySelectorAll("section");

		Sections.forEach((Section) => {
			this.InvokeQuestion(Business, Section);
		})
	}

	InvokeImport (Business, SectionContainer)
	{
		let ImportButton = document.querySelector("[import-data]");

		let NewQuestion = document.querySelector("[new-question]");

		ImportButton.addEventListener("click", function () {

			this.previousElementSibling.click();

		})

		ImportButton.previousElementSibling.addEventListener("change", function () {

			var Element = this;

			System.Process = System.Process.then(function () {

				return new Promise(function (Resolve, Reject) {

					Business.ImportData(Element, SectionContainer).then(function (Questions) {

						Resolve();

						return Questions.reduce(function(QuestionSequence, QuestionData) {
						
							return QuestionSequence.then(function () {

								return new Promise (function (QuestionResolve, QuestionReject) {

									NewQuestion.click();

									var TheNew = SectionContainer.firstElementChild;

									var NewAnswer = TheNew.querySelector("[new-answer]");

									var Content = TheNew.querySelector("ge-content");

									Business.ImportQuestion(TheNew, QuestionData);
									
									return QuestionData.answers.reduce(function(AnswerSquence, AnswerData) {

										return AnswerSquence.then(function () {

											return new Promise (function (AnswerResolve, AnswerReject) {

												NewAnswer.click();

												var Answer = Content.firstElementChild.nextElementSibling;

												var Correct = Answer.querySelector("[ge-correct]");

												Business.ImportAnswer(Answer, AnswerData);

												AnswerResolve();

												console.log(Answer.correct)

												if (AnswerData.correct == 1)
												{
													Correct.click();
												}

											})
										})

									}, System.Process).then(function () {

										QuestionResolve();

									})

								})
							})

						}, System.Process)

					})

				})
			})

		})
	}
}

System.Question = Question;

})();