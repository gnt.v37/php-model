<?php

/**
 * @Author: Rot
 * @Date:   2017-10-26 18:46:41
 * @Last Modified by:   Rot
 * @Last Modified time: 2017-12-16 19:00:35
 */

if (isset($this->ArticleID["Permission"]))
{
	$Header = "";
	
	$ContentTemplate = assets.templates."403.php";
}
else
{

	$HeaderTemplate = users.writes.templates.header;

}

include_once __TEMPLATE__;

?>
