<?php  $Count = isset($this->DraftPractice) ? count($this->DraftPractice) + 1 : 0; ?>
<div class="ge-top-32" ><div class="ge-write-question">
<div><div class="ge-section-space">
<div class="ge-row"><div class="ge-left"><button class="ge-button-icon" new-question="true">
		<span class="ge-icon ge-question-icon"><span class="ge-relative">Q</span></span>
		<span class="ge-new-text">Add a new question</span>
	</button></div>
	<div class="ge-right">
		<input type="file" class="ge-hide">
		<button class="ge-button-icon" import-data="true">
			<span class="ge-icon ge-import-icon"><span class="ge-relative">I</span></span>
			<span class="ge-new-text">Import</span>
		</button>
	</div>
</div><div total-question="<?= $Count  ?>" section-container="true">
<?php if ($Count): ?>

<?php foreach ($this->DraftPractice as $QuestionID => $Data): ?>
	
<section class="ge-section" data-question="<?= $QuestionID ?>"><hidden>
	<input type="hidden" data-question value="<?= $QuestionID ?>"></hidden>
	<div class="ge-count">Question <span question-number><?= $Count = $Count - 1 ?></span></div>
	<ge-content data-question="<?= $QuestionID ?>"><ge-question><div class="ge-writer-space"><div question-container="true">
		<ge-writer><h5 class="ge-text" data="question-title">
			<div contenteditable="true" question="true" data-question="<?= $QuestionID ?>" rule="textbox" question-title="true" class="ge-paragraph"><?= $Data["title"] ?></div>
			<?php if ($Data["title"] == null): ?>
				<div class="ge-absolute ge-title ge-block"><span class="ge-placeholder">Write the question title here...
				</span></div>
			<?php endif ?>
			</h5>
		</ge-writer>
		<ge-writer class="ge-bottom-8" data-question="<?= $QuestionID ?>">
			<?php if ($Data["question"]): ?>
				<?= $Data["question"] ?>
			<?php else: ?>
				<h4 class="ge-text" data="question-content">
					<div contenteditable="true" question="true" rule="textbox" question-content="true" class="ge-paragraph" data-question="<?= $QuestionID ?>"></div>
					<div class="ge-absolute ge-content ge-block"><span class="ge-placeholder">Write the question content here...</span></div>
				</h4>
			<?php endif ?>
		</ge-writer></div>
		<div><button class="ge-button-icon" new-answer="true">
			<span class="ge-icon ge-answer-icon"><span class="ge-relative">A</span></span>
			<span class="ge-new-text">Add a new answer</span>
</button></div></div></ge-question>
<?php if (isset($Data["answer"])): $Answers = $Data["answer"] ?>

<?php foreach ($Answers as $AnswerID => $Answer): ?>
<ge-answer data-answer="<?= $AnswerID ?>"><div><div class="ge-relative"><div class="ge-absolute ge-block">
	<button class="ge-button-icon <?= $Answer["correct"] ? "ge-active": null ?>" ge-correct="true" data-answer="<?= $AnswerID ?>" data-question="<?= $QuestionID ?>"><span class="ge-icon ge-correct-icon"><span class="ge-relative">C</span></span></button>
	</div><ge-writer class="ge-space" data-answer="<?= $AnswerID ?>"><?= $Answer["answer"] ?>
		<?php if ($Answer["answer"] == null): ?>
			<h5 class="ge-text ge-answer" data="answer-content">
				<div contenteditable="true" answer-content="true" rule="textbox" class="ge-paragraph"></div>
				<div class="ge-absolute ge-content ge-block"><span class="ge-placeholder">Write the answer here...</span></div>
			</h5>
		<?php endif ?>
	</ge-writer></div>	
</div></ge-answer>
<?php endforeach ?>
	
<?php endif ?>

</ge-content></section>
<?php endforeach ?>

<?php endif ?>
</div></div>
</div></div></div>