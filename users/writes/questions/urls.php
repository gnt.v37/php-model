<?php

/**
 * @Author: Rot
 * @Date:   2017-11-20 11:33:48
 * @Last Modified by:   Rot
 * @Last Modified time: 2017-12-10 23:23:56
 */

import(users.writes.questions.views, false);

$Urls = [

	/**
	 * URL get question page
	 */
	"/^(\/(?<ArticleID>\d+))?((?<IsEdit>\/edit))?$/" => views.PracticeEditView,

	/**
	 *
	 * @uses 	Ajax save question title
	 * 
	 * @var 	Regex
	 * 
	 */
	"/^\/save_question_title\/\d+$/"			=> views.SaveQuestionTitleObject,
	"/^\/save_question_content\/\d+$/"			=> views.SaveQuestionContentObject,
	"/^\/clone_image\/\d+$/"					=> views.SaveCloneImageObject,
	"/^\/blur_image\/\d+$/"						=> views.SaveBlurImageObject,
	"/^\/save_answer\/\d+$/"					=> views.SaveAnswerObject,
	"/^\/save_correct\/\d+$/"					=> views.SaveCorrectObject,
	"/^\/delete_image\/\d+$/"					=> views.DeletePracticeImageObject,
	"/^\/delete_answer\/\d+$/"					=> views.DeleteAnswerObject,
	"/^\/delete_question\/\d+$/"				=> views.DeleteQuestionObject,


	/**
	 *
	 * @uses 	Ajax import
	 * 
	 * @var 	Regex
	 * 
	 */
	
	"/^\/import_data\/\d+$/"				=> views.ImportDataObject,
]

?>