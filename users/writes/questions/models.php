<?php

/**
 * @Author: Rot
 * @Date:   2017-11-20 11:33:29
 * @Last Modified by:   Rot
 * @Last Modified time: 2017-12-07 13:11:57
 */

namespace System\Models;

import(geye.models);

abstract class WriterProcedure extends Procedure
{
	public $UserID;

	public $ArticleID;

	public $IsEdit;
}

abstract class WriterQuestion extends WriterProcedure
{
	public $QuestionID;

}

class Article extends Schema 
{
	public $Nerve = "articles";
}

class DraftArticle extends Schema 
{
	public $Nerve = "draft_articles";
}

class ArticleQuestions extends WriterProcedure implements Nullable
{
	protected $Name = "GArticleQuestions";

	public static function Nullable ()
	{
		return "IsEdit";
	}

}

class SaveQuestionContent extends WriterQuestion implements Nullable
{
	protected $Name = "DSaveWriteQuestionContent";

	public $Question;

	public static function Nullable ()
	{
		return ["ArticleID", "QuestionID"];
	}
}

class SaveQuestionTitle extends WriterQuestion implements Nullable
{
	protected $Name = "DSaveWriteQuestionTitle";

	public $Title;

	public static function Nullable ()
	{
		return ["ArticleID", "QuestionID"];
	}
	
}

class SaveAnswer extends WriterQuestion implements Nullable
{
	protected $Name = "DSaveWriteAnswer";

	public $AnswerID;

	public $Answer;

	public static function Nullable ()
	{
		return ["ArticleID", "QuestionID", "AnswerID"];
	}

}

class SaveCorrect extends WriterQuestion
{
	protected $Name = "DSaveWriteCorrect";

	public $AnswerID;

	public $Correct;

}

class SaveImage extends Procedure implements Nullable, Humanize
{
	protected $Name = "DSavePracticeImage";

	public $Image;

	public $QuestionID;

	public $AnswerID;

	public $UserID;

	public $ArticleID;

	public static function Nullable ()
	{
		return ["Image", "QuestionID", "AnswerID"];
	}
	
}

class DeleteQuestion extends WriterQuestion
{
	protected $Name = "DDeleteWriteQuestion";

}

class DeleteAnswer extends WriterQuestion 
{
	protected $Name = "DDeleteWriteAnswer";

	public $AnswerID;
}

class DeletePracticeImage extends WriterQuestion implements Nullable, Humanize
{
	protected $Name = "DDeletePracticeImage";
	
	public $ImageID;

	public $AnswerID;

	public static function Nullable ()
	{
		return ["QuestionID", "AnswerID"];
	}

}

?>