<?php

/**
 * @Author: Rot
 * @Date:   2017-11-20 11:33:53
 * @Last Modified by:   Rot
 * @Last Modified time: 2017-12-11 11:46:55
 */

namespace System\Deeps;

import(geye.modules.imagick);

import(geye.modules.msoffice);

use System\Forms\QuestionForm;

class QuestionDeep
{
	public function GetArticleQuestions ()
	{
		$Datas = [];

		if (isset($this->ArticleQuestions))
		{
			foreach ($this->ArticleQuestions as $Key => $Value) 
			{
				$QuestionID = $Value["question_id"];

				if (array_key_exists($QuestionID, $Datas))
				{
					$AnswerID = $Value["answer_id"];

					if ($AnswerID)
					{
						$Answer[$AnswerID] = array (
							"answer" 	=> $Value["answer"],
							"correct"	=> $Value["correct"]
						);

						if (isset($Datas[$QuestionID]["answer"]))
						{
							$Datas[$QuestionID]["answer"] += $Answer;
						}
						else
						{
							$Datas[$QuestionID]["answer"] = $Answer;
						}
					}
				}
				else
				{
					$Answer = null;

					$AnswerID = $Value["answer_id"];

					if ($AnswerID)
					{
						$Answer[$AnswerID] = array (
							"answer" 	=> $Value["answer"],
							"correct"	=> $Value["correct"]
						);
					}
					else
					{
						$Answer = null;
					}
					

					$Question = array (
						"title" 		=> $Value["title"],
						"question" 		=> $Value["question"],
					);

					if ($Answer)
					{
						$Question["answer"] = $Answer;
					}

					$Datas[$QuestionID] = $Question;
				}
			}
		}

		

		return $Datas;
	}

	public function CloneImage ()
	{
		$Magic = new \Imagick();

		$Form = new QuestionForm(null, null);

		$URLImage = $Form->Search("URLImage");

		$File = $Magic->Clone(root.users.writes.questions.images, $URLImage);

		$Size = getimagesize($File);

		return ["Width" => $Size[0], "Height" => $Size[1], "Mime" => $Size["mime"], "Name" => str_replace(root, "", $File)];
	}

	public function BlurImage()
	{
		$Magic = new \Imagick();

		$Form = $this->QuestionForm;

		$Form->Search("ImageBase64");

		$Form->Search("URLImage");

		$Form->Search("Mime");

		$File = $Magic->Image(
			root.practices.images, 
			$Form->ImageBase64, 
			$Form->Mime
		);

		$Magic->Destroy($this->QuestionForm->URLImage);

		$File = str_replace(root, "", $File);

		$ImageID = $this->SaveImage->Call([$File, $Form->QuestionID, $Form->AnswerID, $Form->UserID, $Form->ArticleID]);

		return ["Name" => $File, "ID" => $ImageID[0]["image"]];
	}

	public function DeleteImage ()
	{
		$Magic = new \Imagick();

		$Form = $this->QuestionForm;

		$Result = $this->DeletePracticeImage->Call([$Form->ImageID, $Form->AnswerID, $Form->QuestionID,  $Form->UserID, $Form->ArticleID, $Form->IsEdit]);

		$Image = $Result[0]["image"];

		if ($Image && is_dir($Image) == false)
		{
			$Magic->Destroy($Image);

			return ["Result" => 1];
		}

		return ["Result" => 0];
	}

	public function ImportData ()
	{
		$File = $_FILES["File"];

		$FileName = $File["name"];

		if (end_with($FileName, "xls") == false && end_with($FileName, "xlsx") == false)
		{
			return [];
		}

		$Reader = \PHPExcel_IOFactory::createReaderForFile($File["tmp_name"]);

		$Data = $Reader->load($File["tmp_name"]);

		$SheetCouter = $Data->getSheetCount();

		$Result = [];

		$Empty = true;

		for ($i = 0; $i < $SheetCouter; $i++)
		{
			$Sheet = $Data->getSheet($i);

			$Rows = $Sheet->toArray(null, true, true, true);

			$RowIndex = 0;

			foreach ($Rows as $RowColumns)
			{
				$AnswerIndex = 0;

				foreach ($RowColumns as $Column)
				{
					$Fields = explode(",", $Column);

					foreach ($Fields as $Field)
					{
						preg_match("/(?<Attribute>(answer|correct|title|question)):(?<Value>.*?)$/", $Field, $Matches);

						if ($Matches)
						{
							$Attribute = trim($Matches["Attribute"]);

							$Value = trim($Matches["Value"]);

							$Answer = [];

							if (strtolower($Attribute) == "answer")
							{
								$Answer[$AnswerIndex]["answer"] = $Value;

								if (array_key_exists("answers", $Result[$RowIndex]))
								{
									$Result[$RowIndex]["answers"] += $Answer;
								}
								else
								{
									$Result[$RowIndex]["answers"] = $Answer;
								}
							}
							else if (strtolower($Attribute) == "correct")
							{
								$Answer["correct"] = $Value;

								$Result[$RowIndex]["answers"][$AnswerIndex] += $Answer;

								$AnswerIndex++;
							}
							else
							{
								$Result[$RowIndex][$Attribute] = $Value;
							}

							$Empty = false;
						}
					}
				}

				if ($Empty == false)
				{
					$RowIndex++;
				}
			}
		}

		return $Result;
	}

	public function GetPubish ()
	{
		$Form = $this->QuestionForm;

		if ($Form->IsEdit == false)
		{
			return $this->DraftArticle->Remember(["draft_article_id" => $Form->ArticleID, "user_id" => $Form->UserID], true)
				->Get("publish_at");
		}
		else
		{
			return null;
		}
	}

	public function GetAnswer ()
	{
		return $this->SaveAnswer[0];
	}

	public function GetDescription ()
	{
		return $this->SaveDescription;
	}

	public function GetQuestionTitle ()
	{
		return $this->SaveQuestionTitle[0];
	}

	public function GetQuestionContent ()
	{
		return $this->SaveQuestionContent[0];
	}

	public function GetDeleteAnswer ()
	{
		return $this->DeleteAnswer[0];
	}

	public function GetDeleteQuestion ()
	{
		return $this->DeleteQuestion[0];
	}

	public function GetArticleID ()
	{
		$Form = $this->QuestionForm;

		if ($Form->IsEdit)
		{
			$Count = $this->Article->Count(["article_id" => $Form->ArticleID, "user_id" => $Form->UserID]);

			if ($Count["Total"])
			{
				return $Form->ArticleID;
			}
		
		}
		else
		{
			$Count = $this->DraftArticle->Count(["draft_article_id" => $Form->ArticleID, "user_id" => $Form->UserID]);

			if ($Count["Total"])
			{
				return $Form->ArticleID;
			}
		}

		if ($Form->ArticleID == null)
		{
			return null;
		}

		return ["Permission" => 403];
	}

	public function GetIsEdit ()
	{
		return $this->QuestionForm->IsEdit;
	}
}

?>