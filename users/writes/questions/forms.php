<?php

/**
 * @Author: Rot
 * @Date:   2017-11-20 11:33:36
 * @Last Modified by:   Rot
 * @Last Modified time: 2017-12-05 12:42:22
 */

namespace System\Forms;

include_once geye.forms;

class QuestionForm extends Form 
{
	public $QuestionID;

	public $AnswerID;

	public $Question;

	public $Title;

	public $Answer;

	public $Correct;

	public $UserID;

	public $ArticleID;

	public $IsEdit;

	public $Image;

	public $ImageID;

	public $URLImage;

	public $ImageBase64;

	public $Mime;
}

?>