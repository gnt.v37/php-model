(function () {

class Model
{
    constructor ()
    {
        this.Location = location.href;
    }

    Update (Data)
    {
        return System.post(this.Location + `/${new Date().getTime()}/update`, Data);
    }

}

System.Model = Model;

})();