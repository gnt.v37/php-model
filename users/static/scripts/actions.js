/*
* @Author: Rot
* @Date:   2017-11-18 20:52:22
* @Last Modified by:   Rot
* @Last Modified time: 2017-12-02 09:46:20
*/
(function () {

class Action 
{
	constructor ()
	{

		let Edit = document.querySelector("[ge-user-edit]");

		let Business = new System.Business();

		var ImageShow 	= document.querySelector("[ge-user-avatar]");

		if (Edit)
		{
			this.Edit(Business, Edit, ImageShow);
		}

		this.ColorThief(ImageShow);

		this.Chart();

		this.Follow();

	}

	Edit (Business, Edit, ImageShow)
	{
		var FirstName 	= document.querySelector("[ge-user-firstname]");

		var LastName 	= document.querySelector("[ge-user-lastname]");

		var Description = document.querySelector("[ge-user-description]");

		var PickImage 	= document.querySelector(".ge-pick-image");

		var ImageFile 	= document.querySelector("[ge-user-model]");
		
		var ButtonImage = PickImage.querySelector("[ge-user-button-image]");

		var ImageInput 	= ImageFile.querySelector(".ge-input");

		var SubmitModel = ImageFile.querySelector("[ge-submit]");

		var CancelModel = ImageFile.querySelector("[ge-cancel]");

		Edit.addEventListener("click", function (e) {

			if (this.hasAttribute("disabled"))
			{
				e.preventDefault();

				return false;
			}

			if (this.getAttribute("ge-user-update") == "true")
			{
				this.setAttribute("ge-user-update", "false");

				this.setAttribute("disabled", "disabled");
				
				System.Process = System.Process.then(function () {

					return new Promise(function(Resolve, Reject) {

						Business.Update(Edit, FirstName, LastName, Description, PickImage).then(function () {
							Resolve();
						});

					})

				})
			}
			else
			{
				Business.Edit(Edit, FirstName, LastName, Description, PickImage);
			}
			
		})

		ButtonImage.addEventListener("click", function () {
			Business.EditImageShow(ImageFile);
		})

		SubmitModel.addEventListener("click", function () {
			Business.SetImage(ImageFile, ImageInput, ImageShow);
		})

		CancelModel.addEventListener("click", function () {
			ImageFile.style.display = "";
		})


	}

	Follow ()
	{
		let FollowElement = document.querySelector(".ge-follow");

		let FollowerElement = document.querySelector(".ge-follower");

		const Follow = location.href;

		if (FollowElement == null)
		{
			return null;
		}

		FollowElement.addEventListener("click", function () {

			System.Process = System.Process.then(function () {

				return new Promise(function (Resolve, Reject) {

					System.get(Follow + `/${new Date().getTime()}` + "/follow" ).then(function () {

						var ClassList = FollowElement.classList;

						if (ClassList.contains("ge-following"))
						{
							ClassList.remove("ge-following");

							FollowElement.innerHTML = "Follow";

							FollowerElement.innerHTML = parseInt(FollowerElement.innerHTML) - 1;
						}
						else
						{
							ClassList.add("ge-following");

							FollowElement.innerHTML = "Following";

							FollowerElement.innerHTML = parseInt(FollowerElement.innerHTML) + 1;
						}

						Resolve();
					})

				})

			})

		})
	}

	ColorThief (ImageShow)
	{
		var Thief = new ColorThief();

		var ImageBorders = document.querySelectorAll(".ge-avatar-radius");

		ImageShow.onload = function ()
		{
			setTimeout(function () {

				var Color = Thief.getColor(ImageShow).toString();

				ImageShow.nextElementSibling.style.fill = `rgb(${Color})`;

				ImageBorders.forEach( function(ImageBorder) {

					ImageBorder.style.fill = `rgb(${Color})`;

				});

			}, 64)
			
		}
	}

	Chart ()
	{
		var ScoreChart = document.querySelector(".ge-score-chart");

		var Data = JSON.parse(ScoreChart.querySelector("input").value);

		var Index = [], Scores = [], Length = Data.length;

		for(var i = Length - 1; i >= 0; i--)
		{
			Index.push(Length - i);
			Scores.push(Data[i].score);
		}

		var Content = ScoreChart.querySelector("[ge-chart]");

		new Chart(Content, {
			type: "line",
			data: 
			{
				labels: Index,
				datasets: 
				[
					{
						data:Scores,
						backgroundColor: "rgb(54, 162, 235)",
						borderColor: "rgb(54, 162, 235)",
					},
					
				]
			},
			options: {
				legend: 
				{ 
					position: "bottom", 
					display: false,
				},
				 scales: {
				 	xAxes: 
				 	[
						{
							gridLines: {
								color: "rgb(255, 255, 255)"
							}
						}
				 	],
					yAxes: 
					[
						{
							gridLines: {
								color: "rgb(255, 255, 255)"
							}
						}
					]
				},
		
			}

		})
	}

}


System.Action = Action;

})();