(function () {

class Business
{
    constructor ()
    {
        this.Model = new System.Model();

        this.UpdateData = new FormData();
    }

    Edit (Edit, FirstName, LastName, Description, PickImage)
    {
        FirstName.setAttribute("contenteditable", "true");

		LastName.setAttribute("contenteditable", "true");

		Description.setAttribute("contenteditable", "true");

		PickImage.style.display = "block";

		Edit.innerHTML = "Update";

        Edit.setAttribute("ge-user-update", "true");

		setTimeout(function () {
			LastName.focus();
		}, 64)
    }

    EditImageShow (ImageFile)
    {
        var IsShow = ImageFile.style;

		IsShow.display == "" ? IsShow.display = "block" : IsShow.display = "";
    }

    SetImage (ImageFile, ImageInput, ImageShow)
    {
        if (ImageShow.src != ImageInput.value)
        {
            ImageShow.src = ImageInput.value;

            this.UpdateData.set("ImageLink", ImageInput.value);
        }

		ImageFile.style.display = "";
    }

    Update (Edit, FirstName, LastName, Description, PickImage)
    {
        Edit.innerHTML = "Pending";

         FirstName.setAttribute("contenteditable", "false");

        LastName.setAttribute("contenteditable", "false");

        Description.setAttribute("contenteditable", "false");

        this.UpdateData.set("FirstName", FirstName.textContent);

        this.UpdateData.set("LastName", LastName.textContent);

        this.UpdateData.set("Description", Description.textContent);

        return this.Model.Update(this.UpdateData).then( () => {

            if (this.UpdateData.get("ImageLink"))
            {
                window.location.reload();
            }

            this.UpdateData.delete("FirstName");

            this.UpdateData.delete("LastName");
        
            this.UpdateData.delete("Description");

            this.UpdateData.delete("ImageLink");

            PickImage.style.display = "";

            Edit.innerHTML = "Edit";

            Edit.removeAttribute("disabled");
        })
    }
}

System.Business = Business;

})();