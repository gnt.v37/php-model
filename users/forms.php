<?php

namespace System\Forms;

include_once geye.forms;

class UserForm extends Form
{
	public $Attributes = ["UserID", "FirstName", "LastName", "Description", "ImageLink", "Follower"];

	public $LimitRecord = 8;

	public $OffsetRecord = 0;
}

?>