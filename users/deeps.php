<?php

namespace System\Deeps;

import(geye.modules.imagick);

class UserDeep
{
	public function User ()
	{
		return $this->User->Remember(
			["user_id" => $this->UserForm->UserID], 
		true)->Get();
	}

	public function UserFollow ()
	{
		$Following = $this->UserFollow->Count(["follower" => $this->UserForm->UserID]);

		$Follwer = $this->UserFollow->Count(["following" => $this->UserForm->UserID]);

		return [$Following["Total"], $Follwer["Total"]];
	}

	public function Following ()
	{
		if (isset($_SESSION[__SIGNIN__]))
		{
			return $this->UserFollow->Count(["follower" => $_SESSION[__SIGNIN__]["userid"], "following" => $this->UserForm->UserID]);
		}

		return null;
	}

	public function Follow ()
	{

		if (isset($_SESSION[__SIGNIN__]))
		{
			$Followed = self::Following();

			if ($Followed["Total"])
			{
				$this->UserFollow->Forget(["follower" => $_SESSION[__SIGNIN__]["userid"], "following" => $this->UserForm->UserID]);
			}
			else
			{
				$this->UserFollow->Write(["follower" => $_SESSION[__SIGNIN__]["userid"], "following" => $this->UserForm->UserID]);
			}
		}
		
		return null;
	}



	public function UserTimeLine ()
	{
		return $this->UserTimeLine->Remember(["user_id" => $this->UserForm->UserID])
			->Limit($this->UserForm->LimitRecord, $this->UserForm->OffsetRecord)->Get();
	}

	public function AboutPractice()
	{
		$Finished = $this->UserTimeLine->Count(["user_id" => $this->UserForm->UserID, "status" => 1]);

		$UnFinished = $this->UserTimeLine->Count(["user_id" => $this->UserForm->UserID, "status" => 0]);

		return [$Finished["Total"], $UnFinished["Total"]];
	}

	public function ScoreChart ()
	{
		return $this->UserTimeLine->Remember(["user_id" => $this->UserForm->UserID, "category" => 2, "status" => 1])
			->Limit(18)->Get("score");
	}

	public function UserUpdate ()
	{
		$Form = $this->UserForm;

		$_SESSION[__SIGNIN__]["session_firstname"] = $Form->FirstName;

		$_SESSION[__SIGNIN__]["session_lastname"] = $Form->LastName;

		$_SESSION[__SIGNIN__]["session_description"] = $Form->Description;

		if ($Form->ImageLink)
		{
			$Image = new \Imagick();

			$ImageName = md5($Form->LastName.mktime());

			$Link = page.users.images;

			$ImageReal = $Image->Resize(users.images."158", $Form->ImageLink, $ImageName, 158);

			$Image->Resize(users.images."64", $Link."158/{$ImageReal}", $ImageName, 64);

			$Image->Resize(users.images."40", $Link."64/{$ImageReal}", $ImageName, 40);

			$ImageLocal = $_SESSION[__SIGNIN__]["session_image_local"];

			$_SESSION[__SIGNIN__]["session_image_local"] = $ImageReal;

			$_SESSION[__SIGNIN__]["session_image_link"] = $Form->ImageLink;

			$Image->Destroy(users.images."158/".$ImageLocal);
			$Image->Destroy(users.images."64/".$ImageLocal);
			$Image->Destroy(users.images."40/".$ImageLocal);

			return $this->User->Change([

				"firstname" => $Form->FirstName, 

				"lastname" => $Form->LastName, 

				"description" => $Form->Description,

				"image_local" => $ImageReal,

				"image_link" => $Form->ImageLink

			], ["user_id" => $Form->UserID]);

		}
		

		return $this->User->Change([

			"firstname" => $Form->FirstName, 

			"lastname" => $Form->LastName, 

			"description" => $Form->Description,

		], ["user_id" => $Form->UserID]);
	}

}

?>