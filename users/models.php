<?php
namespace System\Models;

import(geye.models);

class User extends Schema
{
	protected $Nerve = "users";

	protected $Prime = "user_id";

	public $NerveCells = [
		"user_id", "firstname", "lastname",
		"email", "image_local", "image_link",
		"role", "description", "twitter", 
		"instagram", "created_at"
	];

}

class UserTimeLine extends Schema 
{
	protected $Nerve = "vuser_timelines";
}

class UserFollow extends Schema 
{
	protected $Nerve = "user_follows";
}

?>