<?php

/**
 * @Author: Rot
 * @Date:   2017-08-29 11:30:00
 * @Last Modified by:   Rot
 * @Last Modified time: 2017-11-30 21:27:35
 */

import(users.views, false);

$Urls = [
	"/^(?<UserID>\d+)$/"				=> views.UserView,
	"/^(?<UserID>\d+)\/articles/"		=> articles.urls,
	"/^(?<UserID>\d+)\/practices/"		=> practices.urls,
	"/^(?<UserID>\d+)\/\d+\/scroll$/"	=> views.UserScroll,
	"/^(?<UserID>\d+)\/\d+\/update$/"	=> views.UpdateUserObject,
	"/^(?<UserID>\d+)\/\d+\/follow$/"	=> views.FollowObject,
]

?>