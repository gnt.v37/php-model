<?php

/**
 * @Author: Rot
 * @Date:   2017-11-19 07:54:38
 * @Last Modified by:   Rot
 * @Last Modified time: 2017-12-02 15:12:44
 */

namespace System\Deeps;

class PracticeDeep 
{
	public function Practices ($IsComplete = 0)
	{
		$Form = $this->UserForm;

		return $this->Practice->Chain(["articles" => "article_id"])
		->Remember(["status" => $IsComplete, "practices.user_id" => $Form->UserID])
			->Sort(["practices.created_at" => "desc"])
				->Limit([$Form->LimitRecord, $Form->OffsetRecord])
					->Get(["articles.article_id", "practices.user_id", "practices.created_at", "finish_time", "limit_time", "times", "score", "status", "title"]);
	}

	public function PracticeCounter ()
	{
		
	}
}
?>