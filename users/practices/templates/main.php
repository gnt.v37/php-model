<?php

/**
 * @Author: Rot
 * @Date:   2017-11-09 10:51:19
 * @Last Modified by:   Rot
 * @Last Modified time: 2017-11-24 22:41:06
 */

$Header = "";

preg_match("/^.*?\/@(?<UserID>\d+)\//", $_SERVER["REQUEST_URI"], $Matches);

if ($Matches["UserID"] != $_SESSION[__SIGNIN__]["userid"])
{
	$ContentTemplate = assets.templates."403.php";
}

include_once __TEMPLATE__;

?>