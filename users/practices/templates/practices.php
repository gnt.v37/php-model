<?php foreach ($this->Practices as $Practice): ?>
	<li>
		<div class="ge-tab-light">
			<div class="ge-relativ">
				<div>
					<?php $TimeLeft = floor($Practice["limit_time"] - (strtotime(date("Y-m-d h:i:sa")) - strtotime($Practice["created_at"]))); ?>
					<ge-content>
						<div>
							<div class="ge-row">
								<div class="ge-col l4 m4">
									<div><h4><a href="<?= page."view?v=".$Practice["article_id"] ?>" ><?= $Practice["title"]?></a></h4></div>
									<?php if ($Practice["status"] == 0): ?>
										<div class="ge-text-color ge-top-4"><time article="<?= $Practice["article_id"] ?>" time-left="<?= $TimeLeft ?>"><?= $TimeLeft ?></time> left</div>
									<?php endif ?>
								</div>
								<div class="ge-col l4 m4">
									<div>
										<?php if ($Practice["status"]): ?>
											<h1 class="ge-text-score"><a href="<?= page."summary/@{$Practice["user_id"]}/{$Practice["article_id"]}/{$Practice["times"]}" ?>" class="ge-flex-cc"><span class="ge-score">Score</span><span><?= $Practice["score"] ?></span></a></h1>
										<?php else: ?>
											<h3 class="ge-text-score"><a href="<?= page."try?p={$Practice["article_id"]}" ?>" class="ge-flex-cc"><span>You haven't finished this practice</span></a></h3>
										<?php endif ?>
										
									</div>
								</div>
								<div class="ge-col l4 m4">
									<div class="ge-text-color">
										<div class="ge-right-align"><span class="ge-right-4">Started</span><time datetime="<?= $Practice["created_at"] ?>"><?= $Practice["created_at"] ?></time></div>
										<?php if ($Practice["finish_time"]): ?>
											<div class="ge-right-align ge-top-4"><span class="ge-right-4">Finished</span><time datetime="<?= $Practice["finish_time"] ?>"><?= $Practice["finish_time"] ?></time></div>
										<?php endif ?>
										
									</div>
								</div>
							</div>
						</div>
					</ge-content>
				</div>
			</div>
		</div>
	</li>
<?php endforeach ?>
