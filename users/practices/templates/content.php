<div class="ge-top-32"><div class="ge-main-width"><div>
<div class="ge-title">
	<div class="ge-row">
		<div class="ge-left">
			<h2>Your practices</h2>
		</div>
		<div class="ge-right">
			<div>
				<a href="<?= page ?>" class="ge-write">Try more</a>
			</div>
		</div>
	</div>
</div>
<div><div class="ge-article-mode">
	<ul class="ge-row">
		<li class="ge-left ge-fstyle" ge-mode="true"><div><a href="<?= page."@".$_SESSION[__SIGNIN__]["userid"]."/practices/incomplete" ?>" class="ge-button">Incomplete <?= $this->Counter[0] ?></a></div></li>
		<li class="ge-left ge-fstyle" ge-mode="true"><div><a href="<?= page."@".$_SESSION[__SIGNIN__]["userid"]."/practices/complete" ?>" class="ge-button">Complete <?= $this->Counter[1] ?></a></div></li>
	</ul>
</div><div><ul class="ge-container">
<?php include_once $App.templates."practices.php"; ?>
</ul></div></div></div></div></div>