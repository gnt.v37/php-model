<?php

/**
 * @Author: Rot
 * @Date:   2017-11-19 07:54:24
 * @Last Modified by:   Rot
 * @Last Modified time: 2017-11-20 11:07:24
 */

import(users.practices.views, false);

$Urls = [
	"/^\/incomplete$/"						=> views.IncompleteView,
	"/^\/complete$/"						=> views.CompleteView,
	"/^\/incomplete\/\d+\/scroll/"			=> views.IncompleteObject,
	"/^\/complete\/\d+\/scroll/"			=> views.CompleteObject,
]

?>