/*
* @Author: Rot
* @Date:   2017-11-26 12:29:53
* @Last Modified by:   Rot
* @Last Modified time: 2017-11-26 15:20:11
*/

(function () {

class Models
{
	constructor ()
	{
		this.Page = document.querySelector(`[idata="page"]`).value;
	}


	FinishPractice (Data)
	{
		return System.post(`${this.Page}try/${new Date().getTime()}/finish_practice`, Data);
	}
}

System.Models = Models;

})();