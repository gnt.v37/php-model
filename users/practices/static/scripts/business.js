/*
* @Author: Rot
* @Date:   2017-11-26 12:29:42
* @Last Modified by:   Rot
* @Last Modified time: 2017-11-30 23:08:19
*/

(function () {

class Business
{
	constructor ()
	{
		this.Models = new System.Models();
	}

	CountDown (Time, Days, Hours, Minutes, Seconds)
	{
		if (Days > 0)
		{
			Time += "<span>%D</span> Day";

			if (Days > 1)
			{
				Time += "s";
			}
		}

		if (Hours > 0)
		{
			Time += " <span>%H</span> hour";

			if (Hours > 1)
			{
				Time += "s";
			}
		}

		if (Minutes > 0)
		{
			Time += " <span>%M</span> minute";

			if (Minutes > 1)
			{
				Time += "s";
			}
		}

		if (Seconds > 0)
		{
			Time += " <span>%S</span> second";

			if (Seconds > 1)
			{
				Time += "s";
			}
		}

		return Time;
	}

	Finish (Element, ArticleID)
	{
		console.log(Element);

		var Parent = Element.parentElement;

		let Form = new FormData();

		Form.set("ArticleID", ArticleID);

		while (Parent && Parent.nodeName != "LI")
		{
			Parent = Parent.parentElement;
		}

		if (Parent)
		{
			return this.Models.FinishPractice(Form).then(function () {
				Parent.remove();
			});
		}

		return Promise.resolve();
	}
}

System.Business = Business;

})();