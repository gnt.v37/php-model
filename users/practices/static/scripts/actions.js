/*
* @Author: Rot
* @Date:   2017-11-26 12:29:37
* @Last Modified by:   Rot
* @Last Modified time: 2017-11-30 23:08:02
*/

(function () {

let Process = Promise.resolve();

class Practice 
{
	constructor ()
	{
		let Business = new System.Business();

		this.TimeLeft(Business);
	}

	TimeLeft (Business)
	{
		document.querySelectorAll("[time-left]").forEach(function (Element) {

			var Timeline = new Date().getTime() + parseInt(Element.getAttribute("time-left")) * 1000;

			let Time, Days, Hours, Minutes, Seconds;

			const ArticleID = Element.getAttribute("article");

			if (Timeline < new Date().getTime())
			{
				Timeline = new Date().getTime() + 2000;
			}

			console.log(Element);

			$(Element).countdown(Timeline, function (e) {

				Time = "";

				Days = e.strftime("%D");

				Hours = e.strftime("%H");

				Minutes = e.strftime("%M");

				Seconds = e.strftime("%S");

				Time = Business.CountDown(Time, Days, Hours, Minutes, Seconds);

				this.innerHTML = e.strftime(Time);

			}).on("finish.countdown", function () {

				Process = Process.then( () => {

					return new Promise( (Resolve, Reject) => {

						Business.Finish(this, ArticleID).then(function () {
							
							Resolve();
							
						});

					})
					
				})
				

				
			})
		})
	}
}

System.Practice = Practice;

})()