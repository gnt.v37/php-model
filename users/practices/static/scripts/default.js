/*
* @Author: Rot
* @Date:   2017-11-20 08:35:45
* @Last Modified by:   Rot
* @Last Modified time: 2017-11-26 16:11:40
*/
(function () {

window.onload = function ()
{
	const Location = location.href;
	
	let Mode = document.querySelectorAll("[ge-mode]");

	let Container = document.querySelector(".ge-container");

	if (Location.endsWith("incomplete"))
	{
		Mode[0].classList.add("ge-active");

		new System.Practice();
	}
	else
	{
		Mode[1].classList.add("ge-active");
	}

	let Scroll = new System.Scroll ({
		Models: `${Location}/${new Date().getTime()}/scroll`,
		Offsets: 12,
		Limits: 8
	});

	Scroll.Done(function (Response) {

		var Children = Response.children;

		var Length = Children.length;

		for (var i = 0; i < Length; i++)
		{
			Container.appendChild(Children[0]);
		}

	})

	
}

})();