<?php

/**
 * @Author: Rot
 * @Date:   2017-11-19 07:53:59
 * @Last Modified by:   Rot
 * @Last Modified time: 2017-12-06 16:50:16
 */

namespace System\Views;

import(geye.views.generic);

class IncompleteView extends GenericView
{
	protected $Models = ["Practice"];

	protected $Styles = [users.styles => "user.css"];

	protected $Scripts = [
		assets.scripts => ["scroll/actions.js", "scroll/business.js", "scroll/models.js"],
		practices.scripts.modules => "jquery.countdown.min.js"
	];

	protected function Resolver ()
	{
		$this->Practices = $this->Deeps->Practices(0);

		$this->Counter = $this->Deeps->PracticeCounter();

	}
}

class CompleteView extends IncompleteView
{
	protected function Resolver ()
	{
		$this->Practices = $this->Deeps->Practices(1);

		$this->Counter = $this->Deeps->PracticeCounter();

	}
}

class IncompleteObject extends GenericView
{
	protected $Models = "Practice";

	protected $Templates = "practices.php";

	protected function Resolver ()
	{
		$this->Practices = $this->Deeps->Practices(0);
	}
}

class CompleteObject extends IncompleteObject
{
	protected function Resolver ()
	{
		$this->Practices = $this->Deeps->Practices(1);
	}
}

?>