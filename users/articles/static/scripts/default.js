/*
* @Author: Rot
* @Date:   2017-11-20 08:28:00
* @Last Modified by:   Rot
* @Last Modified time: 2017-11-20 11:03:02
*/
(function () {

window.onload = function ()
{
	var Location = location.href;
	
	var Mode = document.querySelectorAll("[ge-mode]");

	if (Location.endsWith("drafts"))
	{
		Mode[0].classList.add("ge-active")
	}
	else
	{
		Mode[1].classList.add("ge-active");
	}

	let Container = document.querySelector(".ge-container");

	let Scroll = new System.Scroll ({
		Models: `${Location}/${new Date().getTime()}/scroll`,
		Offsets: 12,
		Limits: 8
	});

	Scroll.Done(function (Response) {

		var Children = Response.children;

		var Length = Children.length;

		for (var i = 0; i < Length; i++)
		{
			Container.appendChild(Children[0]);
		}

	})
}

})();