<?php

/**
 * @Author: Rot
 * @Date:   2017-11-19 07:54:38
 * @Last Modified by:   Rot
 * @Last Modified time: 2017-11-20 08:24:44
 */

namespace System\Deeps;

class ArticleDeep
{
	public function Drafts ()
	{
		$Form = $this->UserForm;

		return $this->DraftArticle->Remember(["user_id" => $Form->UserID])
				->Limit([$Form->LimitRecord, $Form->OffsetRecord])->Get();
	}

	public function Publics ()
	{
		$Form = $this->UserForm;

		return $this->Article->Remember(["user_id" => $Form->UserID])
				->Limit([$Form->LimitRecord, $Form->OffsetRecord])->Get();
	}

	public function ArticleCounter ()
	{
		$User = $this->UserForm->UserID;

		$Drafts = $this->DraftArticle->Count(["user_id" => $User]);

		$Publics = $this->Article->Count(["user_id" => $User]);

		return [$Drafts["Total"], $Publics["Total"]];
	}
	
}

?>