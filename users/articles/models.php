<?php

/**
 * @Author: Rot
 * @Date:   2017-11-19 07:53:52
 * @Last Modified by:   Rot
 * @Last Modified time: 2017-11-19 07:58:04
 */

namespace System\Models;

import(geye.models.schema);

class DraftArticle extends Schema
{
	protected $Nerve = "draft_articles";

	protected $NerveCells = ["draft_article_id", "title", "created_at", "last_edited_at"];

}

class Article extends Schema 
{
	protected $Nerve = "articles";

	protected $NerveCells = ["article_id", "title", "created_at", "published_at"];

}

?>