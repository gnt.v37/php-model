<?php foreach ($this->Articles as $Article): ?>
	<li>
		<div class="ge-tab-light">
			<div class="ge-row">
				<div class="ge-col l10">
					<h4 class="ge-limit-text"><a href="<?= page ?><?= isset($Article["article_id"]) ? "view?v={$Article["article_id"]}" : "write/{$Article["draft_article_id"]}"  ?>"><?= $Article["title"] ? $Article["title"] : "Untitled article" ?></a></h4>
				</div>
				<div class="ge-right">
					<time datetime="<?= $Article["created_at"] ?>" class="ge-text-color"><?= $Article["created_at"] ?></time>
				</div>
			</div>
			<?php if (isset($Article["last_edited_at"])): ?>
			<div class="ge-edited">
				<div class="ge-text-color"><span class="ge-right-4">Last edited</span><time datetime="<?= $Article["last_edited_at"] ?>"><?= $Article["last_edited_at"] ?></time></div>
			</div>
			<?php endif ?>
			
		</div>
	</li>
<?php endforeach ?>
