<div class="ge-top-32"><div class="ge-main-width"><div>
<div class="ge-title">
	<div class="ge-row">
		<div class="ge-left">
			<h2>Your articles</h2>
		</div>
		<div class="ge-right">
			<div>
				<a href="<?= page ?>write" class="ge-write">Write a article</a>
			</div>
		</div>
	</div>
</div>
<div><div class="ge-article-mode">
	<ul class="ge-row">
		<li class="ge-left ge-fstyle" ge-mode="true"><div><a href="<?= page."@".$_SESSION[__SIGNIN__]["userid"]."/articles/drafts" ?>" class="ge-button">Drafts <?= $this->Counter[0] ?></a></div></li>
		<li class="ge-left ge-fstyle" ge-mode="true"><div><a href="<?= page."@".$_SESSION[__SIGNIN__]["userid"]."/articles/publics" ?>" class="ge-button">Public <?= $this->Counter[1] ?></a></div></li>
	</ul>
</div><div><ul class="ge-container">
<?php include_once $App.templates."articles.php"; ?>
</ul></div></div></div></div></div>