<?php

/**
 * @Author: Rot
 * @Date:   2017-11-19 07:54:24
 * @Last Modified by:   Rot
 * @Last Modified time: 2017-11-20 10:24:48
 */
import(users.articles.views, false);

$Urls = [
	"/^\/$|\/drafts$/"			=> views.DraftView,
	"/^\/publics$/"				=> views.PublicView,
	"/^\/drafts\/\d+\/scroll/"	=> views.DraftObject,
	"/^\/publics\/\d+\/scroll/"	=> views.PublicObject,
	
]

?>