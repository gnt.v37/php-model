<?php

/**
 * @Author: Rot
 * @Date:   2017-11-19 07:53:59
 * @Last Modified by:   Rot
 * @Last Modified time: 2017-11-20 10:23:54
 */

namespace System\Views;

import(geye.views.generic);

class DraftView extends GenericView
{
	protected $Models = ["DraftArticle", "Article"];

	protected $Styles = [users.styles => "user.css"];

	protected $Scripts = [assets.scripts => ["scroll/models.js", "scroll/business.js", "scroll/actions.js"], "default.js"];

	protected function Resolver ()
	{
		$this->Articles = $this->Deeps->Drafts();

		$this->Counter = $this->Deeps->ArticleCounter();

	}
}

class PublicView extends DraftView
{
	protected function Resolver ()
	{
		$this->Articles = $this->Deeps->Publics();

		$this->Counter = $this->Deeps->ArticleCounter();

	}
}

class DraftObject extends GenericView
{
	protected $Models = "DraftArticle";

	protected $Templates = "articles.php";

	protected function Resolver ()
	{
		$this->Articles = $this->Deeps->Drafts();
	}
}

class PublicObject extends GenericView
{
	protected $Models = "Article";

	protected $Templates = "articles.php";

	protected function Resolver ()
	{
		$this->Articles = $this->Deeps->Publics();
	}
}

?>