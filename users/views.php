<?php
namespace System\Views;

import(geye.views.generic);

class UserView extends GenericView
{
	protected $Scripts = [summaries.scripts => "chart.bundle.min.js", assets.scripts => ["scroll/models.js", "scroll/business.js", "scroll/actions.js"]];
	
	protected $Models = ["User", "UserFollow", "UserTimeLine"];

	protected function Resolver ()
	{
		$this->User = $this->Deeps->User();

		$Follow = $this->Deeps->UserFollow();

		$this->Following = $Follow[0];

		$this->Followers = $Follow[1];

		$this->TimeLines = $this->Deeps->UserTimeLine();

		$this->ScoreChart = json_encode($this->Deeps->ScoreChart());

		$this->UserFollow = $this->Deeps->Following();

		$this->AboutPractice = $this->Deeps->AboutPractice();
	}
}

class UpdateUserObject extends ObjectView 
{
	protected $Models = "User";

	protected function Resolver ()
	{
		$this->Deeps->UserUpdate();
	}
}

class FollowObject extends ObjectView
{
	protected $Models = "UserFollow";

	protected function Resolver ()
	{
		$this->Deeps->Follow();
	}

}

class UserScroll extends GenericView
{
	protected $Models = ["UserTimeLine"];

	protected $Templates = "mixin.php";

	protected function Resolver ()
	{
		$this->TimeLines = $this->Deeps->UserTimeLine();
	}
}

?>