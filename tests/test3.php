<?php

/**
 * @Author: Rot
 * @Date:   2017-09-23 23:01:49
 * @Last Modified by:   Rot
 * @Last Modified time: 2017-09-23 23:01:51
 */
include_once("articles/models.php");

use System\Models\Article;
use System\Models\Comment;
use System\Models\ReplyComment;
use System\Models\CommentLove;
use System\Models\ArticleDetail;

class ArticleDeep
{
	private $ArticleID;

	public function __construct ($ArticleID = null)
	{
		$this->ArticleID = $ArticleID;
	}

	public function Content ()
	{
		$Article = new Article();

		return $Article->Search(
			["article_id" => $this->ArticleID]
		, true)->Chain(["employees" => "employee_id"])
					->Columns([
						"article_id",
						"employees.employee_id",
						"category_id",
						"title",
						"articles.image_local",
						"articles.image_link",
						"user_image_local" => "employees.image_local",
						"user_image_link" => "employees.image_link",
						"firstname",
						"lastname",
						"articles.description",
						"articles.created_at"
					]);
	}

	public function Header ()
	{
		$Article = new Article();

		return $Article->Search(
			array("article_id" => $this->ArticleID)
		, true)->Columns(["article_id", "image_local", "image_link"]);
	}

	public function Detail ()
	{
		$Detail = new ArticleDetail();

		return $Detail->Count(array("article_id" => $this->ArticleID));
	}

	public function SetArticleID ($ArticleID)
	{
		$this->ArticleID = $ArticleID;
	}
}

class CommentDeep
{
	private $ArticleID;

	public function __construct ($ArticleID = null)
	{
		$this->ArticleID = $ArticleID;
	}
	public function Comment ()
	{
		$Comment = new Comment ();

		$Comments = $Comment->Chain(array("employees" => "employee_id"))
			->Search(array("article_id" => $this->ArticleID))
				->Sort(array("comments.created_at" => "DESC"))
					->Columns(
						array("article_id", 
							"employees.employee_id", 
							"comment_id", 
							"firstname", 
							"lastname", 
							"image_local", 
							"comment",
							"loves",
							"reply_to",
							"comments.created_at")
					);
		return $Comment->Tree($Comments);

	}

	public function Save ($CommentString)
	{
		$Comment = new Comment();

		$Comment->Create([
				"article_id" => $this->ArticleID,
				"employee_id" => 1,
				"comment" => $CommentString
			]);

	}

	public function Reply($CommentID, $CommentString)
	{
		$Comment = new ReplyComment();

		$Comment->Call([$this->ArticleID, 1, $CommentID, $CommentString]);
	}

	public function Love ($CommentID)
	{
		$Comment = new CommentLove();

		$Comment->Call($CommentID);
	}

	public function SetArticleID ($ArticleID)
	{
		$this->ArticleID = $ArticleID;
	}

}