class ResolveAudience
{
	AudienceBox (Modal)
	{
		Modal = Modal.classList;

		if (Modal.contains("ge-block"))
		{
			Modal.remove("ge-block");
		}
		else
		{
			Modal.add("ge-block");
		}
	}

	AudienceMode (Element)
	{
		Element.parentElement.querySelector(".ge-active").classList.remove("ge-active");

		Element.classList.add("ge-active");
	}

}

class BaseWriter
{
	constructor (Editor, Page, Figures)
	{
		this.Setted 	 = false;

		this.Container 	 = null;

		this.Selection 	 = null;

		this.Editor 	 = Editor;

		this.Title 		 = this.GetTitle();

		this.Description = this.GetDescription();

		this.Model = new System.DescriptionModel(System.URLResolver());

		this.Transform = new System.WriterTransform(this.Model, Page, Figures);

		this.TitleData		 = new FormData();

		this.DescriptionData = new FormData();

		this.DescriptionDetailData = new FormData();

	}

	Choose (Target)
	{
		var Container = Target;

		while (Container)
		{
			Container = Container.parentElement;

			if (Container && (Container.nodeName == "SECTION" || Container.nodeName == "GE-MODE"))
				break;
		}

		this.Selection = Target;

		console.log(this.Selection)

		this.Container = Container;

	}

	Paste ()
	{
		return new Promise((Resolve, Reject) => {
			
			setTimeout( () => {

				/**
				 * Remove all attribute of paste values.
				 *
				 * @PasteElements       {<ListNode>}
				 */
				var PasteElements = this.Selection.querySelectorAll("[style]");
			
				for (let PasteElement of PasteElements)
				{
					var Attributes 	= PasteElement.attributes,
						Length 		= Attributes.length,
						Attribute;

					for (var i = 0; i < Length; i++)
					{
						Attribute = Attributes[0].name;

						if (Attribute == "href" || Attribute == "src" || Attribute == "data-src" || Attribute == "style" || Attribute == "target")
							continue;

						PasteElement.removeAttribute(Attribute);
					}
				}

				if (this.Selection)
				{
					var Content = this.Selection.textContent.trim();

					if (Content.length > 0)
					{
						var Transform = this.Transform.Do(Content),
							Length = Transform.children.length;

						if (Length > 0)
						{

							if (this.Selection.nodeName == "GED")
							{

								this.Selection.innerHTML = Transform.innerHTML;
							}
							else
							{
								var Selection = this.Selection.parentElement;

								while (Selection)
								{
									if (Selection.nodeName == "GED")
										break;

									Selection = Selection.parentElement;
								}

								if (Selection)
								{
									Selection.innerHTML = Transform.innerHTML;
								}
								else
								{
									this.Container.innerHTML = "<ged>" + Transform.innerHTML + "</ged>";
								}

								
							}
							this.Selection.classList.remove("ge-paragraph");

						}
						

					}
				}

				Resolve();

			}, 256)
		})
	}

	FillElement ()
	{
		if (this.Editor.children.length == 0)
		{
			this.Editor.innerHTML = document.querySelector("[default-data-paragraph]").value;
		}
		else
		{
			var Section = this.Editor.querySelector("section");

			var Gemode = this.Editor.querySelector("ge-mode");

			if (Section)
			{
				console.log("ge-mode")
				if (Gemode == null)
				{
					var Node = document.createElement("de-mode");

					Node.classList.add("ge-padding ", "ge-small-mode");

					var Child = Selection.firstElementChild;

					while (Child)
					{
						if (Child.nodeName != "GE-MODE")
						{
							Node.appendChild(Child);
						}
					}

					this.Section.insertBefore(Node, this.Selection.firstChild);
				}
			}
			
		}
	}

	ResolveText ()
	{
		if (this.Editor.children.length == 0)
		{
			this.FillElement();
		}
		
		if (this.Selection.parentElement)
		{
			var Placeholders = this.Selection.parentElement.querySelectorAll(".ge-placeholder"),
				Content, PlaceholderContent;

			Placeholders.forEach (function (Placeholder) {

				Content = Placeholder.parentElement.textContent.trim();

				PlaceholderContent = Placeholder.textContent.trim();

				if (Content.length > PlaceholderContent.length)
				{
					Placeholder.remove();
				}
			})
		}

	}

	Backspace ()
	{
		if (this.Editor.children.length == 0)
		{
			this.FillElement();

			return false;
		}

		if (this.Selection == null)
		{
			return true;
		}

		var Placeholder = this.Selection.querySelector(".ge-placeholder");

		if (Placeholder)
		{
			Placeholder.remove();
		}

		var Content = this.Selection.textContent;

		if (Content.length == 0)
		{
			var Previous = this.Selection.previousElementSibling,
				Next 	 = this.Selection.nextElementSibling;

			if (Previous)
			{
				this.Selection = Previous;
			}
			else if (Next)
			{
				this.Selection.remove();

				Next.focus();
				
				this.Selection = Next;

				return false;
			}
			else 
			{
				if (this.Selection.hasAttribute("style"))
				{
					this.Selection.removeAttribute("style");

					this.Selection.classList.add("ge-paragraph");
				}

				this.Placeholder(this.Selection);

				return false;
			}
		}

		return true;
	}

	DefaultLine()
	{
		var Node = document.createElement("ged");

		Node.classList.add("ge-paragraph");

		Node.innerHTML = "<br />";

		this.Container.appendChild(Node);

		Node.setAttribute("id", this.GetID());

	}

	GetID ()
	{
		var ID = this.Editor.querySelector("[latest-id]"), Split = ID.value.split("-");

		var NextID = Split[0] + "-" + (parseInt(Split[1]) + 1);

		ID.value =  NextID;

		return NextID;
	}

	Placeholder (Element = null)
	{
		var Needed = Element;

		if (Element == null)
		{
			if (this.Editor == null)
			{
				return null;
			}
			
			var Lastest = this.Editor.lastElementChild.lastElementChild.lastElementChild;

			if (Lastest && Lastest.textContent.trim().length == 0 && Lastest.classList.contains("ge-paragraph"))
			{
				Needed = Lastest;
			}
		}

		if (Needed)
		{
			Needed.innerHTML = this.EmptyParagrahp;
		}
		
	}

	SetEditor (Editor)
	{
		this.Editor = Editor;
	}

}

class Writer extends BaseWriter
{
	ChangeDescription ()
	{
		var GeModes = this.Editor.querySelectorAll("ge-mode"),
			Content, Children, Child;

		GeModes.forEach( (GeMode) => {

			Children = GeMode.children;

			for (Child of Children)
			{
				if (Child.isSameNode(this.Title))
				{
					continue;
				}

				Content = Child.textContent.trim();

				if (Content.length > 25)
				{
					
					if (Child.hasAttribute("data-description") || Child.isSameNode(this.Description))
					{
						return null;
					}

					var Data = GeMode.querySelector("[data-description]");

					if (Data)
					{
						Data.removeAttribute("data-description");
					}

					Child.setAttribute("data-description", "true");

					return Child;

				}
			}


		})

		return Child;
	}

	GetDescription ()
	{
		if (this.Editor == null)
		{
			return null;
		}

		var Description = this.Editor.querySelector("[data-description]");

		if (Description)
		{
			return Description;
		}
		else
		{
			var GeModes = this.Editor.querySelectorAll("ge-mode"),
				Content, Children, Child;

			GeModes.forEach( (GeMode) => {

				Children = GeMode.children;

				for (Child of Children)
				{
					if (Child.isSameNode(this.Title))
					{
						continue;
					}
					else
					{
						Content = Child.textContent.trim();

						if (Content.length > 25)
						{
							Child.setAttribute("data-description", "true");

							return Child;
						}
					}
				}

			})

			return Child;
		}
		
	}

	ChangeTitle ()
	{
		var GeModes = this.Editor.querySelectorAll("ge-mode"),
			Content, Children, Child;

		GeModes.forEach( (GeMode) => {

			Children = GeMode.children;

			for (Child of Children)
			{
				if (Child.nodeName == "FIGURE")
				{
					continue;
				}

				Content = Child.textContent.trim();

				if (Content.length > 0)
				{
					if (Child.hasAttribute("data-title") || Child.isSameNode(this.Title))
					{
						return null;
					}
					else
					{
						var Data =	GeMode.querySelector("[data-title]");

						if (Data)
						{
							Data.removeAttribute("data-title");
						}

						Child.setAttribute("data-title", "true");

						return Child;
					}
					
				}
			}

		})

		return Child;
	}

	GetTitle ()
	{
		if (this.Editor == null)
		{
			return null;
		}

		var Title = this.Editor.querySelector("[data-title]");

		if (Title)
		{
			return Title;
		}
		else
		{
			var ID = document.querySelector("[default-data-id]").value;

			Title = document.getElementById(ID);

			if (Title)
			{
				Title.setAttribute("data-title", "true");
			
				return Title;
			}
			else
			{
				var GeModes = this.Editor.querySelectorAll("ge-mode"),
					Content, Children, Child;

				GeModes.forEach( (GeMode) => {

					Children = GeMode.children;

					for (Child of Children)
					{
						if (Child.nodeName == "FIGURE")
						{
							continue;
						}

						Content = Child.textContent.trim();

						if (Content.length > 1)
						{

							Child.setAttribute("data-title", "true");

							return Child;
						}
					}

				})

			}

			return null;
		}
	}

	
}

class ResolveWriter extends Writer
{
	constructor (Editor, EmptyParagrahp, Page, Figures)
	{
		super(Editor, Page, Figures);

		this.EmptyParagrahp = EmptyParagrahp;
	}

	SaveTitle ()
	{
		this.TitleData.set("Title", this.Title.textContent.trim());

		return this.Model.SaveTitle(this.TitleData).then ( (Response) => {

			if (this.Setted == false)
			{
				Response = Response.match(/{.*?}/);

				var DraftID = JSON.parse(Response).Result;

				var Url = `${location.href}/${DraftID}`;

				history.pushState(null, null, Url);

				this.Model.DraftID = DraftID;

				this.Setted = true;
			}

		})
	}

	SaveDescription ()
	{
		this.DescriptionData.set("Description", this.Description.textContent.trim());

		return this.Model.SaveDescription(this.DescriptionData);
	}

	Save ()
	{
		var ChangeTitle			= this.ChangeTitle(),
			ChangeDescription	= this.ChangeDescription(),
			IsDescription		= false,
			IsTitle				= false;

		if (this.Selection.isSameNode(this.Title))
		{
			IsTitle = true;
		}

		if (this.Selection.isSameNode(this.Description))
		{
			IsDescription = true;
		}

		if (ChangeTitle)
		{
			this.Title = ChangeTitle;

			IsTitle = true;
		}

		if (ChangeDescription)
		{
			this.Description = ChangeDescription;

			IsDescription = true;
		}

		this.DescriptionDetailData.set("DescriptionDetail", System.TextContent(this.Editor));

		if (IsTitle)
		{
			/**
			 * Save title first and wait 1024ms then save description.
			 */
			return this.SaveTitle().then( () => {

				setTimeout( () => { this.Model.SaveDescriptionDetail(this.DescriptionDetailData); }, 1024);
				
			});
		}

		if (IsDescription)
		{
			return this.SaveDescription().then( () => {

				setTimeout( () => { this.Model.SaveDescriptionDetail(this.DescriptionDetailData); }, 1024);
				
			}, function (Message) {
				console.log(Message)
			});
		}
		

		return this.Model.SaveDescriptionDetail(this.DescriptionDetailData).then( function (Response) {
			console.log(Response)
		}, function (Message) {
			console.log(Message)
		});
	}


}