<?php

/**
 * @Author: Rot
 * @Date:   2017-09-24 15:11:03
 * @Last Modified by:   Rot
 * @Last Modified time: 2017-09-24 15:11:04
 */
include_once("assets/views.php");
include_once("practices/deeps.php");
include_once("practices/cookies.php");

use System\Deeps\PracticeDeep;
use System\Cookies\PracticeCookie;

class PracticeView extends ListView
{
	public function __construct ()
	{
		$this->starttime = microtime(true);

		parent::__construct(array(
			"FixedHeader"  => __ROOT__ . "practices/templates/header.php",
			"Main" => __ROOT__ . "practices/templates/main.php",
		));
	}

	public function Index ($Form)
	{
		$Cookie = new PracticeCookie($Form->V);
		$Deep = new PracticeDeep($Form->V);

		if ($Cookie->IsExist())
		{
			$this->Questions = $Deep->PracticeByCookie($Cookie->Questions);
			$this->Answers = $Cookie->Answers;
		}
		else
		{
			$Ques = "70: 219; 66: 210; 64: 201, 204; 72: 225; 109: 518; 59: 181; 65: 207; 99: 434; 75: 244; 74: 238; 71: 221";
			$Cookie->SetQuestions($Ques);
			$this->Questions = $Deep->Practice();
		}

		
		
	}

	

}