public function Home ()
	{
		$Home = new Home();
		return $Home->Call();
	}

	public function FollowingTopics ($EmployeeID, $RowNumber = 6)
	{
		$Topics = new FollowingTopics();
		return $Topics->Call([
			$EmployeeID, $RowNumber
		]);
	}

	public function FollowingEmployees ($EmployeeID, $RowNumber = 6)
	{
		$Topics = new FollowingEmployees();
		return $Topics->Call([
			$EmployeeID
		]);
	}

	public function Recommended ($EmployeeID)
	{
		$Topics = new Recommended();
		return $Topics->Call([
			$EmployeeID
		]);
	}

	public function IsValidToken ($EmployeeID, $Token)
	{
		$SignIn = new SignIn();
		$Result = $SignIn->Call([$EmployeeID, $Token]);
		if ($Result["Result"] == 1)
		{
			return true;
		}
		return false;
	}

	public function Employee($EmployeeID)
	{
		$Employee = new Employee();

		return $Employee->Search(["employee_id" => $EmployeeID], true)->Columns();
	}

	public function Token ($Email)
	{

		$Token = new Token();
		$Employee = new Employee();
		$TokenCode = \System\RandomString(24);

		$Search = $Employee->Search(["email" => $Email], true);
		$Count = $Search->Count();
		if ($Count["Total"] > 0)
		{
			$EmployeeID = $Search->Columns("employee_id");
			
		}
		else
		{
			$Employee->Create([
				"email" => $Email
			]);
			$EmployeeID = $Employee->Search(["email" => $Email]).Columns("employee_id");
		}

		$EmployeeID = $EmployeeID["employee_id"];

		$Token->Create([
			"employee_id" => $EmployeeID,
			"token" => $TokenCode,
			"expire_at" => date('Y-m-d H:i:s', strtotime("+15 minutes"))
		]);

		$this->Mail($Email, $EmployeeID, $TokenCode);
	}

	public function Mail ($Email, $EmployeeID, $Token)
	{
		include_once("assets/modules/mails/mail.php");

		$Subject = "Sign in to Geye";
		$SignInLink = "http://localhost/geye/home&ajax=signin?e={$EmployeeID}&t={$Token}";
		$Body = array(
			"Content" => '
				<div style="font-family: Segoe UI">
					<h1 style="font-size: 64px; text-align: center; margin: 0; margin-bottom: 32px; ">
						<span style="color: #1B95E0">G</span>E<span style="color: #1B95E0">Y</span>E
					</h1>
					<div style="margin-bottom: 32px; font-size: 14px;">
						<div>Click and confirm that you want to sign in to Geye. This link will expire in fifteen minutes and can only be used once.</div>
						<div><a href='. $SignInLink .' style="padding: 8px 16px; background-color: #1da1f2; color: white; border-radius: 2px; font-size: 14.5; margin: 16px 0; width: 128px; display: inline-block; text-align: center; text-decoration: none;">Sign in to Geye</a></div><div><div>Or sign in using this link</div><a href='. $SignInLink .'>'. $SignInLink .'</a></div>
					</div>
					<div style="color: rgba(0,0,0,.44); text-align: center;">If you did not make this request, please contact us at response.geye@gmail.com</div>
					<div style="color: rgba(0,0,0,.44); text-align: center;">Sent by Geye · 48 Hoàng Phan Thái, TP Vinh, Nghệ An, Việt Nam</div>
				</div>'
		);

		new Mail($Email, $Subject, $Body);
	}