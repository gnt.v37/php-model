<?php

/**
 * @Author: Rot
 * @Date:   2017-09-24 15:24:16
 * @Last Modified by:   Rot
 * @Last Modified time: 2017-12-02 20:16:55
 */
<?php

namespace System\Deeps;

include_once("practices/models.php");

use System\Models\GetPratice;
use System\Models\ScoreByCookie;
use System\Models\PracticeByCookie;

class PracticeDeep 
{
	private $TryID;
	public function __construct ($TryID = null)
	{
		$this->TryID = $TryID;
	}

	public function Practice ()
	{
		$GetPratice = new GetPratice();

		return self::AnswerToQuestion($GetPratice->Call(array($this->TryID)));
	}

	public function Tick ()
	{
		
	}

	public function FinishForCookie ($Cookie)
	{
		$Score = new ScoreByCookie();

		return $Score->Call([$this->TryID, $Cookie]);
	}

	public function PracticeByCookie ($Questions)
	{
		$Practice = new PracticeByCookie();

		return self::AnswerToQuestion($Practice->Call([$this->TryID, $Questions]));
	}

	private function AnswerToQuestion ($Questions)
	{
		$Result = array();

		foreach ($Questions as $Question) 
		{
			$QuestionID = $Question["question_id"];

			$Answer = array (
					$Question["answer_id"] => $Question["answer"]
				);

			if (array_key_exists($QuestionID, $Result))
			{
				$Len = count($Result[$QuestionID]["answers"]);
				$Result[$QuestionID]["answers"] += $Answer;

			}
			else
			{
				$Result[$QuestionID] = array(
						"title" => $Question["title"],
						"question" => $Question["question"],
						"multi_choice" => $Question["multi_choice"]
					);

				$Result[$QuestionID]["answers"] = $Answer;
			}
		}

		return $Result;
	}

	public function SetTryID ($TryID)
	{
		$this->TryID = $TryID;
	}
}

class PracticeDeep 
{
	private $TryID;
	public function __construct ($TryID = null)
	{
		$this->TryID = $TryID;
	}

	public function Practice ()
	{
		$GetPratice = new GetPratice();

		return self::AnswerToQuestion($GetPratice->Call(array($this->TryID)));
	}

	public function Tick ()
	{
		
	}

	public function FinishForCookie ($Cookie)
	{
		$Score = new ScoreByCookie();

		return $Score->Call([$this->TryID, $Cookie]);
	}

	public function PracticeByCookie ($Questions)
	{
		$Practice = new PracticeByCookie();

		return self::AnswerToQuestion($Practice->Call([$this->TryID, $Questions]));
	}

	private function AnswerToQuestion ($Questions)
	{
		$Result = array();

		foreach ($Questions as $Question) 
		{
			$QuestionID = $Question["question_id"];

			$Answer = array (
					$Question["answer_id"] => $Question["answer"]
				);

			if (array_key_exists($QuestionID, $Result))
			{
				$Len = count($Result[$QuestionID]["answers"]);
				$Result[$QuestionID]["answers"] += $Answer;

			}
			else
			{
				$Result[$QuestionID] = array(
						"title" => $Question["title"],
						"question" => $Question["question"],
						"multi_choice" => $Question["multi_choice"]
					);

				$Result[$QuestionID]["answers"] = $Answer;
			}
		}

		return $Result;
	}

	public function SetTryID ($TryID)
	{
		$this->TryID = $TryID;
	}
}

class PracticeDeep 
{
	private $TryID;
	public function __construct ($TryID = null)
	{
		$this->TryID = $TryID;
	}

	public function Practice ()
	{
		$GetPratice = new GetPratice();

		return self::AnswerToQuestion($GetPratice->Call(array($this->TryID)));
	}

	public function Tick ()
	{
		
	}

	public function FinishForCookie ($Cookie)
	{
		$Score = new ScoreByCookie();

		return $Score->Call([$this->TryID, $Cookie]);
	}

	public function PracticeByCookie ($Questions)
	{
		$Practice = new PracticeByCookie();

		return self::AnswerToQuestion($Practice->Call([$this->TryID, $Questions]));
	}

	private function AnswerToQuestion ($Questions)
	{
		$Result = array();

		foreach ($Questions as $Question) 
		{
			$QuestionID = $Question["question_id"];

			$Answer = array (
					$Question["answer_id"] => $Question["answer"]
				);

			if (array_key_exists($QuestionID, $Result))
			{
				$Len = count($Result[$QuestionID]["answers"]);
				$Result[$QuestionID]["answers"] += $Answer;

			}
			else
			{
				$Result[$QuestionID] = array(
						"title" => $Question["title"],
						"question" => $Question["question"],
						"multi_choice" => $Question["multi_choice"]
					);

				$Result[$QuestionID]["answers"] = $Answer;
			}
		}

		return $Result;
	}

	public function SetTryID ($TryID)
	{
		$this->TryID = $TryID;
	}
}

class PracticeDeep 
{
	private $TryID;
	public function __construct ($TryID = null)
	{
		$this->TryID = $TryID;
	}

	public function Practice ()
	{
		$GetPratice = new GetPratice();

		return self::AnswerToQuestion($GetPratice->Call(array($this->TryID)));
	}

	public function Tick ()
	{
		
	}

	public function FinishForCookie ($Cookie)
	{
		$Score = new ScoreByCookie();

		return $Score->Call([$this->TryID, $Cookie]);
	}

	public function PracticeByCookie ($Questions)
	{
		$Practice = new PracticeByCookie();

		return self::AnswerToQuestion($Practice->Call([$this->TryID, $Questions]));
	}

	private function AnswerToQuestion ($Questions)
	{
		$Result = array();

		foreach ($Questions as $Question) 
		{
			$QuestionID = $Question["question_id"];

			$Answer = array (
					$Question["answer_id"] => $Question["answer"]
				);

			if (array_key_exists($QuestionID, $Result))
			{
				$Len = count($Result[$QuestionID]["answers"]);
				$Result[$QuestionID]["answers"] += $Answer;

			}
			else
			{
				$Result[$QuestionID] = array(
						"title" => $Question["title"],
						"question" => $Question["question"],
						"multi_choice" => $Question["multi_choice"]
					);

				$Result[$QuestionID]["answers"] = $Answer;
			}
		}

		return $Result;
	}

	public function SetTryID ($TryID)
	{
		$this->TryID = $TryID;
	}
}

class PracticeDeep 
{
	private $TryID;
	public function __construct ($TryID = null)
	{
		$this->TryID = $TryID;
	}

	public function Practice ()
	{
		$GetPratice = new GetPratice();

		return self::AnswerToQuestion($GetPratice->Call(array($this->TryID)));
	}

	public function Tick ()
	{
		
	}

	public function FinishForCookie ($Cookie)
	{
		$Score = new ScoreByCookie();

		return $Score->Call([$this->TryID, $Cookie]);
	}

	public function PracticeByCookie ($Questions)
	{
		$Practice = new PracticeByCookie();

		return self::AnswerToQuestion($Practice->Call([$this->TryID, $Questions]));
	}

	private function AnswerToQuestion ($Questions)
	{
		$Result = array();

		foreach ($Questions as $Question) 
		{
			$QuestionID = $Question["question_id"];

			$Answer = array (
					$Question["answer_id"] => $Question["answer"]
				);

			if (array_key_exists($QuestionID, $Result))
			{
				$Len = count($Result[$QuestionID]["answers"]);
				$Result[$QuestionID]["answers"] += $Answer;

			}
			else
			{
				$Result[$QuestionID] = array(
						"title" => $Question["title"],
						"question" => $Question["question"],
						"multi_choice" => $Question["multi_choice"]
					);

				$Result[$QuestionID]["answers"] = $Answer;
			}
		}

		return $Result;
	}

	public function SetTryID ($TryID)
	{
		$this->TryID = $TryID;
	}
}
class PracticeDeep 
{
	private $TryID;
	public function __construct ($TryID = null)
	{
		$this->TryID = $TryID;
	}

	public function Practice ()
	{
		$GetPratice = new GetPratice();

		return self::AnswerToQuestion($GetPratice->Call(array($this->TryID)));
	}

	public function Tick ()
	{
		
	}

	public function FinishForCookie ($Cookie)
	{
		$Score = new ScoreByCookie();

		return $Score->Call([$this->TryID, $Cookie]);
	}

	public function PracticeByCookie ($Questions)
	{
		$Practice = new PracticeByCookie();

		return self::AnswerToQuestion($Practice->Call([$this->TryID, $Questions]));
	}

	private function AnswerToQuestion ($Questions)
	{
		$Result = array();

		foreach ($Questions as $Question) 
		{
			$QuestionID = $Question["question_id"];

			$Answer = array (
					$Question["answer_id"] => $Question["answer"]
				);

			if (array_key_exists($QuestionID, $Result))
			{
				$Len = count($Result[$QuestionID]["answers"]);
				$Result[$QuestionID]["answers"] += $Answer;

			}
			else
			{
				$Result[$QuestionID] = array(
						"title" => $Question["title"],
						"question" => $Question["question"],
						"multi_choice" => $Question["multi_choice"]
					);

				$Result[$QuestionID]["answers"] = $Answer;
			}
		}

		return $Result;
	}

	public function SetTryID ($TryID)
	{
		$this->TryID = $TryID;
	}
}

class PracticeDeep 
{
	private $TryID;
	public function __construct ($TryID = null)
	{
		$this->TryID = $TryID;
	}

	public function Practice ()
	{
		$GetPratice = new GetPratice();

		return self::AnswerToQuestion($GetPratice->Call(array($this->TryID)));
	}

	public function Tick ()
	{
		
	}

	public function FinishForCookie ($Cookie)
	{
		$Score = new ScoreByCookie();

		return $Score->Call([$this->TryID, $Cookie]);
	}

	public function PracticeByCookie ($Questions)
	{
		$Practice = new PracticeByCookie();

		return self::AnswerToQuestion($Practice->Call([$this->TryID, $Questions]));
	}

	private function AnswerToQuestion ($Questions)
	{
		$Result = array();

		foreach ($Questions as $Question) 
		{
			$QuestionID = $Question["question_id"];

			$Answer = array (
					$Question["answer_id"] => $Question["answer"]
				);

			if (array_key_exists($QuestionID, $Result))
			{
				$Len = count($Result[$QuestionID]["answers"]);
				$Result[$QuestionID]["answers"] += $Answer;

			}
			else
			{
				$Result[$QuestionID] = array(
						"title" => $Question["title"],
						"question" => $Question["question"],
						"multi_choice" => $Question["multi_choice"]
					);

				$Result[$QuestionID]["answers"] = $Answer;
			}
		}

		return $Result;
	}

	public function SetTryID ($TryID)
	{
		$this->TryID = $TryID;
	}
}

class PracticeDeep 
{
	private $TryID;
	public function __construct ($TryID = null)
	{
		$this->TryID = $TryID;
	}

	public function Practice ()
	{
		$GetPratice = new GetPratice();

		return self::AnswerToQuestion($GetPratice->Call(array($this->TryID)));
	}

	public function Tick ()
	{
		
	}

	public function FinishForCookie ($Cookie)
	{
		$Score = new ScoreByCookie();

		return $Score->Call([$this->TryID, $Cookie]);
	}

	public function PracticeByCookie ($Questions)
	{
		$Practice = new PracticeByCookie();

		return self::AnswerToQuestion($Practice->Call([$this->TryID, $Questions]));
	}

	private function AnswerToQuestion ($Questions)
	{
		$Result = array();

		foreach ($Questions as $Question) 
		{
			$QuestionID = $Question["question_id"];

			$Answer = array (
					$Question["answer_id"] => $Question["answer"]
				);

			if (array_key_exists($QuestionID, $Result))
			{
				$Len = count($Result[$QuestionID]["answers"]);
				$Result[$QuestionID]["answers"] += $Answer;

			}
			else
			{
				$Result[$QuestionID] = array(
						"title" => $Question["title"],
						"question" => $Question["question"],
						"multi_choice" => $Question["multi_choice"]
					);

				$Result[$QuestionID]["answers"] = $Answer;
			}
		}

		return $Result;
	}

	public function SetTryID ($TryID)
	{
		$this->TryID = $TryID;
	}
}

class PracticeDeep 
{
	private $TryID;
	public function __construct ($TryID = null)
	{
		$this->TryID = $TryID;
	}

	public function Practice ()
	{
		$GetPratice = new GetPratice();

		return self::AnswerToQuestion($GetPratice->Call(array($this->TryID)));
	}

	public function Tick ()
	{
		
	}

	public function FinishForCookie ($Cookie)
	{
		$Score = new ScoreByCookie();

		return $Score->Call([$this->TryID, $Cookie]);
	}

	public function PracticeByCookie ($Questions)
	{
		$Practice = new PracticeByCookie();

		return self::AnswerToQuestion($Practice->Call([$this->TryID, $Questions]));
	}

	private function AnswerToQuestion ($Questions)
	{
		$Result = array();

		foreach ($Questions as $Question) 
		{
			$QuestionID = $Question["question_id"];

			$Answer = array (
					$Question["answer_id"] => $Question["answer"]
				);

			if (array_key_exists($QuestionID, $Result))
			{
				$Len = count($Result[$QuestionID]["answers"]);
				$Result[$QuestionID]["answers"] += $Answer;

			}
			else
			{
				$Result[$QuestionID] = array(
						"title" => $Question["title"],
						"question" => $Question["question"],
						"multi_choice" => $Question["multi_choice"]
					);

				$Result[$QuestionID]["answers"] = $Answer;
			}
		}

		return $Result;
	}

	public function SetTryID ($TryID)
	{
		$this->TryID = $TryID;
	}
}

class PracticeDeep 
{
	private $TryID;
	public function __construct ($TryID = null)
	{
		$this->TryID = $TryID;
	}

	public function Practice ()
	{
		$GetPratice = new GetPratice();

		return self::AnswerToQuestion($GetPratice->Call(array($this->TryID)));
	}

	public function Tick ()
	{
		
	}

	public function FinishForCookie ($Cookie)
	{
		$Score = new ScoreByCookie();

		return $Score->Call([$this->TryID, $Cookie]);
	}

	public function PracticeByCookie ($Questions)
	{
		$Practice = new PracticeByCookie();

		return self::AnswerToQuestion($Practice->Call([$this->TryID, $Questions]));
	}

	private function AnswerToQuestion ($Questions)
	{
		$Result = array();

		foreach ($Questions as $Question) 
		{
			$QuestionID = $Question["question_id"];

			$Answer = array (
					$Question["answer_id"] => $Question["answer"]
				);

			if (array_key_exists($QuestionID, $Result))
			{
				$Len = count($Result[$QuestionID]["answers"]);
				$Result[$QuestionID]["answers"] += $Answer;

			}
			else
			{
				$Result[$QuestionID] = array(
						"title" => $Question["title"],
						"question" => $Question["question"],
						"multi_choice" => $Question["multi_choice"]
					);

				$Result[$QuestionID]["answers"] = $Answer;
			}
		}

		return $Result;
	}

	public function SetTryID ($TryID)
	{
		$this->TryID = $TryID;
	}
}
class PracticeDeep 
{
	private $TryID;
	public function __construct ($TryID = null)
	{
		$this->TryID = $TryID;
	}

	public function Practice ()
	{
		$GetPratice = new GetPratice();

		return self::AnswerToQuestion($GetPratice->Call(array($this->TryID)));
	}

	public function Tick ()
	{
		
	}

	public function FinishForCookie ($Cookie)
	{
		$Score = new ScoreByCookie();

		return $Score->Call([$this->TryID, $Cookie]);
	}

	public function PracticeByCookie ($Questions)
	{
		$Practice = new PracticeByCookie();

		return self::AnswerToQuestion($Practice->Call([$this->TryID, $Questions]));
	}

	private function AnswerToQuestion ($Questions)
	{
		$Result = array();

		foreach ($Questions as $Question) 
		{
			$QuestionID = $Question["question_id"];

			$Answer = array (
					$Question["answer_id"] => $Question["answer"]
				);

			if (array_key_exists($QuestionID, $Result))
			{
				$Len = count($Result[$QuestionID]["answers"]);
				$Result[$QuestionID]["answers"] += $Answer;

			}
			else
			{
				$Result[$QuestionID] = array(
						"title" => $Question["title"],
						"question" => $Question["question"],
						"multi_choice" => $Question["multi_choice"]
					);

				$Result[$QuestionID]["answers"] = $Answer;
			}
		}

		return $Result;
	}

	public function SetTryID ($TryID)
	{
		$this->TryID = $TryID;
	}
}

class PracticeDeep 
{
	private $TryID;
	public function __construct ($TryID = null)
	{
		$this->TryID = $TryID;
	}

	public function Practice ()
	{
		$GetPratice = new GetPratice();

		return self::AnswerToQuestion($GetPratice->Call(array($this->TryID)));
	}

	public function Tick ()
	{
		
	}

	public function FinishForCookie ($Cookie)
	{
		$Score = new ScoreByCookie();

		return $Score->Call([$this->TryID, $Cookie]);
	}

	public function PracticeByCookie ($Questions)
	{
		$Practice = new PracticeByCookie();

		return self::AnswerToQuestion($Practice->Call([$this->TryID, $Questions]));
	}

	private function AnswerToQuestion ($Questions)
	{
		$Result = array();

		foreach ($Questions as $Question) 
		{
			$QuestionID = $Question["question_id"];

			$Answer = array (
					$Question["answer_id"] => $Question["answer"]
				);

			if (array_key_exists($QuestionID, $Result))
			{
				$Len = count($Result[$QuestionID]["answers"]);
				$Result[$QuestionID]["answers"] += $Answer;

			}
			else
			{
				$Result[$QuestionID] = array(
						"title" => $Question["title"],
						"question" => $Question["question"],
						"multi_choice" => $Question["multi_choice"]
					);

				$Result[$QuestionID]["answers"] = $Answer;
			}
		}

		return $Result;
	}

	public function SetTryID ($TryID)
	{
		$this->TryID = $TryID;
	}
}

class PracticeDeep 
{
	private $TryID;
	public function __construct ($TryID = null)
	{
		$this->TryID = $TryID;
	}

	public function Practice ()
	{
		$GetPratice = new GetPratice();

		return self::AnswerToQuestion($GetPratice->Call(array($this->TryID)));
	}

	public function Tick ()
	{
		
	}

	public function FinishForCookie ($Cookie)
	{
		$Score = new ScoreByCookie();

		return $Score->Call([$this->TryID, $Cookie]);
	}

	public function PracticeByCookie ($Questions)
	{
		$Practice = new PracticeByCookie();

		return self::AnswerToQuestion($Practice->Call([$this->TryID, $Questions]));
	}

	private function AnswerToQuestion ($Questions)
	{
		$Result = array();

		foreach ($Questions as $Question) 
		{
			$QuestionID = $Question["question_id"];

			$Answer = array (
					$Question["answer_id"] => $Question["answer"]
				);

			if (array_key_exists($QuestionID, $Result))
			{
				$Len = count($Result[$QuestionID]["answers"]);
				$Result[$QuestionID]["answers"] += $Answer;

			}
			else
			{
				$Result[$QuestionID] = array(
						"title" => $Question["title"],
						"question" => $Question["question"],
						"multi_choice" => $Question["multi_choice"]
					);

				$Result[$QuestionID]["answers"] = $Answer;
			}
		}

		return $Result;
	}

	public function SetTryID ($TryID)
	{
		$this->TryID = $TryID;
	}
}

class PracticeDeep 
{
	private $TryID;
	public function __construct ($TryID = null)
	{
		$this->TryID = $TryID;
	}

	public function Practice ()
	{
		$GetPratice = new GetPratice();

		return self::AnswerToQuestion($GetPratice->Call(array($this->TryID)));
	}

	public function Tick ()
	{
		
	}

	public function FinishForCookie ($Cookie)
	{
		$Score = new ScoreByCookie();

		return $Score->Call([$this->TryID, $Cookie]);
	}

	public function PracticeByCookie ($Questions)
	{
		$Practice = new PracticeByCookie();

		return self::AnswerToQuestion($Practice->Call([$this->TryID, $Questions]));
	}

	private function AnswerToQuestion ($Questions)
	{
		$Result = array();

		foreach ($Questions as $Question) 
		{
			$QuestionID = $Question["question_id"];

			$Answer = array (
					$Question["answer_id"] => $Question["answer"]
				);

			if (array_key_exists($QuestionID, $Result))
			{
				$Len = count($Result[$QuestionID]["answers"]);
				$Result[$QuestionID]["answers"] += $Answer;

			}
			else
			{
				$Result[$QuestionID] = array(
						"title" => $Question["title"],
						"question" => $Question["question"],
						"multi_choice" => $Question["multi_choice"]
					);

				$Result[$QuestionID]["answers"] = $Answer;
			}
		}

		return $Result;
	}

	public function SetTryID ($TryID)
	{
		$this->TryID = $TryID;
	}
}

class PracticeDeep 
{
	private $TryID;
	public function __construct ($TryID = null)
	{
		$this->TryID = $TryID;
	}

	public function Practice ()
	{
		$GetPratice = new GetPratice();

		return self::AnswerToQuestion($GetPratice->Call(array($this->TryID)));
	}

	public function Tick ()
	{
		
	}

	public function FinishForCookie ($Cookie)
	{
		$Score = new ScoreByCookie();

		return $Score->Call([$this->TryID, $Cookie]);
	}

	public function PracticeByCookie ($Questions)
	{
		$Practice = new PracticeByCookie();

		return self::AnswerToQuestion($Practice->Call([$this->TryID, $Questions]));
	}

	private function AnswerToQuestion ($Questions)
	{
		$Result = array();

		foreach ($Questions as $Question) 
		{
			$QuestionID = $Question["question_id"];

			$Answer = array (
					$Question["answer_id"] => $Question["answer"]
				);

			if (array_key_exists($QuestionID, $Result))
			{
				$Len = count($Result[$QuestionID]["answers"]);
				$Result[$QuestionID]["answers"] += $Answer;

			}
			else
			{
				$Result[$QuestionID] = array(
						"title" => $Question["title"],
						"question" => $Question["question"],
						"multi_choice" => $Question["multi_choice"]
					);

				$Result[$QuestionID]["answers"] = $Answer;
			}
		}

		return $Result;
	}

	public function SetTryID ($TryID)
	{
		$this->TryID = $TryID;
	}
}
?>