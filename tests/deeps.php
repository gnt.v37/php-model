<?php

/**
 * @Author: Rot
 * @Date:   2017-09-11 18:15:29
 * @Last Modified by:   Rot
 * @Last Modified time: 2017-09-20 08:55:28
 */
/**
	 * { function_description }
	 *
	 * @param      <string>  $File   The file
	 *
	 * @return     array   ( All url classess )
	 */
	public function ClassUrl ($File)
	{
		/**
		 * $Handle is a file urls.php
		 *
		 * @var        Function
		 */
		$Handle = fopen($File, "r");

		/**
		 * if we want to call a class we need to know what namespace it belong to.
		 *
		 * @var        string
		 */
		$Namespace = "";

		/**
		 * Get all of class of this file.
		 *
		 * @var        array
		 */
		$Classes = array();

		/**
		 * File content
		 *
		 * @var        Function
		 */
		$Content = fread($Handle, filesize($File));

		fclose($Handle);

		/**
		 * Convert string file content to array
		 *
		 * @var        Function
		 */
		$Tokens = token_get_all($Content);

		/**
		 * Token length
		 * @var        Function
		 */
		$TokenLength = count($Tokens);

		/**
		 * if has { continue search
		 */
		if (strpos($Content, "{") == false)
		 {
		    continue;
		}

		/**
		 * Tokens is a array string of file.
		 */
		foreach ($Tokens as $Key => $Token) 
		{
			/**
			 * if Token is a T_Namespace
			 */
		    if ($Token[0] === T_NAMESPACE)
			{
			    for ($i = $Key + 1; $i < $TokenLength; $i++)
			    {
			    	/**
			    	 * T_String always in next two element when we found T_Namespace
			    	 */
			    	if ($Tokens[$i][0] === T_STRING)
			    	{
			    		$Namespace .= "\\".$Tokens[$i][1];
			    	}
			    	else if ($Tokens[$i] == "{" || $Tokens[$i] == ";")
			    	{
			    		break;
			    	}
			    }
			}

			if ($Token[0] === T_CLASS)
			{
				/**
				 * Class name is next two element in array.
				 *
				 * @var        integer
				 */
				$ClassIndex = $Key + 2;

				/**
				 * extends word if next class name two element.
				 *
				 * @var        integer
				 */
			    $ExtendsIndex = $ClassIndex + 2;

			    /**
			     * T_Extends always in next four element when we found T_Class
			     */
				if ($ExtendsIndex < $TokenLength && $Tokens[$ExtendsIndex][0] === T_EXTENDS)
			    {
			    	$Extends = "";

			    	/**
			    	 * The is diffrent in extends because it can be System\Urls\Url, so we need to get all of it.
			    	 */
			    	for ($j = $ExtendsIndex + 2; $j < $TokenLength; $j++)
			    	{
			    		if ($Tokens[$j] == "{")
			    		{
			    			break;
			    		}
			    		else
			    		{
			    			$Extends .= $Tokens[$j][1];
			    		}
			    	}

			    	$Extends = trim($Extends);

			    	if ($Extends == "Url" || $Extends == "Urls\\Url" || $Extends == "\\System\\Urls\\Url")
			    	{
			    		/**
			    		 * if found a class extends from Url we add it to classess.
			    		 */
			    		array_push($Classes, $Namespace."\\".$Tokens[$ClassIndex][1]);
			    	}

			    }
			    else
			    {
			    	continue;
			    }
			}
		}

		/**
		 * @return classes we found.
		 */
		return $Classes;

	}
