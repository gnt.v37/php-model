<?php foreach ($this->Articles as $Key => $Article): ?>
<li class="article-item"><ge-article class="no-grid"><div class="ge-space">
	<div class="ge-row ge-margin">
		<div class="ge-left">
			<ge-user><div class="ge-inline">
				<a href="<?= page."@{$Article["user_id"]}"; ?>" prevent="true">
					<div class="ge-relative ge-image40">
						<?php if ($Article["user_image_local"]): ?>
							<img data-src="<?= page.users.images."40/".$Article["user_image_local"] ?>">
						<?php else: ?>
							<img data-name="<?= $Article["lastname"] || $Article["firstname"] ?  "{$Article["lastname"]} {$Article["firstname"]}" : $Article["email"] ?>" data-src="" ge-user-avatar="true" >
						<?php endif ?>
						<div class="ge-avatar-radius">
							<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 70 70">
								<path d="M5.53538374,19.9430227 C11.180401,8.78497536 22.6271155,1.6 35.3571429,1.6 C48.0871702,1.6 59.5338847,8.78497536 65.178902,19.9430227 L66.2496695,19.401306 C60.4023065,7.84329843 48.5440457,0.4 35.3571429,0.4 C22.17024,0.4 10.3119792,7.84329843 4.46461626,19.401306 L5.53538374,19.9430227 Z"></path><path d="M65.178902,49.9077131 C59.5338847,61.0657604 48.0871702,68.2507358 35.3571429,68.2507358 C22.6271155,68.2507358 11.180401,61.0657604 5.53538374,49.9077131 L4.46461626,50.4494298 C10.3119792,62.0074373 22.17024,69.4507358 35.3571429,69.4507358 C48.5440457,69.4507358 60.4023065,62.0074373 66.2496695,50.4494298 L65.178902,49.9077131 Z"></path></svg>
				</div></div></a></div>
				<div class="ge-inline ge-left-12">
					<div><a href="<?= page."@{$Article["user_id"]}"; ?>" prevent="true" class="ge-name"><?= $Article["lastname"] || $Article["firstname"] ?  "{$Article["lastname"]} {$Article["firstname"]}" : $Article["email"] ?></a></div>
					<div class="ge-time"><time datetime="<?= $Article["created_at"] ?>"><?= $Article["created_at"] ?></time></div>
				</div></ge-user>
		</div>
		<div class="ge-right">
			<button class="ge-article-option" type=""><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width=18 height=18 viewBox="0 0 60 60" xml:space="preserve"><g><path d="M8,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S12.411,22,8,22z"/><path d="M52,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S56.411,22,52,22z"/><path d="M30,22c-4.411,0-8,3.589-8,8s3.589,8,8,8s8-3.589,8-8S34.411,22,30,22z"/></g></svg></button>
		</div>
	</div><div class="ge-row"><div class="ge-padding1"><ge-image>
			<a href="view?v=<?= $Article["article_id"] ?>" prevent="true" class="ge-scale"><ge-real class="ge-bk-img ge-hover" data-src="<?= home.images."280/".$Article["image"]; ?>"></ge-real></a>
		</ge-image></div>
		<ge-content><ul class="article-info"><li class="ge-relative">
			<div class=""><h4 class=""><a href="view?v=<?php echo $Article["article_id"] ?>" class="
			<?php 
				$Height = "height-mode-z"; 
				$IsShow = true;
				$TitleLength = strlen($Article["title"]);
				if (false) 
				{
					$Height = "height-mode-o";
					if ($TitleLength >= 39)
						$IsShow = false;
				}
				else 
				{
					if ($TitleLength >= 39 && $TitleLength < 74)
						$Height = "height-mode-tw";
					else if ($TitleLength >= 74)
						$Height = "height-mode-th";
				}	
			?> article-title" ><?= $Article["title"] ?></a>
			</h4><div class="article-description <?= $Height ?>">
				<span><?php if ($IsShow) echo $Article["description"] ?></span>	
			</div><div class=""><a class="ge-read-more" href="<?= page."view?v={$Article["article_id"]}" ?>" >Read more...</a></div>
			</div></li><li class="ge-row"><div class="ge-left">
				<a class="ge-claps" href="<?= page."view?v={$Article["article_id"]}" ?>"><svg class="" width="25" height="25" viewBox="0 0 25 25"><g fill-rule="evenodd"><path d="M11.739 0l.761 2.966L13.261 0z"></path><path d="M14.815 3.776l1.84-2.551-1.43-.471z"></path><path d="M8.378 1.224l1.84 2.551L9.81.753z"></path><path d="M20.382 21.622c-1.04 1.04-2.115 1.507-3.166 1.608.168-.14.332-.29.492-.45 2.885-2.886 3.456-5.982 1.69-9.211l-1.101-1.937-.955-2.02c-.315-.676-.235-1.185.245-1.556a.836.836 0 0 1 .66-.16c.342.056.66.28.879.605l2.856 5.023c1.179 1.962 1.379 5.119-1.6 8.098m-13.29-.528l-5.02-5.02a1 1 0 0 1 .707-1.701c.255 0 .512.098.707.292l2.607 2.607a.442.442 0 0 0 .624-.624L4.11 14.04l-1.75-1.75a.998.998 0 1 1 1.41-1.413l4.154 4.156a.44.44 0 0 0 .624 0 .44.44 0 0 0 0-.624l-4.152-4.153-1.172-1.171a.998.998 0 0 1 0-1.41 1.018 1.018 0 0 1 1.41 0l1.172 1.17 4.153 4.152a.437.437 0 0 0 .624 0 .442.442 0 0 0 0-.624L6.43 8.222a.988.988 0 0 1-.291-.705.99.99 0 0 1 .29-.706 1 1 0 0 1 1.412 0l6.992 6.993a.443.443 0 0 0 .71-.501l-1.35-2.856c-.315-.676-.235-1.185.246-1.557a.85.85 0 0 1 .66-.16c.342.056.659.28.879.606L18.628 14c1.573 2.876 1.067 5.545-1.544 8.156-1.396 1.397-3.144 1.966-5.063 1.652-1.713-.286-3.463-1.248-4.928-2.714zM10.99 5.976l2.562 2.562c-.497.607-.563 1.414-.155 2.284l.265.562-4.257-4.257a.98.98 0 0 1-.117-.445c0-.267.104-.517.292-.706a1.023 1.023 0 0 1 1.41 0zm8.887 2.06c-.375-.557-.902-.916-1.486-1.011a1.738 1.738 0 0 0-1.342.332c-.376.29-.61.656-.712 1.065a2.1 2.1 0 0 0-1.095-.562 1.776 1.776 0 0 0-.992.128l-2.636-2.636a1.883 1.883 0 0 0-2.658 0 1.862 1.862 0 0 0-.478.847 1.886 1.886 0 0 0-2.671-.012 1.867 1.867 0 0 0-.503.909c-.754-.754-1.992-.754-2.703-.044a1.881 1.881 0 0 0 0 2.658c-.288.12-.605.288-.864.547a1.884 1.884 0 0 0 0 2.659l.624.622a1.879 1.879 0 0 0-.91 3.16l5.019 5.02c1.595 1.594 3.515 2.645 5.408 2.959a7.16 7.16 0 0 0 1.173.098c1.026 0 1.997-.24 2.892-.7.279.04.555.065.828.065 1.53 0 2.969-.628 4.236-1.894 3.338-3.338 3.083-6.928 1.738-9.166l-2.868-5.043z"></path></g></svg>
				</a></div><div class="ge-right ge-response"><a href="href="<?= page."view?v={$Article["article_id"]}" ?>"">Responses</a>
			</div></li>
			<?php if (isset($this->ArticleTags[$Article["article_id"]])): ?>
				<li><div><ul class="ge-article-tags">
				<?php foreach ($this->ArticleTags[$Article["article_id"]] as $Key => $Value): ?>
					<li class="ge-inline">
					<?php if ($Key != 0): ?>
						<span class="ge-tag-dot">.</span>
					<?php endif ?>
						<a href="<?= folder ?>tag/<?= $Value["0"] ?>" class="ge-text-color">#<?= $Value["1"] ?></a></li>
				<?php endforeach ?>
				</ul></div></li>
			<?php endif ?>
		</ul></ge-content>
	</div></div>
</ge-article></li>		
<?php endforeach ?>	