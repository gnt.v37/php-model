<div class="ge-search-width"><div class="ge-row ge-main-top"><div class="ge-left"><div><ul class="ge-container">
<?php include_once $App.templates."articles.php"; ?>
</ul></div></div>
<div class="ge-right"><div class="ge-left-24">
	<?php if (isset($this->Tags)): ?>
		<div class="ge-tags"><ul class="">
			<li class="search-bar-title"><span>Tags</span></li>
		<?php foreach ($this->Tags as $Key => $Tag): ?>
			<li class="tag-item">
			<?php if ($Key != 0): ?>
				<span class="ge-tag-dot">.</span>
			<?php endif ?>
				<a href="<?= folder ?>tag/<?= $Tag["tag_id"] ?>"><span class="ge-tag">#<?= $Tag["tag"] ?></span></a>
			</li>
		<?php endforeach ?>	
		</ul></div>
	<?php endif ?>
	<div class="ge-people">
	<ul class="ge-top-16">
	<li class="search-bar-title"><span>People</span></li>
	<?php foreach ($this->Users as $User): ?>
		<?php $Name = $User["lastname"] || $User["firstname"] ? "{$User["lastname"]} {$User["firstname"]}" : $User["email"] ?>
		<li class="user-wrapper"><ge-user><div class="user-width"><div class="table-cell">
			<a href="@<?= $User["user_id"]; ?>" prevent="true"><div class="ge-relative">
			<?php if ($User["image_local"]): ?>
				<img src="<?= users.images."64/".$User["image_local"] ?>" class="ge-avatar-radius">
			<?php else: ?>
				<img ge-user-avatar="true" data-name="<?= $Name ?>" data-src="" class="ge-avatar-radius">
			<?php endif ?>
			</div></a>
			</div><div class="table-cell"><div class="ge-left-16">
				<div class="ge-bottom-4"><h5><a href="@<?= $User["user_id"] ?>" prevent="true"><?= $Name ?></a></h5></div>
				<div class="ge-bottom-12"><span class="ge-text"><?= $User["description"] ?></span></div>
				<div class="">
					<?php if (isset($User["following"]) && $User["following"]): ?>
						<div>
							<button data-user="<?= $User["user_id"] ?>" class="ge-follow ge-following">Following</button>
						</div>
					<?php else: ?>
							<button data-user="<?= $User["user_id"] ?>" class="ge-follow">Follow</button>
					<?php endif ?>
				</div>
			</div></div>
		</div></ge-user></li>
	<?php endforeach ?>
</ul></div></div></div></div></div>