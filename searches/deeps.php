<?php

namespace System\Deeps;

class SearchDeep 
{
	public function ConvertTags ()
	{
		$Article = "";

		foreach ($this->SearchArticles as $Value) 
		{
			$Article .= "{$Value["article_id"]},";
		}

		$Article = substr($Article, 0, -1);

		$ArticleTags = $this->ArticleTags->Call([null, $Article]);

		$Datas = [];

		$Index = 0;

		foreach ($ArticleTags as $Key => $Value) 
		{
			$Article = $Value["article_id"];

			if (array_key_exists($Article, $Datas))
			{
				$Datas[$Article] += array($Index, array($Value["tag_id"], $Value["tag"]));

				$Index++;
			}
			else
			{
				$Datas[$Article] = array(array($Value["tag_id"], $Value["tag"]));

				$Index = 1;
			}
		}

		return $Datas;
	}
}

?>