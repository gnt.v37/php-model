<?php
namespace System\Models;

import(geye.models.routine);

class SearchProcedure extends Procedure implements Nullable
{
	public $UserID;

	public $Query;

	public $LimitRecord;

	public $OffsetRecord;

	public static function Nullable ()
	{
		return "UserID";
	}
}

class ArticleTags extends Procedure implements Nullable, Humanize
{
	protected $Name = "GArticleTags";
	
	public $TagID;

	public $ArticleID;

	public static function Nullable ()
	{
		return ["TagID", "ArticleID"];
	}
}

class SearchArticles extends SearchProcedure 
{
	protected $Name = "GSearchArticles";
}

class SearchTags extends SearchProcedure
{
	protected $Name = "GSearchTags";
}

class SearchUsers extends SearchProcedure
{
	protected $Name = "GSearchUsers";
}

?>
