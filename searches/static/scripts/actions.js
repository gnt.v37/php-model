/*
* @Author: Rot
* @Date:   2017-11-30 21:56:06
* @Last Modified by:   Rot
* @Last Modified time: 2017-11-30 22:07:31
*/

(function () {

class Search 
{
	constructor ()
	{
		let Page = document.querySelector(`[idata="page"]`);

		this.Follow(Page);
	}

	Follow (Page)
	{
		var FollowElements = document.querySelectorAll(".ge-follow");

		const Follow = Page.value + "@";

		FollowElements.forEach(function (FollowElement) {

			FollowElement.addEventListener("click", function () {

				var FollowingElement = this;

				var FollowingUser = this.getAttribute("data-user");

				System.Process = System.Process.then(function () {

					return new Promise (function (Resolve, Reject) {

						System.get(Follow + FollowingUser + `/${new Date().getTime()}/follow`).then(function () {

							let ClassList = FollowingElement.classList;

							if (ClassList.contains("ge-following"))
							{
								ClassList.remove("ge-following");

								FollowingElement.innerHTML = "Follow";
							}
							else
							{
								ClassList.add("ge-following");

								FollowingElement.innerHTML = "Following";
							}

							Resolve();

						});

					})

				})

			})

		})
	}
}

System.Search = Search;

})();