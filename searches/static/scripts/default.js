/*
* @Author: Rot
* @Date:   2017-10-25 08:51:12
* @Last Modified by:   Rot
* @Last Modified time: 2017-11-30 22:06:06
*/
(function () {


window.onload = function () 
{

	let Container = document.querySelector(".ge-container");

	let Scroll = new System.Scroll ({
		Models: function () 
		{
			var URL = location.href.replace("?q=", "/");

			return `${URL}/scroll`;
		},
		Offsets: 6,
		Limits: 4
	});

	Scroll.Done(function (Response) {

		var Children = Response.children;

		var Length = Children.length;

		for (var i = 0; i < Length; i++)
		{
			Container.appendChild(Children[0]);
		}

	})

	new System.Search();

}

})();