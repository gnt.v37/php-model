<?php
namespace System\Views;

import (geye.views.generic);

class SearchView extends GenericView
{
	protected $Models = ["SearchArticles", "SearchUsers", "SearchTags", "ArticleTags"];
	
	protected $Scripts = [assets.scripts => ["scroll/models.js", "scroll/business.js", "scroll/actions.js"], "default.js"];

	protected function Resolver ()
	{
		$this->Articles = $this->Deeps->SearchArticles;

		$this->Users = $this->Deeps->SearchUsers;

		$this->Tags = $this->Deeps->SearchTags;

		$this->ArticleTags = $this->Deeps->ConvertTags();
	}

}

class ScrollView extends GenericView
{
	protected $Models = ["SearchArticles", "ArticleTags"];

	protected $Templates = "articles.php";

	protected function Resolver ()
	{
		$this->Articles = $this->Deeps->SearchArticles;

		$this->ArticleTags = $this->Deeps->ConvertTags();
	}
}

?>