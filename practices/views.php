<?php
namespace System\Views;

import(geye.views.generic);

define("questions", "questions/");

class PracticeView extends GenericView
{
	protected $Models = ["PracticeQuestion", "StartPractice", "Article"];

	protected $Styles = [users.writes.questions.styles => "practice.css"];

	protected $Scripts = [assets.scripts.modules => "mathjax/MathJax.js?config=AM_HTMLorMML-full"];

	protected function Resolver ()
	{
		$this->Questions = $this->Deeps->Start();

		$this->TimeLeft = $this->Deeps->TimeLeft();

		$this->LimitTime = $this->Deeps->LimitTime();
	}

	public function Inline ($With, $Answers)
	{
		$AnswerLength = 1076 * $With / 100 - 30;

		foreach ($Answers as $Answer) 
		{
			$Real = preg_replace("/<(\/?).*?>/", "", $Answer["answer"]);

			$ContentLength = strlen($Real);

			$With = round(($ContentLength - 40) * 6.28);

			if ($With > $AnswerLength)
			{
				return false;
			}
		}

		return true;
	}
}

class SaveAnswerObject extends ObjectView
{
	protected $Models = "SaveAnswer";
}

class DeleteAnswerObject extends ObjectView
{
	protected $Models = "DeleteAnswer";
}

class FinishPracticeObject extends ObjectView
{
	protected $Models = "FinishPractice";

	protected function Resolver ()
	{
		if (isset($_SESSION[__SIGNIN__]))
		{
			echo json_encode($this->Deeps->FinishPractice);
		}
		else
		{
			echo json_encode(["Result" => ""]);
		}
		
	}
}

?>