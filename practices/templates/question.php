<?php 
	$Answers = isset($this->Answers) ? $this->Answers : null;

	$Index = 1;

	$DataTicked = [];

	$DataQuestion = "";
?>

<?php foreach ($this->Questions as $QuestionID => $Question): $DataQuestion .= "{$QuestionID}," ?>

<ge-data ticked="false" class=""><div class="ge-question-space">
	<ge-question class="" data-question="<?= $QuestionID ?>">
		<div class="__pad-h-16">
			<h5 class="ge-text"><a class="ge-stt" ge-mini="true"><span>Question</span><?= $Index ?>.</a><span contenteditable="false"><?= $Question["title"] ?></span></h5>
		</div>
		<div class="ge-question-content">
			<div class="">
				<div class="__pad-h-16"><?= $Question["question"] ?></div>
			</div>
		</div>
	</ge-question>
	<ge-answer class="ge-top-16">
		<ul class="ge-row">
		<?php 
			$Width = 99.99999 / count($Question["answers"]);

			$IsInline = $this->Inline($Width, $Question["answers"]);

			$Type = $Question["multi_choice"] ? "checkbox" : "radio";

			$Setted = false;

			foreach ($Question["answers"] as $AnswerID => $Answer):

				if ($Answer["ticked"] == 0)
				{
					$Ticked = null;
				}
				else
				{
					$Ticked = 'checked="checked"';

					if ($Setted == false)
					{
						array_push($DataTicked, $Index);

						$Setted = true;
					}
				}
				
		?>
			<li <?php if ($IsInline): ?>
				class="ge-col" style="width: <?= $Width ?>%"
			<?php else: ?>
				class="ge-answer-line"
			<?php endif ?> >
				<a class="ge-answer-text ge-flex-sc" data-question="<?= $QuestionID ?>" data-answer="<?= $AnswerID ?>">
					<input type="<?= $Type ?>" name="Q<?= $QuestionID ?>" data="<?= "{$QuestionID}, {$AnswerID}" ?>" <?= $Ticked ?>>
					<ins class="ge-mark-type ge-inline"><span></span></ins><div class="ge-question ge-inline"><?= $Answer["answer"] ?></div>
				</a>
			</li>

		<?php endforeach ?>
		</ul>
	</ge-answer></div>
</ge-data>
<?php $Index++; endforeach ?>