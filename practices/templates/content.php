<div class="ge-top-32">
	<div class="ge-width-auto">
		<div class="">
			<div class="ge-practice-content">
				<div class="__white __bor">
					<form action="<?= page."summary" ?><?= isset($User) ? "/@{$User["userid"]}" : null ?>" name="practice" method="post" accept-charset="utf-8">
						<div>
							<?php include_once practices.templates."question.php"; ?>
						</div>
						<button ge-submit="true" class="ge-hide"></button>
					</form>
				</div>
			</div>
		</div>
		<div class="ge-message-box">
			<div class="ge-card-box">
				<div class="ge-message">
					<div class="ge-note">NOTE: <span class="ge-question-note"></span></div>
					<div>Please tick the <span ge-message="true"></span></div>
				</div>
			</div>
		</div>
	</div>
	<hidden>
		<input type="hidden" data-questions="true" value='<?= substr($DataQuestion, 0, -1) ?>'>
		<input type="hidden" data-ticked="true" value='<?= json_encode($DataTicked) ?>'>
	</hidden>
</div>
	

