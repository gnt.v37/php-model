<div class="ge-top-32">
	<div class="ge-border-top">
		<div class="ge-row">
			<div class="ge-col l4">
				<ul class="ge-row">
					<li class="ge-left ge-fstyle ge-active"><div><a class="ge-button" ge-tick="all">All</a></div></li>
					<li class="ge-left ge-fstyle"><div><a class="ge-button" ge-tick="unticked">Unticked</a></div></li>
					<li class="ge-left ge-fstyle"><div><a class="ge-button" ge-tick="ticked">Ticked</a></div></li>
				</ul>
			</div>
			<div class="ge-col l4">
				<div class="ge-time ge-flex-cc">
					<div class="ge-color" practice-limit="<?= $this->LimitTime["limit_time"] ?>">
						<input type="hidden" value='<?= json_encode($this->TimeLeft) ?>'>
						<time datetime="<?= $this->TimeLeft["created_at"] ?>" ge-time="true"><?= $this->TimeLeft["limit_time"] ?></time>
					</div>
					<hidden><input type="hidden" ge-limit-time="true" value="<?= $this->LimitTime["limit_time"] ?>" name=""></hidden>
				</div>
			</div>
			<div class="ge-col l4">
				<ul class="ge-row ge-right">
					<li class="ge-left ge-fstyle ge-finish"><div><a class="ge-button">Finish</a></div></li>
				</ul>
			</div>
		</div>
	</div>
	<div class="ge-full-line"></div>
</div>