<?php

namespace System\Cookies;

class PracticeCookie
{
	private $ArticleID;

	public function __construct ($ArticleID = NULL)
	{
		$this->ArticleID = $ArticleID;
	}

	public function IsExist ()
	{
		if (isset($_COOKIE["PRAC".$this->ArticleID]) && isset($_SESSION[__SIGNIN__]) == false)
		{
			return true;
		}

		return false;
	}

	public function SetArticle($ArticleID)
	{
		$this->ArticleID = $ArticleID;
	}

	public function Get ()
	{
		return isset($_COOKIE["PRAC".$this->ArticleID]) ? $_COOKIE["PRAC".$this->ArticleID] : null;
	}

	public function Set ($Cookie)
	{
		setcookie("PRAC".$this->ArticleID, $Cookie, time() + 10 * 3600, "/");
	}

	public function Remove()
	{
		setcookie("PRAC".$this->ArticleID, "", time() + 3600, "/");
	}

	public function Convert()
	{
		$QuestionAnswer = explode(";", self::Get());

		$Explodes = explode(",", $QuestionAnswer[0]);

		$QuestionString = "";

		$AnswerString = "";

		foreach ($Explodes as $Fragment) 
		{
			$Data = explode(":", $Fragment);

			$Question = $Data[0];

			if (count($Data) > 1)
			{	
				foreach ($Data as $Key => $Answer) 
				{
					if ($Key == 0) 
					{
						continue;
					}

					$AnswerString .= "${Answer},"; 
				}

			}

			$QuestionString .= "{$Question},";	

		}
		
		return [substr($QuestionString, 0, -1), substr($AnswerString, 0, -1)];
	}
}

?>