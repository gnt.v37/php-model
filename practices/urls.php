<?php

import(practices.views, false);

$Urls = [
	"/^&p=(?<ArticleID>\d+)/" 	=> views.PracticeView,
	"/^\/\d+\/save_answer/"		=> views.SaveAnswerObject,
	"/^\/\d+\/delete_answer/"	=> views.DeleteAnswerObject,
	"/^\/\d+\/finish_practice/"	=> views.FinishPracticeObject,
]

?>