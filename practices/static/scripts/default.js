/*
* @Author: Rot
* @Date:   2017-11-07 00:36:40
* @Last Modified by:   Rot
* @Last Modified time: 2017-12-09 21:35:40
*/
(function () {

const Questions = document.querySelector("[data-questions]").value;

window.onload = function () 
{
    var Practice = document.forms.practice;

    var Matches = location.search.match(/^\?p=(\d+)/);

    Practice.action = `${Practice.action}/${Matches[1]}`;
    
    new System.Practice(Questions);
}

})();