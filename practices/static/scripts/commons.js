(function () {

var URLPractice = function ()
{
    var Matches = location.href.match(/(.*?\/try)\?p=(\d+)$/);

    return {URL: Matches[1], ArticleID: Matches[2]};
}

System.URLPractice = URLPractice();

})();