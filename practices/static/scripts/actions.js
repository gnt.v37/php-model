(function () {

class BasePractice
{
    constructor (Questions)
    {
        let Business = new System.PracticeBusiness(Questions);
        
        let Getime = document.querySelector("[ge-time]");

        const LimitTime = document.querySelector("[ge-limit-time]").value;

        document.querySelectorAll(`[contenteditable]`).forEach(function (Editor) {

            Business.TurnOffEditMode(Editor);

            Editor.addEventListener("mouseup", function () {
                
                Business.TextToSpeech();

            })
            

        })


        if (!Business.User)
        {
            Business.StartPractice( () => {

                let Time = Getime.previousElementSibling.value;

                if (Time == "" || Time == "false")
                {
                    Time = {limit_time: LimitTime, created_at: new Date()};

                    Getime.previousElementSibling.value = JSON.stringify(Time);

                    this.TimeLeft(Business, Getime);
                }

                console.log(Time)

            });
                
        }

        this.SaveAnswer(Business, Getime, LimitTime);

        this.Finish(Business);

        this.TimeLeft(Business, Getime);

    }

    SaveAnswer (Business, Getime, LimitTime)
    {
        var Answers = document.querySelectorAll("a[data-answer]"),
            ParentElement = Answers[0].parentElement;

        let Time = Getime.previousElementSibling.value;

        let TimeLeft = this.TimeLeft;

        Answers.forEach(function (Answer) {

            Answer.addEventListener("click", function () {

                var Element = this;

                if (Business.User)
                {
                    System.Process = System.Process.then(function () {

                        return new Promise(function (Resolve, Reject) {

                            Business.UpdateAnswer(Element).then(function () {

                                if (Time == "" || Time == "false")
                                {
                                    Time = {limit_time: LimitTime, created_at: new Date()};

                                    Getime.previousElementSibling.value = JSON.stringify(Time);

                                    TimeLeft(Business, Getime);
                                }

                                Resolve();
                            })

                        })

                    })
                }
                else
                {
                    Business.CookieUpdateAnswer(this);

                    if (Time == "" || Time == "false")
                    {
                        Time = {limit_time: LimitTime, created_at: new Date()};

                        Getime.previousElementSibling.value = JSON.stringify(Time);

                        TimeLeft(Business, Getime);
                    }
                }
            })

        })
    }

    Finish (Business)
    {
        var Finish = document.querySelector(".ge-finish"),
            Submit = document.forms.practice;

        Finish.addEventListener("click", function () {

            System.Process = System.Process.then(function () {

                return new Promise (function (Resolve, Reject) {

                    Business.Finish().then(function () {
                        Submit.submit();
                    }, function () {
                        Resolve();
                    })

                })

            })
        })
    }

    TimeLeft (Business, Getime)
    {
        let DataTime = JSON.parse(Getime.previousElementSibling.value);

        let Time, Days, Hours, Minutes, Seconds;

        if (DataTime.limit_time == null)
        {
            let FinalDate = DataTime.created_at;

            if (FinalDate)
            {
                $(Getime).countdown(FinalDate, {elapse: true}).on("update.countdown", function (e) {

                    Time = "";

                    Days = e.strftime("%D");

                    Hours = e.strftime("%H");

                    Minutes = e.strftime("%M");

                    Seconds = e.strftime("%S");

                    Time = Business.TimeLeft(Time, Days, Hours, Minutes, Seconds);

                    this.innerHTML = e.strftime(Time);
                })
            }

            return null;
        }

        var Timeline = new Date(new Date(DataTime.created_at).getTime() + parseInt(DataTime.limit_time) * 1000);

        $(Getime).countdown(Timeline, function (e) {

            Time = "";

            Days = e.strftime("%D");

            Hours = e.strftime("%H");

            Minutes = e.strftime("%M");

            Seconds = e.strftime("%S");

            Time = Business.TimeLeft(Time, Days, Hours, Minutes, Seconds);

            this.innerHTML = e.strftime(Time);
            

        }).on("finish.countdown", function () {

            let Submit = document.forms.practice;

            System.Process = System.Process.then(function () {

                return new Promise(function (Resolve, Reject) {

                    Business.AutoFinish().then(function () {

                        Resolve();
                        
                        console.log(Submit);

                        window.location.href = `${Submit.action}`;

                    })

                })
                    
            })
        })
    }
}

class Practice extends BasePractice
{
    constructor (Questions)
    {
        super(Questions);

        let Ticks = document.querySelectorAll("[ge-tick]");

        let Gedatas = document.querySelectorAll("ge-data");

        let Ticked = JSON.parse(document.querySelector("[data-ticked]").value);

        Ticks.forEach(function (Tick) {

            Tick.addEventListener("click", function () {

                var Type = this.getAttribute("ge-tick");

                switch (Type) 
                {
                    case "all":
                        
                        Gedatas.forEach(function(Gedata) {
                            $(Gedata).slideDown()
                        })

                        break;
                    case "unticked":
                       Gedatas.forEach(function(Gedata) {

                            Gedata.getAttribute("ticked") == "true" ? $(Gedata).slideUp() : $(Gedata).slideDown();

                        })
                        break;
                    case "ticked":
                        Gedatas.forEach(function(Gedata) {

                            Gedata.getAttribute("ticked") == "true" ? $(Gedata).slideDown() : $(Gedata).slideUp();

                        })
                        break;
                    default:
                         Gedatas.forEach(function(Gedata) {
                            $(Gedata).slideDown()
                        })
                        break;
                }

            })

        })

        Gedatas.forEach(function (Gedata, Index) {

            var Length = Ticked.length;

            for (var i = 0; i < Length; i++)
            {
                if (Ticked[i] == Index + 1)
                {
                    Gedata.setAttribute("ticked", "true");
                    Ticked.splice(i, 1);
                }
                
            }

            Gedata.querySelector("[ge-mini]").addEventListener("click", function () {

                var Parent = this.parentElement;

                while (Parent.nodeName != "GE-QUESTION")
                {
                    Parent = Parent.parentElement;
                }

                if (Parent)
                {
                    var Question = Parent.lastElementChild;

                    var Answer = Parent.nextElementSibling;

                    if (Answer.style.display == "")
                    {
                        $(Answer).slideUp();
                    }
                    else
                    {
                        $(Answer).slideDown();
                    }

                    if (Question.style.display == "")
                    {
                        $(Question).slideUp();
                    }
                    else
                    {
                        $(Question).slideDown();
                    }
                }

            })

        })
    }
}

System.Practice = Practice;

})();