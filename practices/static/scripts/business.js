(function () {

class BaseBusiness
{
    constructor ()
    {
        this.Questions = document.querySelectorAll("ge-data");

        this.User = document.querySelector(`[idata="user"]`).value;

        this.Cookie = new System.Cookie();

        this.Audio = new System.Audio();

    }

    TextToSpeech ()
    {
        var Text = "";

        if (window.getSelection)
        {
            Text = window.getSelection().toString();
        }
        else
        {
            Text = document.selection.createRange();
        }

        this.Audio.Speak(Text);
    }

    TurnOffEditMode (Editor)
    {
        let Content = Editor.textContent;

        if (Content.trim().length == 0)
        {
            Editor.style.display = "none";
        }

        Editor.setAttribute("contenteditable", "false");
    }

    Check ()
    {
        let Check;

        var GeMessageBox = document.querySelector(".ge-message-box");

        var GeqNote = document.querySelector(".ge-question-note");

        var GeMessage = document.querySelector("[ge-message]");

        for (let Question of this.Questions)
        {
            var Answers = Question.querySelectorAll("input");

            var Stt = Question.querySelector(".ge-stt").innerHTML;

            Check = false;

            Answers.forEach(function (Answer) {

                if (Answer.checked == true)
                {
                    Check = true;
                }
            })

            if (Check == false)
            {
                Question.querySelector("input").focus();

                GeMessageBox.style.display = "block";

                GeqNote.innerHTML = Stt;

                GeMessage.innerHTML = Stt;

                setTimeout(function () {

                    GeMessageBox.style.display = "none";
                    

                }, 5000);

                return false;
            }
        }

        return true;
    }

    SetTick (Element, Checked)
    {
        var Parent = Element.parentElement;

        while (Parent && Parent.nodeName != "GE-DATA")
        {
            Parent = Parent.parentElement;
        }

        if (Parent)
        {
            if (Checked)
            {
                Parent.setAttribute("ticked", "false");
            }
            else
            {
                Parent.setAttribute("ticked", "true");
            }
        }
    }

    TimeLeft (Time, Days, Hours, Minutes, Seconds)
    {
        if (Days > 0)
        {
             Time += "<span>%D</span> Day";

            if (Day > 1)
            {
                Time += "s";
            }
        }

        if (Hours > 0)
        {
            Time += " <span>%H</span> hour";

            if (Hours > 1)
            {
                Time += "s";
            }
        }

        if (Minutes > 0)
        {
            Time += " <span>%M</span> minute";

            if (Minutes > 1)
            {
                Time += "s";
            }
        }

        if (Seconds > 0)
        {
            Time += " <span>%S</span> second";

            if (Seconds > 1)
            {
                Time += "s";
            }
        }

        return Time;
    }

    StartPractice (Callback)
    {
        var Name = "PRAC" + System.URLPractice.ArticleID;

        if (this.Cookie.Remember(Name) == null)
        {
            this.Cookie.Write = {Name: Name, Value: `${this.Models.Questions};${new Date().toGMTString()}`, Expire: 10};

            console.log(typeof Callback)
            if (typeof Callback === "function")
            {
                Callback();
            }
        }
    }

    FinishPracticeCookie ()
    {
        var Name = "PRAC" + System.URLPractice.ArticleID;

        var Cookie = this.Cookie.Remember(Name, true);

        var Split = Cookie.Value.split(";");

        var Needed = Split[0];

        var Unticked = Needed.match(/,?\d+,?/g);

        Unticked.forEach( function(Element) {

            var Matches = Element.match(/\d+/);

            Needed = Needed.replace(Matches, `${Matches}:null`); 

        });

        Cookie.Value = Needed + ";" + Split[1];

        this.Cookie.Write = {Name: Name, Value: Cookie.Value, Expire: 10};

        console.log("?")
    }

    CookieUpdateAnswer (Element)
    {
        var Name = "PRAC" + System.URLPractice.ArticleID;

        var Input = Element.querySelector("input");

        var Cookie = this.Cookie.Remember(Name, true);

        var Question = Element.getAttribute("data-question");

        var Answer = Element.getAttribute("data-answer");

        var Regex = new RegExp(`,?(${Question}(:\\d+)?),?`);

        if (Cookie)
        {
            var Type = Input.getAttribute("type");

            var Split = Cookie.Value.split(";");

            var Needed = Split[0];

            var StartTime = Split[1];

            var Questions = null;

            if (Type == "radio")
            {
                var Matches = Needed.match(Regex);

                var Real = Matches[0];

                if (Matches[2] == `:${Answer}`)
                {
                    Questions = Needed.replace(Regex, Real.replace(Matches[1], Question));
                }
                else
                {
                    Questions = Needed.replace(Regex, Real.replace(Matches[1], `${Question}:${Answer}`));
                }

                Questions = Questions + ";" + StartTime;

                if (Matches)
                {
                    this.Cookie.Write = {Name: Name, Value: Questions, Expire: 10};
                }
            }
            else if (Type == "checkbox")
            {
                var QuestionRegex = new RegExp(`,?(${Question}((:\\d+)*)),?`);
            
                var AnswerRegex = new RegExp(`:(${Answer}):?`);

                var Matches = Needed.match(QuestionRegex);

                var Real = Matches[0];

                var AnswerMatch = Matches[2].match(AnswerRegex);

                if (AnswerMatch)
                {
                    var Mark = "";

                    if (AnswerMatch[0].endsWith(":"))
                    {
                        Mark = ":";
                    }

                    Questions = Needed.replace(QuestionRegex, Real.replace(Matches[2], Matches[2].replace(AnswerRegex, Mark)));
                }
                else
                {
                    Questions = Needed.replace(QuestionRegex, Real.replace(Matches[1], `${Matches[1]}:${Answer}`));
                }

                Questions = Questions + ";" + StartTime;

                if (Matches)
                {
                    this.Cookie.Write = {Name: Name, Value: Questions, Expire: 10};
                }
            }
        }
        else
        {
            var Matches = this.Models.Questions.match(Regex);

            if (Matches)
            {
                Matches = Matches[0];

                var Questions = this.Models.Questions.replace(Regex, Matches.replace(Question, `${Question}:${Answer}`));

                this.Cookie.Write = {Name: Name, Value: `${Questions};${new Date().toGMTString()}`, Expire: 10};
            }
            
        }

        if (Input.checked)
        {
             this.SetTick(Element, Input.checked);

            Input.checked = false;
        }
        else
        {
             this.SetTick(Element, Input.checked);

            Input.checked = true;
        }
       
    }
}

class PracticeBusiness extends BaseBusiness
{
    constructor (Questions)
    {
        super();

        this.AnswerData = new FormData();

        this.Models = new System.PracticeModel(System.URLPractice, Questions);

        document.querySelectorAll("[transform]").forEach(function (Transform) {
            Transform.removeAttribute("style");
        })


    }

    UpdateAnswer (Element)
    {
        var Question = Element.getAttribute("data-question"),
            Answer   = Element.getAttribute("data-answer"),
            Input    = Element.querySelector("input");
        console.log(Element)

        this.AnswerData.set("QuestionID", Question);

        this.AnswerData.set("AnswerID", Answer);

        if (Input.checked == true)
        {
            return this.Models.DeleteAnswer(this.AnswerData).then( () => {

                Input.removeAttribute("checked");

                this.SetTick(Element, Input.checked);

                Input.checked = false;
            })
        }

        return this.Models.SaveAnswer(this.AnswerData).then( (Response) => {

            this.SetTick(Element, Input.checked);

            Input.checked = true;

        });
        
    }

    Finish ()
    {
        if (this.Check())
        {
            return this.AutoFinish();
        }

        return Promise.reject();
    }

    AutoFinish ()
    {
        var Practice = document.forms.practice;

        console.log(Practice)

        return this.Models.Finish(new FormData()).then(function(Response) {

            Response = Response.match(/^{.*?}/);

            var Times = JSON.parse(Response);

            Practice.action = `${Practice.action}/${Times.Result}`;
        });
    }
}

System.PracticeBusiness = PracticeBusiness;

})();