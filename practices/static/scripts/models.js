(function () {

class PracticeModel
{
    constructor (URLPractice, Questions)
    {
       this.URL = URLPractice.URL;

       this.ArticleID = URLPractice.ArticleID;

       this.Questions = Questions;

       this.Ticked = JSON.parse(document.querySelector("[data-ticked]").value);

    }

    SaveAnswer (Data)
    {
        Data.set("ArticleID", this.ArticleID);

        if (this.Ticked.length == 0)
        {
            Data.set("Questions", this.Questions);
        }
        else if (Data.has("Questions"))
        {
            Data.delete("Questions");
        }

        return System.post(`${this.URL}/${new Date().getTime()}/save_answer`, Data);
    }

    DeleteAnswer (Data)
    {
        Data.set("ArticleID", this.ArticleID);

        return System.post(`${this.URL}/${new Date().getTime()}/delete_answer`, Data);
    }

    Finish (Data)
    {
        Data.set("ArticleID", this.ArticleID);
        
        return System.post(`${this.URL}/${new Date().getTime()}/finish_practice`, Data);
    }
}

System.PracticeModel = PracticeModel;

})();