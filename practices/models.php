<?php

namespace System\Models;

import(geye.models);

class Article extends Schema 
{
	protected $Nerve = "articles";
}

class Practice extends Procedure 
{
	public $UserID;

	public $ArticleID;
}

class PracticeQuestion extends Practice implements Nullable, Humanize
{
	protected $Name = "GPracticeQuestion";

	public $Questions;

	public $Answers;

	public static function Nullable ()
	{
		return ["UserID", "Questions", "Answers"];
	}
}

class SaveAnswer extends Practice implements Nullable
{
	protected $Name = "DSavePracticeAnswer";

	public $Questions;

	public $QuestionID;

	public $AnswerID;

	public static function Nullable ()
	{
		return "Questions";
	}
}

class StartPractice extends Practice implements Humanize, Nullable
{
	protected $Name = "DStartPractice";

	public $Questions;

	public $UserID;

	public $ArticleID;

	public static function Nullable ()
	{
		return "Questions";
	}
}

class FinishPractice extends Dynamic 
{
	protected $Name = "DPracticeFinish";

	public $UserID;

	public $ArticleID;
}

class DeleteAnswer extends Dynamic
{
	protected $Name = "DDeletePracticeAnswer";

	public $UserID;

	public $ArticleID;

	public $QuestionID;

	public $AnswerID;
}

?>