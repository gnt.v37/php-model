<?php

/**
 * @Author: Rot
 * @Date:   2017-10-21 13:00:01
 * @Last Modified by:   Rot
 * @Last Modified time: 2017-12-29 17:55:15
 */

namespace System\Deeps;

include_once "cookies.php";

use System\Cookies\PracticeCookie;

class PracticeDeep 
{
	private $Cookie;

	public function __construct ()
	{
		$this->Cookie = new PracticeCookie();
	}

	public function GetPractice ()
	{
		$this->Cookie->SetArticle($this->PracticeForm->ArticleID);

		if ($this->Cookie->IsExist())
		{

			$CookieData = explode(";", $this->Cookie->Get());

			if (isset($CookieData[2]))
			{
				header ("Location: " . page."summary/".$this->PracticeForm->ArticleID."/");

				die();
			}
			else
			{
				$Convert = $this->Cookie->Convert();

				return self::AnswerToQuestion($this->PracticeQuestion->Call([$Convert[0], $Convert[1], null, $this->PracticeForm->ArticleID]));
			}
		}
		else
		{
			$Practice = $this->PracticeQuestion->Call([null, null, $this->PracticeForm->UserID, $this->PracticeForm->ArticleID]);

			if (isset($Practice[0]["LimitTry"]) || isset($Practice[0]["Permission"]))
			{
				return $Practice[0];
			}
			else
			{
				return self::AnswerToQuestion($Practice);
			}
		}
	}

	public function Start ()
	{
		$Practice = self::GetPractice();

		if (isset($Practice["LimitTry"]) == false && isset($Practice["Permission"]) == false && $this->PracticeForm->UserID)
		{
			$Questions = "";

			foreach ($Practice as $QuestionID => $Value) 
			{
				$Questions .= "{$QuestionID},";
			}

			$Questions = substr($Questions, 0, -1);

			$this->StartPractice->Call([$Questions, $this->PracticeForm->UserID, $this->PracticeForm->ArticleID]);
		}

		return $Practice;
	}

	public function TimeLeft ()
	{
		if ($this->Cookie->IsExist())
		{
			$Article = $this->Article->Remember(["article_id" => $this->PracticeForm->ArticleID], true)->Get("limit_time");

			$Cookie = explode(";", $this->Cookie->Get());

			$Article["created_at"] = $Cookie[1];

			return $Article;
		}

		return $this->Article->Chain(["practices" => "article_id"])
			->Remember([
				"practices.article_id" 	=> $this->PracticeForm->ArticleID, 
				"practices.user_id" 	=> $this->PracticeForm->UserID,
				"practices.status"		=> 0
			], true)->Get(["limit_time", "practices.created_at"]);
	}

	public function LimitTime ()
	{
		return $this->Article->Remember(["articles.article_id" => $this->PracticeForm->ArticleID], true)->Get("limit_time");
	}

	private function AnswerToQuestion ($Questions)
	{
		$Result = array();

		foreach ($Questions as $Question) 
		{
			$QuestionID = $Question["question_id"];

			$Answer = array (
				$Question["answer_id"] 	=> array(
					"answer" => $Question["answer"], 
					"ticked" => $Question["ticked"]
				)
			);


			if (array_key_exists($QuestionID, $Result))
			{
				$Len = count($Result[$QuestionID]["answers"]);
				$Result[$QuestionID]["answers"] += $Answer;

			}
			else
			{
				$Result[$QuestionID] = array(
						"title" => $Question["title"],
						"question" => $Question["question"],
						"multi_choice" => $Question["multi_choice"]
					);

				$Result[$QuestionID]["answers"] = $Answer;
			}
		}

		return $Result;
	}

}

?>