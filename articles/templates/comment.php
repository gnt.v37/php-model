<?php if ($this->Comments): ?><?php foreach ($this->Comments as $Comment): ?>
<section class="ge-comment ge-border-bottom" comment="<?= $Comment["comment_id"] ?>"><div class="ge-white">
	<div class="ge-padding1620"><ul class="" comment-parent="true"><li class="ge-user-comment ge-relative"><div class="ge-row"><div class="ge-left">
		<ge-user><div class="ge-inline"><a href="<?= page."@{$Comment["user_id"]}"; ?>" prevent="true">
			<div class="ge-inline ge-relative ge-image40">
			<?php if ($Comment["image_local"]): ?>
				<img src="<?= page.users.images."40/{$Comment["image_local"]}" ?>">
			<?php else: ?>
				<img data-src="" ge-user-avatar="true" data-name="<?= $Comment["lastname"] || $Comment["firstname"] ? "{$Comment["lastname"]} {$Comment["firstname"]}" : $Comment["email"] ?>">
			<?php endif ?>
				<div class="ge-avatar-radius"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 70 70"><path d="M5.53538374,19.9430227 C11.180401,8.78497536 22.6271155,1.6 35.3571429,1.6 C48.0871702,1.6 59.5338847,8.78497536 65.178902,19.9430227 L66.2496695,19.401306 C60.4023065,7.84329843 48.5440457,0.4 35.3571429,0.4 C22.17024,0.4 10.3119792,7.84329843 4.46461626,19.401306 L5.53538374,19.9430227 Z"></path><path d="M65.178902,49.9077131 C59.5338847,61.0657604 48.0871702,68.2507358 35.3571429,68.2507358 C22.6271155,68.2507358 11.180401,61.0657604 5.53538374,49.9077131 L4.46461626,50.4494298 C10.3119792,62.0074373 22.17024,69.4507358 35.3571429,69.4507358 C48.5440457,69.4507358 60.4023065,62.0074373 66.2496695,50.4494298 L65.178902,49.9077131 Z"></path>
			</svg></div></div>
	</a></div><div class="ge-inline ge-left-12">
		<div><a href="<?= page."@{$Comment["user_id"]}"; ?>" prevent="true" class="ge-name"><?=$Comment["lastname"] || $Comment["firstname"] ? "{$Comment["lastname"]} {$Comment["firstname"]}" : $Comment["email"]  ?></a></div>
		<div class="ge-time"><time datetime="<?= $Comment["created_at"] ?>"></time></div>
	</div></ge-user></div>
		<div class="ge-right"><div><button type="" class="ge-comment-option-button" data-comment="<?= $Comment["comment_id"] ?>"><span class="ge-icon"><svg width="23" height="23" viewBox="-480.5 272.5 21 21"><path d="M-463 284.6c.9 0 1.6-.7 1.6-1.6s-.7-1.6-1.6-1.6-1.6.7-1.6 1.6.7 1.6 1.6 1.6zm0 .9c-1.4 0-2.5-1.1-2.5-2.5s1.1-2.5 2.5-2.5 2.5 1.1 2.5 2.5-1.1 2.5-2.5 2.5zm-7-.9c.9 0 1.6-.7 1.6-1.6s-.7-1.6-1.6-1.6-1.6.7-1.6 1.6.7 1.6 1.6 1.6zm0 .9c-1.4 0-2.5-1.1-2.5-2.5s1.1-2.5 2.5-2.5 2.5 1.1 2.5 2.5-1.1 2.5-2.5 2.5zm-7-.9c.9 0 1.6-.7 1.6-1.6s-.7-1.6-1.6-1.6-1.6.7-1.6 1.6.7 1.6 1.6 1.6zm0 .9c-1.4 0-2.5-1.1-2.5-2.5s1.1-2.5 2.5-2.5 2.5 1.1 2.5 2.5-1.1 2.5-2.5 2.5z"></path></svg></span></button>
		</div>
	</div></div></li>
	<li class="ge-comment-content"><span><?= $Comment["comment"] ?></span></li>
		<li class="ge-comment-action"><ul class="ge-row" select="false">
			<li class="ge-left ge-right-8"><div class="ge-clap-flex">
				<button type="button" class="ge-comment-clap" data-user="<?= $Comment["clap_user_id"] == $_SESSION[__SIGNIN__]["userid"] ? 1 : 0 ?>" data-comment="<?= $Comment["comment_id"] ?>">
					<span class="ge-icon"><svg width="26" height="26" viewBox="0 0 33 33"><g fill-rule="evenodd"><path d="M29.58 17.1l-3.854-6.78c-.365-.543-.876-.899-1.431-.989a1.491 1.491 0 0 0-1.16.281c-.42.327-.65.736-.7 1.207v.001l3.623 6.367c2.46 4.498 1.67 8.802-2.333 12.807-.265.265-.536.505-.81.728 1.973-.222 3.474-1.286 4.45-2.263 4.166-4.165 3.875-8.6 2.215-11.36zm-4.831.82l-3.581-6.3c-.296-.439-.725-.742-1.183-.815a1.105 1.105 0 0 0-.89.213c-.647.502-.755 1.188-.33 2.098l1.825 3.858a.601.601 0 0 1-.197.747.596.596 0 0 1-.77-.067L10.178 8.21c-.508-.506-1.393-.506-1.901 0a1.335 1.335 0 0 0-.393.95c0 .36.139.698.393.95v.001l5.61 5.61a.599.599 0 1 1-.848.847l-5.606-5.606c-.001 0-.002 0-.003-.002L5.848 9.375a1.349 1.349 0 0 0-1.902 0 1.348 1.348 0 0 0 0 1.901l1.582 1.582 5.61 5.61a.6.6 0 0 1-.848.848l-5.61-5.61c-.51-.508-1.393-.508-1.9 0a1.332 1.332 0 0 0-.394.95c0 .36.139.697.393.952l2.363 2.362c.002.001.002.002.002.003l3.52 3.52a.6.6 0 0 1-.848.847l-3.522-3.523h-.001a1.336 1.336 0 0 0-.95-.393 1.345 1.345 0 0 0-.949 2.295l6.779 6.78c3.715 3.713 9.327 5.598 13.49 1.434 3.527-3.528 4.21-7.13 2.086-11.015zM11.817 7.727c.06-.328.213-.64.466-.893.64-.64 1.755-.64 2.396 0l3.232 3.232c-.82.783-1.09 1.833-.764 2.992l-5.33-5.33z"></path><path d="M13.285.48l-1.916.881 2.37 2.837z"></path><path d="M21.719 1.361L19.79.501l-.44 3.697z"></path><path d="M16.502 3.298L15.481 0h2.043z"></path></g></svg></span></button>
			<span class='ge-color' is-comment='true' comment-value='<?= $Comment["claps"]?>'>
			<?php if ($Comment["claps"] > 0): ?>
				<?= $Comment["claps"] ?>
			<?php endif ?>
			</span></div></li><li class="ge-left">
		<button class="ge-reply" data-user="<?= $Comment["user_id"] ?>" data-comment="<?= $Comment["comment_id"] ?>">
			<span class="ge-icon ge-color"><svg width="26" height="26" viewBox="0 0 26 26" class="__svgi"><path d="M21.27 20.058c1.89-1.826 2.754-4.17 2.754-6.674C24.024 8.21 19.67 4 14.1 4 8.53 4 4 8.21 4 13.384c0 5.175 4.53 9.385 10.1 9.385 1.007 0 2-.14 2.95-.41.285.25.592.49.918.7 1.306.87 2.716 1.31 4.19 1.31.276-.01.494-.14.6-.36a.625.625 0 0 0-.052-.65c-.61-.84-1.042-1.71-1.282-2.58a5.417 5.417 0 0 1-.154-.75zm-3.85 1.324l-.083-.28-.388.12a9.72 9.72 0 0 1-2.85.424c-4.96 0-8.99-3.706-8.99-8.262 0-4.556 4.03-8.263 8.99-8.263 4.95 0 8.77 3.71 8.77 8.27 0 2.25-.75 4.35-2.5 5.92l-.24.21v.32c0 .07 0 .19.02.37.03.29.1.6.19.92.19.7.49 1.4.89 2.08-.93-.14-1.83-.49-2.67-1.06-.34-.22-.88-.48-1.16-.74z"></path></svg></span>
		</button></li></ul></li></ul>
		<div class="ge-reply-counter">
		<?php if ($Comment["reply_counter"]): ?>
			<button data-comment="<?= $Comment["comment_id"] ?>" class="ge-open-reply-comment ge-color"><span>View all <?= $Comment["reply_counter"] ?> replies</span></button>
		<?php endif ?>
		</div>
	</div><div class="ge-comment-reply">
	
	</div></div></section>
	<?php endforeach ?>	
<?php endif ?>