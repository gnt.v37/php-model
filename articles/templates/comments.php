<div class="ge-smaill-width"><div class="ge-article-space">
<ul class="ge-row">
<?php foreach ($this->SuggestArticles as $Article): ?>
<li class="article-item"><ge-article>
	<div class="ge-white ge-article-box"><div class="ge-row"><ge-image>
		<?php $Image = home.images; $Style = "view-mode-o" ? $Image .= "300/" : $Image .= "280/" ?>
		<a href="view?v=<?= $Article["article_id"] ?>" prevent="true" class="ge-scale"><div class="ge-bk-img ge-hover ge-real" data-src="<?=  $Image.$Article["image"] ?>"></div></a>
	</ge-image><ge-content class="ge-hoz-16"><ul class="article-info ge-ver-16 flex-content"><li class="flex-mode" style="position: relative;">
	<div class="absolute-full flex-content"><h4 class="">
		<a href="view?v=<?php echo $Article["article_id"] ?>" class="article-title" ><?php echo $Article["title"] ?></a>
	</h4><div class="article-description"><span><?= $Article["description"] ?></span>	
	</div></div></li><li class="ge-top-12"><div class="ge-row">
	<ge-user><div class="ge-inline"><a href="<?= page."@{$Article["user_id"]}"; ?>" prevent="true">
		<?php $DisplayName = $Article["lastname"] || $Article["firstname"] ? "{$Article["lastname"]} {$Article["firstname"]}" : $Article["email"]; ?>
		<div class="ge-inline ge-relative ge-image40">
		<?php if ($Article["user_image_local"]): ?>
			<img src="<?= users.images."40/{$Article["user_image_local"]}" ?>">
		<?php else: ?>
			<img ge-user-avatar="true" data-name="<?= $DisplayName ?>" data-src="">
		<?php endif ?>
	<div class="ge-avatar-radius"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 70 70">
		<path d="M5.53538374,19.9430227 C11.180401,8.78497536 22.6271155,1.6 35.3571429,1.6 C48.0871702,1.6 59.5338847,8.78497536 65.178902,19.9430227 L66.2496695,19.401306 C60.4023065,7.84329843 48.5440457,0.4 35.3571429,0.4 C22.17024,0.4 10.3119792,7.84329843 4.46461626,19.401306 L5.53538374,19.9430227 Z"></path><path d="M65.178902,49.9077131 C59.5338847,61.0657604 48.0871702,68.2507358 35.3571429,68.2507358 C22.6271155,68.2507358 11.180401,61.0657604 5.53538374,49.9077131 L4.46461626,50.4494298 C10.3119792,62.0074373 22.17024,69.4507358 35.3571429,69.4507358 C48.5440457,69.4507358 60.4023065,62.0074373 66.2496695,50.4494298 L65.178902,49.9077131 Z"></path>
	</svg></div></div></a></div><div class="ge-inline ge-left-12"><div>
		<a href="<?= page."@{$Article["user_id"]}"; ?>" prevent="true" class="ge-name"><?= $DisplayName ?></a></div>
	<div class="ge-time"><time datetime="<?= $Article["created_at"] ?>"></time></div></div>
	</ge-user></div></li></ul>
	</ge-content></div></div></ge-article>
</li>
<?php endforeach ?>
</ul>	
</div></div>
<div class="ge-small-content ge-bottom-32">
<div class="">
<?php $User = $_SESSION[__SIGNIN__]; ?>
<div class="ge-response"><h6 >Responses</h6></div><div class="ge-relative ge-white ge-border-box"><div class="" select="false">
	<div class="ge-padding1620"><div class="ge-flex-sc ge-comment-box"><ge-user><div class="ge-inline">
		<a href="<?= page."@{$User["userid"]}"; ?>" prevent="true"><div class="ge-inline ge-relative ge-image40">
			<?php if ($User["session_image_local"]): ?>
				<img src="<?= page.users.images."40/{$User["session_image_local"]}" ?>">
			<?php else: ?>
				<img data-src="" ge-user-avatar="true" data-name="<?= $User["session_lastname"] || $User["session_firstname"] ? "{$User["session_lastname"]} {$User["session_firstname"]}" : $User["session_email"] ?>">
			<?php endif ?>
			<div class="ge-avatar-radius"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 70 70">
				<path d="M5.53538374,19.9430227 C11.180401,8.78497536 22.6271155,1.6 35.3571429,1.6 C48.0871702,1.6 59.5338847,8.78497536 65.178902,19.9430227 L66.2496695,19.401306 C60.4023065,7.84329843 48.5440457,0.4 35.3571429,0.4 C22.17024,0.4 10.3119792,7.84329843 4.46461626,19.401306 L5.53538374,19.9430227 Z"></path><path d="M65.178902,49.9077131 C59.5338847,61.0657604 48.0871702,68.2507358 35.3571429,68.2507358 C22.6271155,68.2507358 11.180401,61.0657604 5.53538374,49.9077131 L4.46461626,50.4494298 C10.3119792,62.0074373 22.17024,69.4507358 35.3571429,69.4507358 C48.5440457,69.4507358 60.4023065,62.0074373 66.2496695,50.4494298 L65.178902,49.9077131 Z"></path></svg>
		</div></div></a></div>
	<div class="ge-inline ge-left-12"><div class="ge-row"><div class="ge-left"><span class="ge-comment-description">Write a public response</span></div><div class="ge-right ge-send">
		<button type=""><span class="ge-icon"><svg width="26" height="26" viewBox="0 0 512.001 512.001" ><path d="M406.262,152.467l-26.55,17.03l-26.55,17.03c-8.84,5.693-17.723,11.316-26.526,17.067l-52.909,34.357 c-8.83,5.707-17.617,11.483-26.4,17.263l-26.362,17.324c-8.781,5.784-17.592,11.521-26.335,17.367l-26.27,17.467 c-8.759,5.818-17.519,11.638-26.234,17.525l-26.181,17.608c-6.264,4.248-12.524,8.5-18.781,12.758l47.605,87.001 c5.792,10.585,19.868,13.133,29.003,5.252l34.634-29.885l-16.41-8.425l-6.216-1.96c-0.403-0.127-0.261-0.518,0.273-1.092 l-1.046-0.537l-0.543-0.278c-7.27-3.733-10.139-12.653-6.405-19.924c0.723-1.41,1.691-2.697,2.755-3.759 c12.317-12.191,24.703-24.314,36.961-36.562l36.836-36.684l36.697-36.818l18.329-18.428l18.271-18.484l36.54-36.969l36.407-37.098 l12.296-12.548C410.853,149.532,408.551,150.991,406.262,152.467z"/><path d="M486.015,60.718c-3.952,1.16-8.413,3.092-13.445,6.026L8.491,292.119c-12.551,5.998-10.793,24.392,2.668,27.903 l60.878,15.88c9.598-5.701,19.18-11.427,28.759-17.158l27.013-16.304c9.016-5.416,17.988-10.901,26.96-16.386l26.924-16.444 c8.988-5.459,17.909-11.025,26.859-16.544l26.832-16.587c8.947-5.523,17.893-11.052,26.793-16.648l53.478-33.466 c8.929-5.553,17.776-11.233,26.668-16.844l26.644-16.882l26.644-16.882c8.895-5.607,17.728-11.309,26.586-16.973l53.12-34.026 c4.679-2.997,10.902-1.634,13.898,3.045c2.559,3.995,1.939,9.115-1.209,12.392l-0.047,0.045 c-12.001,12.496-24.079,24.919-36.017,37.475l-35.895,37.593l-35.87,37.618l-35.736,37.748l-17.868,18.873l-17.809,18.93 l-35.58,37.898l-35.441,38.033c-7.069,7.562-14.095,15.167-21.115,22.777l28.705,13.394l152.024,46.158 c8.052,2.445,16.532-2.249,18.737-10.37l95.172-350.677C515.423,70.381,501.238,56.248,486.015,60.718z"/>
	</svg></span></button></div></div></div>
</ge-user></div><form name="comments" method="POST" style="display: none;"><div class="ge-top-12">
	<div class="ge-height160" role="textbox" aria-multiline="true" contenteditable="plaintext-only" data-placeholder="Write a public response"></div>
		<div class=""><button type="button" class="ge-button ge-comment-publish">Publish</button>
			<button type="button" class="ge-button ge-comment-cancel">Cancel</button>
</div></div></form></div></div></div></div>
<div class="ge-top-24"><div class="ge-comment-container">
<?php include_once "comment.php"; ?>
</div></div></div>
