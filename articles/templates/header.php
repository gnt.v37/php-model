<div class="ge-top-32">
	<div class="ge-border-top">
		<div class="ge-row">
			<div class="ge-left">
				<ul class="ge-row">
					<li class="ge-left ge-fstyle"><div><a class="ge-button" href="<?= page."try?p={$this->Article["article_id"]}"?>">Try it yourself</a></div></li>
					<li class="ge-left ge-fstyle ge-des ge-active"><div><a class="ge-button" href="<?= page."view?v={$this->Article["article_id"]}"?>">Description</a></div></li>
					<li class="ge-left ge-fstyle ge-sct "><div><a class="ge-button" href="<?= page."view/score/top?v={$this->Article["article_id"]}" ?>">Top score</a></div></li>
					<?php if (isset($_SESSION[__SIGNIN__])): ?>
					<li class="ge-left ge-fstyle ge-scu"><div><a class="ge-button" href="<?= page."view/score/@{$_SESSION[__SIGNIN__]["userid"]}?v={$this->Article["article_id"]}" ?>">Your score</a></div></li>
					<?php endif ?>
				</ul>
			</div>
			<div class="ge-right">
				<ul class="ge-row">
					<?php if (isset($_SESSION[__SIGNIN__])): $User = $_SESSION[__SIGNIN__];  ?>
						<?php if ($this->Article["user_id"] == $User["userid"]): ?>
							<li class="ge-left ge-fstyle"><div><a href="<?= page."write/".$this->Article["article_id"]."/edit" ?>" class="ge-button">Edit</a></div></li>
							<li class="ge-left ge-fstyle ge-manage"><div><a href="<?= page."view/manage?v=".$this->Article["article_id"] ?>" class="ge-button">Manage</a></div></li>
						<?php endif ?>
					<?php endif ?>
					<li class="ge-left ge-fstyle"><div><a class="ge-button">Options</a></div></li>
				</ul>
			</div>
		</div>
	</div>
	<div class="ge-full-line"></div>
</div>