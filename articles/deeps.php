<?php

namespace System\Deeps;

use System\Models\Schema;

class ArticleDeep
{
	public function GetTags ()
	{
		return $this->ArticleTag->Chain(["tags" => "tag_id"])
			->Remember(["article_id" => $this-> ArticleForm->ArticleID])->Get(["tags.tag_id", "tag"]);
	}

	public function GetYourClap ()
	{
		return $this->ArticleClaps->Count(["article_id" => $this->ArticleForm->ArticleID, "user_id" => $this->ArticleForm->UserID]);
	}

	public function GetClaps ()
	{
		return $this->ArticleClaps->Count(["article_id" => $this->ArticleForm->ArticleID]);
	}

	public function ClapToArticle ()
	{
		$Clapped = $this->GetYourClap();

		if ($Clapped["Total"])
		{
			$this->ArticleClaps->Forget(["article_id" => $this->ArticleForm->ArticleID, "user_id" => $this->ArticleForm->UserID]);
		}
		else
		{
			$this->ArticleClaps->Write(["article_id" => $this->ArticleForm->ArticleID, "user_id" => $this->ArticleForm->UserID]);
		}
	}

	public function ClapToComment ()
	{
		$Data = ["comment_id" => $this->ArticleForm->CommentID, "user_id" => $_SESSION[__SIGNIN__]["userid"]];

		$Clapped = $this->ArticleCommentClaps->Count($Data);

		if ($Clapped["Total"])
		{
			$Clapped = $this->ArticleCommentClaps->Forget($Data);

		}
		else
		{
			$Clapped = $this->ArticleCommentClaps->Write($Data);
		}
	}

	public function SuggestUsers ()
	{
		$ArticleUserID = $this->Article->Remember(["article_id" => $this->ArticleForm->ArticleID], true)->Get("user_id");

		return $this->UserFollow->Chain(["users" => "user_id"])->Remember(["follower" => $ArticleUserID["user_id"]])->Limit(2)->Get();
	}

	public function GetComments ()
	{
		$Comments = $this->Comment->Chain(["users" => "user_id"])
		->Chain(["article_comment_claps" => ["comment_id", "article_comment_claps.user_id" => $_SESSION[__SIGNIN__]["userid"]]], Schema::Left)
			->Remember(["article_id" => $this->ArticleForm->ArticleID, Schema::IsNull("reply_to")])
			->Limit([$this->ArticleForm->LimitRecord, $this->ArticleForm->OffsetRecord])
			->Get(["users.user_id", "article_id", "clap_user_id" => "article_comment_claps.user_id", "firstname", "lastname", "email", "image_local", "article_comments.comment_id", "comment", "claps", "reply_to", "reply_counter", "article_comments.created_at"]);

		return $Comments;
	}

	public function GetReplyComments ()
	{
		$Comments = $this->Comment->Chain(["users" => "user_id"])
		->Chain(["article_comment_claps" => ["comment_id", "article_comment_claps.user_id" => $_SESSION[__SIGNIN__]["userid"]]], Schema::Left)
			->Remember(["article_id" => $this->ArticleForm->ArticleID, "article_comments.reply_to" => $this->ArticleForm->CommentID])
			->Get(["users.user_id", "article_id", "clap_user_id" => "article_comment_claps.user_id", "firstname", "lastname", "email", "image_local", "article_comments.comment_id", "comment", "claps", "reply_to", "reply_counter", "article_comments.created_at"]);

		return $Comments;
	}

	public function InsertComment()
	{
		$Form = $this->ArticleForm;

		$Data = ["user_id" => $_SESSION[__SIGNIN__]["userid"], "article_id" => $Form->ArticleID, "comment" => $Form->Comment];
		
		if ($Form->ReplyID)
		{
			$Exist = $this->Comment->Count(["article_id" => $Form->ArticleID, "comment_id" => $Form->ReplyID, Schema::IsNull("reply_to")]);

			if ($Exist["Total"] == 1)
			{
				$Data["reply_to"] = $Form->ReplyID;

				$Counter = $this->Comment->Remember(["comment_id" => $Form->ReplyID], true)->Get("reply_counter");

				$this->Comment->Change(["reply_counter" => $Counter["reply_counter"] + 1], ["comment_id" => $Form->ReplyID]);

			}
		}

		$this->Comment->Write($Data);

		return $this->Comment->Remember(["user_id" => $_SESSION[__SIGNIN__]["userid"], "article_id" => $Form->ArticleID], true)->Limit(1)->Get("comment_id");
	}

	public function DeleteComment ()
	{
		$Form = $this->ArticleForm;

		$Data = ["user_id" => $_SESSION[__SIGNIN__]["userid"], "article_id" => $Form->ArticleID, "comment_id" => $Form->CommentID];

		$Exists = $this->Comment->Count($Data);

		if ($Exists["Total"] > 0)
		{
			$Reply = $this->Comment->Remember($Data, true)->Get("reply_to");

			if ($Reply["reply_to"])
			{
				$DataReply = ["comment_id" => $Reply["reply_to"]];
				
				$Counter = $this->Comment->Remember($DataReply, true)->Get("reply_counter");

				$this->Comment->Change(["reply_counter" => $Counter["reply_counter"] + 1], $DataReply);
			}
				

			$this->Comment->Forget($Data);
		}

		return $Exists;
	}
}

?>