<div class="ge-top-32">
	<div class="ge-width-auto">
		<div>
			<?php $Index = 1; ?>
			<div class="ge-ver">
				<ul>
				<?php foreach ($this->Questions as $QuestionID => $Question): ?>
					<li class="ge-bottom-32">
						<div>
							<div class="ge-question">
								<div class="ge-inline"><span>Question</span> <span><?= $Index ?> </span></div>
								<div class="ge-inline ge-full-width">
									<?= $Question["question"] ?>
								</div>
							</div>
							<div class="ge-top-16">
								<div class="ge-row">
									<input type="hidden" ge-chart-data="true" value='<?= json_encode($Question["answers"]) ?>' name="">
									<div class="ge-col l7 ge-right-24">
										<canvas></canvas>
									</div>
									<div class="ge-col l5">
										<canvas></canvas>
									</div>
								</div>
							</div>
						</div>
					</li>
					<?php $Index++; ?>
				<?php endforeach ?>
				</ul>
			</div>
		</div>
	</div>
</div>