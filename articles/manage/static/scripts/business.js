/*
* @Author: Rot
* @Date:   2017-12-10 00:34:20
* @Last Modified by:   Rot
* @Last Modified time: 2017-12-10 00:57:37
*/

(function () {

class Business 
{
	constructor ()
	{
		document.querySelectorAll(`[contenteditable="true"]`).forEach(function (Editor) {

			if (Editor.textContent.trim() == 0)
			{
				Editor.style.display = "none";
			}

			Editor.nextElementSibling.style.display = "none";

			Editor.removeAttribute("contenteditable");

		})

		document.querySelectorAll("[ge-figure]").forEach(function (Figure) {

			Figure.parentElement.classList.remove("ge-inline");

			Figure.removeAttribute("style");

		})
	}
}

System.Business = Business;

})();