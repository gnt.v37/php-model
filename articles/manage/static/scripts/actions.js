/*
* @Author: Rot
* @Date:   2017-11-30 01:53:15
* @Last Modified by:   Rot
* @Last Modified time: 2017-12-15 17:16:29
*/
(function () {

class Manage 
{
	constructor ()
	{
		let Business = new System.Business;
		
		this.Chart();
	}

	Chart ()
	{
		let Node = document.createElement("div");

		let GeCharts = document.querySelectorAll("[ge-chart-data]");

		let DataCharts = [];

		GeCharts.forEach(function (GeChart) {
			DataCharts.push(GeChart);
		})

		GeCharts = null;

		DataCharts.reduce( function(Sequence, DataChart) {

			return Sequence.then(function () {

				return new Promise(function (Resolve, Reject) {

					let Json = JSON.parse(DataChart.value);

					let Answers = [];

					let Correct = [];

					let Ticked = [];

					let Content = "";

					let TotalTicked = 0;

					let CorrectTicked = 0;

					for (let Offset in Json)
					{
						var Datas = Json[Offset];

						Node.innerHTML = Datas.answer;

						let Children = Node.children;

						for (let Child of Children)
						{
							if (Child.hasAttribute("ge-iframe"))
							{
								Content = "HTML[IFrame Content]";

								break;
							}
							else if (Child.hasAttribute("ge-figure"))
							{
								Content = "HTML[Image Content]";

								break;
							}
							else
							{
								Content = Node.querySelector("[answer-content]").textContent;
							}
						}

						

						Answers.push(Content.substring(0, 12));

						if (Datas.correct == 1)
						{
							CorrectTicked += Datas.ticked;

							Correct.push(Datas.ticked);
							Ticked.push(0);
						}
						else
						{
							Ticked.push(Datas.ticked);
							Correct.push(0);
						}

						TotalTicked += Datas.ticked;
					}

					let TickedCanvas = DataChart.nextElementSibling;

					let CorrectCanvas = TickedCanvas.nextElementSibling;

					new Chart(TickedCanvas.firstElementChild.getContext("2d"), {
						type: "bar",
						data: 
						{
							labels: Answers,
							datasets: 
							[
								{
									backgroundColor: "rgb(255, 99, 132)",
									data: Ticked
								},
								{
									backgroundColor: "rgb(54, 162, 235)",
									data: Correct
								}
							]
						},
						options:
						{
							title: 
							{
								display: true,
								padding: 24,
								text: "Ticked Answers Chart"
							},
							scales: 
							{
								xAxes: 
								[
									{
										stacked: true,
										display: true,
										scaleLabel: 
										{
											display: true,
											labelString: "Answers"
										}
									}
								],
								yAxes: 
								[
									{
										stacked: true,
										display: true,
										scaleLabel: 
										{
											display: true,
											labelString: "Ticked"
										}
									}
								]

							},
							legend: 
							{ 
								position: "bottom", 
								display: false
							}

						}
					})

					new Chart(CorrectCanvas.firstElementChild.getContext("2d"), {
						type: "pie",
						data: 
						{
							datasets: 
							[
								{
									backgroundColor: ["rgb(54, 162, 235)", "rgb(255, 99, 132)"],
									data: [CorrectTicked, TotalTicked - CorrectTicked]
								}
							],
							labels: ["Correct", "Incorrect"],
						},
						options:
						{
							responsive: true,
							title: 
							{
								display: true,
								padding: 24,
								text: "Ticked Correct Answers Chart"
							},
							legend: 
							{ 
								display: true,
								position: "left", 
								labels: 
								{
									padding: 24,
								}
							}

						}
					})

					setTimeout(function () {
						Resolve();
					}, 1024)
				})
				
			})
				

		}, Promise.resolve());
	}
}

System.Manage = Manage;

})();