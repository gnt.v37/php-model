<?php

/**
 * @Author: Rot
 * @Date:   2017-11-30 00:28:41
 * @Last Modified by:   Rot
 * @Last Modified time: 2017-11-30 01:44:20
 */

namespace System\Deeps;

class ManageDeep
{
	public function GetQuestion ()
	{
		$Questions = $this->ManageArticle->Remember([
			"article_id" => $this->ManageForm->ArticleID, 
			"user_id" => $this->ManageForm->UserID
		])->Get();

		return self::ConvertQuestion($Questions);
	}

	public function GetArticle ()
	{
		return $this->Article->Remember(["article_id" => $this->ManageForm->ArticleID], true)->Get(["user_id", "article_id"]);
	}

	private function ConvertQuestion ($Questions)
	{
		$Result = array();

		foreach ($Questions as $Question) 
		{
			$QuestionID = $Question["question_id"];

			$TempAnswer = array(
				"answer" => $Question["answer"], 
				"correct" => $Question["correct"],
			);

			if ($Question["ticked"])
			{
				$TempAnswer["ticked"] = 1;
			}
			else
			{
				$TempAnswer["ticked"] = 0;
			}

			$Answer = array (
				$Question["answer_id"] 	=> $TempAnswer
			);

			if (array_key_exists($QuestionID, $Result))
			{
				$AnswerID = $Question["answer_id"];

				if (isset($Result[$QuestionID]["answers"][$AnswerID]) && $Result[$QuestionID]["answers"][$AnswerID] == $Answer[$AnswerID])
				{
					$Result[$QuestionID]["answers"][$AnswerID]["ticked"] += 1;
				}
				else
				{
					$Result[$QuestionID]["answers"] += $Answer;
				}
			}
			else
			{
				$Result[$QuestionID] = array(
						"title" => $Question["title"],
						"question" => $Question["question"],
						"multi_choice" => $Question["multi_choice"]
					);

				$Result[$QuestionID]["answers"] = $Answer;
			}
		}

		return $Result;
	}

}

?>
