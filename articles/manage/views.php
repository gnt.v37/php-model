<?php

/**
 * @Author: Rot
 * @Date:   2017-11-30 00:29:10
 * @Last Modified by:   Rot
 * @Last Modified time: 2017-12-17 11:53:08
 */

namespace System\Views;

import(geye.views.generic);

class ManageView extends GenericView
{
	protected $Scripts = [assets.scripts.modules => "mathjax/MathJax.js?config=AM_HTMLorMML-full", summaries.scripts => "chart.bundle.min.js"];
	
	protected function Resolver ()
	{
		$this->Questions = $this->Deeps->GetQuestion();

		$this->Article = $this->Deeps->GetArticle();
	}
}

?>