<?php

/**
 * @Author: Rot
 * @Date:   2017-10-21 12:59:59
 * @Last Modified by:   Rot
 * @Last Modified time: 2017-12-14 00:54:27
 */

import(articles.views, false);

define("scores", "scores/");

$Urls = [
	"/^&v=(?<ArticleID>\d+)$/" 						=> views.ArticleView,
	"/^\/clap_article&v=(?<ArticleID>\d+)$/" 		=> views.ClapArticleObject,
	"/^\/clap_comment&v=(?<ArticleID>\d+)$/" 		=> views.ClapCommentObject,
	"/^\/delete_comment&v=(?<ArticleID>\d+)$/" 		=> views.DeleteCommentObject,
	"/^\/comment&v=(?<ArticleID>\d+)$/" 			=> views.CommentObject,
	"/^\/get_comment&v=(?<ArticleID>\d+)$/" 		=> views.CommentView,
	"/^\/get_reply_comment&v=(?<ArticleID>\d+)$/" 	=> views.ReplyCommentView,
	"/^\/score\//"									=> scores.urls,
	"/^\/manage/"									=> manage.urls
]


?>