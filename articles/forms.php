<?php

namespace System\Forms;

import (geye.forms);

class ArticleForm extends Form
{
	public $Attributes = ["UserID", "ArticleID", "Comment", "ReplyID", "CommentID"];

	public $LimitRecord = 2;

	public $OffsetRecord = 0;

}

?>