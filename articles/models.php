<?php
namespace System\Models;

import(geye.models);


class Article extends Schema
{
	protected $Nerve = "articles";

}

class UserFollow extends Schema
{
	protected $Nerve = "user_follows";

	protected $Prime = "following";

}

class ArticleTag extends Schema 
{
	protected $Nerve = "article_tags";
}

class ArticleClaps extends Schema
{
	protected $Nerve = "article_claps";
}

class ArticleCommentClaps extends Schema 
{
	protected $Nerve = "article_comment_claps";
}

class ArticleDetail extends Procedure implements Nullable 
{
	protected $Name = "GArticleDetail";

	public $UserID;

	public $ArticleID;

	public static function Nullable ()
	{
		return "UserID";
	}
}


class SuggestArticle extends Procedure implements Nullable  
{
	protected $Name = "GSuggestArticle";

	public $UserID;

	public $ArticleID;

	public static function Nullable ()
	{
		return "UserID";
	}
}

class Comment extends Schema
{
	protected $Nerve = "article_comments";

	protected $Fields = [
		"comment_id", "article_id", "user_id",
		"comment", "claps", "reply_to", "created_at"
	];

	public function Tree ($Comments)
	{
		$Node = [];
		
		foreach ($Comments as $Comment)
		{
			$Data = array (
				"article_id" => $Comment["article_id"],
				"user_id" => $Comment["user_id"],
				"clap_user_id" => $Comment["clap_user_id"],
				"comment_id" => $Comment["comment_id"],
				"firstname" => $Comment["firstname"],
				"lastname" => $Comment["lastname"],
				"email" => $Comment["email"],
				"image_local" => $Comment["image_local"],
				"comment" => $Comment["comment"],
				"claps" => $Comment["claps"],
				"created_at" => $Comment["created_at"]
			);

			$Reply = $Comment["reply_to"];

			if ($Reply == "")
			{
				$CommentID = $Comment["comment_id"];

				// Because $Fetch node is desc, if $Node have reply we use += else we need only =;

				if (empty($Node[$CommentID]))
				{
					$Node[$CommentID] = $Data;
				}
				else
				{
					$Node[$CommentID] += $Data;
				}
			}
			else
			{
				if (empty($Node[$Reply]["reply_to"]))
				{
					$Node[$Reply]["reply_to"] = array($Data);
				}
				else
				{
					$Length = count($Node[$Reply]["reply_to"]);
					$Node[$Reply]["reply_to"] += array($Length => $Data);
				}
			}
		}
		return $Node;
	}

}
?>