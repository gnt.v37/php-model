<?php foreach ($this->Scores as $Index => $Score): ?>
<li class="ge-col ge-border-bottom ge-score"><div class="ge-score-space"><div class="ge-row"><div class="ge-left">
	<ge-user><div class="ge-inline">
		<a href="<?= page."@{$Score["score_user_id"]}"; ?>" prevent="true"><div class="ge-inline ge-relative">
		<?php if ($Score["image_local"]): ?>
			<div><img class="ge-user-avatar" ge-user-avatar="true" data-src="<?= page.users.images."40/{$Score["image_local"]}" ?>" data-name="<?= $Score["lastname"] ?>"></div>
		<?php else: ?>
			<div><img class="ge-user-avatar" ge-user-avatar="true" data-src="" data-name="<?= $Score["lastname"] ? $Score["lastname"] : $Score["email"] ?>" ></div>
		<?php endif ?>
		<div class="ge-avatar-radius"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 70 70">
			<path d="M5.53538374,19.9430227 C11.180401,8.78497536 22.6271155,1.6 35.3571429,1.6 C48.0871702,1.6 59.5338847,8.78497536 65.178902,19.9430227 L66.2496695,19.401306 C60.4023065,7.84329843 48.5440457,0.4 35.3571429,0.4 C22.17024,0.4 10.3119792,7.84329843 4.46461626,19.401306 L5.53538374,19.9430227 Z"></path><path d="M65.178902,49.9077131 C59.5338847,61.0657604 48.0871702,68.2507358 35.3571429,68.2507358 C22.6271155,68.2507358 11.180401,61.0657604 5.53538374,49.9077131 L4.46461626,50.4494298 C10.3119792,62.0074373 22.17024,69.4507358 35.3571429,69.4507358 C48.5440457,69.4507358 60.4023065,62.0074373 66.2496695,50.4494298 L65.178902,49.9077131 Z">
		</path></svg></div></div></a></div>
		<div class="ge-inline ge-left-12"><div><a href="<?= page."@{$Score["score_user_id"]}"; ?>" prevent="true" class="ge-color">
		<?= $Score["lastname"] || $Score["firstname"] ? "{$Score["lastname"]} {$Score["firstname"]}" : $Score["email"]; ?></a></div>
		<div class="ge-time"><time datetime="<?= $Score["created_at"] ?>"><?= $Score["created_at"] ?></time></div></div>
	</ge-user></div><div class="ge-right">
	<div><div class="ge-score-text ge-flex-cc"><span class="">Score</span> <span><?= $Score["score"] ?></span></div>
</div></div></div></div></li>
<?php endforeach ?>