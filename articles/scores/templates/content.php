<div class="ge-top-32"><div class="ge-width-auto"><div>
	<div>
		<ul class="ge-row ge-action-space ge-border-bottom">
			<li class="ge-col l3"><div data="times" class="ge-relative ge-paragraph" >
				<div contenteditable="true" role="textbox" class="ge-relative" ge-times="true"></div></div></li>
			<li class="ge-col l6"><div data="name" class="ge-relative ge-paragraph">
				<div contenteditable="true" role="textbox" class="ge-relative  ge-center" ge-info="true"></div></div></li>
			<li class="ge-col l3"><div class="ge-right-align">
				<input type="file" class="ge-hide"><button class="ge-ver-6 ge-text-color" ge-export="true">Export</button></li>
		</ul>
	</div>
	<div class="">
		<ul class="ge-row ge-scores">
			<?php include_once "scores.php" ?>
	</ul></div>
</div></div></div>