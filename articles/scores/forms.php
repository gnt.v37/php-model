<?php

/**
 * @Author: Rot
 * @Date:   2017-11-29 21:04:26
 * @Last Modified by:   Rot
 * @Last Modified time: 2017-12-14 19:36:14
 */

namespace System\Forms;

import (geye.forms);

class ScoreForm extends Form
{
	public $Attributes = ["UserID", "ArticleID"];

	public $Times = 1;

	public $Info;

	public $LimitRecord = 30;

	public $OffsetRecord = 0;
}

?>