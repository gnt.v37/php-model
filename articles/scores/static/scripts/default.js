/*
* @Author: Rot
* @Date:   2017-11-29 23:28:48
* @Last Modified by:   Rot
* @Last Modified time: 2017-12-15 00:37:20
*/

(function () {

let Active;

if (location.href.match(/^.*?\/score\/@/))
{
	Active = document.querySelector(".ge-scu");
	
	document.querySelector(".ge-action-space").remove();
	
}
else
{
	Active = document.querySelector(".ge-sct");
}

Active.parentElement.querySelector(".ge-active").classList.remove("ge-active");

Active.classList.add("ge-active");


window.onload = function ()
{
	new System.Score();
}

})();