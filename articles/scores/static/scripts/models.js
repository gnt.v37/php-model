/*
* @Author: Rot
* @Date:   2017-12-14 14:51:46
* @Last Modified by:   Rot
* @Last Modified time: 2017-12-14 22:58:27
*/

(function () {

class Models 
{
	constructor ()
	{
		this.Page = location.origin + location.pathname;
	}

	SearchScore (Data)
	{
		return System.post(`${this.Page}/search${location.search}`, Data)
	}

	ExportScore (Data)
	{
		let Times = "";

		let Info = "";

		if (Data.has("Info"))
		{
			Info = "&Info=" + Data.get("Info");
		}

		if (Data.has("Times"))
		{
			Times = "&Times=" +Data.get("Times")
		}

		window.open(`${this.Page}/export_score${location.search}${Times}${Info}`);

		return new Promise(function (Resolve, Reject)  {

			setTimeout(function () {

				Resolve();
				
			}, 1024)

		})
	}

}

System.Models = Models;

})();