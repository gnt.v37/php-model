/*
* @Author: Rot
* @Date:   2017-12-14 14:51:30
* @Last Modified by:   Rot
* @Last Modified time: 2017-12-15 00:36:48
*/

(function () {

class Score 
{
	constructor ()
	{
		let Business = new System.Business();

		let Container = document.querySelector(".ge-scores");

		document.querySelectorAll(`[contenteditable="true"]`).forEach( (Editor) => {

			Business.Writer.Placeholder(Editor);

			if (Editor.hasAttribute("ge-times"))
			{
				this.SearchByTimes(Business, Container, Editor);
			}
			else
			{
				this.SearchByInfo(Business, Container, Editor);
			}

		})

		this.ExportScore(Business, Container);
	}

	SearchByTimes (Business, Container, Element)
	{
		Element.addEventListener("keydown", function (e) {

			if (e.key.match(/^\d+$/) == null && e.which != 8)
			{
				e.preventDefault();
			}
		})

		Element.addEventListener("keyup", function (e) {

			if (e.key.match(/^\d+$/) || e.which == 8)
			{
				System.Process = System.Process.then(function () {

					return new Promise(function (Resolve, Reject) {

						Business.SearchScore(Container, Element).then(function () {

							Resolve();

						});

					})
				})
			}
		
		})
	}

	SearchByInfo (Business, Container, Element)
	{
		Element.addEventListener("keyup", function () {

			System.Process = System.Process.then(function () {

				return new Promise(function (Resolve, Reject) {

					Business.SearchScore(Container, Element, false).then(function () {

						Resolve();

					});

				})
			})
			
		})
	}

	ExportScore (Business, Container)
	{
		let Export = document.querySelector("[ge-export]");

		if (Export)
		{
			Export.addEventListener("click", function () {

				System.Process = System.Process.then(function () {

					return new Promise(function (Resolve, Reject) {

						Business.ExportScore(Container).then(function () {

							Resolve();

						});

					})
				})
				
			})
		}
			
	}
	
}

System.Score = Score;

})();