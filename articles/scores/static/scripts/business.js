/*
* @Author: Rot
* @Date:   2017-12-14 14:51:38
* @Last Modified by:   Rot
* @Last Modified time: 2017-12-15 00:36:12
*/

(function () {

class BaseBusiness
{
	constructor ()
	{
		this.Models = new System.Models();

		this.SearchData = new FormData();
	}

	SearchScore (Container, Element, IsTimes = true)
	{
		this.Writer.Placeholder(Element);

		if (IsTimes)
		{
			let Times = parseInt(Element.textContent);

			if (!Times)
			{
				Times = 1;
			}

			this.SearchData.set("Times", Times);
		}
		else
		{
			let Info = Element.textContent.trim();

			this.SearchData.set("Info", Info);
		}

		return this.Models.SearchScore(this.SearchData).then(function (Response) {

			Container.innerHTML = Response;

			System.load(Container);

		});
	}

	ExportScore ()
	{
		return this.Models.ExportScore(this.SearchData);
	}
}

class Business extends BaseBusiness
{
	constructor ()
	{
		super();

		if (System.Writer)
		{
			this.Writer = new System.Writer({
				Placeholder: {
					times: "Write the times to find score",
					name: "Write the email, first name, last name, score"
				}
			})
		}
			
	}

}

System.Business = Business;

})();