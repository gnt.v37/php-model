<?php

/**
 * @Author: Rot
 * @Date:   2017-11-29 20:37:35
 * @Last Modified by:   Rot
 * @Last Modified time: 2017-12-14 21:01:47
 */

namespace System\Views;

import(geye.views.generic);

class TopScoreView extends GenericView
{
	protected $Models = ["Practice", "Article"];

	protected $Scripts = [users.writes.scripts => "writer.js"];

	protected function Resolver ()
	{
		$this->Scores = $this->Deeps->Scores();

		$this->Article = $this->Deeps->GetArticle();
	}
}

class TopSearchScoreView extends GenericView
{
	protected $Models = ["Practice", "Article"];

	protected $Templates = "scores.php";

	protected function Resolver ()
	{
		$this->Scores = $this->Deeps->Scores();
	}
}

class ExportScoreObject extends ObjectView
{
	protected $Models = ["Practice", "Article"];

	protected function Resolver ()
	{
		$this->Deeps->ExportScore();
	}
}

class UserScoreView extends GenericView
{
	protected $Models = ["Practice", "Article"];

	protected function Resolver ()
	{
		$this->Scores = $this->Deeps->Scores(true);

		$this->Article = $this->Deeps->GetArticle();
	}
}

?>