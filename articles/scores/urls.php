<?php

/**
 * @Author: Rot
 * @Date:   2017-11-29 20:48:50
 * @Last Modified by:   Rot
 * @Last Modified time: 2017-12-14 21:00:54
 */

import(articles.scores.views, false);

$Urls = [
	"/^top&v=(?<ArticleID>\d+)/" 				=> views.TopScoreView,
	"/^top\/search&v=(?<ArticleID>\d+)/" 		=> views.TopSearchScoreView,
	"/^top\/export_score&v=(?<ArticleID>\d+)/" 	=> views.ExportScoreObject,
	"/^@(?<UserID>\d+)&v=(?<ArticleID>\d+)/"	=> views.UserScoreView
]

?>