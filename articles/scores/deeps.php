<?php

/**
 * @Author: Rot
 * @Date:   2017-11-29 20:37:26
 * @Last Modified by:   Rot
 * @Last Modified time: 2017-12-25 19:44:14
 */

namespace System\Deeps;

use System\Models\Schema;

class ScoreDeep
{
	public function Scores ($IsUser = false)
	{
		if ($IsUser)
		{
			$Constrains = [
				"article_id" => $this->ScoreForm->ArticleID,
				"users.user_id" => $this->ScoreForm->UserID,
				Schema::And(["status" => 1]),
			];

			$Sort = ["times" => Schema::Decrease];
		}
		else
		{
			$Constrains = [
				"article_id" => $this->ScoreForm->ArticleID,
				Schema::And(["times" => $this->ScoreForm->Times, "status" => 1]),
				Schema::Or(Schema::Contains([
					"firstname" => $this->ScoreForm->Info, 
					"lastname" => $this->ScoreForm->Info, 
					"email" => $this->ScoreForm->Info, 
					"score" => $this->ScoreForm->Info
				]))
			];

			$Sort = ["score" => Schema::Decrease];
		}
		return $this->Practice->Chain(["users" => "user_id"])
		->Remember($Constrains)->Sort($Sort)
			->Limit([$this->ScoreForm->LimitRecord, $this->ScoreForm->OffsetRecord])
				->Get(["score_user_id" => "users.user_id", "firstname", "lastname", "email", "image_local", "score", "practices.created_at"]);
	}

	public function ExportScore ()
	{
		import(geye.modules.msoffice);

		$Scores = self::Scores();

		$Article = $this->Article->Remember(["article_id" => $this->ScoreForm->ArticleID], true)->Get(["title"]);

		$FileName = "{$Article["title"]} " . md5(mktime());

		$FileName = str_replace(" ", "_", $FileName);

		$Excel = new \PHPExcel();

		$Excel->setActiveSheetIndex(0);

		$Index = 5;

		$Excel->getActiveSheet()
				->setCellValue("A2", $Article["title"])
				->setCellValue("A4", "Index")
				->setCellValue("B4", "Given Name")
				->setCellValue("C4", "Family Name")
				->setCellValue("D4", "Email")
				->setCellValue("E4", "Score");

		$Excel->getActiveSheet()->getStyle("A4:E4")->applyFromArray(
			array("alignment" => array("horizontal" => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER))
		);

		$Excel->getActiveSheet()->getStyle("A4:E4")->applyFromArray(
			array("font" => array("bold" => "true"))
		);

		$Excel->getActiveSheet()->mergeCells("A2:E2");

		$Excel->getActiveSheet()->getStyle("A2:E2")->applyFromArray(
			array("font" => array("size" => "24"))
		);

		$Excel->getActiveSheet()->getStyle("A2:E2")->applyFromArray(
			array("alignment" => array("horizontal" => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER))
		);

		foreach ($Scores as $Row => $Score) 
		{
			$Excel->getActiveSheet()
				->setCellValue("A".$Index, $Row + 1)
				->setCellValue("B".$Index, $Score["lastname"])
				->setCellValue("C".$Index, $Score["firstname"])
				->setCellValue("D".$Index, $Score["email"])
				->setCellValue("E".$Index, $Score["score"]);

			$Excel->getActiveSheet()->getStyle("A{$Index}:E{$Index}")->applyFromArray(
				array("alignment" => array("horizontal" => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER))
			);

			$Index++;
		}

		$Excel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
		$Excel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
		$Excel->getActiveSheet()->getColumnDimension('D')->setWidth(30);
		$Excel->getActiveSheet()->getColumnDimension('E')->setWidth(30);

		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');

		header("Content-Disposition: attachment;filename={$FileName}.xls");

		header('Cache-Control: max-age=0');

		$Writer = \PHPExcel_IOFactory::createWriter($Excel, 'Excel5');

		$Writer->save('php://output');
	}

	public function GetArticle ()
	{
		return $this->Article->Remember(["article_id" => $this->ScoreForm->ArticleID], true)->Get(["user_id", "article_id"]);
	}


}

?>