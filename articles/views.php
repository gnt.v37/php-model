<?php

/**
 * @Author: Rot
 * @Date:   2017-10-21 12:59:59
 * @Last Modified by:   Rot
 * @Last Modified time: 2017-12-14 11:49:23
 */

namespace System\Views;

import(geye.views.generic);

class ArticleView extends GenericView
{
	protected $Models = ["Article", "ArticleTag", "ArticleDetail", "SuggestArticle", "ArticleClaps", "Comment", "UserFollow"];

	protected $Styles = [users.writes.styles => "article.css"];

	protected $Scripts = [assets.scripts => ["scroll/models.js", "scroll/business.js", "scroll/actions.js"]];

	protected function Resolver ()
	{
		$this->Article = $this->Deeps->ArticleDetail[0];

		$this->SuggestArticles = $this->Deeps->SuggestArticle;

		$this->SuggestUsers = $this->Deeps->SuggestUsers();

		$this->Tags = $this->Deeps->GetTags();

		if (isset($_SESSION[__SIGNIN__]))
		{
			$this->Claps = $this->Deeps->GetClaps();

			$this->YourClap = $this->Deeps->GetYourClap();

			$this->Comments = $this->Deeps->GetComments();

		}
	}
}

class CommentView extends GenericView
{
	protected $Models = ["Comment"];

	protected $Templates = "comment.php";

	protected function Resolver ()
	{
		$this->Comments = $this->Deeps->GetComments();
	}
}

class ReplyCommentView extends GenericView
{
	protected $Models = ["Comment"];

	protected $Templates = "comment.php";

	protected function Resolver ()
	{
		$this->Comments = $this->Deeps->GetReplyComments();
	}
}

class ClapArticleObject extends ObjectView
{
	protected $Models = "ArticleClaps";

	protected function Resolver ()
	{
		$this->Deeps->ClapToArticle();
	}
}

class ClapCommentObject extends ObjectView
{
	protected $Models = "ArticleCommentClaps";

	protected function Resolver ()
	{
		$this->Deeps->ClapToComment();
	}
}

class DeleteCommentObject extends ObjectView
{
	protected $Models = "Comment";

	protected function Resolver ()
	{
		echo json_encode($this->Deeps->DeleteComment());
	}
}

class CommentObject extends ObjectView
{
	protected $Models = "Comment";

	protected function Resolver ()
	{
		echo json_encode($this->Deeps->InsertComment());
	}
}


?>