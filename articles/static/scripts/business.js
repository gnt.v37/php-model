/*
* @Author: Rot
* @Date:   2017-12-02 21:08:53
* @Last Modified by:   Rot
* @Last Modified time: 2017-12-15 17:21:31
*/

(function () {

class BaseBusiness
{
	constructor ()
	{
		this.Models = new System.Models();

		this.CommentData = new FormData();

		this.ClapCommentData = new FormData();

		this.DeleteCommentData = new FormData();

		this.ReplyCommentData = new FormData();

		this.Node = document.createElement("div");

	}

	SaveComment (Element, Reply = false, Callback)
	{
		if (Element.textContent.trim() == "")
		{
			return Promise.resolve();
		}

		var ReplyID = null;

		this.CommentData.set("Comment", Element.textContent);

		if (Reply == true)
		{
			ReplyID = Element.getAttribute("reply");

			this.CommentData.set("ReplyID", ReplyID);

		}

		return this.Models.SaveComment(this.CommentData).then( (Response) => {

			Response = Response.match(/^{.*?}/);

			let Json = JSON.parse(Response);

			this.Node.innerHTML = this.CommentSection(Json.comment_id, ReplyID, Element.textContent);

			let Comment = this.Node.firstElementChild;

			this.ResolveComment(Element, Comment);

			if (typeof Callback === "function")
			{
				Callback(Comment);
			}

			if (typeof Reply === "function")
			{
				Reply(Comment);
			}
			

		})
	}

	ShowReplyComments(Container, Element)
	{
		this.ReplyCommentData.set("CommentID", Element.getAttribute("data-comment"));

		return this.Models.ShowReplyComments(this.ReplyCommentData).then(function (Response) {

			if (Container)
			{
				Container.innerHTML = Response;

				System.load(Container);
			}
		});
	}

	SaveClapComment (ClapElement)
	{
		this.ClapCommentData.set("CommentID", ClapElement.getAttribute("data-comment"));

		return this.Models.SaveClapComment(this.ClapCommentData).then( () => {
			
			this.ClapComment(ClapElement);
			
		})
	}

	SaveClapArticle (ClapElements)
	{
		return this.Models.SaveClapArticle().then(() => {
			this.ClapArticle(ClapElements);
		});
	}


	DeleteComment (Element)
	{
		this.DeleteCommentData.set("CommentID", Element.getAttribute("data-comment"));

		return this.Models.DeleteComment(this.DeleteCommentData).then(function (Response) {

			Response = Response.match(/{.*?}/);

			let Json = JSON.parse(Response);

			if (Json.Total > 0)
			{
				var Parent = Element.parentElement;

				while (Parent && Parent.nodeName != "SECTION")
				{
					Parent = Parent.parentElement;
				}

				$(Parent).slideUp(function () {

					Parent.remove();
					
				})
			}
		})
	}

}

class Business extends BaseBusiness
{
	constructor ()
	{
		super();

		this.CommentBox = document.querySelector(".ge-comment-box");

		this.CommentContainer = document.querySelector(".ge-comment-container");

		this.CommentOptions = document.querySelector("[comment-options]");
	}

	ClapArticle (ClapElements)
	{
		ClapElements.forEach(function (ClapElement) {
								
			var Clapped = ClapElement.getAttribute("clap-data");

			var ClapNumber = ClapElement.parentElement.querySelector(".ge-clap-number span");

			if (Clapped == 1)
			{
				var Numb = parseInt(ClapNumber.innerHTML) - 1;

				if (Numb > 0)
				{
					ClapNumber.innerHTML = Numb;
				}
				else
				{
					ClapNumber.innerHTML = "";
				}

				ClapElement.setAttribute("clap-data", 0);
			}
			else
			{
				var Numb = parseInt(ClapNumber.innerHTML) + 1;


				if (Numb)
				{
					ClapNumber.innerHTML = Numb;
				}
				else
				{
					ClapNumber.innerHTML = 1;
				}

				ClapElement.setAttribute("clap-data", 1);
			}
		})
	}

	ClapComment (ClapElement)
	{
		let YourClap = ClapElement.getAttribute("data-user");

		let ClapNumber = ClapElement.nextElementSibling;

		if (YourClap == "1")
		{
			var Numb = parseInt(ClapNumber.innerHTML) - 1;

			if (Numb > 0)
			{
				ClapNumber.innerHTML = Numb;
			}
			else
			{
				ClapNumber.innerHTML = "";
			}

			ClapElement.setAttribute("data-user", "0");
		}
		else
		{
			var Numb = parseInt(ClapNumber.innerHTML) + 1;

			if (Numb)
			{
				ClapNumber.innerHTML = Numb;

			}
			else
			{
				ClapNumber.innerHTML = 1;

			}

			ClapElement.setAttribute("data-user", "1");
		}
	}

	Reply (Element)
	{
		var Container = Element.parentElement.parentElement;

		if (Container.parentElement.children.length == 1)
		{
			let Clone = this.CommentBox.cloneNode(true);

			let Editor = document.forms.comments.querySelector("[contenteditable]").cloneNode("true");

			let Description = Clone.querySelector(".ge-comment-description")

			Clone.classList.add("ge-top-16");

			Editor.classList.remove("ge-height160");

			Editor.classList.add("ge-text-box");

			Clone.style.display = "none";

			if (Editor.hasAttribute("reply") == false)
			{
				Editor.setAttribute("reply", Element.getAttribute("data-comment"));
			}

			Editor.textContent = "";

			Description.parentElement.replaceChild(Editor, Description)

			Container = Container.parentElement;

			Container.appendChild(Clone);

			Container.parentElement.nextElementSibling.classList.add("ge-left-62");

			$(Clone).slideDown();

			return Clone;
		}
		else
		{
			var Reply = Container.nextElementSibling;

			var CounterClassList = Container.parentElement.parentElement.nextElementSibling.classList;

			if (Reply.style.display == "")
			{
				CounterClassList.remove("ge-left-62");

				$(Reply).slideUp();
			}
			else
			{
				CounterClassList.add("ge-left-62");

				$(Reply).slideDown();
			}

			return Container.nextElementSibling;
		}
	}

	ResolveComment (Element, Comment)
	{
		let Section = Element.parentElement;

		let Image = this.CommentBox.querySelector("img").src;

		if (Image)
		{
			Comment.querySelector("img").src = Image;
		}
		else
		{
			$(Comment.querySelector("img")).initial();
		}
		

		if (Element.hasAttribute("reply"))
		{
			while (Section && Section.nodeName != "SECTION")
			{
				Section = Section.parentElement;
			}

			if (Section.classList.contains("ge-comment-reply") == false)
			{
				Section = Section.querySelector(".ge-comment-reply");
			}

		}
		else
		{
			Section = this.CommentContainer;
		}

		Section.insertBefore(Comment, Section.firstElementChild);

		this.CommentData.delete("ReplyID");

		setTimeout(function () {
			System.load(Comment);
		}, 256)
	}

	CommentSection (CommentID, ReplyID, Comment)
	{
		const User = document.querySelector(`[idata="userid"]`);

		const Image = User.nextElementSibling.nextElementSibling.value;

		const UserID = User.value;

		const UserName = User.nextElementSibling.value;

		const Time = new Date();

		return `<section class="ge-comment ge-border-bottom" comment="${CommentID}">
					<div class="ge-white">
						<div class="ge-padding1620">
							<ul class="" comment-child="true">
								<li class="ge-user-comment">
									<div class="ge-row"><div class="ge-left"><ge-user><div class="ge-inline">
								<a href="http://localhost/geye/@${UserID}" prevent="true">
									<div class="ge-inline ge-relative ge-image40"><img data-src="${Image}" ge-user-avatar="true" data-name="${UserName.trim()}">
										<div class="ge-avatar-radius">
											<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 70 70">
												<path d="M5.53538374,19.9430227 C11.180401,8.78497536 22.6271155,1.6 35.3571429,1.6 C48.0871702,1.6 59.5338847,8.78497536 65.178902,19.9430227 L66.2496695,19.401306 C60.4023065,7.84329843 48.5440457,0.4 35.3571429,0.4 C22.17024,0.4 10.3119792,7.84329843 4.46461626,19.401306 L5.53538374,19.9430227 Z"></path>
												<path d="M65.178902,49.9077131 C59.5338847,61.0657604 48.0871702,68.2507358 35.3571429,68.2507358 C22.6271155,68.2507358 11.180401,61.0657604 5.53538374,49.9077131 L4.46461626,50.4494298 C10.3119792,62.0074373 22.17024,69.4507358 35.3571429,69.4507358 C48.5440457,69.4507358 60.4023065,62.0074373 66.2496695,50.4494298 L65.178902,49.9077131 Z">
												</path>
											</svg>
										</div>
									</div>
								</a></div>
								<div class="ge-inline ge-left-12">
									<div><a href="http://localhost/geye/@${UserID}" prevent="true" class="ge-name">${UserName}</a></div>
									<div class="ge-time"><time datetime="${Time.getFullYear()}/${Time.getMonth() + 1}/${Time.getDate()} ${Time.getHours()}:${Time.getMinutes()}:${Time.getSeconds()}"></time></div>
								</div>
							</ge-user></div><div class="ge-right">
								<button type="" class="ge-comment-option"><span class="ge-icon"><svg width="25" height="25" viewBox="-480.5 272.5 21 21"><path d="M-463 284.6c.9 0 1.6-.7 1.6-1.6s-.7-1.6-1.6-1.6-1.6.7-1.6 1.6.7 1.6 1.6 1.6zm0 .9c-1.4 0-2.5-1.1-2.5-2.5s1.1-2.5 2.5-2.5 2.5 1.1 2.5 2.5-1.1 2.5-2.5 2.5zm-7-.9c.9 0 1.6-.7 1.6-1.6s-.7-1.6-1.6-1.6-1.6.7-1.6 1.6.7 1.6 1.6 1.6zm0 .9c-1.4 0-2.5-1.1-2.5-2.5s1.1-2.5 2.5-2.5 2.5 1.1 2.5 2.5-1.1 2.5-2.5 2.5zm-7-.9c.9 0 1.6-.7 1.6-1.6s-.7-1.6-1.6-1.6-1.6.7-1.6 1.6.7 1.6 1.6 1.6zm0 .9c-1.4 0-2.5-1.1-2.5-2.5s1.1-2.5 2.5-2.5 2.5 1.1 2.5 2.5-1.1 2.5-2.5 2.5z"></path></svg></span></button>
							</div></div>
								</li>
								<li class="ge-comment-content">
									<span>${Comment}</span>
								</li>
								<li class="ge-comment-action">
									<ul class="ge-row __pad-t-8" select="false">
										<li class="ge-left ge-right-16"><div class="ge-clap-flex">
											<button class="ge-comment-clap" data-comment="${CommentID}">
												<span class="ge-icon"><svg width="26" height="26" viewBox="0 0 33 33"><g fill-rule="evenodd"><path d="M29.58 17.1l-3.854-6.78c-.365-.543-.876-.899-1.431-.989a1.491 1.491 0 0 0-1.16.281c-.42.327-.65.736-.7 1.207v.001l3.623 6.367c2.46 4.498 1.67 8.802-2.333 12.807-.265.265-.536.505-.81.728 1.973-.222 3.474-1.286 4.45-2.263 4.166-4.165 3.875-8.6 2.215-11.36zm-4.831.82l-3.581-6.3c-.296-.439-.725-.742-1.183-.815a1.105 1.105 0 0 0-.89.213c-.647.502-.755 1.188-.33 2.098l1.825 3.858a.601.601 0 0 1-.197.747.596.596 0 0 1-.77-.067L10.178 8.21c-.508-.506-1.393-.506-1.901 0a1.335 1.335 0 0 0-.393.95c0 .36.139.698.393.95v.001l5.61 5.61a.599.599 0 1 1-.848.847l-5.606-5.606c-.001 0-.002 0-.003-.002L5.848 9.375a1.349 1.349 0 0 0-1.902 0 1.348 1.348 0 0 0 0 1.901l1.582 1.582 5.61 5.61a.6.6 0 0 1-.848.848l-5.61-5.61c-.51-.508-1.393-.508-1.9 0a1.332 1.332 0 0 0-.394.95c0 .36.139.697.393.952l2.363 2.362c.002.001.002.002.002.003l3.52 3.52a.6.6 0 0 1-.848.847l-3.522-3.523h-.001a1.336 1.336 0 0 0-.95-.393 1.345 1.345 0 0 0-.949 2.295l6.779 6.78c3.715 3.713 9.327 5.598 13.49 1.434 3.527-3.528 4.21-7.13 2.086-11.015zM11.817 7.727c.06-.328.213-.64.466-.893.64-.64 1.755-.64 2.396 0l3.232 3.232c-.82.783-1.09 1.833-.764 2.992l-5.33-5.33z"></path><path d="M13.285.48l-1.916.881 2.37 2.837z"></path><path d="M21.719 1.361L19.79.501l-.44 3.697z"></path><path d="M16.502 3.298L15.481 0h2.043z"></path></g></svg>
											</span></button>
											<span class='ge-color' is-comment='true' comment-value=''></span></div>
										</li><li class="ge-left">
											<button class="ge-reply" data-comment="${ReplyID}"><span class="ge-icon ge-color">
												<svg width="26" height="26" viewBox="0 0 26 26" class="__svgi">
													<path d="M21.27 20.058c1.89-1.826 2.754-4.17 2.754-6.674C24.024 8.21 19.67 4 14.1 4 8.53 4 4 8.21 4 13.384c0 5.175 4.53 9.385 10.1 9.385 1.007 0 2-.14 2.95-.41.285.25.592.49.918.7 1.306.87 2.716 1.31 4.19 1.31.276-.01.494-.14.6-.36a.625.625 0 0 0-.052-.65c-.61-.84-1.042-1.71-1.282-2.58a5.417 5.417 0 0 1-.154-.75zm-3.85 1.324l-.083-.28-.388.12a9.72 9.72 0 0 1-2.85.424c-4.96 0-8.99-3.706-8.99-8.262 0-4.556 4.03-8.263 8.99-8.263 4.95 0 8.77 3.71 8.77 8.27 0 2.25-.75 4.35-2.5 5.92l-.24.21v.32c0 .07 0 .19.02.37.03.29.1.6.19.92.19.7.49 1.4.89 2.08-.93-.14-1.83-.49-2.67-1.06-.34-.22-.88-.48-1.16-.74z"></path>
												</svg></span>
											</button>
										</li>
									</ul>
								</li>
							</ul>
						</div>
						<div class="ge-comment-reply"></div>
					</div>
				</section>`;
	}

	OpenCommentOptions (Element)
	{
		let Previous = Element.previousElementSibling;

		let Comment = Element.getAttribute("data-comment");

		if (Previous)
		{
			Previous.querySelector("a").setAttribute("comment", Comment);

			let Style = Previous.classList;

			if (Style.contains("ge-block"))
			{
				Style.remove("ge-block");
			}
			else
			{
				Style.add("ge-block");
			}
		}
		else
		{
			var Node = document.createElement("div");

			Node.innerHTML = `<div class="ge-shadow">
			<ul><li class="ge-comment-option"><a class="ge-comment-delete"><span></span><span>Delete</span></a></li>
			</ul></div>`;

			Node.classList.add("ge-comment-options", "ge-block")

			Element.parentElement.insertBefore(Node, Element);

			Node.querySelector("a").setAttribute("data-comment", Comment);

			return Node;
		}

		return null;
		
	}

}

System.Business = Business;

})();