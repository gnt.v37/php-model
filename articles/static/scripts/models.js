/*
* @Author: Rot
* @Date:   2017-12-02 21:43:38
* @Last Modified by:   Rot
* @Last Modified time: 2017-12-14 00:30:02
*/

(function () {

class Models
{
	constructor ()
	{
		this.Page = document.querySelector(`[idata="page"]`).value;
	}

	SaveComment (Data)
	{
		return System.post(`${this.Page}view/comment${location.search}`, Data);
	}


	ShowReplyComments (Data)
	{
		return System.post(`${this.Page}view/get_reply_comment${location.search}`, Data);
	}

	SaveClapArticle ()
	{
		return System.get(`${this.Page}view/clap_article${location.search}`);
	}

	SaveClapComment (Data)
	{
		return System.post(`${this.Page}view/clap_comment${location.search}`, Data);
	}

	DeleteComment (Data)
	{
		return System.post(`${this.Page}view/delete_comment${location.search}`, Data);

	}
}

System.Models = Models;

})();