/*
* @Author: Rot
* @Date:   2017-11-30 19:11:44
* @Last Modified by:   Rot
* @Last Modified time: 2017-12-14 14:30:30
*/

(function () {

class BaseArticle
{
	constructor ()
	{

	}

	InvokeReplyComment (Business, Reply)
	{
		let _this = this;

		Reply.addEventListener("click", function () {

			let Clone = Business.Reply(this);
					
			Clone.querySelector(".ge-send").addEventListener("click", function () {

				let Editor = Clone.querySelector("[contenteditable]");

				System.Process = System.Process.then(function () {

					return new Promise(function (Resolve, Reject) {

						Business.SaveComment(Editor, true, function (Comment) {

							_this.InvokeReplyComment(Business, Comment.querySelector(".ge-reply"));

							_this.InvokeClapComment(Business, Comment.querySelector(".ge-comment-clap"));

						}).then(function () {
								
							Resolve();

							Editor.textContent = "";

						});

					})
				})
					
					
			})

		})
	}

	InvokeClapComment (Business, Geclap)
	{
		Geclap.addEventListener("click",  function () {

			System.Process = System.Process.then(function () {

				return new Promise(function (Resolve, Reject) {

					Business.SaveClapComment(Geclap).then(function () {

						Resolve();
						
					})

				})

			})

		})
	}

	InvokeCommmentOption (Business, Element)
	{
		Element.addEventListener("click", function () {

			let TheNew = Business.OpenCommentOptions(this);

			TheNew.querySelectorAll("a").forEach(function (Action) {

				Action.addEventListener("click", function () {

					System.Process = System.Process.then(function () {

						return new Promise(function (Resolve, Reject) {

							Business.DeleteComment(Action).then(function () {

								Resolve();

							});

						})

					})

				})

			})

		})
	}
}

class Article extends BaseArticle
{
	constructor ()
	{
		super();

		let Business = new System.Business();

		this.ClapArticle(Business);

		this.ClapComment(Business);

		this.Comments(Business);

		this.ReplyComments(Business);

		this.CommentOptions(Business);

		this.ScrollComments(Business);

		this.Share();

	}

	ClapArticle (Business)
	{
		const ClapElements = document.querySelectorAll(".ge-claps");

		ClapElements.forEach(function (ClapElement) {

			ClapElement.addEventListener("click", function () {

				System.Process = System.Process.then(function () {

					return new Promise (function (Resolve, Reject) {

						Business.SaveClapArticle(ClapElements).then(function () {
							Resolve();

						});

					})

				})

			})

		})		
	}

	ClapComment (Business, Element = document)
	{
		let Geclaps = Element.querySelectorAll(".ge-comment-clap");

		Geclaps.forEach( (Geclap) => {

			this.InvokeClapComment(Business, Geclap);

		})
	}

	Comments (Business)
	{
		var Form = document.forms.comments;

		let _this = this;

		document.querySelector(".ge-comment-box").addEventListener("click", function (e) {

			if (e.target.closest("img") == null)
			{
				if (Form.style.display == "")
				{
					$(Form).slideUp();
				}
				else
				{
					$(Form).slideDown(500, function () {
						this.querySelector("[contenteditable]").focus();
					});

				}
			}
		})

		document.querySelector(".ge-comment-cancel").addEventListener("click", function () {
			$(Form).slideUp();
		})

		document.querySelector(".ge-comment-publish").addEventListener("click", function () {

			$(Form).slideUp();

			System.Process = System.Process.then(function () {

				return new Promise(function (Resolve, Reject) {

					Business.SaveComment(Form.querySelector("[contenteditable]"), function (Comment) {

						_this.InvokeReplyComment(Business, Comment.querySelector(".ge-reply"));

						_this.InvokeClapComment(Business, Comment.querySelector(".ge-comment-clap"));

					}).then(function () {
						Resolve();
					});
					

				})

			})
		})



		document.querySelectorAll(".ge-reply").forEach( (Reply) => {

			_this.InvokeReplyComment(Business, Reply);

		})
	}

	ReplyComments (Business, Element = document)
	{
		Element.querySelectorAll(".ge-open-reply-comment").forEach( function(Reply) {

			Reply.addEventListener("click", function () {

				var Container = this.parentElement.parentElement.nextElementSibling;

				if (Container.children.length > 0)
				{
					var ClassList = Container.classList;

					if (ClassList.contains("ge-hide"))
					{
						ClassList.remove("ge-hide");
					}
					else
					{
						ClassList.add("ge-hide")
					}
				}
				else
				{
					System.Process = System.Process.then(function () {

						return new Promise(function (Resolve, Reject) {

							Business.ShowReplyComments(Container, Reply).then(function () {

								Resolve();

							});

						})

					})
				}
			})

		});
	}

	CommentOptions (Business, Element = document)
	{
		Element.querySelectorAll(".ge-comment-option-button").forEach( (Option) => {

			this.InvokeCommmentOption(Business, Option);

		})					
	}

	ScrollComments (Business)
	{
		var Location = document.querySelector(`[idata="page"]`).value;
	
		let Container = document.querySelector(".ge-comment-container");

		let Scroll = new System.Scroll ({
			Models: `${Location}/view/get_comment${location.search}`,
			Offsets: 2,
			Limits: 6
		});

		Scroll.Done( (Response) => {

			var Children = Response.children;

			var Length = Children.length;

			let Child = null;

			for (var i = 0; i < Length; i++)
			{
				Child = Children[0];

				Container.appendChild(Children[0]);

				this.CommentOptions(Business, Child);

				this.ReplyComments(Business, Child);

				this.ClapComment(Business, Child);
			}



			System.load(Container);

		})
	}

	Share ()
	{
		let Share = new System.Share();

		const Href = location.href;

		document.querySelectorAll(".ge-socials").forEach(function (Socials) {

			let Children = Socials.querySelectorAll("a");

			for (let Social of Children)
			{
				Social.addEventListener("click", function () {

					switch (this.getAttribute("data")) 
					{
						case "twitter":

							Share.Twitter = Href;

							break;
						case "facebook":
							
							Share.Facebook = Href;

							break;
						case "google":

							Share.Google = Href;

							break;
						default:
							// statements_def
							break;
					}


				})
			}

		})	
	}
}

System.Article = Article;

})();