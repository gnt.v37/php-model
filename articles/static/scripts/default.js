(function () {

let Page = document.querySelector(`[idata="page"]`).value;

document.querySelectorAll("ge-mode > [ge-figure]").forEach(function (Figure) {
    Figure.removeAttribute("style");

    Figure.querySelector(`[contenteditable="true"]`).removeAttribute("contenteditable");
})

document.querySelectorAll(`[contenteditable="true"]`).forEach(function (Editor) {
    Editor.removeAttribute("contenteditable");
});

document.querySelectorAll("figure[image]").forEach( function(Figure) {
	
	Figure.querySelector("img").setAttribute("data-src", Page + Figure.getAttribute("image"));
});

window.onload = function ()
{
	new System.Article();
}

})();