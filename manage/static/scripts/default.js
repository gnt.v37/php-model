/*
* @Author: Rot
* @Date:   2017-11-13 00:35:17
* @Last Modified by:   Rot
* @Last Modified time: 2017-11-18 11:33:25
*/
(function () {

$(window).on("load", function() {
	
	let Location = location.href.match(/^.*?\/(\w+)$/);

	let HeaderOptions = document.querySelector("[ge-head-option]").children;

	switch(Location[1])
	{
		case "articles":
			HeaderOptions[1].classList.add("ge-active");
			break;
		case "practices":
			HeaderOptions[2].classList.add("ge-active");
			break;
		case "users":
			HeaderOptions[3].classList.add("ge-active");
			break;
		default:
			HeaderOptions[0].classList.add("ge-active");
			break;
	}

	new System.Action();



});

})();