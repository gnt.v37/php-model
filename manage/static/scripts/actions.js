/*
* @Author: Rot
* @Date:   2017-11-17 14:47:28
* @Last Modified by:   Rot
* @Last Modified time: 2017-11-18 14:23:25
*/
(function () {

let Process = Promise.resolve();

class Action 
{
	constructor ()
	{
		let Boxs = document.querySelectorAll("[ge-search]");

		let Deletes = document.querySelectorAll("[ge-delete]");

		let Business = new System.Business();

		let WindowHeght = window.innerHeight;
        
		Boxs.forEach(function (Box) {

			Box.addEventListener("keyup", function () {

				Process = Process.then(function () {

					return new Promise(function (Resolve, Reject) {

						Business.Search(Boxs).then(function () {
							Resolve();
						})

					})

				})

			})

		})

		Deletes.forEach(function (Delete) {

			Delete.addEventListener("dblclick", function () {

				var Element = this;

				Process = Process.then(function () {

					return new Promise(function (Resolve, Reject) {

						Business.Delete(Element).then(function () {
							Resolve();
						})

					})

				})

			})

		})

		window.onresize = function ()
		{
			WindowHeght = window.innerHeight;
		}

		window.onscroll = function ()
		{
			if (Business.Ready && Business.Exist)
			{

				Business.Scroll(WindowHeght).then(function () {

					setTimeout(function () {
						Business.Ready = true;
					}, 256)

				});
			}
		}
	}


}

System.Action = Action;

})();