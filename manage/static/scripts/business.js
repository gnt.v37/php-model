/*
* @Author: Rot
* @Date:   2017-11-17 14:47:36
* @Last Modified by:   Rot
* @Last Modified time: 2017-11-18 15:25:54
*/
(function () {

class Business 
{
	constructor ()
	{
		this.Container = document.querySelector("[ge-container]");

		this.Node = document.createElement("div");

		this.Model = new System.Model();

		this.BoxData = new FormData();

		this.DeleteBox = new FormData();

		this.LimitRecord = 15;

		this.OffsetRecord = 0;

		this.Ready = true;

		this.Exist = true;
	}

	Search (Boxs)
	{
		this.OffsetRecord = 0;

		this.Ready = true;

		Boxs.forEach( (Box) => {

			var Content = Box.value.trim();

			if (Content != "")
			{
				var Attribute = Box.getAttribute("ge-search");

				this.BoxData.set(Attribute, Content);
			}
			else
			{
				if (this.BoxData.has(Attribute))
				{
					this.BoxData.remove(Attribute);
				}
			}

		})

		this.BoxData.set("OffsetRecord", this.OffsetRecord);

		return this.Model.Search(this.BoxData).then( (Response) => {
			this.Container.innerHTML = Response;
		});
	}

	Scroll (WindowHeght)
	{
		this.Ready = false;

		var ScrollY = window.pageYOffset + WindowHeght;

		var	PageHeight = document.body.clientHeight;

		var	Limit = (PageHeight - WindowHeght);

        if (ScrollY > Limit)
		{
			this.OffsetRecord += this.LimitRecord;

			this.BoxData.set("OffsetRecord", this.OffsetRecord);

			return this.Model.Search(this.BoxData).then( (Response) => {

				this.Node.innerHTML = Response;

				var Children = this.Node.children,
					Length 	 = Children.length;

				for (var i = 0; i < Length; i++)
				{
					this.Container.appendChild(Children[0]);
				}

				if (Length < 2)
				{
					this.Exist = false;
				}

				
			});

		}
		else
		{
			return Promise.resolve(null);
		}
	}

	Delete (Element)
	{
		this.DeleteBox.set("PracticeID", Element.getAttribute("ge-data"));

		return this.Model.Delete(this.DeleteBox).then(function () {

			var Parent = Element.parentElement;

			while (Parent.classList.contains("ge-flex") == false)
			{
				Parent = Parent.parentElement;
			}

			Parent.remove();
			
		})
	}
}

System.Business = Business;

})();