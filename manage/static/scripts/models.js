/*
* @Author: Rot
* @Date:   2017-11-17 14:47:45
* @Last Modified by:   Rot
* @Last Modified time: 2017-11-18 11:44:44
*/
(function () {

class Model 
{
	constructor ()
	{
		this.Location = location.href;
	}

	Search (Data)
	{
		return System.post(this.Location + `/${new Date().getTime()}/search`, Data);
	}

	Delete (Data)
	{
		return System.post(this.Location + `/${new Date().getTime()}/delete`, Data);
	}

}

System.Model = Model;

})();