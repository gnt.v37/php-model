<?php foreach ($this->Users as $User): ?>
<div class="ge-flex">
	<div class="ge-tdata ge-flex-sc"><ge-image><div><div></div><a href="<?= page."@{$User["user_id"]}" ?>"><img src="<?= $User["image_link"] ? $User["image_link"] : $User["image_local"] ?>" alt=""></a></div></ge-image><ge-info></ge-info></div>
	<div class="ge-tdata ge-flex-sc"><a href="<?= page."@{$User["user_id"]}" ?>" class="ge-text-limit"><span><?= $User["lastname"] ?></span> <span><?= $User["firstname"] ?></span></a></div>
	<div class="ge-tdata ge-flex-sc"><?= $User["email"] ?></div>
	<div class="ge-tdata ge-flex-sc"><?= $User["address"] ?></div>
	<div class="ge-tdata ge-flex-sc"><?= $User["birthday"] ?></div>
	<div class="ge-tdata ge-flex-sc"><?= $User["created_at"] ?></div>
	<div class="ge-tdata ge-flex-sc"><?= $User["role"] == "A" ? "Administration" : "Member"?></div>
	<div class="ge-tdata ge-flex-sc"><button class="ge-delete" ge-data="<?= $User["user_id"] ?>" ge-delete="true">Delete</button></div>
</div>
<?php endforeach ?>