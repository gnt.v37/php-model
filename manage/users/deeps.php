<?php

/**
 * @Author: Rot
 * @Date:   2017-11-14 17:26:57
 * @Last Modified by:   Rot
 * @Last Modified time: 2017-11-18 02:33:12
 */

namespace System\Deeps;

use System\Models\Schema;

class UserDeep
{
	public function Users ()
	{
		$Form = $this->UserForm;

		return $this->User->Limit($Form->LimitRecord, $Form->OffsetRecord)
			->Remember([
				Schema::Contains(["email" => $Form->Email, "address" => $Form->Address, "role" => $Form->Role]), 
				Schema::Equal(["created_at" => $Form->Created]),
				Schema::Or(Schema::Contains(["firstname" => $Form->FullName, "lastname" => $Form->FullName])),
			])->Get();
	}
}

?>