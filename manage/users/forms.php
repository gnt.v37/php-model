<?php

/**
 * @Author: Rot
 * @Date:   2017-11-14 17:27:04
 * @Last Modified by:   Rot
 * @Last Modified time: 2017-11-18 02:32:27
 */

namespace System\Forms;

import (geye.forms);

class UserForm extends Form
{
	public $UserID;

	public $FullName;

	public $Email;

	public $Address;

	public $Role;

	public $Created;

	public $LimitRecord = 15;

	public $OffsetRecord = 0;

}

?>