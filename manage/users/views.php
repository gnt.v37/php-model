<?php

/**
 * @Author: Rot
 * @Date:   2017-11-14 17:27:16
 * @Last Modified by:   Rot
 * @Last Modified time: 2017-11-20 08:39:17
 */

namespace System\Views;

import(geye.views.generic);

class UserView extends GenericView
{
	protected $Scripts = [manage.scripts => ["models.js", "business.js", "actions.js", "default.js"]];

	protected $Styles  = [manage.styles => "admin.css", "user.css"];
	
	protected function Resolver ()
	{
		$this->Users = $this->Deeps->Users();
	}
} 

class UserSearchView extends GenericView
{
	protected $Templates = "users.php";

	protected function Resolver ()
	{
		$this->Users = $this->Deeps->Users();
	}
}

class UserDeleteObject extends ObjectView
{
	protected $Models = "AdminUserDelete";
}

?>