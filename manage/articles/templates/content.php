<div class="ge-width-auto">
	<div class="ge-space">
		<div class="ge-table-wrapper">
			<div class="ge-table-space">
				<div class="ge-table">
					<div>
						<div class="ge-right-30">
							<div class="ge-thead ge-row">
								<div data-sort="string" class="ge-hdata">
									<div>Title</div>
									<div><input type="search" ge-search="Title" placeholder="Write title" name=""></div>
								</div>
								<div data-sort="string" class="ge-hdata"><div>Full name</div>
									<div><input type="search" ge-search="FullName"  placeholder="Write full name" name=""></div></div>
								<div data-sort="string" class="ge-hdata"><div>Topic</div>
									<div>
										<input type="search" ge-search="Topic" placeholder="Write topic" name="">
									</div>
								</div>
								<div data-sort="string" class="ge-hdata"><div>Created At</div>
									<div><input type="search" ge-search="Created" placeholder="Write create at" ></div>
								</div>
								<div class="ge-hdata">Delete</div>
							</div>
						</div>
						<div class="ge-scroll">
							<div  class="ge-tbody" ge-scroll="true">
								<div ge-container="true">
									<?php include_once "articles.php"; ?>
								</div>
							</div>
						</div>
					</table>
				</div>
					
			</div>
		</div>
	</div>
</div>