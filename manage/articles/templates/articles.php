<?php foreach ($this->Articles as $Article): ?>
<div class="ge-flex">
	<div class="ge-tdata ge-flex-sc"><a href="<?= page."view?v={$Article["article_id"]}" ?>" class="ge-text-limit"><?= $Article["title"] ?></a></div>
	<div class="ge-tdata ge-flex-sc"><a href="<?= page."@{$Article["user_id"]}" ?>" title=""><span><?= $Article["lastname"] ?></span> <span><?= $Article["firstname"] ?></span></a></div>
	<div class="ge-tdata  ge-flex-sc"><a href="<?= page."topic/{$Article["topic_id"]}" ?>" title=""><?= $Article["topic"] ?></a></div>
	<div class="ge-tdata ge-flex-sc"><?= $Article["created_at"] ?></div>
	<div class="ge-tdata ge-flex-sc"><button class="ge-delete" ge-delete="true">Delete</button></div>
</div>	
<?php endforeach ?>