<?php

/**
 * @Author: Rot
 * @Date:   2017-11-13 15:24:09
 * @Last Modified by:   Rot
 * @Last Modified time: 2017-11-17 23:11:13
 */

namespace System\Deeps;

use System\Models\Schema;

class ArticleDeep 
{
	
	public function Articles ()
	{
		$Form = $this->ArticleForm;

		return $this->ArticleAdmin->Limit($Form->LimitRecord, $Form->OffsetRecord)
			->Remember([
				Schema::Contains(["title" => $Form->Title, "topic" => $Form->Topic]), 
				Schema::Equal(["published_at" => $Form->Published,"created_at" => $Form->Created]),
				Schema::Or(Schema::Contains(["firstname" => $Form->FullName, "lastname" => $Form->FullName])),
			])->Get();
	}
}

?>