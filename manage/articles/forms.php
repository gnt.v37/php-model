<?php

/**
 * @Author: Rot
 * @Date:   2017-11-13 15:24:28
 * @Last Modified by:   Rot
 * @Last Modified time: 2017-11-17 23:48:02
 */

namespace System\Forms;

import (geye.forms);

class ArticleForm extends Form
{
	public $UserID;

	public $Title;

	public $Topic;

	public $FullName;

	public $Published;

	public $Created;

	public $LimitRecord = 15;

	public $OffsetRecord = 0;

}


?>