<?php
namespace System\Views;

import(geye.views.generic);

class ArticleView extends GenericView
{
	protected $Styles 	= [manage.styles => "admin.css", "article.css"];

	protected $Scripts 	= [manage.scripts => ["models.js", "business.js", "actions.js", "default.js"]];

	protected $Models 	= "ArticleAdmin";

	protected function Resolver ()
	{
		$this->Articles = $this->Deeps->Articles();
	}
}

class ArticleSearchView extends GenericView
{
	protected $Templates = "articles.php";

	protected $Models = "ArticleAdmin";

	protected function Resolver ()
	{
		$this->Articles = $this->Deeps->Articles();
	}
}

?>