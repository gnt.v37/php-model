<?php

/**
 * @Author: Rot
 * @Date:   2017-11-14 17:27:10
 * @Last Modified by:   Rot
 * @Last Modified time: 2017-11-18 15:34:33
 */

namespace System\Models;

import(geye.models.schema);

class Practice extends Schema 
{
	protected $Nerve = "practices";
}

class PracticeAdmin extends Schema 
{
	protected $Nerve = "vadmin_practices";
}

?>