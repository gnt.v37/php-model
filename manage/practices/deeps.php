<?php

/**
 * @Author: Rot
 * @Date:   2017-11-14 17:26:57
 * @Last Modified by:   Rot
 * @Last Modified time: 2017-11-18 15:34:58
 */

namespace System\Deeps;

use System\Models\Schema;

class PracticeDeep
{
	public function Practices ()
	{
		$Form = $this->PracticeForm;

		return $this->PracticeAdmin->Limit($Form->LimitRecord, $Form->OffsetRecord)
			->Remember([
				Schema::Contains(["title" => $Form->Title]), 
				Schema::Equal(["start_time" => $Form->Start, "finish_time" => $Form->Finish, "minutes" => $Form->Minutes, "score" => $Form->Score, "times" => $Form->Times]),
				Schema::Or(Schema::Contains(["firstname" => $Form->FullName, "lastname" => $Form->FullName])),
			])->Get();
	}

	public function Delete ()
	{
		return $this->Practice->Forget(["practice_id" => $this->PracticeForm->PracticeID]);
	}

	
}

?>