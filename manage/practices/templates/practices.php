<?php foreach ($this->Practices as $Practices): ?>
<div class="ge-flex">
	<div class="ge-tdata ge-flex-sc"><a href="<?= page."view?v={$Practices["article_id"]}" ?>" class="ge-text-limit"><?= $Practices["title"] ?></a></div>
	<div class="ge-tdata ge-flex-sc"><a href="<?= page."@{$Practices["user_id"]}" ?>" title=""><span><?= $Practices["lastname"] ?></span> <span><?= $Practices["firstname"] ?></span></a></div>
	<div class="ge-tdata ge-flex-sc"><?= $Practices["score"] ?></div>
	<div class="ge-tdata ge-flex-sc"><?= $Practices["times"] ?></div>
	<div class="ge-tdata ge-flex-sc"><?= $Practices["finish_time"] ?></div>
	<div class="ge-tdata ge-flex-sc"><button class="ge-delete" ge-data="<?= $Practices["practice_id"] ?>" ge-delete="true">Delete</button></div>
</div>	
<?php endforeach ?>