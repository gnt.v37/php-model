<div class="ge-width-auto">
	<div class="ge-space">
		<div class="ge-table-wrapper">
			<div class="ge-table-space">
				<div class="ge-table">
					<div>
						<div class="ge-right-30">
							<div class="ge-thead ge-row">
								<div data-sort="string" class="ge-hdata">
									<div>Title</div>
									<div><input type="search" ge-search="Title" placeholder="Write title" name=""></div>
								</div>
								<div data-sort="string" class="ge-hdata"><div>Full name</div>
									<div><input type="search" ge-search="FullName"  placeholder="Write full name" name=""></div></div>
								<div data-sort="string" class="ge-hdata"><div>Score</div>
									<div><input type="search" ge-search="Score" placeholder="Write score" ></div>
								</div>
								<div data-sort="string" class="ge-hdata"><div>Times</div>
									<div><input type="search" ge-search="Times" placeholder="Write times" ></div>
								</div>
								<div class="ge-hdata"><div>Finished Time</div>
									<div><input type="search" ge-search="Finish" placeholder="Write finish time" ></div>
								</div>
								<div class="ge-hdata">Delete</div>
							</div>
						</div>
						<div class="ge-scroll">
							<div  class="ge-tbody" ge-scroll="true">
								<div ge-container="true">
									<?php include_once "practices.php"; ?>
								</div>
							</div>
						</div>
					</table>
				</div>
					
			</div>
		</div>
	</div>
</div>