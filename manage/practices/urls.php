<?php

/**
 * @Author: Rot
 * @Date:   2017-11-14 19:17:04
 * @Last Modified by:   Rot
 * @Last Modified time: 2017-11-20 08:37:22
 */

import(manage.practices.views, false);

$Urls = [
	"/^$/" 				=> views.PracticeView,
	"/^\/\d+\/search/" 	=> views.PracticeSearchView,
	"/^\/\d+\/delete/" 	=> views.DeletePracticeObject,

]

?>