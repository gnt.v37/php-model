<?php

/**
 * @Author: Rot
 * @Date:   2017-11-14 17:27:16
 * @Last Modified by:   Rot
 * @Last Modified time: 2017-11-20 08:38:59
 */

namespace System\Views;

import(geye.views.generic);

class PracticeView extends GenericView
{
	protected $Scripts = [manage.scripts => ["models.js", "business.js", "actions.js", "default.js"]];

	protected $Styles  = [manage.styles => "admin.css", "practice.css"];

	protected $Models = "PracticeAdmin";

	protected function Resolver ()
	{
		$this->Practices = $this->Deeps->Practices();
	}
} 


class PracticeSearchView extends GenericView
{
	protected $Templates = "practices.php";

	protected $Models = "PracticeAdmin";

	protected function Resolver ()
	{
		$this->Practices = $this->Deeps->Practices();
	}
}


class DeletePracticeObject extends ObjectView
{
	protected $Models = "Practice";
	
	protected function Resolver ()
	{
		$this->Deeps->Delete();
	}
}

?>