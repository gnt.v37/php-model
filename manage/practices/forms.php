<?php

/**
 * @Author: Rot
 * @Date:   2017-11-14 17:27:04
 * @Last Modified by:   Rot
 * @Last Modified time: 2017-11-18 15:18:28
 */

namespace System\Forms;

import (geye.forms);

class PracticeForm extends Form
{
	public $UserID;

	public $PracticeID;

	public $Title;

	public $FullName;

	public $Minutes;

	public $Score;

	public $Times;

	public $Start;

	public $Finish;

	public $LimitRecord = 15;

	public $OffsetRecord = 0;

}

?>