/*
* @Author: Rot
* @Date:   2017-10-22 20:17:58
* @Last Modified by:   Rot
* @Last Modified time: 2017-11-30 22:37:37
*/
(function () {


window.onload = function () 
{
	let Container = document.querySelector(".ge-container");

	let Follow = document.querySelector(".ge-follow");

	let Process = Promise.resolve();

	let Scroll = new System.Scroll ({
		Models: `${location.href}/scroll`,
		Offsets: 14,
		Limits: 14
	});

	Scroll.Done(function (Response) {

		var Children = Response.children;

		var Length = Children.length;

		for (var i = 0; i < Length; i++)
		{
			Container.appendChild(Children[0]);
		}

	})

	new System.Topic();

}

})();