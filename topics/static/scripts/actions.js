/*
* @Author: Rot
* @Date:   2017-11-30 22:28:11
* @Last Modified by:   Rot
* @Last Modified time: 2017-11-30 22:38:42
*/

(function () {

class Topic
{
	constructor ()
	{
		this.Follow();
	}

	Follow ()
	{
		let FollowElement = document.querySelector(".ge-follow");

		const Follow = location.href + "/follow";

		FollowElement.addEventListener("click", function () {

			System.Process = System.Process.then(function () {

				return new Promise(function (Resolve, Reject) {

					System.get(Follow).then(function () {

						var ClassList = FollowElement.classList;

						if (ClassList.contains("ge-following"))
						{
							ClassList.remove("ge-following");

							FollowElement.innerHTML = "Follow";
						}
						else
						{
							ClassList.add("ge-following");

							FollowElement.innerHTML = "Following";
						}

						Resolve();

					})

				})

			})

		})
	}
}


System.Topic = Topic;
})();