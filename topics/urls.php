<?php

/**
 * @Author: Rot
 * @Date:   2017-10-21 13:00:02
 * @Last Modified by:   Rot
 * @Last Modified time: 2017-12-04 09:26:33
 */

import(topics.views, false);

$Urls = [
	"/^\/(?<TopicID>\d+)$/" 			=> views.TopicView,
	"/^\/(?<TopicID>\d+)\/follow/" 		=> views.FollowObject,
	"/^\/(?<TopicID>\d+)\/scroll$/"		=> views.ScrollView,
]


?>