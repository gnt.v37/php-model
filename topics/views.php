<?php

/**
 * @Author: Rot
 * @Date:   2017-10-21 13:00:02
 * @Last Modified by:   Rot
 * @Last Modified time: 2017-12-04 09:15:17
 */

namespace System\Views;

import (geye.views.generic);

class TopicView extends GenericView
{
	protected $Scripts = [assets.scripts => ["scroll/models.js", "scroll/business.js", "scroll/actions.js"], "default.js"];
	
	protected function Resolver ()
	{
		$this->Articles = $this->Deeps->Topics;

		$this->Details = $this->Deeps->TopicDetails();
	}
}

class FollowObject extends ObjectView
{
	protected $Models = "UserTopic";

	protected function Resolver ()
	{
		$this->Deeps->Follow();
	}
}

class ScrollView extends GenericView
{
	protected $Models = ["Topics"];

	protected $Templates = "articles.php";

	protected function Resolver ()
	{
		$this->Articles = $this->Deeps->Topics;
	}
}

?>