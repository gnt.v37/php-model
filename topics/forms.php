<?php

/**
 * @Author: Rot
 * @Date:   2017-09-10 14:59:15
 * @Last Modified by:   Rot
 * @Last Modified time: 2017-11-25 15:20:19
 */
namespace System\Forms;

include_once geye.forms;

class TopicForm extends Form
{
	public $Attributes = ["UserID"];

	public $TopicID;

	public $LimitRecord = 14;

	public $OffsetRecord = 0;

}

?>