<?php foreach ($this->Articles as $Key => $Article): ?>
	
<?php if ($Key % 14 == 0): ?>
	<section class="view-mode-o __mar-b-24">
					
	<div class="ge-row">
<?php endif ?>

<ge-article>
	<div class="ge-white __card-box">
		<div class="ge-row">
			<ge-image>
				<a href="<?= page."view?v={$Article["article_id"]}" ?>" prevent="true" class="ge-scale"><div class="ge-bk-img ge-hover ge-real" data-src="<?= page.home.images."300/{$Article["image"]}" ?>"></div></a>
			</ge-image>
			
			<ge-content>
				<ul class="article-info flex-content">
					<li class="flex-mode" style="position: relative;">
						<div class="absolute-full flex-content">
							<h4 class="">
								<a href="<?= page."view?v={$Article["article_id"]}" ?>" class="article-title" ><?= $Article["title"] ?></a>
							</h4>
							<?php 
								$Height = "height-mode-z"; 
								$IsShow = true;
							?> 
							<div class="article-description <?= $Height ?>">
								<span><?php if ($IsShow) echo $Article["description"] ?></span>	
							</div>
						</div>
					</li>
					<li class="ge-top-12">
						<div class="ge-row">
							<ge-user><div class="ge-inline">
								<a href="<?= page."@{$Article["user_id"]}"; ?>" prevent="true">
									<div class="ge-inline ge-relative ge-image40">
										<?php if ($Article["user_image_local"]): ?>
											<img src="<?= page.users.images."40/{$Article["user_image_local"]}" ?>">
										<?php else: ?>
											<img data-src="" ge-user-avatar="true" data-name="<?= $Article["lastname"] || $Article["firstname"] ? "{$Article["lastname"]} {$Article["firstname"]}" : $Article["email"] ?>">
										<?php endif ?>
										<div class="ge-avatar-radius">
											<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 70 70">
												<path d="M5.53538374,19.9430227 C11.180401,8.78497536 22.6271155,1.6 35.3571429,1.6 C48.0871702,1.6 59.5338847,8.78497536 65.178902,19.9430227 L66.2496695,19.401306 C60.4023065,7.84329843 48.5440457,0.4 35.3571429,0.4 C22.17024,0.4 10.3119792,7.84329843 4.46461626,19.401306 L5.53538374,19.9430227 Z"></path>
												<path d="M65.178902,49.9077131 C59.5338847,61.0657604 48.0871702,68.2507358 35.3571429,68.2507358 C22.6271155,68.2507358 11.180401,61.0657604 5.53538374,49.9077131 L4.46461626,50.4494298 C10.3119792,62.0074373 22.17024,69.4507358 35.3571429,69.4507358 C48.5440457,69.4507358 60.4023065,62.0074373 66.2496695,50.4494298 L65.178902,49.9077131 Z">
												</path>
											</svg>
										</div>
									</div>
								</a></div>
								<div class="ge-inline ge-left-12">
									<div><a href="<?= page."@{$Article["user_id"]}"; ?>" prevent="true" class="ge-name"><?= "{$Article["lastname"]} {$Article["firstname"]}"; ?></a></div>
									<div class="ge-time"><time datetime="<?= $Article["created_at"] ?>"></time></div>
								</div>
							</ge-user>
							
						</div>
					</li>
				</ul>
			</ge-content>
		</div>
		
	</div>
	
</ge-article>

<?php if ($Key + 1 % 14 == 0): ?>
	</div></section>
<?php endif ?>

<?php endforeach ?>




