<div class="ge-home-top">
	<div class="ge-width-auto">
		<div class="ge-topics-space">
			<div class="ge-bottom-16">
				<div>
					<?php if (isset($_SESSION[__SIGNIN__])): ?>
						<div class="ge-bottom-5">
							<span class="ge-text-color">Handpicked by Medium Staff</span>
						</div>
					<?php endif ?>
					<div class="ge-bottom-5">
						<div class="ge-row"><div class="ge-left"><h2><?= $this->Details[0]["topic"] ?></h2></div>
						<div class="ge-right">
							<?php if (isset($_SESSION[__SIGNIN__])): ?>
								<?php if ($this->Details[1]["Total"]): ?>
									<a class="ge-follow ge-following">Following</a>
								<?php else: ?>
									<a class="ge-follow">Follow</a>
								<?php endif ?>
								
							<?php endif ?>
						</div></div>
					</div>
				</div>
			</div>
			<div>
				<ul class="ge-tab-line">
					<li class="ge-inline ge-right-20 ge-topics ge-text-color">Related topics</li>
				<?php foreach ($this->Details[2] as $Value): ?>
					<li class="ge-inline"><a href="<?= page."topic/{$Value["topic_id"]}" ?>" class="ge-topics"><?= $Value["topic"] ?></a><span class="ge-right-5">, </span></li>
				<?php endforeach ?>
				</ul>
				
			</div>
		</div>
		<div class="ge-container">
			<?php include_once $App.templates."articles.php" ?>
		</div>
	</div>
		
</div>