<?php

/**
 * @Author: Rot
 * @Date:   2017-11-25 09:53:36
 * @Last Modified by:   Rot
 * @Last Modified time: 2017-11-25 15:12:37
 */

namespace System\Deeps;

use System\Models\Schema;

class TopicDeep
{
	public function TopicDetails ()
	{
		$Detail = $this->Topic->Remember(["topic_id" => $this->TopicForm->TopicID], true)->Get("topic");

		$Related = $this->Topic->Remember(
			["topic_id" => Schema::NotIn(["topic_id" => $this->TopicForm->TopicID])]
		)->Sort(["topic_id" => Schema::Random])->Limit(6)->Get(["topic_id", "topic"]);

		$Follow = $this->UserTopic->Count(["user_id" => $this->TopicForm->UserID, "topic_id" => $this->TopicForm->TopicID]);

		return [$Detail, $Follow, $Related];
	}

	public function Follow ()
	{
		$Data = ["user_id" => $this->TopicForm->UserID, "topic_id" => $this->TopicForm->TopicID];
		
		$Follow = $this->UserTopic->Count($Data);

		if ($Follow["Total"])
		{
			$this->UserTopic->Forget($Data);
		}
		else
		{
			$this->UserTopic->Write($Data);
		}
		
	}
}


?>