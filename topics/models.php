<?php

/**
 * @Author: Rot
 * @Date:   2017-10-21 13:00:02
 * @Last Modified by:   Rot
 * @Last Modified time: 2017-12-04 09:14:56
 */

namespace System\Models;

import(geye.models);

class Topics extends Procedure implements Nullable
{
	protected $Name = "GTopicArticles";

	public $UserID;

	public $TopicID;

	public $LimitRecord;

	public $OffsetRecord;

	public static function Nullable ()
	{
		return "UserID";
	}
}

class Topic extends Schema 
{
	protected $Nerve = "topics";
}

class UserTopic extends Schema 
{
	protected $Nerve = "user_topics";
}

?>
