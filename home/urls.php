<?php

/**
 * @Author: Rot
 * @Date:   2017-07-03 22:25:50
 * @Last Modified by:   Rot
 * @Last Modified time: 2017-12-11 23:30:24
 */

import(home.views, false);

define("auth", "auth/");


$Urls = [
	"/^$/" 				  	 	=> views.HomeView,
	"/^\/poll\&time=\d+$/"	 	=> views.PollObject,
	"/^\/schedule\&time=\d+$/"	=> views.ScheduleObject,
	"/^\/expire\&time=\d+$/"	=> views.ExpireObject,
	"/^\/notification_list$/"	=> views.NotificationsView,
	"/^\/notification_count$/"	=> views.NotificationCountObject,
	"/^\/.*?\/scroll_ajax$/" 	=> views.ScrollView,

	"/^\/auth/"					=> auth.urls,
]

?>