<?php

namespace System\Deeps;

use System\Models\Schema;

class HomeDeep 
{
	private $Datas = [];

	public function MergeModels ()
	{
		$RecommendedCounter = count($this->Recommended);
		
		if ($RecommendedCounter == 6)
		{
			$FirstStep = array_merge($this->Recommended, $this->FollowingTopics);

			return array_merge($FirstStep, $this->FollowingUsers);

		}	
		return array_merge($this->FollowingTopics, $this->FollowingUsers);
	}

	public function MergeScrollArtile ()
	{
		if (isset($_SESSION[__SIGNIN__]) == false)
		{
			self::MergeSet("Home");
		}
		else
		{
			self::MergeSet("FollowingUsers");
			self::MergeSet("FollowingTopics");
			self::MergeSet("RecommendedUsers");
			self::MergeSet("RecommendedTopics");
		}
		

		return $this->Datas;
	}
	
	public function GetNotificationList ()
	{
		$Constraints = [];

		$Seen = "";

		if ($this->HomeForm->Detect)
		{
			$Constraints = ["notifications.user_id" => $this->HomeForm->UserID, "notifications.detect" => 0];
		}
		else
		{
			$Constraints = ["notifications.user_id" => $this->HomeForm->UserID];

			$Seen = ", seen = 1";
		}

		$Notifications = $this->Notification->Chain([
			"articles" => "article_id", 
			"users" => "user_id",
		])->Chain(["practices" => "practice_id"], Schema::Left)->Remember($Constraints)->Limit(8)->Sort(["seen" => Schema::Increase, "created_at" => Schema::Decrease])
			->Get([
				"notification_id",
				"practices.practice_id",
				"users.user_id", 
				"users.firstname", 
				"users.lastname", 
				"users.email",
				"users.image_local",
				"articles.article_id", 
				"articles.title", 
				"articles.image",
				"audience",
				"expired",
				"score",
				"times",
				"notifications.created_at"
		]);

		$NotificationIDs = "";

		foreach ($Notifications as $Notification) 
		{
			$NotificationIDs .= "{$Notification["notification_id"]},";
		}

		$NotificationIDs = substr($NotificationIDs, 0, -1);

		$this->Notification->Illusion("UPDATE notifications SET detect = 1 ${Seen} WHERE (seen = 0 OR detect = 0) AND DIn('{$NotificationIDs}', notification_id) AND notifications.user_id = {$this->HomeForm->UserID}", null, false);

		return $Notifications;
	}

	public function NotificationCounter ()
	{
		$Counter = $this->Notification->Count(["user_id" => $this->HomeForm->UserID, "seen" => 0]);

		if ($Counter["Total"] > 0)
		{
			$this->Notification->Change(["detect" => 1], ["user_id" => $this->HomeForm->UserID, "detect" => 0]);
		}

		return $Counter;
	}

	public function GetModelExists ()
	{
		return $this->IsModels;
	}

	public function Home ()
	{
		return $this->Home;
	}

	private function MergeSet ($Model)
	{
		if (isset($this->$Model) && empty($this->$Model) == false)
		{
			$this->IsModels = array_merge($this->IsModels, ["Offset{$Model}" => true]);
			$this->Datas 	= array_merge($this->Datas, $this->$Model);
		}
		else
		{
			$this->IsModels = array_merge($this->IsModels, ["Offset{$Model}" => null]);
		}
	}

}

?>