<?php

/**
 * @Author: Rot
 * @Date:   2017-09-21 12:50:31
 * @Last Modified by:   Rot
 * @Last Modified time: 2017-12-11 23:13:59
 */

namespace System\Forms;

import (geye.forms);

class HomeForm extends Form
{
	public $UserID;

	public $RowNumber = 6;

	public $LimitRecord = 12;

	public $OffsetHome = 0;

	public $Email;

	public $Token;

	public $Time;

	public $Expire;

	public $Detect = 0;

	public $OffsetRecommendedUsers = 0;

	public $OffsetRecommendedTopics = 0;

	public $OffsetFollowingTopics = 0;

	public $OffsetFollowingUsers = 0;

}

?>