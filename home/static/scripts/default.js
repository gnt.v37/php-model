/*
* @Author: Rot
* @Date:   2017-10-14 23:52:05
* @Last Modified by:   Rot
* @Last Modified time: 2017-11-20 08:56:02
*/
(function () {


window.onload = function () 
{
	let User = document.querySelector(`[idata="user"]`);

	setTimeout(function () {
		let Scroll = new System.HomeScroll({
		
			Content: function ()
			{
				return document.querySelector(".ge-container");
			},
			
			Url: function () 
			{
				return "g/";
			},
			Datas: function ()
			{
				if (User.value)
				{
					return {
						Models: [
							"OffsetFollowingUsers", 
							"OffsetFollowingTopics", 
							"OffsetRecommendedUsers",
							"OffsetRecommendedTopics"
						],

						Offset: [12, 12, 0, 0],
						Limit: 6
					};
						
				}

				return {
					Models: ["OffsetHome"], 
					Offset: [12], 
					Limit: 12
				};
			}
		});
	}, 512)

	

}

})();