/*
* @Author: Rot
* @Date:   2017-10-19 00:49:05
* @Last Modified by:   Rot
* @Last Modified time: 2017-11-20 09:00:10
*/
(function () {

class HomeScroll
{
	constructor (Options)
	{
		var Resolver = Options.Resolver || null,
			Content = Options.Content || null,
			Datas = Options.Datas || null,
			Url = Options.Url || null;

		let Business = new System.HomeScrollBusiness(Resolver, Content, Datas, Url);

		this.Action(Business);
	}


	Action (Business)
	{
		var WindowHeght = window.innerHeight,
			Process = false,
			Ready = true;

		window.onresize = function ()
		{
			WindowHeght = window.innerHeight;
		}

		window.onscroll = function (e) 
		{
			if (Ready == true && Process == false)
			{
				Ready = Business.Ready;
				
				Business.Binding(WindowHeght);

			}

			Process = Business.Process;
			
		}
	}
}

System.HomeScroll = HomeScroll;

})();