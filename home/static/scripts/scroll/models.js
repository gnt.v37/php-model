/*
* @Author: Rot
* @Date:   2017-10-19 10:53:48
* @Last Modified by:   Rot
* @Last Modified time: 2017-11-20 08:47:07
*/
(function () {

class BaseModel
{
	constructor (AppURL, Datas)
	{
		let Require = Datas();

		this.Models = Require.Models;

		this.Offset = Require.Offset;

		this.Limit  = Require.Limit;
		
		if (typeof AppURL === "function")
		{
			this.AppURL = AppURL();
		}
		else
		{
			this.AppURL = AppURL;
		}
	}

	RandomOffset (Limit = 2)
	{
		var Length = this.Models.length;

		if (Length == 0)
		{
			return null;
		}
		else if (Length == 1)
		{
			return [0];
		}
		else
		{
			/**
			 * Random offset
			 *
			 * @type       {Array}
			 */
			var Offset = [];

			/**
			 * Init for array
			 *
			 * @type       {number}
			 */
			var Index = 0;

			/**
			 * Preg to check the exist value
			 *
			 * @type       {string}
			 */
			var List = "";

			/**
			 * Random value
			 *
			 * @type       {<type>}
			 */
			var Numb; 

			while (true)
			{
				/**
				 * Get random value limit by length
				 */
				Numb = Math.floor(Math.random() * Length);

				/**
				 * Regex to check if it exist in list
				 *
				 * @type       {RegExp}
				 */
				var Regex = new RegExp(`^${Numb}\\s|\s${Numb}\\s|\\s${Numb}$`);

				/**
				 * If random number does not exist in list
				 */
				if (!`${List}`.match(Regex))
				{
					/**
					 * Add random number to list
					 */
					List += `${Numb} `;

					/**
					 * Set value to offset and make sure the begining of array is 0
					 */
					Offset[Index] = Numb;

					Index++;
				}
				
				if (Index == Limit)
				{
					return Offset;
				}
			}
		}

	}

	ConvertUrl (OffsetOne, OffsetTwo)
	{
		/**
		 * Get length of the models
		 *
		 * @type       {<type>}
		 */
		var ModelsLength = this.Models.length;

		/**
		 * Temp url
		 *
		 * @type       {string}
		 */
		var BaseUrl = "";

		for (var i = 0; i < ModelsLength; i++)
		{
			/**
			 * If Os = i we set model name and value to url
			 * otherwise we set model name to null
			 */
			if (i == OffsetOne || i == OffsetTwo)
			{
				BaseUrl += `${this.Models[i]}=${this.Offset[i]}&`;
			}
			else
			{
				BaseUrl += `${this.Models[i]}=null&`;
			}
		}

		return `${this.AppURL}?${BaseUrl}LimitRecord=${this.Limit}&time=${new Date().getTime()}/scroll_ajax`;
	}
}

class HomeScrollModels extends BaseModel
{
	constructor (AppURL, Datas)
	{
		super(AppURL, Datas);

		this.NodeTemp = document.createElement("div");
		
	}

	FetchData (OffsetRecord)
	{
		return new Promise ( (Resolve, Reject) => {

			/**
			 * Get random offset
			 *
			 * @type       {<type>}
			 */
			var Offset = this.RandomOffset();

			if (Array.isArray(Offset))
			{
				var Length = Offset.length,
					Url, Result, Regex;

				switch (Length) 
				{
					case 2:
						var Os1 = Offset[0],
							Os2 = Offset[1];

						/**
						 * Set url
						 *
						 * @type       {<string>}
						 */
						Url = this.ConvertUrl(Os1, Os2);

						/**
						 * Get result from url
						 *
						 * @type       {<string>}
						 */
						Result = System.get(Url);

						/**
						 *  Check if model has value
						 *
						 * @type       {RegExp}
						 */
						Regex = new RegExp(`${this.Models[Os1]} \\w*|${this.Models[Os2]} \\w*`, "g");

						Result.then ( (Response) => {
							
							var Matches = Response.match(Regex);

							if (Matches && Matches.length == 2)
							{
								this.Offset[Os1] += this.Limit;
								this.Offset[Os2] += this.Limit;

								for (let Match of Matches)
								{
									var Split = Match.split(" ");

									if (Split[1] == "")
									{
										var Key = getKeyByValue(this.Models, Split[0]);

										this.Models.splice(Key, 1);
										this.Offset.splice(Key, 1);

									}
								}

								Resolve(Response);

							}
							else
							{
								Reject.Condition = false;
							}
							
						})

						break;
					case 1:
						var Os = Offset[0];

						/**
						 * Set url
						 *
						 * @type       {<string>}
						 */
						Url = this.ConvertUrl(Os, null);

						/**
						 * Get result from url
						 *
						 * @type       {<string>}
						 */
						Result = System.get(Url);

						/**
						 *  Check if model has value
						 *
						 * @type       {RegExp}
						 */
						Regex = new RegExp(`${this.Models[Os]} \\w*`, "g");

						Result.then ( (Response) => {

							var Matches = Response.match(Regex);

							if (Matches && Matches.length == 1)
							{
								this.Offset[Os] += this.Limit;

								for (let Match of Matches)
								{
									var Split = Match.split(" ");

									if (Split[1] == "")
									{
										this.Models.splice(0, 1);

										this.Offset.splice(0, 1);

									}
								}
								
								Response = Response.replace(/^<div ge-exist.*?>\s*.*?<\/div>/, "");

								Resolve(Response);

							}
							else
							{
								Reject.Condition = false;
							}
							
						}, function (Message) {
							Reject(Message);
						})
						break;
					
					default:
						Response(null);
						break;
				}
				console.log(Url);
			}
			else
			{
				console.log("Done");
				Resolve(null);
			}
		})
		
	}
}

System.HomeScrollModels = HomeScrollModels;

})();