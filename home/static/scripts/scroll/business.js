/*
* @Author: Rot
* @Date:   2017-10-15 14:05:11
* @Last Modified by:   Rot
* @Last Modified time: 2017-11-20 09:00:31
*/
(function () {

class HomeScrollBusiness
{
	constructor (Resolver, Content, Datas, Url)
	{
		this.Models = new System.HomeScrollModels(Url, Datas);

		this.Ready = true;

		this.Process = false;

		this.Node = document.createElement("div");

		if (Content)
		{
			if (typeof Content === "function")
			{
				this.Content = Content();
			}
			else
			{
				this.Content = Content;
			}
			
		}

		if (Resolver)
		{
			this.IsResolver = true;

			this.Resolver = Resolver;
		}
		else 
		{
			this.IsResolver = false;
		}

		
	}

	Binding (WindowHeght)
	{
		
		var ScrollY = window.pageYOffset + WindowHeght,
			PageHeight = document.body.clientHeight,
			Limit = (PageHeight - WindowHeght);

		if (ScrollY > Limit)
		{
			if (this.Process == false)
			{
				let Result = this.Models.FetchData();

				this.Process = true;

				Result.then ( (Response) => {

					if (Response == null)
					{
						this.Ready = false;
						return;
					}

					this.Node.innerHTML = Response;

					var ChildrenNode = this.Node.children,
						Length = ChildrenNode.length;

					System.load(this.Node);

					if (this.IsResolver)
					{
						this.Resolver(this.Node);
					}
					else
					{
						for (var i = 0; i < Length; i++)
						{
							this.Content.appendChild(ChildrenNode[0]);

						}
					}
					
					this.Process = false;

				}, function (Message) {
					console.log(Message);
				})
			}
		}
			
	}
}

System.HomeScrollBusiness = HomeScrollBusiness;

})();