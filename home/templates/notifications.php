<?php if ($this->Notifications): ?>
	<?php foreach ($this->Notifications as $Notification): ?>
		<li class="ge-border-bottom"><div class="ge-hoz-16">
		<div class="ge-bottom-4 ge-flex">
			<div class="">
				<a href="<?= page."@{$Notification["user_id"]}" ?>" class="ge-image40">
				<?php if ($Notification["image_local"]): ?>
					<img data-src="<?= page.users.images."40/{$Notification["image_local"]}" ?>" class="ge-max110"></a>
				<?php else: ?>
					<img data-src="" data-name="<?= $Notification["lastname"] || $Notification["firstname"] ? "{$Notification["lastname"]} {$Notification["firstname"]}" : $Notification["email"]?>" class="ge-max110"></a>
				<?php endif ?>
				</a>
			</div>
			<div class=" ge-notification-text">
				<?php if ($Notification["audience"]): ?>
					<a href="<?= page."view?v={$Notification["article_id"]}" ?>"><span>You have a <span class="ge-article-name"><?= $Notification["title"] ?></span> article by </span>
					<span><?= "{$Notification["lastname"]} {$Notification["firstname"]}" ?></span></a>
				<?php elseif ($Notification["expired"]): ?>
					<a href="<?= page."summary/@{$_SESSION[__SIGNIN__]["userid"]}/{$Notification["article_id"]}/{$Notification["times"]}" ?>"><span>You have finished <span class="ge-article-name"><?= $Notification["title"] ?></span> with <?= $Notification["score"] ?> scores</span></a>
				<?php endif ?>
				
				</div>
			<div class=""><a href="<?= page."view?v={$Notification["article_id"]}" ?>">
				<img data-src="<?= page.home.images."60/{$Notification["image"]}" ?>" class="ge-max110"></a>
			</div>	
			
		</div>
		<div class="ge-text-color"><time datetime="<?= $Notification["created_at"] ?>"><?= $Notification["created_at"] ?></time></div>
		</div></li>
		
	<?php endforeach ?>
<?php else: ?>
	<li class="ge-hoz-16">No notification yet.</li>
<?php endif ?>