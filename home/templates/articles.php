<?php if (isset($this->IsModels) && empty($this->IsModels) == false): ?>
<div ge-exist class="ge-hide">
	<?php foreach ($this->IsModels as $Model => $Value): ?>
		<?=  "$Model $Value" ?>
	<?php endforeach ?>	
</div>
<?php endif ?>

<?php foreach ($this->Articles as $Key => $Article): $Category = $Article["category"];?>
	
<?php if ($Key % 6 == 0): 
	
	$Index = 0;

	$Style = "view-mode-z";

	if ($Key != 0) 
	{
		$Number = mt_rand(1, 9) % 2;
		switch ($Number) 
		{
			case 1:
				$Style = "view-mode-o";
				break;
		}
	}
?>
<section class="<?= $Style ?>" ><div class="__topic ge-seperate">
	<div class="ge-row">
		<div class="ge-left">
		<?php switch ($Category) { 
			case "into_user":
			case "recommend_user": 
				$ShowMore = "@{$Article["user_id"]}";
		?>
			<a href="@<?= $Article["user_id"] ?>"><span class="ge-ntp"><?= "{$Article["lastname"]} {$Article["firstname"]}" ?></span><span class="ge-rcmd">Recommended member for you</span></a>
		<?php break; 
			case "into_topic":
			case "recommend_topic":
				$ShowMore = "topic/{$Article["topic_id"]}";
		?>
			<a href="topic/<?= $Article["topic_id"] ?>"><span class="ge-ntp"><?= $Article["topic"] ?></span><span class="ge-rcmd">Recommended practices for you</span></a>
		<?php break; case "recommend":?>
			<a href="topic/<?= $Article["topic_id"] ?>"><span class="ge-ntp">Recommended</span></a>
		<?php break; } ?>
		</div>
		<div class="ge-right">
		<?php if ($Category == "recommend_topic" || $Category == "recommend_user"): ?>
					
		<?php else: ?>
			<a class="ge-text" href="<?= $ShowMore ?>"><span class="ge-show-more">Show More</span>
				<span class="ge-imore"><svg width="19" height="19" viewBox="0 0 19 19"><path d="M7.6 5.138L12.03 9.5 7.6 13.862l-.554-.554L10.854 9.5 7.046 5.692" fill-rule="evenodd"></path></svg></span>
			</a>
		<?php endif ?>
		</div>
	</div></div>				
	<div class="ge-row">
<?php endif ?>

<ge-article <?= $Index ?>>
	<div class="ge-white __card-box">
		<div class="ge-row">
			<ge-image>
				<?php 
					$Image = home.images; 
					if ($Style == "view-mode-z")
					{
						$Image .= "300/";
					}  
					else 
					{
						$Image .= "280/"; 
					}
					?>
				<a href="view?v=<?= $Article["article_id"] ?>" prevent="true" class="ge-scale"><div class="ge-bk-img ge-hover ge-real" data-src="<?=  $Image.$Article["image"] ?>"></div></a>
			</ge-image>
			
			<ge-content>
				<ul class="article-info flex-content">
					<li class="flex-mode" style="position: relative;">
						<div class="absolute-full flex-content">
							<h4 class="">
								<a href="view?v=<?= $Article["article_id"] ?>" class="article-title" ><?= $Article["title"] ?></a>
							</h4>
							<?php 
								$Height = "height-mode-z"; 

								$IsShow = true;

								$TitleLength = strlen($Article["title"]) - 9;

								if ($Style == "view-mode-o")
								{
									$Height = "height-mode-z";

									if ($TitleLength >= 46 && $Index > 2)
									{
										$IsShow = false;
									}
								}
								else
								{
									if ($TitleLength >= 39 && $TitleLength < 74)
									{
										$Height = "height-mode-tw";
									}
									else if ($TitleLength >= 74)
									{
										$Height = "height-mode-th";
									}
								}
							?>
							<div class="article-description <?= $Height ?>">
								<span><?php if ($IsShow) echo $Article["description"] ?></span>	
							</div>
						</div>
					</li>
					<li class="ge-top-12">
						<div class="ge-row">
							<ge-user><div class="ge-inline">
								<?php $ArticleUser = $Article["lastname"] || $Article["firstname"] ? "{$Article["lastname"]} {$Article["firstname"]}" : $Article["email"] ?>
								<a href="<?= page."@{$Article["user_id"]}"; ?>" prevent="true">
									<div class="ge-inline ge-relative ge-image40">
										<?php if ($Article["user_image_local"]): ?>
											<img src="<?= users.images."40/{$Article["user_image_local"]}" ?>">
										<?php else: ?>
											<img ge-user-avatar="true" data-name="<?= $ArticleUser ?>" data-src="">
										<?php endif ?>
										<div class="ge-avatar-radius">
											<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 70 70">
												<path d="M5.53538374,19.9430227 C11.180401,8.78497536 22.6271155,1.6 35.3571429,1.6 C48.0871702,1.6 59.5338847,8.78497536 65.178902,19.9430227 L66.2496695,19.401306 C60.4023065,7.84329843 48.5440457,0.4 35.3571429,0.4 C22.17024,0.4 10.3119792,7.84329843 4.46461626,19.401306 L5.53538374,19.9430227 Z"></path>
												<path d="M65.178902,49.9077131 C59.5338847,61.0657604 48.0871702,68.2507358 35.3571429,68.2507358 C22.6271155,68.2507358 11.180401,61.0657604 5.53538374,49.9077131 L4.46461626,50.4494298 C10.3119792,62.0074373 22.17024,69.4507358 35.3571429,69.4507358 C48.5440457,69.4507358 60.4023065,62.0074373 66.2496695,50.4494298 L65.178902,49.9077131 Z">
												</path>
											</svg>
										</div>
									</div>
								</a></div>
								<div class="ge-inline ge-left-12">
									<div><a href="<?= page."@{$Article["user_id"]}"; ?>" prevent="true" class="ge-name"><?= $ArticleUser ?></a></div>
									<div class="ge-time"><time datetime="<?= $Article["created_at"] ?>"></time></div>
								</div>
							</ge-user>
							
						</div>
					</li>
				</ul>
			</ge-content>
		</div>
		
	</div>
	
</ge-article>

<?php $Index++ ?>

<?php if (($Key + 1) % 6 == 0): ?>
	</div></section>
<?php endif ?>

<?php endforeach ?>

