<?php
namespace System\Views;

import (geye.views.generic);

class HomeView extends GenericView
{
	public $Models = ["FollowingUsers", "FollowingTopics", "Recommended", "Home"];

	protected function Resolver ()
	{
		if (isset($_SESSION[__SIGNIN__]))
		{
			$this->Articles = $this->Deeps->MergeModels();
		}
		else
		{
			$this->Articles = $this->Deeps->Home();
		}
	}
}

class ScrollView extends GenericView
{
	protected $Templates = "articles.php";

	public $Models = ["FollowingUsers", "FollowingTopics", "RecommendedUsers", "RecommendedTopics", "Home"];

	protected function Resolver ()
	{
		$this->Articles = $this->Deeps->MergeScrollArtile();

		$this->IsModels = $this->Deeps->GetModelExists();

	}
}

class PollObject extends ObjectView 
{
	protected $Models = "Poll";

	protected function Resolver ()
	{
		echo json_encode($this->Deeps->Poll[0]);
	}
}

class NotificationsView extends GenericView
{
	protected $Models = "Notification";

	protected $Templates = "notifications.php";

	protected function Resolver ()
	{
		$this->Notifications = $this->Deeps->GetNotificationList();
	}
}

class NotificationCountObject extends ObjectView
{
	protected $Models = "Notification";

	protected function Resolver ()
	{
		echo json_encode($this->Deeps->NotificationCounter());
	}
}

class ScheduleObject extends ObjectView
{
	protected $Models = "Schedule";

	protected function Resolver ()
	{
		echo json_encode($this->Deeps->Schedule);
	}
}

class ExpireObject extends ObjectView
{
	protected $Models = "PracticeExpired";

	protected function Resolver ()
	{
		echo json_encode($this->Deeps->PracticeExpired);
	}
}

?>