<?php

/**
 * @Author: Rot
 * @Date:   2017-12-11 23:07:40
 * @Last Modified by:   Rot
 * @Last Modified time: 2017-12-11 23:14:30
 */

namespace System\Forms;

import (geye.forms);

class HomeForm extends Form
{
	public $UserID;

	public $Email;

	public $Token;

	public $Expire;
}

?>