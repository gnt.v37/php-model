<?php

/**
 * @Author: Rot
 * @Date:   2017-12-11 23:07:35
 * @Last Modified by:   Rot
 * @Last Modified time: 2017-12-16 17:16:46
 */

import(home.auth.views, false);

$Urls = [

	"/^\/signin/" 				=> views.SignInView,

	"/^\/signout$/"			 	=> views.SignOutObject,

	"/^\/token_signin$/"	 	=> views.GeyeSignInObject,

	"/^\/twitter_signin/"	 	=> views.TwitterSignInView,

	"/^\/facebook_signin/"	 	=> views.FacebookSignInView,

	"/^\/google_signin/"	 	=> views.GoogleSignInView,

	"/^\/token_valid\/.*?$/" 	=> views.TokenView,

	"/^\/google_url$/" 	=> views.GoogleUrlObject,

	"/^\/facebook_url$/" 	=> views.FacebookUrlObject,

	"/^\/twitter_url$/" 	=> views.TwitterUrlObject,



]

?>