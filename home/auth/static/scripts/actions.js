/*
* @Author: Rot
* @Date:   2017-10-21 07:09:42
* @Last Modified by:   Rot
* @Last Modified time: 2017-12-16 17:21:56
*/
(function () {

class SignInAction 
{
	constructor ()
	{
		this.Business = new System.SignInBusiness();

		this.Refresh();

		this.Social();

	}

	Refresh ()
	{
		var SignInSubmit = document.getElementById("SignInSubmit");

		SignInSubmit.addEventListener("click",  (e) => {

			this.Business.SignInToken(e);

			
		})
	}

	Social ()
	{
		let Socials = ["google_url", "facebook_url", "twitter_url"];

		const Page = location.href;

		var SocialElements = document.querySelector(".ge-google");

		Socials.reduce(function (Sequence, Url) {

			return Sequence.then(function () {

				return new Promise(function (Resolve, Reject) {

					let Data = Page.replace("signin", Url);

					System.get(Data).then(function (Response) {

						Response = Response.match(/\[.*?\]/);

						let Json = JSON.parse(Response);

						SocialElements.querySelector("a").setAttribute("href", Json[0]);

						SocialElements = SocialElements.previousElementSibling;

						Resolve();

					})
				})

			})


		}, Promise.resolve())
	}
}

System.SignInAction = SignInAction;

})();