/*
* @Author: Rot
* @Date:   2017-10-21 07:15:33
* @Last Modified by:   Rot
* @Last Modified time: 2017-10-21 09:54:25
*/
(function () {

class SignInBusiness
{
	constructor ()
	{
		this.Model = new System.SignInModel();
	}

	SignInToken (e)
	{
		let Email = document.getElementById("EmailLogin");

		if (Email.checkValidity()) // Email.validity.valid
		{
			this.Model.DoIt(Email.value);
			
			var Message = document.getElementById("EmailMessage"),
				Container = document.getElementById("SignInContainer");

			Message.innerHTML = Email.value;

			Container.firstElementChild.style.display = "none";

			Container.lastElementChild.style.display = "block";
		}
		else
		{
			Email.nextElementSibling.innerHTML = "Please enter a valid email address";
		}
	}
}

System.SignInBusiness = SignInBusiness;

})();