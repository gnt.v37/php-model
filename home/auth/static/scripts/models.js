/*
* @Author: Rot
* @Date:   2017-10-21 07:12:10
* @Last Modified by:   Rot
* @Last Modified time: 2017-12-28 10:06:00
*/
(function () {

class SignInModel
{
	constructor ()
	{
		this.Page = document.querySelector(`[idata="page"]`).value;
	}

	DoIt (Email)
	{
		let Data = new FormData();

		Data.append("Email", Email);

		return System.post(`${this.Page}g/auth/token_signin`, Data).then( function (Response) {
			console.log(Response);
		}, function (Message) {
			console.log(Message);
		});
	}
}

System.SignInModel = SignInModel;

})();