<?php

/**
 * @Author: Rot
 * @Date:   2017-12-11 23:08:34
 * @Last Modified by:   Rot
 * @Last Modified time: 2017-12-24 10:30:42
 */

namespace System\Deeps;

use System\Models\Schema;

class SignInDeep 
{
	public function GeyeSignIn ()
	{
		$TokenCode = random_string(24);

		$Email = $this->HomeForm->Email;

		$Token = $this->SignIn->Call([$Email, $TokenCode, date('Y-m-d H:i:s', strtotime("+15 minutes"))]);

		$Token = $Token[0];

		self::Mail($Email, $Token["user_id"], $TokenCode);
	}

	public function TwitterAuthUrl ()
	{
		include_once geye.modules.twitter;

		$Twitter = new \Twitter();

		return $Twitter->AuthUrl();
	}

	public function FacebookAuthUrl ()
	{
		include_once geye.modules.facebook;

		$Facebook = new \Facebook();

		return $Facebook->AuthUrl();
	}

	public function GoogleAuthUrl ()
	{
		include_once geye.modules.google;

		$Google = new \Google();

		return $Google->AuthUrl();
	}

	public function GoogleSignIn ()
	{
		include_once geye.modules.google;

		$Google = new \Google();

		$Service = $Google->Service();

		if (isset($_GET['code'])) 
		{
			$Google->Authenticate($_GET['code']);

			$_SESSION["access_token"] = $Google->AccessToken();

			$Redirect = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'];

			header('Location: ' . filter_var($Redirect, FILTER_SANITIZE_URL));
		}

		if (isset($_SESSION["access_token"]) && $_SESSION["access_token"])
		{
			$Google->AccessToken($_SESSION['access_token']);

			$User = $Service->people->get("me");

			$About = $User["aboutMe"];

			$Gender = $User["gender"];

			$ImageLink = $User["image"]["url"];

			$Email = $User->getEmails()[0]["value"];

			$Address = $User->getPlacesLived()[0]["value"];

			$PersonName = $User->getName();

			$FirstName = $PersonName["givenName"];

			$LastName = $PersonName["familyName"];

			$Counter = $this->User->Count(["email" => $Email]);

			if ($Counter["Total"] > 0)
			{
				$Result = $this->User->Remember(["email" => $Email], true)->Get(["user_id", "image_local", "role"]);

				$this->User->Change([

					"description" 	=> $About, 
					"image_link" 	=> $ImageLink, 
					"firstname" 	=> $FirstName,
					"lastname" 		=> $LastName,
					"address"		=> $Address,
					"gender"		=> $Gender,

				], ["user_id" => $Result["user_id"]]);
			}
			else
			{
				$this->User->Write([

					"email" 		=> $Email, 
					"image_link" 	=> $ImageLink, 
					"firstname" 	=> $FirstName,
					"lastname" 		=> $LastName,
					"address" 		=> $Address,
					"birthday"		=> $User["birthday"],
					"gender"		=> $Gender,
					"description"	=> $About,
					"google"		=> $User["id"]
				]);

				$Result = $this->User->Remember(["email" => $Email], true)->Get(["user_id", "image_local", "role"]);
			}

			$_SESSION[__SIGNIN__] = [
				"userid"				=> $Result["user_id"],
			 	"session_firstname"		=> $FirstName,
			 	"session_lastname"		=> $LastName,
			 	"session_email"			=> $Email,
			 	"session_image_link"	=> $ImageLink,
				"session_image_local"	=> $Result["image_local"],
				"session_role"			=> $Result["role"]
			];
		}
	}

	public function FacebookSignIn ()
	{
		include_once geye.modules.facebook;

		$Facebook = new \Facebook();

		$User = $Facebook->Get();

		if ($User)
		{
			$FirstName = $User["first_name"];

			$LastName = $User["last_name"];

			$Email = $User["email"];

			$Gender = $User["gender"];

			$Image = $User["picture"]["url"];

			$FacebookID = $User["id"];

			$Counter = $this->User->Count(["email" => $Email]);

			if ($Counter["Total"] > 0)
			{
				$Result = $this->User->Remember(["email" => $Email], true)->Get(["user_id", "image_local", "role"]);

				$this->User->Change([

					"firstname" 	=> $FirstName,
					"lastname" 		=> $LastName,
					"gender"		=> $Gender,
					"image_link" 	=> $Image, 

				], ["user_id" => $Result["user_id"]]);
			}
			else
			{
				$this->User->Write([

					"firstname" 	=> $FirstName,
					"lastname" 		=> $LastName,
					"gender"		=> $Gender,
					"facebook"		=> $FacebookID,
					"image_link" 	=> $Image, 
				]);

				$Result = $this->User->Remember(["email" => $Email], true)->Get(["user_id", "image_local", "role"]);
			}

		}

		$_SESSION[__SIGNIN__] = [
			"userid"				=> $Result["user_id"],
			"session_firstname"		=> $FirstName,
			"session_lastname"		=> $LastName,
			"session_email"			=> $Email,
			"session_image_link"	=> $Image,
			"session_image_local"	=> $Result["image_local"],
			"session_role"			=> $Result["role"]

		];

	}

	public function TwitterSignIn ()
	{
		if (isset($_GET["oauth_token"]) && isset($_GET["oauth_verifier"]))
		{
			include_once geye.modules.twitter;

			$Verify = new \Twitter($_GET["oauth_token"], $_GET["oauth_verifier"]);

			$AccessToken = $Verify->AccessToken($_GET["oauth_verifier"]);

			$Twitter = new \Twitter($AccessToken["oauth_token"], $AccessToken["oauth_token_secret"]);

			$User = $Twitter->Credential();

			$TwitterID = $User->id;

			$About = trim($User->description);

			$ImageLink = $User->profile_image_url_https;

			$Address = trim($User->location);

			$FirstName = trim($User->name);

			$Counter = $this->User->Count(["twitter" => $TwitterID]);

			if ($Counter["Total"] > 0)
			{
				$Result = $this->User->Remember(["twitter" => $TwitterID], true)->Get(["user_id", "image_local", "role"]);

				$this->User->Change([

					"image_link" 	=> $ImageLink, 
					"firstname" 	=> $FirstName,
					"address" 		=> $Address,
					"description"	=> $About

				], ["user_id" => $Result["user_id"]]);
			}
			else
			{
				$this->User->Write([

					"image_link" 	=> $ImageLink, 
					"firstname" 	=> $FirstName,
					"address" 		=> $Address,
					"twitter"		=> $TwitterID,
					"description"	=> $About
				]);

				$Result = $this->User->Remember(["twitter" => $TwitterID], true)->Get(["user_id", "image_local", "role"]);
			}

			$_SESSION[__SIGNIN__] = [
				"userid"				=> $Result["user_id"],
			 	"session_firstname"		=> $FirstName,
			 	"session_lastname"		=> null,
			 	"session_email"			=> null,
			 	"session_image_link"	=> $ImageLink,
				"session_image_local"	=> $Result["image_local"],
				"session_role"			=> $Result["role"]
			];

		}
	}



	public function TokenValidity ()
	{
		if ($this->TokenValidation["Result"])
		{
			$UserID = $this->HomeForm->UserID;

			$Result = $this->User->Remember(["user_id" => $UserID], true)->Get();

			$_SESSION[__SIGNIN__] = [
				"userid"				=> $UserID,
		 		"session_firstname"		=> $Result["firstname"],
		 		"session_lastname"		=> $Result["lastname"],
		 		"session_email"			=> $Result["email"],
		 		"session_image_local"	=> $Result["image_local"],
		 		"session_image_link"	=> $Result["image_link"],
		 		"session_role"			=> $Result["role"],
			];
		}
	}


	private function Mail ($Email, $UserID, $Token)
	{
		include_once("geye/modules/mailer.php");

		$Subject = "Sign in to Geye";
		
		$SignInLink = "http://localhost/geye/g/auth/token_valid/?userid={$UserID}&token={$Token}";

		$Body = array(
			"Content" => '
				<div style="font-family: Segoe UI">
					<h1 style="font-size: 64px; text-align: center; margin: 0; margin-bottom: 32px; ">
						<span style="color: #1B95E0">G</span>E<span style="color: #1B95E0">Y</span>E
					</h1>
					<div style="margin-bottom: 32px; font-size: 14px;">
						<div>Click and confirm that you want to sign in to Geye. This link will expire in fifteen minutes and can only be used once.</div>
						<div><a href='. $SignInLink .' style="padding: 8px 16px; background-color: #1da1f2; color: white; border-radius: 2px; font-size: 14.5; margin: 16px 0; width: 128px; display: inline-block; text-align: center; text-decoration: none;">Sign in to Geye</a></div><div><div>Or sign in using this link</div><a href='. $SignInLink .'>'. $SignInLink .'</a></div>
					</div>
					<div style="color: rgba(0,0,0,.44); text-align: center;">If you did not make this request, please contact us at response.geye@gmail.com</div>
					<div style="color: rgba(0,0,0,.44); text-align: center;">Sent by Geye · 48 Hoàng Phan Thái, TP Vinh, Nghệ An, Việt Nam</div>
				</div>'
		);

		new \System\Mail($Email, $Subject, $Body);
	}
}

?>