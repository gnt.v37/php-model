<?php

/**
 * @Author: Rot
 * @Date:   2017-12-11 23:07:49
 * @Last Modified by:   Rot
 * @Last Modified time: 2017-12-11 23:15:51
 */

namespace System\Models;

import(geye.models);


class User extends Schema
{
	protected $Nerve = "users";

	public $NerveCells = ["user_id", "email", "firstname", "lastname", "image_local", "image_link"];
}

class SignIn extends Procedure implements Humanize, Nullable
{
	protected $Name = "DSignIn";

	public $Email;

	public $Token;

	public $Expire;

	public static function Nullable ()
	{
		return ["Token", "Expire"];
	}

}

class TokenValidation extends Dynamic 
{
	protected $Name = "DTokenValidation";

	public $UserID;

	public $Token;
}

?>