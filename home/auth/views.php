<?php

/**
 * @Author: Rot
 * @Date:   2017-12-11 23:07:23
 * @Last Modified by:   Rot
 * @Last Modified time: 2017-12-16 17:01:14
 */

namespace System\Views;

import (geye.views.generic);

class SignInView extends GenericView 
{
	protected $Models = "User";
}

class SignOutObject extends ObjectView
{
	protected $Models = "";

	protected function Resolver ()
	{
		session_destroy();
	}
}

class TokenView extends GenericView 
{
	protected $Templates = "callback.php";

	public $Models = ["TokenValidation", "User"];

	protected function Resolver ()
	{
		$this->Deeps->TokenValidity();
	}
}


class GeyeSignInObject extends ObjectView
{
	protected $Models = "SignIn";

	protected function Resolver ()
	{
		$this->Deeps->GeyeSignIn();
	}
}

class TwitterSignInView extends GenericView
{
	protected $Templates = "callback.php";

	protected $Models = "User";

	protected function Resolver ()
	{
		$this->Deeps->TwitterSignIn();
	}
}

class FacebookSignInView extends GenericView
{
	protected $Templates = "callback.php";

	protected $Models = "User";

	protected function Resolver ()
	{
		$this->Deeps->FacebookSignIn();
	}
}

class GoogleSignInView extends GenericView
{
	protected $Templates = "callback.php";

	protected $Models = "User";

	protected function Resolver ()
	{
		$this->Deeps->GoogleSignIn();
	}
}

class GoogleUrlObject extends ObjectView
{
	protected $Models = "";
	
	protected function Resolver ()
	{
		echo json_encode(array($this->Deeps->GoogleAuthUrl()));
	}
}

class FacebookUrlObject extends ObjectView
{
	protected $Models = "";

	protected function Resolver ()
	{
		echo json_encode(array($this->Deeps->FacebookAuthUrl()));
	}
}

class TwitterUrlObject extends ObjectView
{
	protected $Models = "";

	protected function Resolver ()
	{
		echo json_encode(array($this->Deeps->TwitterAuthUrl()));
	}
}

?>