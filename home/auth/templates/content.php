<div class="ge-small-width"><div><center class=""><h1 class="ge-header"><a href="<?= page ?>"><span class="ge-color">G</span>E<span class="ge-color">Y</span>E</a>
</h1></center></div>
<div id="SignInContainer" select="false">
<div><ul><li><center class="ge-text-font"><h2>Welcome back.</h2><div class="ge-modal-text">
	<div>Sign in to access your personalized homepage, follow authors and topics you love, and clap for articles that matter to you.</div></div></center></li>
	<li class="ge-hoz-32">
		<div class="ge-text-font"><input type="email" id="EmailLogin" name="email" required="required" placeholder="yourname@example.com" class="ge-input ge-email"><center class="ge-error"></center></div></li>		
	<li class="ge-ver-24"><center><button type="" class="ge-color" data-href="token" id="SignInSubmit">
		<div class="ge-inline ge-width48"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 334.5 334.5" xml:space="preserve" width="25" height="25"><path d="M332.797,13.699c-1.489-1.306-3.608-1.609-5.404-0.776L2.893,163.695c-1.747,0.812-2.872,2.555-2.893,4.481  s1.067,3.693,2.797,4.542l91.833,45.068c1.684,0.827,3.692,0.64,5.196-0.484l89.287-66.734l-70.094,72.1  c-1,1.029-1.51,2.438-1.4,3.868l6.979,90.889c0.155,2.014,1.505,3.736,3.424,4.367c0.513,0.168,1.04,0.25,1.561,0.25  c1.429,0,2.819-0.613,3.786-1.733l48.742-56.482l60.255,28.79c1.308,0.625,2.822,0.651,4.151,0.073  c1.329-0.579,2.341-1.705,2.775-3.087L334.27,18.956C334.864,17.066,334.285,15.005,332.797,13.699z"/></svg>
		</div>	<div class="ge-inline ge-local-email">Email me link to sign in</div></button></center></li>
	</ul><div class="separate"><span>Or sign in with</span></div>
	<ul><li class="ge-hoz-32"><center><ul class="ge-list"><li class="ge-twitter ge-round"><a class="ge-padding-v4" href="">
		<div class="ge-inline ge-width48"><svg width="25" height="25" viewBox="0 0 25 25" fill="white">
			<path d="M21.725 5.338c-.744.47-1.605.804-2.513 1.006a3.978 3.978 0 0 0-2.942-1.293c-2.22 0-4.02 1.81-4.02 4.02 0 .32.034.63.07.94-3.31-.18-6.27-1.78-8.255-4.23a4.544 4.544 0 0 0-.574 2.01c.04 1.43.74 2.66 1.8 3.38-.63-.01-1.25-.19-1.79-.5v.08c0 1.93 1.38 3.56 3.23 3.95-.34.07-.7.12-1.07.14-.25-.02-.5-.04-.72-.07.49 1.58 1.97 2.74 3.74 2.8a8.49 8.49 0 0 1-5.02 1.72c-.3-.03-.62-.04-.93-.07A11.447 11.447 0 0 0 8.88 21c7.386 0 11.43-6.13 11.414-11.414.015-.21.01-.38 0-.578a7.604 7.604 0 0 0 2.01-2.08 7.27 7.27 0 0 1-2.297.645 3.856 3.856 0 0 0 1.72-2.23"></path></svg></div><div class="ge-inline">Continue with Twitter<div class="ge-size-12 ge-smoke">We won't post without asking</div></div>
		</a></li><li class="ge-facebook ge-round"><a class="ge-padding-v4" href="">
		<div class="ge-inline ge-width48"><svg width="25" height="25" viewBox="0 0 25 25"><path d="M20.292 4H4.709A.709.709 0 0 0 4 4.708v15.584c0 .391.317.708.709.708h8.323v-6.375h-2.125v-2.656h2.125V9.844c0-2.196 1.39-3.276 3.348-3.276.938 0 1.745.07 1.98.1v2.295h-1.358c-1.066 0-1.314.507-1.314 1.25v1.756h2.656l-.531 2.656h-2.125L15.73 21h4.562a.708.708 0 0 0 .708-.708V4.708A.708.708 0 0 0 20.292 4" fill-rule="evenodd"></path></svg></div><div class="ge-inline __left-align">Continue with Facebook
		<div class="ge-size-12 ge-smoke">We won't post without asking</div></div></a></li>
		<li class="ge-google ge-round"><a class="ge-padding-v4 ge-flex-cc" href="">
			<div class="ge-inline ge-width48"><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="25" height="25" viewBox="0 0 512 512" xml:space="preserve">
			<path style="fill:#FBBB00;" d="M113.47,309.408L95.648,375.94l-65.139,1.378C11.042,341.211,0,299.9,0,256 c0-42.451,10.324-82.483,28.624-117.732h0.014l57.992,10.632l25.404,57.644c-5.317,15.501-8.215,32.141-8.215,49.456 C103.821,274.792,107.225,292.797,113.47,309.408z"/>
			<path style="fill:#518EF8;" d="M507.527,208.176C510.467,223.662,512,239.655,512,256c0,18.328-1.927,36.206-5.598,53.451 c-12.462,58.683-45.025,109.925-90.134,146.187l-0.014-0.014l-73.044-3.727l-10.338-64.535 c29.932-17.554,53.324-45.025,65.646-77.911h-136.89V208.176h138.887L507.527,208.176L507.527,208.176z"/>
			<path style="fill:#28B446;" d="M416.253,455.624l0.014,0.014C372.396,490.901,316.666,512,256,512 c-97.491,0-182.252-54.491-225.491-134.681l82.961-67.91c21.619,57.698,77.278,98.771,142.53,98.771 c28.047,0,54.323-7.582,76.87-20.818L416.253,455.624z"/>
			<path style="fill:#F14336;" d="M419.404,58.936l-82.933,67.896c-23.335-14.586-50.919-23.012-80.471-23.012 c-66.729,0-123.429,42.957-143.965,102.724l-83.397-68.276h-0.014C71.23,56.123,157.06,0,256,0 C318.115,0,375.068,22.126,419.404,58.936z"/>
		</svg></div><div class="ge-inline __left-align __rlt">Continue with Google
		<div class="ge-size-12 ge-text-color">We won't post without asking</div></div></a></li></ul>
	</center></li></ul></div>
	<div style="display: none;"><center class="ge-text-font">
		<h2>Check your inbox.</h2><div class="ge-modal-text">
			<div>We just emailed a confirmation link to <span id="EmailMessage"></span>. Click the link, and you’ll be signed in.</div>
		<div><a href="<?= page ?>" class="ge-ok ge-color">OK</a></div></div>
	</center></div></div>
</div>