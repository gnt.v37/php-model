<?php

/**
 * @Author: Rot
 * @Date:   2017-12-11 23:10:17
 * @Last Modified by:   Rot
 * @Last Modified time: 2017-12-12 20:31:30
 */

if (isset($_SESSION[__SIGNIN__]))
{
	header("Location: http://localhost/geye/");
	
	die();
}

$Header = "";

include_once __TEMPLATE__;

?>