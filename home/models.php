<?php
namespace System\Models;

import(geye.models);

class Home extends Procedure implements UserRequirement
{
	protected $Name = "GHome";

	public $RowNumber;

	public $LimitRecord;
	
	public $OffsetHome;

	public static function User ()
	{
		return false;
	}
}

class Schedule extends Procedure implements UserRequirement
{
	protected $Name = "DPublishSchedule";
	
	public $UserID;

	public static function User ()
	{
		return true;
	}
}

class Recommended extends Procedure 
{
	protected $Name = "GRecommended";

	public $UserID;

	public $Fields;

}

class FollowingTopics extends Procedure 
{
	protected $Name = "GFollowingTopics";

	public $UserID;

	public $RowNumber;

	public $LimitRecord;

	public $OffsetFollowingTopics;
}

class FollowingUsers extends Procedure 
{
	protected $Name = "GFollowingUsers";

	public $UserID;

	public $RowNumber;

	public $LimitRecord;

	public $OffsetFollowingUsers;

}

class RecommendedUsers extends Procedure 
{
	protected $Name = "GRecommendedUsers";

	public $UserID;

	public $RowNumber;

	public $LimitRecord;

	public $OffsetRecommendedUsers;

}

class RecommendedTopics extends Procedure 
{
	protected $Name = "GRecommendedTopics";

	public $UserID;

	public $RowNumber;

	public $LimitRecord;
	
	public $OffsetRecommendedTopics;

}

class PracticeExpired extends Dynamic
{
	protected $Name = "DPracticeExpired";

	public $UserID;
}

class Poll extends Procedure implements UserRequirement
{
	protected $Name = "DPoll";

	public $UserID;

	public static function User ()
	{
		return true;
	}
}

class Notification extends Schema 
{
	protected $Nerve = "notifications";
}


?>